using System.Data.Entity;
using Mac3Migration.Mapping;


namespace Mac3Migration
{
    public partial class MacafurDev3112Context : DbContext
    {
        static MacafurDev3112Context()
        {
            Database.SetInitializer<MacafurDev3112Context>(null);
        }

        public MacafurDev3112Context()
            : base("Name=MacafurDev3112Context")
        {
        }

        public DbSet<AeroDefinition> AeroDefinitions { get; set; }
        public DbSet<AssetArchive> AssetArchives { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<DivaDefinition> DivaDefinitions { get; set; }
        public DbSet<FactoryDefinition> FactoryDefinitions { get; set; }
        public DbSet<FactoryServerDefinition> FactoryServerDefinitions { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<ProcessesArchive> ProcessesArchives { get; set; }
        public DbSet<ProcessesLog> ProcessesLogs { get; set; }
        public DbSet<ProcessesLogArchive> ProcessesLogArchives { get; set; }
        public DbSet<QuicDefinition> QuicDefinitions { get; set; }        
        public DbSet<System> Systems { get; set; }       
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowServer> WorkflowServers { get; set; }        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AeroDefinitionMap());
            modelBuilder.Configurations.Add(new AssetArchiveMap());
            modelBuilder.Configurations.Add(new AssetMap());
            modelBuilder.Configurations.Add(new DivaDefinitionMap());
            modelBuilder.Configurations.Add(new FactoryDefinitionMap());
            modelBuilder.Configurations.Add(new FactoryServerDefinitionMap());
            modelBuilder.Configurations.Add(new ProcessMap());
            modelBuilder.Configurations.Add(new ProcessesArchiveMap());
            modelBuilder.Configurations.Add(new ProcessesLogMap());
            modelBuilder.Configurations.Add(new ProcessesLogArchiveMap());
            modelBuilder.Configurations.Add(new QuicDefinitionMap());            
            modelBuilder.Configurations.Add(new SystemMap());            
            modelBuilder.Configurations.Add(new WorkflowMap());
            modelBuilder.Configurations.Add(new WorkflowServerMap());            
        }
    }
}
