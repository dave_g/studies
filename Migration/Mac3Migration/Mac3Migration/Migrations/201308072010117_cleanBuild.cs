using System.Data.Entity.Migrations;

namespace Mac3Migration.Migrations
{
    
    
    public partial class cleanBuild : DbMigration
    {
        public override void Up()
        {
            #region Drop Tables
            DropIndex("WorkflowAeros", new[] { "workflowid" });
            DropIndex("WorkflowAeros", new[] { "areoid" });
            DropIndex("WorkflowDivas", new[] { "workflowid" });
            DropIndex("WorkflowDivas", new[] { "divaid" });
            DropIndex("WorkflowQuics", new[] { "workflowid" });
            DropIndex("WorkflowQuics", new[] { "quicid" });
            DropIndex("ProcessesLog", new[] { "ProcessesId" });
            DropIndex("WorkflowServers", new[] { "workflowid" });
            DropIndex("WorkflowServers", new[] { "serverid" });
            DropIndex("WorkflowServers", new[] { "factoryid" });
            DropIndex("Processes", new[] { "factoryServerId" });
            DropIndex("Processes", new[] { "factoryid" });
            DropIndex("Processes", new[] { "divaid" });
            DropIndex("Processes", new[] { "assetId" });
            DropIndex("Processes", new[] { "aeroId" });
            DropForeignKey("WorkflowAeros", "workflowid", "Workflows");
            DropForeignKey("WorkflowAeros", "areoid", "AeroDefinitions");
            DropForeignKey("WorkflowDivas", "workflowid", "Workflows");
            DropForeignKey("WorkflowDivas", "divaid", "DivaDefinitions");
            DropForeignKey("WorkflowQuics", "workflowid", "Workflows");
            DropForeignKey("WorkflowQuics", "quicid", "QuicDefinitions");
            DropForeignKey("ProcessesLog", "ProcessesId", "Processes");
            DropForeignKey("WorkflowServers", "workflowid", "Workflows");
            DropForeignKey("WorkflowServers", "serverid", "FactoryServerDefinitions");
            DropForeignKey("WorkflowServers", "factoryid", "FactoryDefinitions");
            DropForeignKey("Processes", "factoryServerId", "FactoryServerDefinitions");
            DropForeignKey("Processes", "factoryid", "FactoryDefinitions");
            DropForeignKey("Processes", "divaid", "DivaDefinitions");
            DropForeignKey("Processes", "assetId", "Assets");
            DropForeignKey("Processes", "aeroId", "AeroDefinitions");
            DropTable("WorkflowAeros");
            DropTable("WorkflowDivas");
            DropTable("WorkflowQuics");
            DropTable("System");
            DropTable("ProcessesLogArchive");
            DropTable("ProcessesArchive");
            DropTable("AssetArchive");
            DropTable("ProcessesLog");
            DropTable("QuicDefinitions");
            DropTable("FactoryServerDefinitions");
            DropTable("FactoryDefinitions");
            DropTable("WorkflowServers");
            DropTable("Workflows");
            DropTable("DivaDefinitions");
            DropTable("Assets");
            DropTable("Processes");
            DropTable("AeroDefinitions");
            #endregion

            #region CreateTables
            CreateTable(
                "AeroDefinitions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 255),
                        inboxPath = c.String(nullable: false, maxLength: 255),
                        outboxPath = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "Processes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        workflowid = c.String(maxLength: 100),
                        assetId = c.Int(),
                        status = c.String(maxLength: 32),
                        updatedAt = c.DateTime(nullable: false),
                        divaid = c.Int(nullable: false),
                        filename = c.String(maxLength: 255),
                        fileLocation = c.String(maxLength: 255),
                        factoryid = c.Int(nullable: false),
                        wait = c.Boolean(nullable: false),
                        factoryServerId = c.Int(),
                        aeroId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("AeroDefinitions", t => t.aeroId)
                .ForeignKey("Assets", t => t.assetId)
                .ForeignKey("DivaDefinitions", t => t.divaid, cascadeDelete: true)
                .ForeignKey("FactoryDefinitions", t => t.factoryid, cascadeDelete: true)
                .ForeignKey("FactoryServerDefinitions", t => t.factoryServerId)
                .Index(t => t.aeroId)
                .Index(t => t.assetId)
                .Index(t => t.divaid)
                .Index(t => t.factoryid)
                .Index(t => t.factoryServerId);

            CreateTable(
                "Assets",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        houseId = c.String(nullable: false, maxLength: 32),
                        title = c.String(maxLength: 64),
                        isci = c.String(maxLength: 64),
                        comment = c.String(maxLength: 100),
                        clipSom = c.Int(),
                        clipEom = c.Int(),
                        clipDuration = c.Int(),
                        playSom = c.Int(),
                        playEom = c.Int(),
                        playDuration = c.Int(),
                        status = c.String(nullable: false, maxLength: 32),
                        updatedAt = c.DateTime(),
                        createdAt = c.DateTime(nullable: false),
                        filename = c.String(maxLength: 255),
                        fileLocation = c.String(maxLength: 255),
                        type = c.String(maxLength: 64),
                        preFrames = c.Int(),
                        postFrames = c.Int(),
                        airDate = c.DateTime(),
                        dropFrame = c.Boolean(),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "DivaDefinitions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 255),
                        category = c.String(nullable: false, maxLength: 50),
                        divaGroup = c.String(maxLength: 50),
                        location = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "Workflows",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50),
                        workflow_id = c.String(nullable: false, maxLength: 100),
                        active = c.Boolean(nullable: false),
                        prefix = c.String(maxLength: 50),
                        engine_id = c.Int(),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "WorkflowServers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        workflowid = c.Int(nullable: false),
                        serverid = c.Int(nullable: false),
                        factoryid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("FactoryDefinitions", t => t.factoryid, cascadeDelete: true)
                .ForeignKey("FactoryServerDefinitions", t => t.serverid, cascadeDelete: true)
                .ForeignKey("Workflows", t => t.workflowid, cascadeDelete: true)
                .Index(t => t.factoryid)
                .Index(t => t.serverid)
                .Index(t => t.workflowid);

            CreateTable(
                "FactoryDefinitions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 255),
                        factory = c.String(nullable: false, maxLength: 255),
                        inboxAlias = c.String(nullable: false, maxLength: 255),
                        account = c.String(nullable: false, maxLength: 255),
                        inboxPath = c.String(nullable: false, maxLength: 255),
                        password = c.String(nullable: false, maxLength: 255),
                        outboxPath = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "FactoryServerDefinitions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        serverName = c.String(nullable: false, maxLength: 255),
                        applicationKey = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "QuicDefinitions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 255),
                        definition = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "ProcessesLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessesId = c.Int(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Status = c.String(nullable: false, maxLength: 32),
                        Detail = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Processes", t => t.ProcessesId, cascadeDelete: true)
                .Index(t => t.ProcessesId);

            CreateTable(
                "AssetArchive",
                c => new
                    {
                        id = c.Int(nullable: false),
                        houseId = c.String(nullable: false, maxLength: 32),
                        title = c.String(maxLength: 64),
                        isci = c.String(maxLength: 64),
                        comment = c.String(maxLength: 100),
                        clipSom = c.Int(),
                        clipEom = c.Int(),
                        clipDuration = c.Int(),
                        playSom = c.Int(),
                        playEom = c.Int(),
                        playDuration = c.Int(),
                        status = c.String(nullable: false, maxLength: 32),
                        updatedAt = c.DateTime(),
                        createdAt = c.DateTime(nullable: false),
                        filename = c.String(),
                        fileLocation = c.String(),
                        type = c.String(),
                        preFrames = c.Int(),
                        postFrames = c.Int(),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "ProcessesArchive",
                c => new
                    {
                        id = c.Int(nullable: false),
                        workflowid = c.String(maxLength: 100),
                        assetId = c.Int(),
                        status = c.String(maxLength: 32),
                        updatedAt = c.DateTime(nullable: false),
                        divaCategory = c.String(maxLength: 50),
                        divaFileLocation = c.String(maxLength: 255),
                        filename = c.String(maxLength: 255),
                        fileLocation = c.String(maxLength: 255),
                        flipServer = c.String(maxLength: 255),
                        flipServerFactory = c.String(maxLength: 255),
                        flipServerAccount = c.String(maxLength: 255),
                        wait = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "ProcessesLogArchive",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ProcessesId = c.Int(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Status = c.String(nullable: false, maxLength: 32),
                        Detail = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "System",
                c => new
                    {
                        id = c.Int(nullable: false),
                        wait = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "WorkflowQuics",
                c => new
                    {
                        quicid = c.Int(nullable: false),
                        workflowid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.quicid, t.workflowid })
                .ForeignKey("QuicDefinitions", t => t.quicid, cascadeDelete: true)
                .ForeignKey("Workflows", t => t.workflowid, cascadeDelete: true)
                .Index(t => t.quicid)
                .Index(t => t.workflowid);

            CreateTable(
                "WorkflowDivas",
                c => new
                    {
                        divaid = c.Int(nullable: false),
                        workflowid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.divaid, t.workflowid })
                .ForeignKey("DivaDefinitions", t => t.divaid, cascadeDelete: true)
                .ForeignKey("Workflows", t => t.workflowid, cascadeDelete: true)
                .Index(t => t.divaid)
                .Index(t => t.workflowid);

            CreateTable(
                "WorkflowAeros",
                c => new
                    {
                        areoid = c.Int(nullable: false),
                        workflowid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.areoid, t.workflowid })
                .ForeignKey("AeroDefinitions", t => t.areoid, cascadeDelete: true)
                .ForeignKey("Workflows", t => t.workflowid, cascadeDelete: true)
                .Index(t => t.areoid)
                .Index(t => t.workflowid);
#endregion

            #region Views
            Sql(@"IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vuProcesses]')) DROP VIEW [dbo].[vuProcesses]");
            Sql(@"CREATE VIEW [dbo].[vuProcesses] AS
                    SELECT p.id, p.workflowid, p.status, w.name AS workflowname, a.houseId, a.title, a.clipSom, a.clipEom, a.clipDuration, a.playSom, a.playEom, a.playDuration, a.isci, 
                      a.comment, p.updatedAt, a.createdAt, a.filename AS assetFileName, a.fileLocation AS assetLocation, fs.serverName, fs.applicationKey, f.name, f.factory, f.account, 
                      f.inboxPath, f.inboxAlias, f.outboxPath, f.password, d.name AS divaname, d.category, d.divaGroup, d.location AS divaLocation, p.wait, a.id AS assetid, 
                      p.filename AS processFileName, p.fileLocation AS processFileLocation, a.type, p.aeroId, dbo.AeroDefinitions.name AS AeroName, 
                      dbo.AeroDefinitions.inboxPath AS AeroInBox
                    FROM  dbo.Processes AS p INNER JOIN
                      dbo.Workflows AS w ON w.workflow_id = p.workflowid INNER JOIN
                      dbo.Assets AS a ON p.assetId = a.id INNER JOIN
                      dbo.FactoryDefinitions AS f ON f.id = p.factoryid INNER JOIN
                      dbo.FactoryServerDefinitions AS fs ON fs.id = p.factoryServerId INNER JOIN
                      dbo.DivaDefinitions AS d ON d.id = p.divaid LEFT OUTER JOIN
                      dbo.AeroDefinitions ON p.id = dbo.AeroDefinitions.id");

            Sql(@"IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vuAllProcessLogs]')) DROP VIEW [dbo].[vuAllProcessLogs]");
            Sql(@"CREATE VIEW [dbo].[vuAllProcessLogs] AS
                    SELECT Id, ProcessesId, UpdatedAt, Status, Detail
                    FROM dbo.ProcessesLog
                    union
                    SELECT Id, ProcessesId, UpdatedAt, Status, Detail
                    FROM dbo.ProcessesLogArchive
                    ");

            Sql(@"IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vuAllProcesses]')) DROP VIEW [dbo].[vuAllProcesses]");
            Sql(@"CREATE VIEW [dbo].[vuAllProcesses]
                    AS select pr.id, pr.workflowid, w.name as workflowName, pr.assetId, pr.status, pr.updatedAt
                    , dd.category as divaCategory, dd.location as divaFileLocation, pr.filename, pr.fileLocation
                    , fs.serverName as flipServer, fd.factory as flipServerFactory, fd.account as flipServerAccount, pr.wait
                    from Processes pr
                    left outer join DivaDefinitions dd on pr.divaId = dd.Id
                    left outer join FactoryDefinitions fd on pr.factoryId = fd.Id
                    left outer join FactoryServerDefinitions fs on pr.factoryServerId = fs.Id
                    left outer join Workflows w on pr.workflowid = w.workflow_id
                    union
                    select pa.id, pa.workflowid, w.name, pa.assetId, pa.[status], pa.updatedAt, pa.divaCategory
                    , pa.divaFileLocation, pa.filename, pa.fileLocation, pa.flipServer, pa.flipServerFactory
                    , pa.flipServerAccount, pa.wait
                    from ProcessesArchive pa
                    left outer join Workflows w on pa.workflowid = w.workflow_id");
            
            Sql(@"IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vuAllAssets]')) DROP VIEW [dbo].[vuAllAssets]");
            Sql(@"CREATE VIEW [dbo].[vuAllAssets] AS
                    SELECT id, houseId, title, isci, comment, clipSom, clipEom, clipDuration, playSom, playEom, playDuration, 
                    status, updatedAt, createdAt, filename, fileLocation,type, preFrames, postFrames
                    FROM dbo.Assets
                    union
                    SELECT id, houseId, title, isci, comment, clipSom, clipEom, clipDuration, playSom, playEom, playDuration, 
                    status, updatedAt, createdAt, filename, fileLocation,
                    type, preFrames, postFrames
                    FROM dbo.AssetArchive");
            #endregion

            #region Procedures



            #endregion
        }
        
        public override void Down()
        {
            //DropIndex("WorkflowAeros", new[] { "workflowid" });
            //DropIndex("WorkflowAeros", new[] { "areoid" });
            //DropIndex("WorkflowDivas", new[] { "workflowid" });
            //DropIndex("WorkflowDivas", new[] { "divaid" });
            //DropIndex("WorkflowQuics", new[] { "workflowid" });
            //DropIndex("WorkflowQuics", new[] { "quicid" });
            //DropIndex("ProcessesLog", new[] { "ProcessesId" });
            //DropIndex("WorkflowServers", new[] { "workflowid" });
            //DropIndex("WorkflowServers", new[] { "serverid" });
            //DropIndex("WorkflowServers", new[] { "factoryid" });
            //DropIndex("Processes", new[] { "factoryServerId" });
            //DropIndex("Processes", new[] { "factoryid" });
            //DropIndex("Processes", new[] { "divaid" });
            //DropIndex("Processes", new[] { "assetId" });
            //DropIndex("Processes", new[] { "aeroId" });
            //DropForeignKey("WorkflowAeros", "workflowid", "Workflows");
            //DropForeignKey("WorkflowAeros", "areoid", "AeroDefinitions");
            //DropForeignKey("WorkflowDivas", "workflowid", "Workflows");
            //DropForeignKey("WorkflowDivas", "divaid", "DivaDefinitions");
            //DropForeignKey("WorkflowQuics", "workflowid", "Workflows");
            //DropForeignKey("WorkflowQuics", "quicid", "QuicDefinitions");
            //DropForeignKey("ProcessesLog", "ProcessesId", "Processes");
            //DropForeignKey("WorkflowServers", "workflowid", "Workflows");
            //DropForeignKey("WorkflowServers", "serverid", "FactoryServerDefinitions");
            //DropForeignKey("WorkflowServers", "factoryid", "FactoryDefinitions");
            //DropForeignKey("Processes", "factoryServerId", "FactoryServerDefinitions");
            //DropForeignKey("Processes", "factoryid", "FactoryDefinitions");
            //DropForeignKey("Processes", "divaid", "DivaDefinitions");
            //DropForeignKey("Processes", "assetId", "Assets");
            //DropForeignKey("Processes", "aeroId", "AeroDefinitions");
            //DropTable("WorkflowAeros");
            //DropTable("WorkflowDivas");
            //DropTable("WorkflowQuics");
            //DropTable("System");
            //DropTable("ProcessesLogArchive");
            //DropTable("ProcessesArchive");
            //DropTable("AssetArchive");
            //DropTable("ProcessesLog");
            //DropTable("QuicDefinitions");
            //DropTable("FactoryServerDefinitions");
            //DropTable("FactoryDefinitions");
            //DropTable("WorkflowServers");
            //DropTable("Workflows");
            //DropTable("DivaDefinitions");
            //DropTable("Assets");
            //DropTable("Processes");
            //DropTable("AeroDefinitions");
        }
    }
}
