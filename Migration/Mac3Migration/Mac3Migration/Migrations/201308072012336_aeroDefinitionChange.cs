using System.Data.Entity.Migrations;

namespace Mac3Migration.Migrations
{    
    
    public partial class aeroDefinitionChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("AeroDefinitions", "fooPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("AeroDefinitions", "fooPath");
        }
    }
}
