using System.Collections.Generic;

namespace Mac3Migration
{
    public partial class AeroDefinition
    {
        public AeroDefinition()
        {
            this.Processes = new List<Process>();
            this.Workflows = new List<Workflow>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string inboxPath { get; set; }
        public string outboxPath { get; set; }        
        public virtual ICollection<Process> Processes { get; set; }
        public virtual ICollection<Workflow> Workflows { get; set; }
    }
}
