using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class ProcessesLogArchiveMap : EntityTypeConfiguration<ProcessesLogArchive>
    {
        public ProcessesLogArchiveMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Detail)
                .IsRequired()
                .HasMaxLength(1024);

            // Table & Column Mappings
            this.ToTable("ProcessesLogArchive");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProcessesId).HasColumnName("ProcessesId");
            this.Property(t => t.UpdatedAt).HasColumnName("UpdatedAt");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Detail).HasColumnName("Detail");
        }
    }
}
