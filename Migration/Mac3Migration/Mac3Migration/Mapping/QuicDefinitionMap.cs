using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class QuicDefinitionMap : EntityTypeConfiguration<QuicDefinition>
    {
        public QuicDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.definition)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("QuicDefinitions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.definition).HasColumnName("definition");

            // Relationships
            this.HasMany(t => t.Workflows)
                .WithMany(t => t.QuicDefinitions)
                .Map(m =>
                    {
                        m.ToTable("WorkflowQuics");
                        m.MapLeftKey("quicid");
                        m.MapRightKey("workflowid");
                    });


        }
    }
}
