using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class AssetMap : EntityTypeConfiguration<Asset>
    {
        public AssetMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.houseId)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.title)
                .HasMaxLength(64);

            this.Property(t => t.isci)
                .HasMaxLength(64);

            this.Property(t => t.comment)
                .HasMaxLength(100);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.filename)
                .HasMaxLength(255);

            this.Property(t => t.fileLocation)
                .HasMaxLength(255);

            this.Property(t => t.type)
                .HasMaxLength(64);

            // Table & Column Mappings
            this.ToTable("Assets");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.houseId).HasColumnName("houseId");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.isci).HasColumnName("isci");
            this.Property(t => t.comment).HasColumnName("comment");
            this.Property(t => t.clipSom).HasColumnName("clipSom");
            this.Property(t => t.clipEom).HasColumnName("clipEom");
            this.Property(t => t.clipDuration).HasColumnName("clipDuration");
            this.Property(t => t.playSom).HasColumnName("playSom");
            this.Property(t => t.playEom).HasColumnName("playEom");
            this.Property(t => t.playDuration).HasColumnName("playDuration");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.updatedAt).HasColumnName("updatedAt");
            this.Property(t => t.createdAt).HasColumnName("createdAt");
            this.Property(t => t.filename).HasColumnName("filename");
            this.Property(t => t.fileLocation).HasColumnName("fileLocation");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.preFrames).HasColumnName("preFrames");
            this.Property(t => t.postFrames).HasColumnName("postFrames");
            this.Property(t => t.airDate).HasColumnName("airDate");
            this.Property(t => t.dropFrame).HasColumnName("dropFrame");
        }
    }
}
