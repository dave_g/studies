using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class WorkflowMap : EntityTypeConfiguration<Workflow>
    {
        public WorkflowMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.workflow_id)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.prefix)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Workflows");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.workflow_id).HasColumnName("workflow_id");
            this.Property(t => t.active).HasColumnName("active");
            this.Property(t => t.prefix).HasColumnName("prefix");
            this.Property(t => t.engine_id).HasColumnName("engine_id");
        }
    }
}
