using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class ProcessMap : EntityTypeConfiguration<Process>
    {
        public ProcessMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.workflowid)
                .HasMaxLength(100);

            this.Property(t => t.status)
                .HasMaxLength(32);

            this.Property(t => t.filename)
                .HasMaxLength(255);

            this.Property(t => t.fileLocation)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Processes");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.assetId).HasColumnName("assetId");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.updatedAt).HasColumnName("updatedAt");
            this.Property(t => t.divaid).HasColumnName("divaid");
            this.Property(t => t.filename).HasColumnName("filename");
            this.Property(t => t.fileLocation).HasColumnName("fileLocation");
            this.Property(t => t.factoryid).HasColumnName("factoryid");
            this.Property(t => t.wait).HasColumnName("wait");
            this.Property(t => t.factoryServerId).HasColumnName("factoryServerId");
            this.Property(t => t.aeroId).HasColumnName("aeroId");

            // Relationships
            this.HasOptional(t => t.AeroDefinition)
                .WithMany(t => t.Processes)
                .HasForeignKey(d => d.aeroId);
            this.HasOptional(t => t.Asset)
                .WithMany(t => t.Processes)
                .HasForeignKey(d => d.assetId);
            this.HasRequired(t => t.DivaDefinition)
                .WithMany(t => t.Processes)
                .HasForeignKey(d => d.divaid);
            this.HasRequired(t => t.FactoryDefinition)
                .WithMany(t => t.Processes)
                .HasForeignKey(d => d.factoryid);
            this.HasOptional(t => t.FactoryServerDefinition)
                .WithMany(t => t.Processes)
                .HasForeignKey(d => d.factoryServerId);

        }
    }
}
