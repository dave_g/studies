using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class FactoryDefinitionMap : EntityTypeConfiguration<FactoryDefinition>
    {
        public FactoryDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.factory)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.inboxAlias)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.account)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.inboxPath)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.password)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.outboxPath)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("FactoryDefinitions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.factory).HasColumnName("factory");
            this.Property(t => t.inboxAlias).HasColumnName("inboxAlias");
            this.Property(t => t.account).HasColumnName("account");
            this.Property(t => t.inboxPath).HasColumnName("inboxPath");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.outboxPath).HasColumnName("outboxPath");
        }
    }
}
