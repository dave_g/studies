using System.Collections.Generic;

namespace Mac3Migration
{
    public partial class FactoryServerDefinition
    {
        public FactoryServerDefinition()
        {
            this.Processes = new List<Process>();
            this.WorkflowServers = new List<WorkflowServer>();
        }

        public int id { get; set; }
        public string serverName { get; set; }
        public string applicationKey { get; set; }
        public virtual ICollection<Process> Processes { get; set; }
        public virtual ICollection<WorkflowServer> WorkflowServers { get; set; }
    }
}
