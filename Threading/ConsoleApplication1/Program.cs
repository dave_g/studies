﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            //RunThreadPooledShortRunningTask();
            //RunNonThreadPoolLongRunningTask();
            //RunTaskWithReturnValues();
            //RunPrimeNumberTask();            
            //RunTaskWithException();
            //RunTaskWithAwaiter();
            //RunTaskWithAwaiterException();
            //RunTaskWithAsyncCancel();
            //Wait();
            RunReportableTask();
        }

        private static void RunReportableTask()
        {
            var runner = new TaskRunner();
            runner.RunReportableTask();
            Thread.Sleep(10000);
            Console.WriteLine("Hit key to continue...");
            Console.ReadLine();
        }

        private static void Wait()
        {
            var runner = new TaskRunner();
            runner.Wait(5000);
            Console.WriteLine("Hit key to continue...");
            Console.ReadLine();
        }

        private static void RunTaskWithAsyncCancel()
        {
            var runner = new TaskRunner();
            runner.RunWithCancel();
            Console.WriteLine("Hit key to continue...");
            Console.ReadLine();
        }

        private static void RunTaskWithAsync()
        {
            var runner = new TaskRunner();
            runner.RunTaskWithAsync();
            Console.WriteLine("Sleeping...");
            //Thread.Sleep(10000);
            Console.WriteLine("Hit key to continue...");
            Console.ReadLine(); 
        }

        private static void RunTaskWithAwaiter()
        {
            Task<int> primeNumberTask = Task.Run(() =>
  Enumerable.Range(2, 3000000).Count(n =>
    Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));

            var awaiter = primeNumberTask.GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                int result = awaiter.GetResult();
                Console.WriteLine(result);       // Writes result
            });

            Console.ReadLine();
        }
                      
        /// <summary>
        /// exceptions get rethrown at .GetResult();
        /// Can also use on non-generice Task where getResult returns void but is usefull in obtaining the error.
        /// </summary>
        private static void RunTaskWithAwaiterException()
        {
            Task<int> primeNumberTask = Task.Run(() => { 
                throw null;
                return 1;
            });

            var awaiter = primeNumberTask.GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                try
                {
                    int result = awaiter.GetResult();
                    Console.WriteLine(result);       // Writes result
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error : \n");
                    Console.WriteLine(ex);
                }                
            });

            Console.ReadLine();
        }

        static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }

        // Start a Task that throws a NullReferenceException:
        private static void RunTaskWithException()
        {
            Task task = Task.Run(() => { throw null; });
            try
            {
                task.Wait();
            }
            catch (AggregateException aex)
            {
                if (aex.InnerException is NullReferenceException)
                    Console.WriteLine("Null!");
                else
                    throw;
            }
            Console.ReadLine();
        }

        private static void RunPrimeNumberTask()
        {
            Task<int> primeNumberTask = Task.Run(() => Enumerable.Range(2, 3000000).Count(n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));

            Console.WriteLine("Counting the number of Primes up to 3000000...");
            Console.WriteLine("The answer is " + primeNumberTask.Result);
            Console.ReadLine();
        }

        private static void RunTaskWithReturnValues()
        {
            Task<int> task = Task.Run(() => { 
                Console.WriteLine("Running Task"); 
                return 3; 
            });
            Console.WriteLine(String.Format("Task returned {0}", task.Result));
            Console.ReadLine();
        }

        private static void RunNonThreadPoolLongRunningTask()
        {
            Task task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(20000);
                Console.WriteLine("This was a long running task");
            },
                                   TaskCreationOptions.LongRunning);
            Console.WriteLine(String.Format("Is task completed : {0}", task.IsCompleted));  // False
            task.Wait();  // Blocks until task is complete
            Console.WriteLine(String.Format("Is task completed : {0}", task.IsCompleted));  // False
            Console.ReadLine();            
        }

        private static void RunThreadPooledShortRunningTask()
        {
            Task task = Task.Run(() =>
            {
                Thread.Sleep(2000);
                Console.WriteLine("This was a short running task");
            });
            Console.WriteLine(String.Format("Is task completed : {0}", task.IsCompleted));  // False
            task.Wait();  // Blocks until task is complete
            Console.WriteLine(String.Format("Is task completed : {0}", task.IsCompleted));  // False
            Console.ReadLine();
        }
    }
}
