﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication1
{
    public class TaskRunner
    {

        #region async await
        /// <summary>
        /// You need to thread sleep after calling this if youre not running a spinning application.
        /// </summary>
        public async void RunTaskWithAsync()
        {
            try
            {
                int result = await GetPrimesCountAsync();
                Console.WriteLine(result);
                //throw null; //add this to see error handling.
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ex : {0}", ex);            
            }
            
        }
       
        private Task<int> GetPrimesCountAsync()
        {
            return Task.Run(() => Enumerable.Range(2, 3000000).Count(n =>
        Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));
        }
        #endregion

        #region general
        public async void Wait(int milliseconds)
        {
            await Task.Delay(milliseconds);
            Console.WriteLine(String.Format("delayed {0} milliseconds." , milliseconds));
        }

        public void RunTaskWithAwaiter()
        {
            Task<int> primeNumberTask = Task.Run(() =>
  Enumerable.Range(2, 3000000).Count(n =>
    Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(i => n % i > 0)));

            var awaiter = primeNumberTask.GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                int result = awaiter.GetResult();
                Console.WriteLine(result);       // Writes result
            });

            Console.ReadLine();
        }
        #endregion

        #region cancellation task
        public void RunWithCancel() { 
            var cancelSource = new CancellationTokenSource();
            CancellationTask(cancelSource.Token);
            Thread.Sleep(5000);
            cancelSource.Cancel();

        }

        async Task CancellationTask(CancellationToken cancellationToken)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
                await Task.Delay(1000, cancellationToken);
            }
        }
        #endregion

        #region progress reporting task
        public async void RunReportableTask() {
            try
            {
                var progress = new Progress<int>(i => Console.WriteLine(i + " %"));
                await ReportableTask(progress);
                //throw null; //add this to see error handling.
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ex : {0}", ex);
            }            
        }

        Task ReportableTask(IProgress<int> onProgressPercentChanged)
        {
            return Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    if (i % 10 == 0) onProgressPercentChanged.Report(i / 10);
                    // Do something compute-bound...
                    Thread.Sleep(50);
                }
            });
        }

        #endregion

    }
}
