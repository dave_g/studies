﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //compareIdLists(@"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\batch2_verify.txt",
            //    @"C:\Temp\NHL_Ngo_Transfer\AllAssets_8_24_2016.csv");
            //compareIdLists(@"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\batch2_verify.txt",
            //    @"C:\Temp\NHL_Ngo_Transfer\AllAssetsInProcess_8_24_2016.csv");

            var prefix = "NHL_";
            var filename = @"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\NHLN-720p-list1-4.txt";
            //var filename = @"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\batch2_verify.txt";
            //var filename = @"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\NHLN-720p-list1.csv";
            //var filename = @"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\NHLN-720p-list2.csv";
            
            //var fileoutname = @"C:\Temp\NHL_Ngo_Transfer\NHLN-720p-list1.txt";
            var fileoutname = @"C:\Documents\Box Sync\Projects\NextGenOrigination\NHL\NHLN-720p-list1-4_sql.txt";
            //select* from (values('NHL_HICECAPPHIWSHCC_0'), ('NHL_HPHIWSH041416_0'), ('NHL_HPHIWSH041616_0')) AS X(ID)

            var sqlString = "select * from (VALUES ";
            var lines = File.ReadAllLines(filename);
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;
                
                sqlString += line.StartsWith(prefix) ? $"('{line}')," : $"('{prefix}{line}'),";
            }
            sqlString = sqlString.LastIndexOf(',') == sqlString.Length - 1 ? sqlString.Remove(sqlString.Length - 1, 1) : sqlString;
            sqlString += $") as X(ids)";
            File.WriteAllText(fileoutname,sqlString);
        }

        static void compareIdLists(string filename1, string filename2)
        {
            var prefix = "NHL_";
            var listToSearchFor = File.ReadAllLines(filename1);
            var listToSearchIn
                = File.ReadAllLines(filename2);
            var missingIds = listToSearchFor.Where(line => !string.IsNullOrEmpty(line)).Where(line => !listToSearchIn.Contains($"{prefix}{line}")).ToList();
        }
    }
}
