﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Text;
using System.Web.DynamicData;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Macafur_Model;

public partial class WorkflowNameTextField : System.Web.DynamicData.FieldTemplateUserControl {
    private const int MAX_DISPLAYLENGTH_IN_LIST = 25;

    //public override string FieldValueString {
    //    get {
    //        string value = base.FieldValueString;
    //        if (ContainerType == ContainerType.List) {
    //            if(value != null && value.Length > MAX_DISPLAYLENGTH_IN_LIST) {
    //               value = value.Substring(0, MAX_DISPLAYLENGTH_IN_LIST - 3) + "...";
    //            }
    //        }
    //        return value;
    //    }
    //}

    protected override void OnDataBinding(EventArgs e)
    {
        base.OnDataBinding(e);        
        var t = Table;
        var col = t.GetColumn("active");

        foreach (MetaColumn c in t.Columns)
        {
            if (c.Name.ToLower() == "active" && (bool)GetColumnValue(c))
            {                
                Label1.ForeColor = Color.Red;                
            }
            
        }                

    }

    public override Control DataControl {
        get {
            return Label1;
        }
    }

}
