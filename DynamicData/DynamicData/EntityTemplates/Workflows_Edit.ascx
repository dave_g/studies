﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Workflows_Edit.ascx.cs" Inherits="DynamicData_EntityTemplates_Workflows" %>

<tr>
    <td>
        <b>Workflow Id :</b>
    </td>
    <td>
        <asp:DynamicControl DataField="id" Mode="Edit" runat="server"/>
    </td>
</tr>
<tr>
    <td>
        <b>Workflow Name :</b>
    </td>
    <td>
        <asp:DynamicControl ID="DynamicControl1" DataField="name" Mode="Edit" runat="server"/>
    </td>
</tr>