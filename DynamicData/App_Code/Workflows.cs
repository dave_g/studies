﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Macafur_Model
{
    [MetadataType(typeof(WorkflowsMetadata))]    
    public partial class Workflows
    {
    }

    public class WorkflowsMetadata
    {
        [UIHint("WorkflowNameText")]
        public object name;
    }
}
