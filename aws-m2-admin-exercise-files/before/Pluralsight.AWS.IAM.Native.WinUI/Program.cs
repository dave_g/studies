﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Web;
using System.Xml;
using System.IO;
using System.Security.Cryptography;

namespace Pluralsight.AWS.IAM.Native.WinUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("** Pluralsight Course Demo - Native HTTP Calls **");
            Console.WriteLine("** Pluralsight Course Demo - Create IAM User **");


            Console.WriteLine("User created.");

            Console.ReadLine();

        }

        private static string CalculateTimestamp()
        {
            string timestamp = Uri.EscapeUriString(string.Format("{0:s}", DateTime.UtcNow));
            timestamp = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            timestamp = HttpUtility.UrlEncode(timestamp).Replace("%3a", "%3A");

            return timestamp;
        }

    }
}
