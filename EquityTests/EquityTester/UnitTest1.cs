﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EquityCount;

namespace EquityTester
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var compound = new CompoundCalculator();
            var a = compound.Compound(1500, .043, 6, 4);
            Assert.AreEqual(1938.84, Math.Round(a, 2));
        }


        [TestMethod]
        public void TestMethod2()
        {
            var a = 0.0;
            var compound = new CompoundCalculator();
            for (int i = 0; i <= 10; i++)
            {
                a += compound.Compound(2750, .043, 1, 4);
                a += 2750*12;
            }

        }

    }
}
