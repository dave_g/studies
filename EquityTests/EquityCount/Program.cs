﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquityCount
{
    class Program
    {
        static void Main(string[] args)
        {
            //base amount added monthy from salaried work after cost of living.
            const int baseSalaryContribution = 2750;
            var monthyContrib = baseSalaryContribution;
            var month = 1;
            const int year = 12;
            const int tenYear = year * 10;
            const int twentyYear = tenYear * 2;
            var totalEquity = 0;            
            const int propertyAmount = 200000;
            const int down = (propertyAmount * 20) / 100; //20%
            const int rentalAmt = 1000;
            const int costOfLiving = 2500;

            var allEquity = new List<Equity>();            
            var eq = new Equity();
            var mutualFund = new MutualFund();

            var months = new List<Month>();

            //calculate the first 10 years
            for(int decade = 1; decade <= 2; decade++)
            {
                for (month = 1; month <= tenYear; month++)
                {
                    eq.AddEquity(String.Format("{0} - {1}", decade, month), monthyContrib);
                    //compare to a mutual fund.
                    mutualFund.AddFunds = baseSalaryContribution;
                    mutualFund.Compound(8);
                    //add to reporting object.
                    months.Add(new Month(mutualFund.FundTotal(), 5, allEquity.Count, month, monthyContrib, eq.GetEquity(), allEquity.Sum(equity => equity.GetEquity())));
                    //once a down payment is accumulated we can buy a new property, move into it, rent the old property and increase monthly contributions with additional rental income.
                    if (eq.SavingDown && eq.GetEquity() >= down)
                    {
                        eq.SavingDown = false;
                        monthyContrib += rentalAmt;
                        continue;
                    }
                    //once a prperty is payed in full, start over. i.e. saving down payment for next property.
                    if (eq.GetEquity() >= propertyAmount)
                    {
                        allEquity.Add(eq);
                        eq = new Equity();
                    }
                }

                //add the last property that may not have been payed off yet.
                allEquity.Add(eq);

                //estimate the effect retirement after 10 years. comment out to estimate working for another 10 years.
                //monthyContrib -= baseSalaryContribution + costOfLiving;

                //calculate the next 10 years
            }
            
            
            //for (month = tenYear + 1; month <= twentyYear; month++)
            //{
            //    eq.AddEquity(month, monthyContrib);
            //    mutualFund.AddFunds = baseSalaryContribution;
            //    mutualFund.Compound(5);

            //    months.Add(new Month(mutualFund.FundTotal(), 5, allEquity.Count, month, monthyContrib, eq.GetEquity(), allEquity.Sum(equity => equity.GetEquity())));

            //    if (eq.SavingDown && eq.GetEquity() >= down)
            //    {
            //        eq.SavingDown = false;
            //        monthyContrib += rentalAmt;
            //        continue;
            //    }

            //    if (eq.GetEquity() >= propertyAmount)
            //    {
            //        allEquity.Add(eq);
            //        eq = new Equity();
            //    }
            //}

            //allEquity.Add(eq);


            foreach (var m in months)
            {
                Console.WriteLine(String.Format("Properties {0}, Month {1}, Contribution{2}, Equity {3} , total Equity{4}, mutual fund total {5}", m.Properties, m.MonthNumber, m.Contribution, m.Equity,m.TotalEquity, m.MutualFundTotal));
            }

            //foreach (var equity in allEquity)
            //{
            //    totalEquity += equity.GetEquity();
            //    foreach (var equityItem in equity.EquityList())
            //    {
            //        Console.WriteLine(String.Format("Properties {0}, Month {1}, Contribution{2}, Equity {3} , total Equity{4}", allEquity.Count, equityItem.Key, equityItem.Value.Contribution, equityItem.Value.Total, totalEquity));
            //    }
            //}

            Console.WriteLine(String.Format("propertyAmount : {0}", propertyAmount));
            Console.WriteLine(String.Format("down : {0}", down));
            Console.WriteLine(String.Format("Number of Equities : {0}", allEquity.Count));
            Console.WriteLine(String.Format("Total Equity : {0}", totalEquity));
            //Console.WriteLine(String.Format("Number of Months : {0}", allEquity.LastOrDefault().GetMonths() ));
            Console.WriteLine(String.Format("MutualFund : {0}", mutualFund));
            
        }
    }

    internal class Month
    {
        public Month(int mutualFundTotal, int mutualFundRor, int properties, int monthNumber, int contribution, int equity, int totalEquity)
        {
            MutualFundTotal = mutualFundTotal;
            MutualFundROR = mutualFundRor;
            Properties = properties;
            MonthNumber = monthNumber;
            Contribution = contribution;
            Equity = equity;
            TotalEquity = totalEquity;
        }

        public int MutualFundTotal { get; set; }
        public int MutualFundROR { get; set; }
        public int Properties { get; set; }
        public int MonthNumber { get; set; }
        public int Contribution { get; set; }
        public int Equity { get; set; }
        public int TotalEquity { get; set; }
    }

    internal class MutualFund
    {
        private int _funds;
        
        public int AddFunds {
            get  {return _funds; }
            set { _funds += value; }
        }

        public void Compound(int rate)
        {
            int fundreturn = ((_funds*rate)/100) / 12;
            _funds = fundreturn + _funds;
        }

        public int FundTotal()
        {
            return
            _funds;
        }

        public override string ToString()
        {
            return _funds.ToString();
        }
    }

    internal class Equity
    {
        public Equity()
        {
            _equity.Add("",new EquityEntry(0,0));
        }

        internal class EquityEntry
        {
            internal EquityEntry(int contribution, int total)
            {
                Contribution = contribution;
                Total = total;
            }
            public int Contribution { get; set; }
            public int Total { get; set; }
        }

        private Dictionary<string, EquityEntry> _equity = new Dictionary<string, EquityEntry>();

        private bool _savingDown = true;        

        public bool SavingDown
        {
            get { return _savingDown; }
            set { _savingDown = value; }
        }       


        public void AddEquity(string month, int amount)
        {
            _equity.Add(month, new EquityEntry(amount, _equity.LastOrDefault().Value.Total + amount)); 
        }

        public int GetEquity()
        {
            return _equity.LastOrDefault().Value.Total;
        }

        //public int GetMonths()
        //{
        //    return _equity.LastOrDefault().Key;
        //}

        public Dictionary<string, EquityEntry> EquityList()
        {
            return _equity;
        }
    }
}
