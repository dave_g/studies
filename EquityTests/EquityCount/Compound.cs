﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EquityCount
{
    public class CompoundCalculator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="P">Principal</param>
        /// <param name="r">Rate of Return as decimal. e.g 4.3% = .043</param>
        /// <param name="t">Number of Years</param>
        /// <param name="n">Number of Compoundings per year</param>
        /// <returns></returns>
        public double Compound(long P, double r, int t, int n)
        {
            
            long power = n * t;
            double rate = 1 + (r/n);
            var raisedRate = Math.Pow(rate, power);
            //var d = P*(1 + rate);
            var a = P*raisedRate;            
            return a;
        }

        /// <summary>
        /// F = D( ( (1 + r/n)nt – 1) / (r/n) ) + B(1 + r/n)nt
/*F = future value (what we are trying to find)
D = monthly deposits = $100
r = interest rate = .05
B = starting balance = $100
n = number of compounds per year = 12 (monthly)
t = time in years = 37
F = 100( ( (1 + .05/12)12*37 – 1) / (.05/12) ) + 100(1 + .05/12)12*37*/
        /// </summary>
        /// <param name="D">deposits</param>
        /// <param name="P">Principal</param>
        /// <param name="r">Rate of Return as decimal. e.g 4.3% = .043</param>
        /// <param name="t">Number of Years</param>
        /// <param name="n">Number of Compoundings per year</param>
        /// <returns></returns>
        public double Compound(long D, long p, double r, int t, int n)
        {
            //F = D( ( raisedRate – 1) / (rate) ) 
            long power = n * t;
            double rate = 1 + (r/n);
            var raisedRate = Math.Pow(rate, power);
            //var d = P*(1 + rate);
            var a = D*((raisedRate -1) / rate);            
            return a;
        }

    }
}
