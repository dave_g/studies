﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterrogatorModel
{
    public class DivaObjectInfo
    {
        public DivaObjectInfo()
        {
            FilesList = new List<string>();
            TapeInstances = new List<DivaTapeInstanceDesc>();
            ActorInstances = new List<DivaActorInstanceDesc>();
        }

        public int Id { get; set; }
        public DivaObjectSummary ObjectSummary { get; set; }       
        public string Uuid { get; set; }
        public int LockStatus { get; set; }
        public int ObjectSize { get; set; }
        public int ObjectSizeBytes { get; set; }

        public List<string> FilesList { get; set; }
        public string ObjectComments { get; set; }
        public long ArchivingDate { get; set; }

        public bool IsInserted { get; set; }

        public List<DivaTapeInstanceDesc> TapeInstances { get; set; }

        public List<DivaActorInstanceDesc> ActorInstances { get; set; }

        public string ObjectSource { get; set; }

        public string RootDirectory { get; set; }

        public List<int> RelatedRequests { get; set; }

        public bool ToBeRepacked { get; set; }

        public int ModifiedOrDeleted { get; set; }

        public bool IsComplex { get; set; }

        public int NbFilesInComplexComponent { get; set; }

        public int NbFoldersInComplexComponent { get; set; }
    }
    
}
