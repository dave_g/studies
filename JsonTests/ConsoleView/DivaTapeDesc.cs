﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterrogatorModel
{
    public class DivaTapeDesc
    {
        public int Id { get; set; }

        public string Vsn { get; set; }

        public bool IsInserted { get; set; }

        public string ExternalizationComment { get; set; }

        public bool IsGoingToBeRepacked { get; set; }

        public int MediaFormatId { get; set; }

        public DivaTapeInstanceDesc DivaTapeInstanceDesc { get; set; }
    }
}
