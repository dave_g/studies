﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterrogatorModel
{
    public class DivaObjectSummary
    {
        public int Id { get; set; }
        public string ObjectName { get; set; }        
        public string ObjectCategory { get; set; }

    }
}
