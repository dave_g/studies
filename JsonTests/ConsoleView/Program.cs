﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;
namespace ConsoleView
{
    class Program
    {
        static void Main(string[] args)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(InterrogatorModel.DivaObjectInfo));
            FileStream stream = new FileStream(@"c:\ssg_logs\json.txt", FileMode.OpenOrCreate);
            serializer.WriteObject(stream, new InterrogatorModel.DivaObjectInfo());
        }
    }
}
