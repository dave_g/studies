﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleView
{
    public class Model
    {
        public int IntProperty { get; set; }
        public string StringProperty { get; set; }
    }
}
