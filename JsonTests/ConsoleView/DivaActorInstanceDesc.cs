﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterrogatorModel
{


    public class DivaActorInstanceDesc
    {
        public int Id { get; set; }

        public string Actor { get; set; }

        public int InstanceId { get; set; }

        public DivaObjectInfo DivaObjectInfo { get; set; }

    }
}
