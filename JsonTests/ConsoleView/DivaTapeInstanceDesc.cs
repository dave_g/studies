﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterrogatorModel
{
    public class DivaTapeInstanceDesc
    {
        public DivaTapeInstanceDesc()
        {
            TapeDesc = new List<DivaTapeDesc>();
        }

        public int Id { get; set; }

        public int InstanceID { get; set; }

        public string GroupName { get; set; }

        public List<DivaTapeDesc> TapeDesc { get; set; }

        public bool IsInserted { get; set; }

        public int ReqStatus { get; set; }

        public DivaObjectInfo DivaObjectInfo { get; set; }
    }
}
