﻿using System.Windows.Forms;

namespace CustomConfigurations
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var pageAppearanceSection =
                (PageAppearanceSection) System.Configuration.ConfigurationManager.GetSection("pageAppearanceGroup/pageAppearance");
            var color = pageAppearanceSection.Color;
            var foo = pageAppearanceSection.Foo;
        }
    }
}
