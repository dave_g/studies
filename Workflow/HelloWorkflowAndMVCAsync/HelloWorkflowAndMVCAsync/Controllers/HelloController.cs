﻿namespace HelloWorkflowAndMVCAsync.Controllers
{
    using System;
    using System.Activities;
    using System.Web.Mvc;

    using HelloWorkflowAndMVCAsync.Workflows;

    using Microsoft.Activities;

    public class HelloController : AsyncController
    {
        //
        // GET: /Hello/

        // Declare the workflow this way for best performance

        #region Constants and Fields

        private static readonly HelloMVC HelloMvcDefinition = new HelloMVC();

        private ActivityInstanceState completionState;

        private WorkflowArguments outputArguments;

        private WorkflowApplication workflowApplication;

        private Exception workflowException;

        #endregion

        #region Public Methods and Operators

        public void IndexAsync()
        {
            // Visual Basic does not support dynamic objects - use ViewData to access the ViewBag

            // Classic way of passing input arguments
            // var input = new Dictionary<string, object> { { "ViewData", this.ViewData } };

            // More "MVC" like method using Microsoft.Activities.WorkflowArguments
            dynamic input = new WorkflowArguments();

            input.ViewData = this.ViewData;
            input.TestError = false;

            this.workflowApplication = new WorkflowApplication(HelloMvcDefinition, input)
                {
                    Completed = wce => this.AsyncManager.Sync(
                        () =>
                            {
                                this.AsyncManager.OutstandingOperations.Decrement();
                                this.completionState = wce.CompletionState;
                                switch (this.completionState)
                                {
                                    case ActivityInstanceState.Closed:
                                        this.outputArguments = WorkflowArguments.FromDictionary(wce.Outputs);
                                        break;
                                    case ActivityInstanceState.Faulted:
                                        this.workflowException = wce.TerminationException;
                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException();
                                }
                            }),
                    Aborted = wae => this.AsyncManager.Sync(
                        () =>
                            {
                                this.AsyncManager.OutstandingOperations.Decrement();
                                this.workflowException = wae.Reason;
                            })
                };

            this.AsyncManager.OutstandingOperations.Increment();
            this.workflowApplication.Run();
        }

        public ActionResult IndexCompleted()
        {
            if (this.workflowException != null)
            {
                throw this.workflowException;
            }

            // ViewBag contains the result set by the workflow
            // See Views / Hellow / Index.cshtml for the related view
            return this.View();
        }

        #endregion
    }
}