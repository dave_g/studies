﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HelloWorkflowAndMVCAsync.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to Workflow and ASP.NET MVC (Async)!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
