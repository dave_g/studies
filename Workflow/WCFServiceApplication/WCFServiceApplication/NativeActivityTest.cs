﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Activities;

namespace WCFServiceApplication
{

    public sealed class NativeActivityTest : NativeActivity
    {
        readonly Collection<Activity> _children = new Collection<Activity>();
        private readonly CompletionCallback _activityCompleteCallback;
        private readonly FaultCallback _activityFaultCallback;
        
        public NativeActivityTest()
        {
            _activityCompleteCallback = activity_complete;
            _activityFaultCallback = activity_fault;
        }

        private void activity_fault(NativeActivityFaultContext faultcontext, Exception propagatedexception, ActivityInstance propagatedfrom)
        {
            throw new NotImplementedException();
        }

        private void activity_complete(NativeActivityContext context, ActivityInstance completedinstance)
        {
            throw new NotImplementedException();
        }

        public Collection<Activity> Branches
        {
            get { return _children; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            //sequencial execution
            foreach (var activity in _children)
            {
                context.ScheduleActivity(activity,_activityCompleteCallback,_activityFaultCallback);
                
            }
        }
    }
}
