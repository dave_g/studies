﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FluentFTP;

namespace FtpTester
{
    public class FftpManager
    {
        private FtpClient _client;

        public FftpManager(string server, string user, string password, int port = 21)
        {
            _client = new FtpClient(server);
            // if you don't specify login credentials, we use the "anonymous" user account
            if (user != null) _client.Credentials = new NetworkCredential(user, password);

            // begin connecting to the server
            _client.Connect();
        }

        //@"amnesisbin\ttdAmnesisBinDeployment@waws-prod-am2-125.ftp.azurewebsites.window";
        //public FtpClient Connect(string server, string user, string password, int port = 21)
        //{
        //    var _client = new FtpClient(server);
        //    // if you don't specify login credentials, we use the "anonymous" user account
        //    if (user != null) _client.Credentials = new NetworkCredential(user, password);

        //    // begin connecting to the server
        //    _client.Connect();
        //    return _client;
        //}


        public List<string> GetListing( string dir)
        {
            var serverFileList = new List<string>();
            foreach (FtpListItem item in _client.GetListing(dir))
            {
                long size = 0;
                //if this is a file
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    // get the file size
                    size = _client.GetFileSize(item.FullName);

                }
                // get modified date/time of the file or folder
                DateTime time = _client.GetModifiedTime(item.FullName);
                // calculate a hash for the file on the server side (default algorithm)
                //FtpHash hash = _client.GetHash(item.FullName); Errors with command not understood.
                serverFileList.Add($"{item.Name} {item.FullName} {size} {time}");
            }
            return serverFileList;
        }

        public bool UploadFile( string fromFile, string toFile, bool overWrite, bool createDir)
        {
            _client.RetryAttempts = 3;
            if (overWrite)
            {
                return _client.UploadFile(fromFile, toFile, FtpExists.Overwrite, createDir, FtpVerify.Retry);
            }
            else
            {
                return _client.UploadFile(fromFile, toFile, FtpExists.Skip, createDir, FtpVerify.Retry);
            }

            //return _client.UploadFile(fromFile, toFile);
        }

        public bool DownloadFile(string fromFile, string toFile)
        {
            return _client.DownloadFile(fromFile, toFile);
        }

        public bool FileExists(string file)
        {
            return _client.FileExists(file);
        }

        public bool DirectoryExists( string dir)
        {
            return _client.DirectoryExists(dir);
        }

        public void RenameFile( string fromFile, string toFile)
        {
            _client.Rename(fromFile, toFile);
        }

        public void DeleteFile( string file)
        {
            _client.DeleteFile(file);
        }

        public string GetWorkingDir()
        {
            return _client.GetWorkingDirectory();
        }
    }
}
