﻿using System;
using System.Windows.Forms;
using System.Net.FtpClient;
using System.Net;
using System.IO;
using Limilabs.FTP.Client;
using Renci.SshNet;
using WinSCP;
using Session = WinSCP.Session;

namespace FtpTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DoSystemNetTest();
            //DoNetDllTest();
            //DoWinScpTest();
            //DoNetSShTest();
            DoFluedntFtpTest();
        }

        private void DoFluedntFtpTest()
        {
           FluentFtpTester.DoTest();
            
        }

        private void DoNetSShTest()
        {
            string host = "10.54.189.49";
            string username = "root";
            string password = "isilon";

            using (var client = new SshClient(host, username, password))
            {
                client.Connect();
                var cmd = @"scp /ifs/data/staging/avscan/projects/client1/bigfile.txt root@10.54.189.50:/ifs/MAC/projects/client1/";
                client.RunCommand(cmd);
                client.Disconnect();
            }
            

        }

        private void DoWinScpTest()
        {
            try
            {
                AppendText($"opening scp session.");
                var sessionOptions = new SessionOptions();
                sessionOptions.HostName = "172.26.40.29";
                //sessionOptions.PortNumber = 22;
                sessionOptions.Protocol = Protocol.Scp;
                sessionOptions.UserName = "";
                sessionOptions.Password = "";
                sessionOptions.GiveUpSecurityAndAcceptAnySshHostKey = true;
                
                var session = new Session();
                session.Open(sessionOptions);
                
                AppendText($"attempting scp list command.");
                session.ExecuteCommand($"cd ").Check();
                var directory = session.ListDirectory("/ifs/data/mac/processing/carbon/out");

                foreach (RemoteFileInfo fileInfo in directory.Files)
                {
                    AppendText($"{fileInfo.Name} with size {fileInfo.Length}, permissions {fileInfo.FilePermissions} and last modification at {fileInfo.LastWriteTime}");
                }
                AppendText($"scp command finished.");

                AppendText($"attempting scp transfer command.");
                session.ExecuteCommand($"scp /ifs/data/mac/processing/TEST_NHL_0315A_MXFWRAP.mxf root@192.168.115.9:/ifs/MAC/TEST").Check();
                AppendText($"scp command finished.");

                AppendText($"closing scp session.");
                session.Close();
            }
            catch (Exception e)
            {
                AppendText($"{e}");
            }
            
        }

        /*Create a connection to FTP server 1, and another connection to FTP server 2. 
On FTP server 1, enter passive mode (data connections will be incoming), and make a note of the servers response to the PASV command (what ip the server will be listening on, and on what port). 
On server 2, send a PORT command (data connections will be outgoing), such that the port command data corresponds to the data in FTP server 1's reply to the PASV command gathered in step 2. 
Finally, send the STOR command to ftp server 1, and the RETR command to FTP server 2.  The data connection will be made between those two servers, and when the transfer is complete the client will be notified by*/
        private void DoNetDllTest()
        {
            AppendText("Trying Connect.\n");
            try
            {
                using (var server1 = new Ftp())
                {
                    var host = @"10.31.40.248"; //cocmcvssgtstdb1
                    var host2 = @"10.147.124.104";
                    var pasv_port = 0;
                    
                    server1.Connect(host);
                    server1.Login("ftpuser", "C0mcast!");
                    //var host = @"192.168.115.9";
                    //client.Login("root", "mAcL0ck!");
                    if (server1.Connected)
                    {
                        textBox1.AppendText("Connected to host 1.\n");
                        
                        using (var server2 = new Ftp())
                        {
                            
                            server2.Connect(host2);
                            server2.Login("ftpuser", "C0mcast!");
                            if (server2.Connected)
                            {
                                //var supportedCmds = server2.SendCommand("HELP");
                                var pasvResponse = server2.SendCommand("PASV");
                                if (pasvResponse.Code == 227)
                                {
                                    //parse ports - Assumes message = Entering Passive Mode (10,31,40,248,253,37)
                                    AppendText($"PASV cmd returned : {pasvResponse.Message}");
                                    var portparams = pasvResponse.Message.Split(new char[] { '(', ')' }); //Remove(pasvResponse.Message.IndexOf(")"));
                                    var portdata = portparams[1].Split(new char[] { ',' });

                                    AppendText($"PASV Port list : ");
                                    foreach (var p in portdata)
                                    {
                                        AppendText($"{p}");
                                    }

                                    //The first set of values represent the ip-address offered. peal off the first port offered.
                                    //if (portdata.Count() > 4)
                                    //{
                                    //    var port = portdata[4];
                                    //    int.TryParse(port, out pasv_port);
                                    //    AppendText($"Port to use : {pasv_port}");
                                    //}

                                    server1.SendCommand("PORT " + portdata[0] + "," + portdata[1] + "," + portdata[2] + "," + portdata[3] + "," + portdata[4] + "," + portdata[5]);
                                    //server1.SendCommand("PORT " , new object[] { portdata[0],portdata[1],portdata[2],portdata[3],portdata[4],portdata[5]});
                                    var filename = "1308_20998_MIGA5243000H_TEST_20150702104029000.zip";
                                    server1.SendCommand("STOR", filename);
                                    server2.SendCommand("RETR", filename);
                                }
                                else
                                {
                                    AppendText($"Error PASV cmd returned : {pasvResponse.Code}");
                                }

                            }
                        }
                        
                    }

                    server1.Close();
                }
            }
            catch (Exception e)
            {
                AppendText($"Error : {e}");
            }
            
        }

        private void AppendText(string message)
        {
            textBox1.AppendText($"{message}\n");
        }

        private void DoSystemNetTest()
        {
            var filename = "TEST_01.mov";
            //var filename = "INHD11653_UrbanWrestlingFederationSnitch_2.mxf";
            
            try
            {
               

                textBox1.AppendText("Trying Connect.\n");
                using (var ReceivingConnection = new FtpClient())
                {
                    //GetNielsenFtpConn(conn);
#if DEBUG
                    ReceivingConnection.Host = @"10.31.40.248";//cocmcvssgtstdb1
                    ReceivingConnection.Credentials = new NetworkCredential("ftpuser", "C0mcast!");
#else
                    //ReceivingConnection.Host = @"192.168.115.9";
                    //ReceivingConnection.Credentials = new NetworkCredential("root", "mAcL0ck!");
                    //ReceivingConnection.Host = @"172.27.96.68"; //unity isilon
                    //ReceivingConnection.Credentials = new NetworkCredential("root", "evertz");
                    ReceivingConnection.Host = @"172.26.40.132"; //ROC Isilon
                    ReceivingConnection.Credentials = new NetworkCredential("root", "Rumpl3st!ltsk!n");
#endif

                    ReceivingConnection.Connect();

                    if (ReceivingConnection.IsConnected)
                    {
                        AppendText("Receiveing Server Connected.\n");
                        //ReceivingConnection.SetWorkingDirectory("/ifs/MAC/TEST/Dave/test");
                        
                        var receivingWorkingDirectory = ReceivingConnection.GetWorkingDirectory();
                        AppendText($"Receiveing Server Working Dir : {receivingWorkingDirectory}");

                        //ReceivingConnection.SetWorkingDirectory("/ifs/data/temp");
                        ReceivingConnection.SetWorkingDirectory("/ifs/data/mac");
                        //receivingWorkingDirectory = ReceivingConnection.GetWorkingDirectory();
                        //AppendText($"Receiveing Server Working Dir : {receivingWorkingDirectory}");

                        var pasvResponse = ReceivingConnection.Execute("PASV");
                        AppendText($"PASV cmd returned : {pasvResponse.Message}");
                        var portparams = pasvResponse.Message.Split(new char[] { '(', ')' });
                        var portdata = portparams[1].Split(new char[] { ',' });

                        AppendText($"Receiving Connection PASV Port list : ");
                        foreach (var p in portdata)
                        {
                            AppendText($"{p}");
                        }
                        var stor_reply = ReceivingConnection.BeginExecute($"STOR {filename}", CallbackStor, ReceivingConnection);
                        
                        using (var SendingConnection = new FtpClient())
                        {
#if DEBUG
                            SendingConnection.Host = @"10.147.124.104"; //ssgweb-po-1p
                            SendingConnection.Credentials = new NetworkCredential("ftpuser", "C0mcast!");
#else
                            SendingConnection.Host = @"192.168.115.54";
                            SendingConnection.Credentials = new NetworkCredential("root", "mAcL0ck!");
                            //SendingConnection.Host = @"172.26.40.132"; //ROC Isilon
                            //SendingConnection.Credentials = new NetworkCredential("root", "Rumpl3st!ltsk!n");
#endif
                            SendingConnection.Connect();
                            if (SendingConnection.IsConnected)
                            {
                                var sendingWorkingDirectory = SendingConnection.GetWorkingDirectory();
                                SendingConnection.SetWorkingDirectory("/ifs/MAC/TEST/Dave");
                                //\\172.26.40.132\mac\projects\indemand\aspera_from_client\_for_qc\baton_hd
                                //SendingConnection.SetWorkingDirectory("/ifs/data");
                                //SendingConnection.SetWorkingDirectory("/ifs/data/mac/projects/indemand/aspera_from_client/_for_qc/baton_hd");
                                AppendText($"Sending Server Working Dir : {sendingWorkingDirectory}");
                                try
                                {
                                    AppendText("Sending Server Connected.");

                                    


                                    var port_reply = SendingConnection.Execute("PORT " + portdata[0] + "," + portdata[1] + "," + portdata[2] + "," + portdata[3] + "," + portdata[4] + "," + portdata[5]);
                                    AppendText(port_reply.Message);
                                    var retr_reply = SendingConnection.BeginExecute($"RETR {filename}", CallbackRETR, SendingConnection);
                                    //AppendText($"RETR Reply :  {retr_reply.Code} {retr_reply.Message}");
                                    //AppendText(retr_reply.ErrorMessage);
                                    AppendText("Function complete.");
                                }
                                catch (Exception ex)
                                {
                                    AppendText($"Error: {ex}");
                                    ReceivingConnection.Disconnect();
                                    SendingConnection.Disconnect();
                                }
                            }
                            else
                            {
                                AppendText("Server 2 Connection Failed.\n");
                            }
                                
                        }
                            
                    }
                    else
                    {
                        AppendText("Server 1 Connection Failed.\n");
                    }
                }
            }
            catch (Exception ex)
            {
                AppendText($"Error: {ex}");
            }
        }

        private void CallbackRETR(IAsyncResult ar)
        {
            AppendText("RETR complete : " + ar.IsCompleted);
            var conn = (FtpClient)ar.AsyncState;
            conn.Disconnect();
        }

        private void CallbackStor(IAsyncResult ar)
        {
            AppendText("STOR complete : " + ar.IsCompleted);
            var conn = (FtpClient)ar.AsyncState;
            conn.Disconnect();
            //AppendText($"Store Reply : {ar.Code} {stor_reply.Message}");
            //ppendText(stor_reply.ErrorMessage);

            //Thread.Sleep(1000);
            //var retr_reply = SendingConnection.Execute($"RETR {sendingWorkingDirectory}/{filename}");
            //var SendingConnection = (FtpClient)ar.AsyncState;

        }

        private void DoSystemNetNielsenTest()
        {
            try
            {


                textBox1.AppendText("Trying Connect.\n");
                using (FtpClient conn = new FtpClient())
                {
                    //GetNielsenFtpConn(conn);
                    GetTestFtpConn(conn);
                    conn.Connect();

                    if (conn.IsConnected)
                    {
                        try
                        {
                            textBox1.AppendText("Connected.\n");
                            textBox1.AppendText(String.Format("The working directory is: {0}",
                                conn.GetWorkingDirectory()));
                            if (conn.DirectoryExists("/NIELSEN_METADATA"))
                            {
                                conn.SetWorkingDirectory("NIELSEN_METADATA");
                                textBox1.AppendText(String.Format("The working directory is: {0}",
                                    conn.GetWorkingDirectory()));

                                UploadFile(conn);
                            }
                            else
                            {
                                UploadFile(conn);
                            }
                        }
                        catch (Exception ex)
                        {
                            textBox1.AppendText($"Error: {ex}");
                            conn.Disconnect();
                        }
                    }
                    else
                    {
                        textBox1.AppendText("Connection Failed.\n");
                    }
                }
            }
            catch (Exception ex)
            {
                textBox1.AppendText($"Error: {ex}");
            }
        }
        private static void UploadFile(FtpClient conn)
        {
            //var writefile = "linuxmint-17.1-cinnamon-64bit.iso";
            var writefile = "1308_20998_MIGA5243000H_TEST_20150702104029000.zip";
            //var writefile = "floss0292_Oscon.mp4";
            //var readfile = @"C:\Users\dgraha001\Downloads\linuxmint-17.1-cinnamon-64bit.iso";
            var readfile = @"C:\Temp\1308_20998_MIGA5243000H_TEST_20150702104029000.zip";
            //var writefile = "pi.zip";
            //var readfile = @"pi.zip";
            //var readfile = @"C:\Users\dgraha001\Downloads\floss0292_Oscon.mp4";

            using (Stream ostream = conn.OpenWrite(writefile))
            {
                try
                {
                    FileStream toSend = File.OpenRead(readfile);

                    byte[] buffer = new byte[16*1024];
                    while (toSend.Position < toSend.Length)
                    {
                        int bytesRead = toSend.Read(buffer, 0, buffer.Length);
                        ostream.Write(buffer, 0, bytesRead);
                    }
                    toSend.Close();
                    // istream.Position is incremented accordingly to the writes you perform
                }
                finally
                {
                    ostream.Close();
                }
            }
        }


        private void GetTestFtpConn(FtpClient conn)
        {
            conn.Host = @"10.31.40.248";
            conn.Credentials = new NetworkCredential("ftpuser", "C0mcast!");
        }

        private static void GetNielsenFtpConn(FtpClient conn)
        {
            conn.Host = @"namft.nielsen.com";
            conn.Credentials = new NetworkCredential("TCF_MFT_COMM_USER@nielsen.com", "TCFmft1234");
        }
    }
}
