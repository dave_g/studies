﻿using System;
using System.Collections.Generic;
using FluentFTP;

namespace FtpTester
{
    public class FluentFtpTester
    {
        

        public static void DoTest()
        {
            var server = "52.233.128.186";
            var user = @"amnesisbin\ttdAmnesisBinDeployment";
            var password = @"Ds3rt56x";
            var fluentFtp = new FftpManager();

            var serverFileList = new List<string>();

            using (var client = fluentFtp.Connect(server, user, password))
            {
                if (client.IsConnected)
                {
                    serverFileList.Add($"{client.GetWorkingDirectory()}");
                    foreach (FtpListItem item in client.GetListing("/site/wwwroot"))
                    {
                        long size = 0;
                        //if this is a file
                        if (item.Type == FtpFileSystemObjectType.File)
                        {

                            // get the file size
                            size = client.GetFileSize(item.FullName);

                        }
                        // get modified date/time of the file or folder
                        DateTime time = client.GetModifiedTime(item.FullName);

                        // calculate a hash for the file on the server side (default algorithm)
                        //FtpHash hash = client.GetHash(item.FullName); Errors with command not understood.
                        serverFileList.Add($"{item.Name} {item.FullName} {size} {time}");
                    }

                }

                client.UploadFile(@"C:\temp\MyVideo.mp4", "/site/wwwroot/wp-content/uploads/big.txt");

                // rename the uploaded file
                client.Rename("/site/wwwroot/wp-content/uploads/big.txt", "/site/wwwroot/wp-content/uploads/big2.txt");

                // download the file again
                client.DownloadFile(@"C:\temp\MyVideo_2.mp4", "/site/wwwroot/wp-content/uploads/big2.txt");

                // delete the file
                client.DeleteFile("/site/wwwroot/wp-content/uploads/big2.txt");

                // delete a folder recursively
                //client.DeleteDirectory("/wp-content/uploads/extras/");

                // check if a file exists
                if (client.FileExists("/site/wwwroot/wp-content/uploads/big2.txt")) { }

                // check if a folder exists
                if (client.DirectoryExists("/site/wwwroot/wp-content/uploads/extras/")) { }

                // upload a file and retry 3 times before giving up
                client.RetryAttempts = 3;
                client.UploadFile(@"C:\MyVideo.mp4", "/site/wwwroot/wp-content/uploads/big.txt", FtpExists.Overwrite, false, FtpVerify.Retry);

                // disconnect! good bye!
                client.Disconnect();
            }
        }

        public static void DoManagerTest()
        {
            var server = "52.233.128.186";
            var user = @"amnesisbin\ttdAmnesisBinDeployment";
            var password = @"Ds3rt56x";
            var fluentFtp = new FftpManager();

            var serverFileList = new List<string>();

            using (var client = fluentFtp.Connect(server, user, password))
            {
                if (client.IsConnected)
                {
                    serverFileList.Add($"{fluentFtp.GetWorkingDir(client)}");
                    serverFileList = fluentFtp.GetListing(client, @"/site/wwwroot");
                    fluentFtp.UploadFile(client, @"C:\temp\MyVideo.mp4", "/site/wwwroot/wp-content/uploads/big.txt", true,false);
                }

                

                // rename the uploaded file
                client.Rename("/site/wwwroot/wp-content/uploads/big.txt", "/site/wwwroot/wp-content/uploads/big2.txt");

                // download the file again
                client.DownloadFile(@"C:\temp\MyVideo_2.mp4", "/site/wwwroot/wp-content/uploads/big2.txt");

                // delete the file
                client.DeleteFile("/site/wwwroot/wp-content/uploads/big2.txt");

                // delete a folder recursively
                //client.DeleteDirectory("/wp-content/uploads/extras/");

                // check if a file exists
                if (client.FileExists("/site/wwwroot/wp-content/uploads/big2.txt")) { }

                // check if a folder exists
                if (client.DirectoryExists("/site/wwwroot/wp-content/uploads/extras/")) { }

                // upload a file and retry 3 times before giving up
                client.RetryAttempts = 3;
                client.UploadFile(@"C:\MyVideo.mp4", "/site/wwwroot/wp-content/uploads/big.txt", FtpExists.Overwrite, false, FtpVerify.Retry);

                // disconnect! good bye!
                client.Disconnect();
            }
        }
    }
}
