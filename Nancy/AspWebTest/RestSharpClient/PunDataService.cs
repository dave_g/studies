﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;

namespace RestSharpClient
{
    class PunDataService
    {
        public Pun[] GetPuns()
        {
            var client = new RestClient("http://localhost:56033");
            var request = new RestRequest("/puns", Method.GET);
            var response = client.Execute<List<Pun>>(request);
            return response.Data.ToArray();
        }
    }
}
