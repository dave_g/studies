﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;

namespace AspWebTest
{
    public class PunModule : NancyModule
    {
        public PunModule() : base("/puns")
        {
            Get["/"] = arguments => GetPuns();
            Get["/{punId}"] = arguments => GetPunsById(arguments.punID);
            Post["/"] = arguments => CreatePun();
        }

        private Pun CreatePun()
        {
            var pun = this.Bind<Pun>();
            var service = new PunDataService();
            service.AddPun(pun);
            return pun;
        }

        private Pun GetPunsById(int punID)
        {
            var service = new PunDataService();
            return service.GetPunById(punID);
        }

        private Pun[] GetPuns()
        {
            var service = new PunDataService();
            return service.GetPuns();
        }
    }
}