﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleView
{
    class Program
    {
        //method signature to pass into a delegate
        static int Square(int x) { return x * x; }
        static int Power(int x) { return x * 2; }
        
        static int ConsoleWriter(int x) { Console.WriteLine(x); return x; }
        static int ConsoleWriter2(int x) { Console.WriteLine(9); return x; }

        //delegate to run a method with signature int m(int);
        delegate int Transformer(int x);

        static void Main(string[] args)
        {
            
            //var runner = new DelegateFunctions();
            //runner.RunPluginMethods(Square);
            Transformer square = Square;
            Transformer power = Power;
            Console.WriteLine(square(3)); //same as t.Invoke(3);
            
            //pass different methods of the same delegate to a method.
            int[] numArray = { 1, 2, 3 };
            Transform(numArray, square);
            foreach (var item in numArray)
            {
                Console.WriteLine(item);
            }

            Transform(numArray, power);
            foreach (var item in numArray)
            {
                Console.WriteLine(item);
            }

            //combine delegates            
            Transformer consoleWriter = ConsoleWriter;            
            consoleWriter += ConsoleWriter2;

            Console.WriteLine("combined delegate");
            int[] numArray2 = { 1, 2, 3 };
            foreach (var item in numArray2)
            {
                consoleWriter(item);
            }
            
            
        }

        /// <summary>
        /// a method that will accept any method assigned to delegate
        /// </summary>
        /// <param name="values"></param>
        /// <param name="t"></param>
        static void Transform(int[] values, Transformer t)
        {
            for (int i = 0; i < values.Length; i++)
                values[i] = t(values[i]);
        }
    }
}
