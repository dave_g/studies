﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleView
{
    public class DelegateFunctions
    {
        public delegate int Transformer(int x);

        public class Util
        {
            public static void Transform(int[] values, Transformer t)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = t(values[i]);
            }
        }        

        public void RunPluginMethods(Transformer t)
        {
            int[] values = { 1, 2, 3 };
            Util.Transform(values, t);      // Hook in the Square method
            foreach (int i in values)
                Console.Write(i + "  ");           // 1   4   9

        }
    }
}
