namespace EFDAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Blogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        BloggerName = c.String(),
                        Properties_Id = c.Int(nullable: false),
                        Properties_Property1 = c.String(),
                        Properties_Property2 = c.String(),
                        Properties_Property3 = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        Blog_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Blogs", t => t.Blog_Id)
                .Index(t => t.Blog_Id);
            
            CreateTable(
                "TableSplit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SomeBigField = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "LeftSplit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Left = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "RightSplit",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Right = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("LeftSplit", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("RightSplit", new[] { "Id" });
            DropIndex("Posts", new[] { "Blog_Id" });
            DropForeignKey("RightSplit", "Id", "LeftSplit");
            DropForeignKey("Posts", "Blog_Id", "Blogs");
            DropTable("RightSplit");
            DropTable("LeftSplit");
            DropTable("TableSplit");
            DropTable("Posts");
            DropTable("Blogs");
        }
    }
}
