﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Model;

namespace EFDAL
{
    public class DBSeed : DropCreateDatabaseAlways<Context>
    {
        protected override void Seed(Context context)
        {
            var blog = new Blog() { BloggerName = "Dave", Title = "Natural", Properties = new BProperties(){Id = 234, Property1 = "one", Property2 = "two", Property3 = "three"} };
            var posts = new List<Post>
                                   {
                                       new Post() { Blog = blog, Content = "Some Content.", Title = "Fancy Title." },
                                       new Post() { Blog = blog, Content = "Some Content.", Title = "Fancy Title." },
                                       new Post() { Blog = blog, Content = "Some Content.", Title = "Fancy Title." },
                                       new Post() { Blog = blog, Content = "Some Content.", Title = "Fancy Title." }
                                   };
            posts.ForEach(p => context.Posts.Add(p));
            context.Blogs.Add(blog);
            base.Seed(context);
        }
    }
}
