﻿using System.Data.Entity;
using Model.DbBuildFromCode;

namespace EFDAL.DbBuildFromCode
{
    public class EFDbBuilderContext: DbContext
    {

        public DbSet<DivaDefinition> DivaDefinitions { get; set; }
        public DbSet<Processing> Processors { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        //public DbSet<AeroDefinition> AeroDefinitions { get; set; }
        
        
        

        public EFDbBuilderContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            //Database.SetInitializer(new DBSeed());
            Database.SetInitializer(new DropCreateDatabaseAlways<EFDbBuilderContext>());
        }
    }
}
