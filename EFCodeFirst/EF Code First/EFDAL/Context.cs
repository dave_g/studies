﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using EFDAL.Migrations;
using Model;

namespace EFDAL
{
    public class Context : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        public Context(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            //Database.SetInitializer(new DBSeed());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Configuration>());
        }

        public Context()            
        {
            //Database.SetInitializer(new DBSeed());
            //Database.SetInitializer(new DropCreateDatabaseAlways<Context>());            
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            //modelBuilder.Entity<Blog>().HasKey(b => b.Id);
            //modelBuilder.Entity<Blog>().HasOptional(b => b.Properties);            
            //modelBuilder.Entity<Split>().Map(m =>
            //                                    {
            //                                        m.Properties(p=> new {p.Id, p.Left});
            //                                        m.ToTable("LeftSplit");
            //                                    })
            //                                    .Map(m =>
            //                                             {
            //                                                 m.Properties( p=> new {p.Id, p.Right});
            //                                                 m.ToTable("RightSplit");
            //                                             });
            //modelBuilder.Entity<HeavyInfo>().Property(p=> p.SomeBigField).HasColumnType("");
            modelBuilder.Entity<LightInfo>().ToTable("TableSplit");
            modelBuilder.Entity<HeavyInfo>().ToTable("TableSplit");
            modelBuilder.Entity<LightInfo>().HasRequired(l => l.BigInfo).WithRequiredPrincipal();
            modelBuilder.ComplexType<BProperties>();
            modelBuilder.Configurations.Add(new SplitConfiguration());

        }


    }
}
