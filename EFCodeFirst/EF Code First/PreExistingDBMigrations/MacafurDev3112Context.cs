using System.Data.Entity;
using PreExistingDBMigrations.Models;
using PreExistingDBMigrations.Models.Mapping;

namespace PreExistingDBMigrations
{
    public partial class MacafurDev3112Context : DbContext
    {
        static MacafurDev3112Context()
        {
            Database.SetInitializer<MacafurDev3112Context>(null);
        }

        public MacafurDev3112Context()
            : base("Name=MacafurDev3112Context")
        {
        }

        public DbSet<AeroDefinition> AeroDefinitions { get; set; }
        public DbSet<AssetArchive> AssetArchives { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<DivaDefinition> DivaDefinitions { get; set; }
        public DbSet<FactoryDefinition> FactoryDefinitions { get; set; }
        public DbSet<FactoryServerDefinition> FactoryServerDefinitions { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<ProcessesArchive> ProcessesArchives { get; set; }
        public DbSet<ProcessesLog> ProcessesLogs { get; set; }
        public DbSet<ProcessesLogArchive> ProcessesLogArchives { get; set; }
        public DbSet<QuicDefinition> QuicDefinitions { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<Models.System> Systems { get; set; }
        public DbSet<wfsb> wfsbs { get; set; }
        public DbSet<workflowfactoriesbackup> workflowfactoriesbackups { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowServer> WorkflowServers { get; set; }
        public DbSet<vuAllAsset> vuAllAssets { get; set; }
        public DbSet<vuAllProcess> vuAllProcesses { get; set; }
        public DbSet<vuAllProcessLog> vuAllProcessLogs { get; set; }
        public DbSet<vuProcess> vuProcesses { get; set; }
        public DbSet<vuWorkflow> vuWorkflows { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AeroDefinitionMap());
            modelBuilder.Configurations.Add(new AssetArchiveMap());
            modelBuilder.Configurations.Add(new AssetMap());
            modelBuilder.Configurations.Add(new DivaDefinitionMap());
            modelBuilder.Configurations.Add(new FactoryDefinitionMap());
            modelBuilder.Configurations.Add(new FactoryServerDefinitionMap());
            modelBuilder.Configurations.Add(new ProcessMap());
            modelBuilder.Configurations.Add(new ProcessesArchiveMap());
            modelBuilder.Configurations.Add(new ProcessesLogMap());
            modelBuilder.Configurations.Add(new ProcessesLogArchiveMap());
            modelBuilder.Configurations.Add(new QuicDefinitionMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new SystemMap());
            modelBuilder.Configurations.Add(new wfsbMap());
            modelBuilder.Configurations.Add(new workflowfactoriesbackupMap());
            modelBuilder.Configurations.Add(new WorkflowMap());
            modelBuilder.Configurations.Add(new WorkflowServerMap());
            modelBuilder.Configurations.Add(new vuAllAssetMap());
            modelBuilder.Configurations.Add(new vuAllProcessMap());
            modelBuilder.Configurations.Add(new vuAllProcessLogMap());
            modelBuilder.Configurations.Add(new vuProcessMap());
            modelBuilder.Configurations.Add(new vuWorkflowMap());
        }
    }
}
