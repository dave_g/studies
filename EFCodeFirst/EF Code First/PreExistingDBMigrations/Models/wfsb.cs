using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class wfsb
    {
        public int workflowid { get; set; }
        public int serverid { get; set; }
        public int factoryid { get; set; }
        public Nullable<int> id { get; set; }
    }
}
