using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class vuWorkflow
    {
        public int id { get; set; }
        public string workflow_name { get; set; }
        public string prefix { get; set; }
        public bool active { get; set; }
        public string factory_name { get; set; }
        public string factory { get; set; }
        public string diva_name { get; set; }
        public string category { get; set; }
        public string divagroup { get; set; }
        public string location { get; set; }
    }
}
