using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class ProcessesLogArchive
    {
        public int Id { get; set; }
        public int ProcessesId { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
    }
}
