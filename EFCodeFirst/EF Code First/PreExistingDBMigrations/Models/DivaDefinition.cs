using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class DivaDefinition
    {
        public DivaDefinition()
        {
            this.Processes = new List<Process>();
            this.Workflows = new List<Workflow>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string divaGroup { get; set; }
        public string location { get; set; }
        public virtual ICollection<Process> Processes { get; set; }
        public virtual ICollection<Workflow> Workflows { get; set; }
    }
}
