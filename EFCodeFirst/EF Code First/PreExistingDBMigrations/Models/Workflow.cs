using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class Workflow
    {
        public Workflow()
        {
            this.WorkflowServers = new List<WorkflowServer>();
            this.AeroDefinitions = new List<AeroDefinition>();
            this.DivaDefinitions = new List<DivaDefinition>();
            this.QuicDefinitions = new List<QuicDefinition>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string workflow_id { get; set; }
        public bool active { get; set; }
        public string prefix { get; set; }
        public Nullable<int> engine_id { get; set; }
        public virtual ICollection<WorkflowServer> WorkflowServers { get; set; }
        public virtual ICollection<AeroDefinition> AeroDefinitions { get; set; }
        public virtual ICollection<DivaDefinition> DivaDefinitions { get; set; }
        public virtual ICollection<QuicDefinition> QuicDefinitions { get; set; }
    }
}
