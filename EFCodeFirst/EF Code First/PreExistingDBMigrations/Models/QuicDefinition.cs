using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class QuicDefinition
    {
        public QuicDefinition()
        {
            this.Workflows = new List<Workflow>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string definition { get; set; }
        public virtual ICollection<Workflow> Workflows { get; set; }
    }
}
