using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class WorkflowServer
    {
        public int workflowid { get; set; }
        public int serverid { get; set; }
        public int factoryid { get; set; }
        public int id { get; set; }
        public virtual FactoryDefinition FactoryDefinition { get; set; }
        public virtual FactoryServerDefinition FactoryServerDefinition { get; set; }
        public virtual Workflow Workflow { get; set; }
    }
}
