using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class Asset
    {
        public Asset()
        {
            this.Processes = new List<Process>();
        }

        public int id { get; set; }
        public string houseId { get; set; }
        public string title { get; set; }
        public string isci { get; set; }
        public string comment { get; set; }
        public Nullable<int> clipSom { get; set; }
        public Nullable<int> clipEom { get; set; }
        public Nullable<int> clipDuration { get; set; }
        public Nullable<int> playSom { get; set; }
        public Nullable<int> playEom { get; set; }
        public Nullable<int> playDuration { get; set; }
        public string status { get; set; }
        public Nullable<DateTime> updatedAt { get; set; }
        public DateTime createdAt { get; set; }
        public string filename { get; set; }
        public string fileLocation { get; set; }
        public string type { get; set; }
        public Nullable<int> preFrames { get; set; }
        public Nullable<int> postFrames { get; set; }
        public Nullable<DateTime> airDate { get; set; }
        public Nullable<bool> dropFrame { get; set; }
        public virtual ICollection<Process> Processes { get; set; }
    }
}
