using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class vuWorkflowMap : EntityTypeConfiguration<vuWorkflow>
    {
        public vuWorkflowMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.workflow_name, t.active });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.workflow_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.prefix)
                .HasMaxLength(50);

            this.Property(t => t.factory_name)
                .HasMaxLength(255);

            this.Property(t => t.factory)
                .HasMaxLength(255);

            this.Property(t => t.diva_name)
                .HasMaxLength(255);

            this.Property(t => t.category)
                .HasMaxLength(50);

            this.Property(t => t.divagroup)
                .HasMaxLength(50);

            this.Property(t => t.location)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("vuWorkflows");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.workflow_name).HasColumnName("workflow_name");
            this.Property(t => t.prefix).HasColumnName("prefix");
            this.Property(t => t.active).HasColumnName("active");
            this.Property(t => t.factory_name).HasColumnName("factory_name");
            this.Property(t => t.factory).HasColumnName("factory");
            this.Property(t => t.diva_name).HasColumnName("diva_name");
            this.Property(t => t.category).HasColumnName("category");
            this.Property(t => t.divagroup).HasColumnName("divagroup");
            this.Property(t => t.location).HasColumnName("location");
        }
    }
}
