using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class AeroDefinitionMap : EntityTypeConfiguration<AeroDefinition>
    {
        public AeroDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.inboxPath)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.outboxPath)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("AeroDefinitions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.inboxPath).HasColumnName("inboxPath");
            this.Property(t => t.outboxPath).HasColumnName("outboxPath");

            // Relationships
            this.HasMany(t => t.Workflows)
                .WithMany(t => t.AeroDefinitions)
                .Map(m =>
                    {
                        m.ToTable("WorkflowAeros");
                        m.MapLeftKey("areoid");
                        m.MapRightKey("workflowid");
                    });


        }
    }
}
