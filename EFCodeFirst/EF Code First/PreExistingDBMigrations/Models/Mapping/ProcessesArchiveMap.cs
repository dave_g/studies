using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class ProcessesArchiveMap : EntityTypeConfiguration<ProcessesArchive>
    {
        public ProcessesArchiveMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.workflowid)
                .HasMaxLength(100);

            this.Property(t => t.status)
                .HasMaxLength(32);

            this.Property(t => t.divaCategory)
                .HasMaxLength(50);

            this.Property(t => t.divaFileLocation)
                .HasMaxLength(255);

            this.Property(t => t.filename)
                .HasMaxLength(255);

            this.Property(t => t.fileLocation)
                .HasMaxLength(255);

            this.Property(t => t.flipServer)
                .HasMaxLength(255);

            this.Property(t => t.flipServerFactory)
                .HasMaxLength(255);

            this.Property(t => t.flipServerAccount)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ProcessesArchive");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.assetId).HasColumnName("assetId");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.updatedAt).HasColumnName("updatedAt");
            this.Property(t => t.divaCategory).HasColumnName("divaCategory");
            this.Property(t => t.divaFileLocation).HasColumnName("divaFileLocation");
            this.Property(t => t.filename).HasColumnName("filename");
            this.Property(t => t.fileLocation).HasColumnName("fileLocation");
            this.Property(t => t.flipServer).HasColumnName("flipServer");
            this.Property(t => t.flipServerFactory).HasColumnName("flipServerFactory");
            this.Property(t => t.flipServerAccount).HasColumnName("flipServerAccount");
            this.Property(t => t.wait).HasColumnName("wait");
        }
    }
}
