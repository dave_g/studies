using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class DivaDefinitionMap : EntityTypeConfiguration<DivaDefinition>
    {
        public DivaDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.category)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.divaGroup)
                .HasMaxLength(50);

            this.Property(t => t.location)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("DivaDefinitions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.category).HasColumnName("category");
            this.Property(t => t.divaGroup).HasColumnName("divaGroup");
            this.Property(t => t.location).HasColumnName("location");

            // Relationships
            this.HasMany(t => t.Workflows)
                .WithMany(t => t.DivaDefinitions)
                .Map(m =>
                    {
                        m.ToTable("WorkflowDivas");
                        m.MapLeftKey("divaid");
                        m.MapRightKey("workflowid");
                    });


        }
    }
}
