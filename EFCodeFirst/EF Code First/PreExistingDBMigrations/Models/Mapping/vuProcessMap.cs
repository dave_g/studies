using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class vuProcessMap : EntityTypeConfiguration<vuProcess>
    {
        public vuProcessMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.workflowname, t.houseId, t.updatedAt, t.createdAt, t.serverName, t.applicationKey, t.name, t.factory, t.account, t.inboxPath, t.inboxAlias, t.outboxPath, t.password, t.divaname, t.category, t.wait, t.assetid });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.workflowid)
                .HasMaxLength(100);

            this.Property(t => t.status)
                .HasMaxLength(32);

            this.Property(t => t.workflowname)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.houseId)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.title)
                .HasMaxLength(64);

            this.Property(t => t.isci)
                .HasMaxLength(64);

            this.Property(t => t.comment)
                .HasMaxLength(64);

            this.Property(t => t.assetFileName)
                .HasMaxLength(255);

            this.Property(t => t.assetLocation)
                .HasMaxLength(255);

            this.Property(t => t.serverName)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.applicationKey)
                .IsRequired()
                .HasMaxLength(5000);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.factory)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.account)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.inboxPath)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.inboxAlias)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.outboxPath)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.password)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.divaname)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.category)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.divaGroup)
                .HasMaxLength(50);

            this.Property(t => t.divaLocation)
                .HasMaxLength(255);

            this.Property(t => t.assetid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.processFileName)
                .HasMaxLength(255);

            this.Property(t => t.processFileLocation)
                .HasMaxLength(255);

            this.Property(t => t.type)
                .HasMaxLength(64);

            this.Property(t => t.AeroName)
                .HasMaxLength(255);

            this.Property(t => t.AeroInBox)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("vuProcesses");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.workflowname).HasColumnName("workflowname");
            this.Property(t => t.houseId).HasColumnName("houseId");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.clipSom).HasColumnName("clipSom");
            this.Property(t => t.clipEom).HasColumnName("clipEom");
            this.Property(t => t.clipDuration).HasColumnName("clipDuration");
            this.Property(t => t.playSom).HasColumnName("playSom");
            this.Property(t => t.playEom).HasColumnName("playEom");
            this.Property(t => t.playDuration).HasColumnName("playDuration");
            this.Property(t => t.isci).HasColumnName("isci");
            this.Property(t => t.comment).HasColumnName("comment");
            this.Property(t => t.updatedAt).HasColumnName("updatedAt");
            this.Property(t => t.createdAt).HasColumnName("createdAt");
            this.Property(t => t.assetFileName).HasColumnName("assetFileName");
            this.Property(t => t.assetLocation).HasColumnName("assetLocation");
            this.Property(t => t.serverName).HasColumnName("serverName");
            this.Property(t => t.applicationKey).HasColumnName("applicationKey");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.factory).HasColumnName("factory");
            this.Property(t => t.account).HasColumnName("account");
            this.Property(t => t.inboxPath).HasColumnName("inboxPath");
            this.Property(t => t.inboxAlias).HasColumnName("inboxAlias");
            this.Property(t => t.outboxPath).HasColumnName("outboxPath");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.divaname).HasColumnName("divaname");
            this.Property(t => t.category).HasColumnName("category");
            this.Property(t => t.divaGroup).HasColumnName("divaGroup");
            this.Property(t => t.divaLocation).HasColumnName("divaLocation");
            this.Property(t => t.wait).HasColumnName("wait");
            this.Property(t => t.assetid).HasColumnName("assetid");
            this.Property(t => t.processFileName).HasColumnName("processFileName");
            this.Property(t => t.processFileLocation).HasColumnName("processFileLocation");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.aeroId).HasColumnName("aeroId");
            this.Property(t => t.AeroName).HasColumnName("AeroName");
            this.Property(t => t.AeroInBox).HasColumnName("AeroInBox");
        }
    }
}
