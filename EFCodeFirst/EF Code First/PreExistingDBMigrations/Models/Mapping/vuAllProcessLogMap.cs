using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class vuAllProcessLogMap : EntityTypeConfiguration<vuAllProcessLog>
    {
        public vuAllProcessLogMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id, t.ProcessesId, t.UpdatedAt, t.Status, t.Detail });

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ProcessesId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Detail)
                .IsRequired()
                .HasMaxLength(1024);

            // Table & Column Mappings
            this.ToTable("vuAllProcessLogs");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProcessesId).HasColumnName("ProcessesId");
            this.Property(t => t.UpdatedAt).HasColumnName("UpdatedAt");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Detail).HasColumnName("Detail");
        }
    }
}
