using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class vuAllAssetMap : EntityTypeConfiguration<vuAllAsset>
    {
        public vuAllAssetMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.houseId, t.status, t.createdAt });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.houseId)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.title)
                .HasMaxLength(64);

            this.Property(t => t.isci)
                .HasMaxLength(64);

            this.Property(t => t.comment)
                .HasMaxLength(64);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(32);

            // Table & Column Mappings
            this.ToTable("vuAllAssets");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.houseId).HasColumnName("houseId");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.isci).HasColumnName("isci");
            this.Property(t => t.comment).HasColumnName("comment");
            this.Property(t => t.clipSom).HasColumnName("clipSom");
            this.Property(t => t.clipEom).HasColumnName("clipEom");
            this.Property(t => t.clipDuration).HasColumnName("clipDuration");
            this.Property(t => t.playSom).HasColumnName("playSom");
            this.Property(t => t.playEom).HasColumnName("playEom");
            this.Property(t => t.playDuration).HasColumnName("playDuration");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.updatedAt).HasColumnName("updatedAt");
            this.Property(t => t.createdAt).HasColumnName("createdAt");
            this.Property(t => t.filename).HasColumnName("filename");
            this.Property(t => t.fileLocation).HasColumnName("fileLocation");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.preFrames).HasColumnName("preFrames");
            this.Property(t => t.postFrames).HasColumnName("postFrames");
        }
    }
}
