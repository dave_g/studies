using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class workflowfactoriesbackupMap : EntityTypeConfiguration<workflowfactoriesbackup>
    {
        public workflowfactoriesbackupMap()
        {
            // Primary Key
            this.HasKey(t => new { t.workflowid, t.factoryid });

            // Properties
            this.Property(t => t.workflowid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.factoryid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("workflowfactoriesbackup");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.factoryid).HasColumnName("factoryid");
        }
    }
}
