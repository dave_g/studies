using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace PreExistingDBMigrations.Models.Mapping
{
    public class SystemMap : EntityTypeConfiguration<System>
    {
        public SystemMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("System");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.wait).HasColumnName("wait");
        }
    }
}
