using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class Process
    {
        public Process()
        {
            this.ProcessesLogs = new List<ProcessesLog>();
        }

        public int id { get; set; }
        public string workflowid { get; set; }
        public Nullable<int> assetId { get; set; }
        public string status { get; set; }
        public DateTime updatedAt { get; set; }
        public int divaid { get; set; }
        public string filename { get; set; }
        public string fileLocation { get; set; }
        public int factoryid { get; set; }
        public bool wait { get; set; }
        public Nullable<int> factoryServerId { get; set; }
        public Nullable<int> aeroId { get; set; }
        public virtual AeroDefinition AeroDefinition { get; set; }
        public virtual Asset Asset { get; set; }
        public virtual DivaDefinition DivaDefinition { get; set; }
        public virtual FactoryDefinition FactoryDefinition { get; set; }
        public virtual FactoryServerDefinition FactoryServerDefinition { get; set; }
        public virtual ICollection<ProcessesLog> ProcessesLogs { get; set; }
    }
}
