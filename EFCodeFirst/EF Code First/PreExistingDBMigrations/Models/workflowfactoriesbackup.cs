using System;
using System.Collections.Generic;

namespace PreExistingDBMigrations.Models
{
    public partial class workflowfactoriesbackup
    {
        public int workflowid { get; set; }
        public int factoryid { get; set; }
    }
}
