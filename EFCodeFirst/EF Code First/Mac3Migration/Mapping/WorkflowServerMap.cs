using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class WorkflowServerMap : EntityTypeConfiguration<WorkflowServer>
    {
        public WorkflowServerMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("WorkflowServers");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.serverid).HasColumnName("serverid");
            this.Property(t => t.factoryid).HasColumnName("factoryid");
            this.Property(t => t.id).HasColumnName("id");

            // Relationships
            this.HasRequired(t => t.FactoryDefinition)
                .WithMany(t => t.WorkflowServers)
                .HasForeignKey(d => d.factoryid);
            this.HasRequired(t => t.FactoryServerDefinition)
                .WithMany(t => t.WorkflowServers)
                .HasForeignKey(d => d.serverid);
            this.HasRequired(t => t.Workflow)
                .WithMany(t => t.WorkflowServers)
                .HasForeignKey(d => d.workflowid);

        }
    }
}
