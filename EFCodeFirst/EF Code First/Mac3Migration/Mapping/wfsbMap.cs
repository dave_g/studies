using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class wfsbMap : EntityTypeConfiguration<wfsb>
    {
        public wfsbMap()
        {
            // Primary Key
            this.HasKey(t => new { t.workflowid, t.serverid, t.factoryid });

            // Properties
            this.Property(t => t.workflowid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.serverid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.factoryid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("wfsb");
            this.Property(t => t.workflowid).HasColumnName("workflowid");
            this.Property(t => t.serverid).HasColumnName("serverid");
            this.Property(t => t.factoryid).HasColumnName("factoryid");
            this.Property(t => t.id).HasColumnName("id");
        }
    }
}
