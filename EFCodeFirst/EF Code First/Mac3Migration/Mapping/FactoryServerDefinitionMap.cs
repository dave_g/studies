using System.Data.Entity.ModelConfiguration;

namespace Mac3Migration.Mapping
{
    public class FactoryServerDefinitionMap : EntityTypeConfiguration<FactoryServerDefinition>
    {
        public FactoryServerDefinitionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.serverName)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.applicationKey)
                .IsRequired()
                .HasMaxLength(5000);

            // Table & Column Mappings
            this.ToTable("FactoryServerDefinitions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.serverName).HasColumnName("serverName");
            this.Property(t => t.applicationKey).HasColumnName("applicationKey");
        }
    }
}
