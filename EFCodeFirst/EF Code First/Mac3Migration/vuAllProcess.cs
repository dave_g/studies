using System;

namespace Mac3Migration
{
    public partial class vuAllProcess
    {
        public int id { get; set; }
        public string workflowid { get; set; }
        public string workflowName { get; set; }
        public Nullable<int> assetId { get; set; }
        public string status { get; set; }
        public DateTime updatedAt { get; set; }
        public string divaCategory { get; set; }
        public string divaFileLocation { get; set; }
        public string filename { get; set; }
        public string fileLocation { get; set; }
        public string flipServer { get; set; }
        public string flipServerFactory { get; set; }
        public string flipServerAccount { get; set; }
        public bool wait { get; set; }
    }
}
