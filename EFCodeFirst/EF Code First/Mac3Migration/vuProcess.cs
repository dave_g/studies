using System;

namespace Mac3Migration
{
    public partial class vuProcess
    {
        public int id { get; set; }
        public string workflowid { get; set; }
        public string status { get; set; }
        public string workflowname { get; set; }
        public string houseId { get; set; }
        public string title { get; set; }
        public Nullable<int> clipSom { get; set; }
        public Nullable<int> clipEom { get; set; }
        public Nullable<int> clipDuration { get; set; }
        public Nullable<int> playSom { get; set; }
        public Nullable<int> playEom { get; set; }
        public Nullable<int> playDuration { get; set; }
        public string isci { get; set; }
        public string comment { get; set; }
        public DateTime updatedAt { get; set; }
        public DateTime createdAt { get; set; }
        public string assetFileName { get; set; }
        public string assetLocation { get; set; }
        public string serverName { get; set; }
        public string applicationKey { get; set; }
        public string name { get; set; }
        public string factory { get; set; }
        public string account { get; set; }
        public string inboxPath { get; set; }
        public string inboxAlias { get; set; }
        public string outboxPath { get; set; }
        public string password { get; set; }
        public string divaname { get; set; }
        public string category { get; set; }
        public string divaGroup { get; set; }
        public string divaLocation { get; set; }
        public bool wait { get; set; }
        public int assetid { get; set; }
        public string processFileName { get; set; }
        public string processFileLocation { get; set; }
        public string type { get; set; }
        public Nullable<int> aeroId { get; set; }
        public string AeroName { get; set; }
        public string AeroInBox { get; set; }
    }
}
