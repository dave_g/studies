using System;

namespace Mac3Migration
{
    public partial class ProcessesLog
    {
        public int Id { get; set; }
        public int ProcessesId { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
        public virtual Process Process { get; set; }
    }
}
