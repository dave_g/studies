using System;

namespace Mac3Migration
{
    public partial class wfsb
    {
        public int workflowid { get; set; }
        public int serverid { get; set; }
        public int factoryid { get; set; }
        public Nullable<int> id { get; set; }
    }
}
