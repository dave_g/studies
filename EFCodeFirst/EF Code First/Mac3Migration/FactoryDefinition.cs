using System.Collections.Generic;

namespace Mac3Migration
{
    public partial class FactoryDefinition
    {
        public FactoryDefinition()
        {
            this.Processes = new List<Process>();
            this.WorkflowServers = new List<WorkflowServer>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string factory { get; set; }
        public string inboxAlias { get; set; }
        public string account { get; set; }
        public string inboxPath { get; set; }
        public string password { get; set; }
        public string outboxPath { get; set; }
        public virtual ICollection<Process> Processes { get; set; }
        public virtual ICollection<WorkflowServer> WorkflowServers { get; set; }
    }
}
