//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace PreExistingDB
{
    public partial class ProcessesLog
    {
        public int Id { get; set; }
        public int ProcessesId { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
    
        public virtual Process Process { get; set; }
    }
    
}
