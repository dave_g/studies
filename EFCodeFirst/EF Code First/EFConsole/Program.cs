﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using EFDAL.DbBuildFromCode;
using EFDAL;
using Model;
using Model.DbBuildFromCode;

namespace EFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var builder = new SqlConnectionStringBuilder();
                builder.DataSource = @".\SQLEXPRESS10_50";
                builder.IntegratedSecurity = true;
                builder.InitialCatalog = "DbBuildFomCode";
                

                var db = new EFDbBuilderContext(builder.ConnectionString);
                //var db = new EFDbBuilderContext();
                //var w = db.Workflows.Find(1);
                //var wx = db.Workflows.Where(wf => wf.id > 0);
                var workflow = new Workflow() { Id = 1, Name = "foo" };
                db.Workflows.Add(workflow);
                //var aero = new AeroDefinition() {id = 1, name = ""};
                //var diva = new DivaDefinition();
                //db.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw;
            }
                
        }

        private static void DoPluralSiteBlogExamples()
        {
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = @".\SQLEXPRESS10_50";
            builder.IntegratedSecurity = true;
            builder.InitialCatalog = "EFDAL.Context";

            var db = new Context(builder.ConnectionString);
            var blogs = db.Blogs.Where(b => b.Id > 0);
            var posts = db.Posts.Where(p => p.Id > 0);

            foreach (var blog in blogs)
            {
                Console.WriteLine(String.Format("{0} {1} {2} {3} {4} ", blog.Id, blog.BloggerName, blog.Title,
                                                blog.Properties.Id, blog.Properties.Property1));
            }
        }

        //private static void CreatePost()
        //{
        //    var db = new Context();
        //    var blog = db.Blogs.Find(1);
        //    var post = new Post() {Blog = blog, Content = "Some Content.", Title = "Fancy Title."};
        //    db.Posts.Add(post);
        //    db.SaveChanges();
        //}

        //private static void CreateBlog()
        //{
        //    var blog = new Blog() {BloggerName = "Dave", Title = "Natural"};
        //    var db = new Context();
        //    db.Blogs.Add(blog);
        //    db.SaveChanges();
        //}
    }
}
