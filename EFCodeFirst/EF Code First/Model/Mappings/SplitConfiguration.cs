﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Data;

namespace Model
{
    public class SplitConfiguration : EntityTypeConfiguration<Split>
    {
        public SplitConfiguration()
        {

            this.Map(m =>
                {
                    m.Properties(p => new { p.Id, p.Left });
                    m.ToTable("LeftSplit");
                })
                .Map(m =>
                {
                    m.Properties(p => new { p.Id, p.Right });
                    m.ToTable("RightSplit");
                });
        }

    }
}
