﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Split
    {
        public int Id { get; set; }
        public string Left { get; set; }
        public string Right { get; set; }
    }
}
