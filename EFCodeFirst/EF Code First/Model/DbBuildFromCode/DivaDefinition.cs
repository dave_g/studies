﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.DbBuildFromCode
{
    public class DivaDefinition
    {
        public DivaDefinition()
        {
            workflows = new List<Workflow>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string inboxPath { get; set; }
        public string outboxPath { get; set; }
        public virtual List<Workflow> workflows { get; set; }
    }
}
