﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Model.DbBuildFromCode
{
    public class Processing
    {
        public Processing()
        {
            Workflows = new List<Workflow>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string InboxPath { get; set; }
        public string OutboxPath { get; set; }
        public virtual List<Workflow> Workflows { get; set; }
    }
}
