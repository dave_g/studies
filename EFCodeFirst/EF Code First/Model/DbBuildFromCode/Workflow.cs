﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.DbBuildFromCode
{
    public class Workflow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Prefix { get; set; }
        public int Engine_Id { get; set; }

        public Workflow()
        {
            DivaDefinitions = new List<DivaDefinition>();
            Processors = new List<Processing>();            
        }

        //public List<AeroDefinition> AeroDefinitions { get; set; }        
        public virtual List<DivaDefinition> DivaDefinitions { get; set; }
        public virtual List<Processing> Processors { get; set; } 
        
       
    }
}
