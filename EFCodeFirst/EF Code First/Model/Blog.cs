﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Model
{
    public class Blog
    {
        public Blog()
        {
            Posts = new List<Post>();
        }

        public int Id { get; set; }        
        public string Title { get; set; }        
        public string BloggerName { get; set; }
        public string BlogCode
        {
            get { return String.Format("{0} {1}", Title.Substring(0, 1), BloggerName.Substring(0, 1)); }
        }

        public List<Post> Posts { get; set; }
        public BProperties Properties { get; set; }
    }

    public class BProperties
    {
        public int Id { get; set; }
        public string Property1 { get; set; }
        public string Property2 { get; set; }
        public string Property3 { get; set; }
    }
}
