﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class LightInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual HeavyInfo BigInfo { get; set; }
    }

    public class HeavyInfo
    {
        public int Id { get; set; }        
        public byte[] SomeBigField { get; set; }
    }
}
