﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel.Activities;
using System.Text;
using System.Windows.Forms;
using WorkflowClient.ServiceReferenceMacWorkflowInvoker;
namespace WorkflowClient
{
    public partial class Form1 : Form
    {
        MacWorkflowInvokerClient _macWorkflowInvoker = new MacWorkflowInvokerClient();
        private string _currentWorkflowId = "86D44D3D-A092-4FC5-8FED-231F67F0CDCE";
        private List<string>  _workflowIds = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {            
            _workflowIds.Add(_macWorkflowInvoker.StartWorkflow(new StartWorkflow()));
        }

        private void buttonResumeBookmark_Click(object sender, EventArgs e)
        {            
            foreach (string workflowId in _workflowIds)
            {
                var ret = _macWorkflowInvoker.ResumeBookmark(new ResumeBookmark() { workflowid = workflowId, bookmark = "Step 1" });
                Console.WriteLine(ret);
            }            
        }

        private void buttonResumeEnd_Click(object sender, EventArgs e)
        {
            foreach (string workflowId in _workflowIds)
            {
                _macWorkflowInvoker.ResumeBookmark(new ResumeBookmark() { workflowid = workflowId, bookmark = "End" });    
            }
            
        }

        private void buttonPauseWorkflow_Click(object sender, EventArgs e)
        {

            _macWorkflowInvoker.ResumeBookmark(new ResumeBookmark() { workflowid = _currentWorkflowId, bookmark = "End" });    

            //using (WorkflowControlClient workflowControlClient = new WorkflowControlClient("ClientControlEndpoint"))
            //{
            //    Guid id = new Guid(_currentWorkflowId);
            //    workflowControlClient.Terminate(id);                
            //    _macWorkflowInvoker.ResumeBookmark(new ResumeBookmark() { workflowid = _currentWorkflowId, bookmark = "End" });    
            //}
        }
    }
}
