﻿namespace WorkflowClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonResumeBookmark = new System.Windows.Forms.Button();
            this.buttonResumeEnd = new System.Windows.Forms.Button();
            this.buttonPauseWorkflow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(12, 12);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(115, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonResumeBookmark
            // 
            this.buttonResumeBookmark.Location = new System.Drawing.Point(12, 41);
            this.buttonResumeBookmark.Name = "buttonResumeBookmark";
            this.buttonResumeBookmark.Size = new System.Drawing.Size(115, 23);
            this.buttonResumeBookmark.TabIndex = 1;
            this.buttonResumeBookmark.Text = "Resume Bookmark";
            this.buttonResumeBookmark.UseVisualStyleBackColor = true;
            this.buttonResumeBookmark.Click += new System.EventHandler(this.buttonResumeBookmark_Click);
            // 
            // buttonResumeEnd
            // 
            this.buttonResumeEnd.Location = new System.Drawing.Point(13, 71);
            this.buttonResumeEnd.Name = "buttonResumeEnd";
            this.buttonResumeEnd.Size = new System.Drawing.Size(114, 23);
            this.buttonResumeEnd.TabIndex = 2;
            this.buttonResumeEnd.Text = "Resume End";
            this.buttonResumeEnd.UseVisualStyleBackColor = true;
            this.buttonResumeEnd.Click += new System.EventHandler(this.buttonResumeEnd_Click);
            // 
            // buttonPauseWorkflow
            // 
            this.buttonPauseWorkflow.Location = new System.Drawing.Point(13, 101);
            this.buttonPauseWorkflow.Name = "buttonPauseWorkflow";
            this.buttonPauseWorkflow.Size = new System.Drawing.Size(114, 23);
            this.buttonPauseWorkflow.TabIndex = 3;
            this.buttonPauseWorkflow.Text = "Pause Workflow";
            this.buttonPauseWorkflow.UseVisualStyleBackColor = true;
            this.buttonPauseWorkflow.Click += new System.EventHandler(this.buttonPauseWorkflow_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.buttonPauseWorkflow);
            this.Controls.Add(this.buttonResumeEnd);
            this.Controls.Add(this.buttonResumeBookmark);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "MacWorkflowInvoker Tester";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonResumeBookmark;
        private System.Windows.Forms.Button buttonResumeEnd;
        private System.Windows.Forms.Button buttonPauseWorkflow;
    }
}

