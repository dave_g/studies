﻿using System;
using System.Activities.Hosting;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Activities;
using System.Threading;
using System.Xaml;
using WorkflowLibrary.MAC;
using WorkflowLibrary.ProWF.Extensions;

namespace MacWorkflowInvoker
{

    public sealed class RunWorkflow : NativeActivity
    {
        //public Activity Body { get; set; }

        //protected override void CacheMetadata(NativeActivityMetadata metadata)
        //{
        //    metadata.AddChild(Body);
        //}

        protected override void Execute(NativeActivityContext context)
        {

            try
            {
                string file = "MacWorkflow.xaml";
                var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(WorkflowLibrary.MAC.CustomControlFlow).Assembly };
                var reader = new XamlXmlReader(file, settings);
                //Activity workflowload = ActivityXamlServices.Load(reader);

                //AutoResetEvent waitEvent = new AutoResetEvent(false);

                //var extension = new HostEventNotifier();
                //var wfApp = InvokeWorkflowApplication(workflowload, null, waitEvent, new object[] { extension });

                //extension.Notification += delegate(object sender, HostNotifyEventArgs args)
                //{
                //    Console.WriteLine("Start, First Step, End");
                //    //Console.WriteLine(args.Message + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                //    //Console.WriteLine(args.BookmarkName + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                //    String expression = Console.ReadLine();

                //    if (expression == "quit")
                //    {
                //        wfApp.Cancel();
                //        Console.WriteLine("App Canceled" + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                //    }

                //    if (isValidBookMark(wfApp, args.BookmarkName))
                //    {
                //        Console.WriteLine("bookmark valid");
                //        BookmarkResumptionResult result = wfApp.ResumeBookmark(args.BookmarkName, expression);
                //        Console.WriteLine(result);
                //    }
                //    else
                //    {
                //        Console.WriteLine("bookmark NOT valid");
                //    }
                //};

                //waitEvent.WaitOne();
            }
            catch (Exception)
            {
                
                throw;
            }
            

            
            //context.ScheduleActivity(Body);
        }

        private static bool isValidBookMark(WorkflowApplication wfApp, string bookMarkName)
        {
            bool result = false;
            ReadOnlyCollection<BookmarkInfo> bookmarks;
            try
            {
                bookmarks = wfApp.GetBookmarks();
            }
            catch (Exception)
            {
                return result;
            }


            if (bookmarks == null) return result;
            result = bookmarks.Any(b => b.BookmarkName == bookMarkName);
            return result;
        }

        private static WorkflowApplication InvokeWorkflowApplication(Activity workflow, Dictionary<string, object> wfParams, AutoResetEvent waitEvent, IEnumerable<object> extensions)
        {

            WorkflowApplication wfapp;


            wfapp = wfParams != null ? new WorkflowApplication(workflow, wfParams) : new WorkflowApplication(workflow);


            wfapp.Completed = delegate(WorkflowApplicationCompletedEventArgs args)
            {
                switch (args.CompletionState)
                {
                    case ActivityInstanceState.Closed:
                        Console.WriteLine("Host: {0} Closed - Thread: {1} - {2}", wfapp.Id, Thread.CurrentThread.ManagedThreadId, args.Outputs.ContainsKey("Result") ? args.Outputs["Result"] : "No Result Supplied");
                        break;
                    case ActivityInstanceState.Canceled:
                        Console.WriteLine("Host: {0} Canceled - Thread: {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                        break;
                    case ActivityInstanceState.Executing:
                        Console.WriteLine("Host: {0} Executing - Thread: {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                        break;
                    case ActivityInstanceState.Faulted:
                        Console.WriteLine("Host: {0} Faulted - Thread: {1} - Error {2} : {3}", wfapp.Id,
                            Thread.CurrentThread.ManagedThreadId, args.TerminationException.GetType(), args.TerminationException.Message);
                        break;
                }
                waitEvent.Set();
            };

            wfapp.OnUnhandledException = delegate(WorkflowApplicationUnhandledExceptionEventArgs args)
            {
                Console.WriteLine("Host: {0} UnHandledException - Thread: {1} - Error {2} ", wfapp.Id,
                   Thread.CurrentThread.ManagedThreadId, args.UnhandledException.Message);
                waitEvent.Set();
                return UnhandledExceptionAction.Cancel;
            };

            wfapp.Aborted = delegate(WorkflowApplicationAbortedEventArgs args)
            {

                Console.WriteLine("Host: {0} Abortion - Thread: {1} - Reason {2} : {3} ", wfapp.Id,
                                Thread.CurrentThread.ManagedThreadId, args.Reason.GetType(), args.Reason);
                waitEvent.Set();
            };

            wfapp.Idle =
                args =>
                Console.WriteLine("Host: {0} Idle - Thread: {1}", wfapp.Id,
                                  Thread.CurrentThread.ManagedThreadId);

            wfapp.PersistableIdle = args =>
            {
                Console.WriteLine("Host: {0} PersisableIdle - Thread: {1}", wfapp.Id,
                                  Thread.CurrentThread.ManagedThreadId);
                return PersistableIdleAction.Unload;
            };
            wfapp.Unloaded =
                args =>
                Console.WriteLine("Host: {0} Unloaded: {1}", wfapp.Id,
                                  Thread.CurrentThread.ManagedThreadId);


            try
            {
                Console.WriteLine("Host: about to run {0} - Thread {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                if (extensions != null && extensions.Any())
                {
                    foreach (var extension in extensions)
                    {
                        wfapp.Extensions.Add(extension);
                    }
                }
                wfapp.Run();
                //if (cancel)
                //{
                //    Thread.Sleep(1000);
                //    //wfapp.Cancel();
                //    //wfapp.Abort("Reason : Too young!");
                //    wfapp.Terminate("I'll be back.");
                //}
                //waitEvent.WaitOne();
            }
            catch (Exception e)
            {

                Console.WriteLine("Host: {0} Exception {1} : {2}", wfapp.Id, e.GetType(), e.Message);
            }

            return wfapp;
        }
    }
}
