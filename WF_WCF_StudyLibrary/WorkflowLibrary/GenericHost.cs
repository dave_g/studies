﻿using System;
using System.Activities;
using System.Activities.DurableInstancing;
using System.Activities.XamlIntegration;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.DurableInstancing;
using System.Threading;
using System.Xaml;
using WorkflowLibrary.MAC;
using WorkflowLibrary.ProWF;
using WorkflowLibrary.ProWF.Extensions;


namespace WorkflowLibrary
{

    public class WorkflowApplicationValues
    {
        public Activity Activity { get; set; }
        public WorkflowApplication WorkflowApplication { get; set; }
        public IDictionary<string, object> inputs { get; set;}


        public WorkflowApplicationValues(Activity activity, WorkflowApplication workflowApplication, IDictionary<string, object> inputs)
        {
            Activity = activity;
            WorkflowApplication = workflowApplication;
            this.inputs = inputs;
        }

        public WorkflowApplicationValues(Activity activity, WorkflowApplication workflowApplication)
        {
            Activity = activity;
            WorkflowApplication = workflowApplication;
        }

        public WorkflowApplicationValues()
        {
            
        }
    }

    public static class GenericHost
    {
        private static ConcurrentDictionary<Guid, WorkflowApplicationValues> runningWorkflows;
        private static ConcurrentDictionary<Guid, WorkflowApplicationValues> unloadedWorkflows = new ConcurrentDictionary<Guid, WorkflowApplicationValues>();

        public static Action HostException;

        private static bool flag = false;
        private static bool _hostCanceled = false;

        #region Private Helper Methods

        public static void ReloadExpiredApplication()
        {
            lock (unloadedWorkflows)
            {
                var e = unloadedWorkflows.Values;

                foreach (WorkflowApplicationValues workflowApplicationValues in e)
                {

                    var application = CreateWorkflowApplication(workflowApplicationValues.Activity, null);

                    LoadInstance(workflowApplicationValues.WorkflowApplication.Id, application);

                    WorkflowApplicationValues wfav = new WorkflowApplicationValues(workflowApplicationValues.Activity, application);

                    runningWorkflows.TryAdd(application.Id, wfav);

                    WorkflowApplicationValues wfappv;

                    unloadedWorkflows.TryRemove(workflowApplicationValues.WorkflowApplication.Id, out wfappv);
                }   
            }            


            //foreach (KeyValuePair<Guid, WorkflowApplicationValues> valuePair in unloadedWorkflows)
            //{
            //    WorkflowApplicationValues applicationValues = valuePair.Value;

            //    var inputs = applicationValues.inputs;

            //    var application = CreateWorkflowApplication(applicationValues.Activity, inputs);

            //    LoadInstance(valuePair.Key, application);

            //    var workflowApplicationValues = new WorkflowApplicationValues(applicationValues.Activity, application);

            //    runningWorkflows.TryAdd(valuePair.Key, workflowApplicationValues);
            //}

        }

        private static Activity CreateActivityFrom(string xaml)
        {
            if (File.Exists(xaml))
            {
                Console.WriteLine(true);
                var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(LoadMacWorkflow).Assembly };
                var reader = new XamlXmlReader(xaml, settings);
                var workflowload = ActivityXamlServices.Load(reader);
                var activity = workflowload as Activity;
                return activity;
            }
            return null;           
        }

        private static InstanceStore CreateInstanceStore()
        {            
            //todo: load datasource from configuration
            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = @".\SQLEXPRESS10_50",
                InitialCatalog = "WorkflowPersistence",
                IntegratedSecurity = true
            };

            var store = new SqlWorkflowInstanceStore(connectionStringBuilder.ConnectionString)
            {
                InstanceLockedExceptionAction = InstanceLockedExceptionAction.BasicRetry,
                InstanceCompletionAction = InstanceCompletionAction.DeleteNothing,
                HostLockRenewalPeriod = TimeSpan.FromSeconds(20),
                RunnableInstancesDetectionPeriod = TimeSpan.FromSeconds(3)                
            };

            var handle = store.CreateInstanceHandle();

            var view = store.Execute(handle, new CreateWorkflowOwnerCommand(),
                                     TimeSpan.FromSeconds(60));

            store.DefaultInstanceOwner = view.InstanceOwner;

            handle.Free();

            return store;
        }

        private static WorkflowApplication CreateWorkflowApplication(Activity activity, IDictionary<string, object> inputs)
        {
            if (runningWorkflows == null)
            {
                runningWorkflows = new ConcurrentDictionary<Guid, WorkflowApplicationValues>();
            }

            WorkflowApplication workflowApp = inputs == null ? new WorkflowApplication(activity) : new WorkflowApplication(activity, inputs);

            HostEventNotifierSingleThread extension = new HostEventNotifierSingleThread();

            extension.Notification += delegate(object sender, HostNotifyEventArgs args)
            {
                Console.WriteLine(args.Message + " thread {0}", Thread.CurrentThread.ManagedThreadId);                
                args.Message = _hostCanceled.ToString();
            };
            workflowApp.Extensions.Add(extension);
            workflowApp.InstanceStore = CreateInstanceStore();
            workflowApp.PersistableIdle = OnIdleAndPersistable;
            workflowApp.Completed = OnWorkflowCompleted;
            workflowApp.Aborted = OnWorkflowAborted;
            workflowApp.Unloaded = OnWorkflowUnloaded;
            workflowApp.OnUnhandledException = OnWorkflowException;            
            
            return workflowApp;
        }

        private static bool ContainsInstance(Guid instanceId)
        {
            return runningWorkflows != null && runningWorkflows.ContainsKey(instanceId);
        }

        private static void RemoveInstance(Guid instanceId)
        {
            if (!ContainsInstance(instanceId)) return;
            WorkflowApplicationValues workflowApplicationValues;            
            runningWorkflows.TryRemove(instanceId, out workflowApplicationValues);
            
        }

        private static BookmarkResumptionResult ResumeBookmark(string bookmarkName, object bookmarkInput,
                                                              WorkflowApplication workflow)
        {
            BookmarkResumptionResult result;

            try
            {
                result = workflow.ResumeBookmark(bookmarkName, bookmarkInput, TimeSpan.FromSeconds(60));
            }
            catch (Exception ex)
            {
                //todo:
                HostException.Invoke();
                throw;
            }


            return result;
        }

        private static void RunWorkflowApplication(WorkflowApplication workflowApp)
        {
           
            try
            {            
                workflowApp.Run();
            }
            catch (Exception ex)
            {
                //todo:
                throw;
            }
        }

        private static void LoadInstance(Guid instanceId, WorkflowApplication workflowApp)
        {
            try
            {
                workflowApp.Load(instanceId, TimeSpan.FromSeconds(60));
            }
            catch (InstanceLockedException ex)
            {
                Console.WriteLine("Instance Locked.");
            }
            catch (Exception ex)
            {
                //todo:
                Console.WriteLine(ex.Message);
            }
        }

        private static void AddToUnloadedCollection(Guid instanceId, WorkflowApplicationValues workflowApplicationValues)
        {
            unloadedWorkflows.TryAdd(instanceId, workflowApplicationValues);
        }

        #endregion

        public static void InvokeInstance(object input, string xaml)
        {
            var inputs = new Dictionary<string, object>();

            if (input != null)
            {
                inputs.Add(input.GetType().Name, input);
            }

            var wf = CreateActivityFrom(xaml);

            var activity = wf;

            WorkflowInvoker.Invoke(activity, inputs);
        }

        public static Guid StartPersistableInstance(IDictionary<string, object> inputs, string xaml)
        {

            var activity = CreateActivityFrom(xaml);

            var workflowApp = CreateWorkflowApplication(activity, inputs);            

            workflowApp.Persist();

            RunWorkflowApplication(workflowApp);

            var workflowApplicationValues = new WorkflowApplicationValues(activity, workflowApp,inputs);

            runningWorkflows.TryAdd(workflowApp.Id, workflowApplicationValues);

            return workflowApp.Id;
        }

        public static Guid StartPersistableInstance(IDictionary<string, object> inputs, Activity activity)
        {        

            var workflowApp = CreateWorkflowApplication(activity, inputs);

            workflowApp.Persist();

            RunWorkflowApplication(workflowApp);

            var workflowApplicationValues = new WorkflowApplicationValues(activity, workflowApp, inputs);

            runningWorkflows.TryAdd(workflowApp.Id, workflowApplicationValues);

            return workflowApp.Id;
        }



        public static bool LoadInstanceWithBookmark(string bookmarkName,
                                                    Guid instanceId,
                                                    object bookmarkInput,
                                                    string xaml)
        {

            BookmarkResumptionResult result;
            WorkflowApplication application;
            if (ContainsInstance(instanceId))
            {
                application = runningWorkflows[instanceId].WorkflowApplication;
            }
            else
            {
                var activity = CreateActivityFrom(xaml);

                application = CreateWorkflowApplication(activity, null);

                LoadInstance(instanceId, application);

                var workflowApplicationValues = new WorkflowApplicationValues(activity, application);

                runningWorkflows.TryAdd(instanceId, workflowApplicationValues);
            }

            result = ResumeBookmark(bookmarkName, bookmarkInput, application);
            Console.WriteLine(instanceId + " resumed @ " + bookmarkName);
            return result == BookmarkResumptionResult.Success;
        }

        public static bool LoadInstanceWithBookmark(string bookmarkName,
                                                    Guid instanceId,
                                                    object bookmarkInput,
                                                    Activity activity)
        {

            BookmarkResumptionResult result;
            WorkflowApplication application;
            if (ContainsInstance(instanceId))
            {
                application = runningWorkflows[instanceId].WorkflowApplication;
            }
            else
            {                
                application = CreateWorkflowApplication(activity, null);

                LoadInstance(instanceId, application);

                var workflowApplicationValues = new WorkflowApplicationValues(activity, application);

                runningWorkflows.TryAdd(instanceId, workflowApplicationValues);
            }

            result = ResumeBookmark(bookmarkName, bookmarkInput, application);
            Console.WriteLine(instanceId + " resumed @ " + bookmarkName);
            return result == BookmarkResumptionResult.Success;
        }


        public static void LoadInstance(Guid instanceId,
                                                    string xaml)
        {

            if (!ContainsInstance(instanceId))
            {

                var activity = CreateActivityFrom(xaml);

                var application = CreateWorkflowApplication(activity, null);

                LoadInstance(instanceId, application);

                var workflowApplicationValues = new WorkflowApplicationValues(activity, application);

                runningWorkflows.TryAdd(instanceId, workflowApplicationValues);

                Console.WriteLine(instanceId + " resumed @ ");

            }
        }

        public static void UnloadInstance(Guid instanceId)
        {
            if (!ContainsInstance(instanceId)) return;

            var workflow = runningWorkflows[instanceId].WorkflowApplication;
            workflow.Unload();
            
            var workflowApplicationValues = new WorkflowApplicationValues();
            runningWorkflows.TryRemove(instanceId, out workflowApplicationValues);
        }

        public static void CancelInstance(Guid instanceId)
        {
            if (!ContainsInstance(instanceId)) return;

            var workflow = runningWorkflows[instanceId].WorkflowApplication;
            workflow.Cancel();
            
            var workflowApplicationValues = new WorkflowApplicationValues();
            runningWorkflows.TryRemove(instanceId, out workflowApplicationValues);
        }

        #region Events

        private static void OnWorkflowCompleted(WorkflowApplicationCompletedEventArgs e)
        {
            if (runningWorkflows != null && runningWorkflows.ContainsKey(e.InstanceId))
            {
                WorkflowApplication workflowApp;
                var workflowApplicationValues = new WorkflowApplicationValues();
                runningWorkflows.TryRemove(e.InstanceId, out workflowApplicationValues);
            }

            Console.WriteLine(e.CompletionState);
        }

        private static PersistableIdleAction OnIdleAndPersistable(WorkflowApplicationIdleEventArgs e)
        {                       
            return PersistableIdleAction.Unload;
        }

        private static void OnWorkflowAborted(WorkflowApplicationAbortedEventArgs e)
        {
            Console.WriteLine(e.Reason);
        }

        private static void OnWorkflowUnloaded(WorkflowApplicationEventArgs e)
        {
            //if it is the collection then it was not removed by Completed event.
            if (runningWorkflows != null && runningWorkflows.ContainsKey(e.InstanceId))
            {
                WorkflowApplication workflowApp;
                var workflowApplicationValues = new WorkflowApplicationValues();
                runningWorkflows.TryRemove(e.InstanceId, out workflowApplicationValues);
                AddToUnloadedCollection(e.InstanceId, workflowApplicationValues);
            }            

            Console.WriteLine(e.InstanceId + " unloaded");
        }

        private static UnhandledExceptionAction OnWorkflowException(WorkflowApplicationUnhandledExceptionEventArgs e)
        {
            //log the exception here using e.UnhandledException 
            return UnhandledExceptionAction.Terminate;
        }

        #endregion

        
    }

}
