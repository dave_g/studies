﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.BookMarkTests
{

    public sealed class CreateBlockingBookmark : NativeActivity<String>
    {
        [RequiredArgument]
        public String BookmarkName { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            context.CreateBookmark(BookmarkName, Resumed);
        }

        private void Resumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (value != null && value is String)
            {
                Result.Set(context, value);
            }
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }
    }
}
