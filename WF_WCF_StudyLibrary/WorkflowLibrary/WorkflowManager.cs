﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowLibrary
{
    public class WorkflowManager
    {
        private Dictionary<Guid, WorkflowApplication> _wfApps = new Dictionary<Guid, WorkflowApplication>();
        
        public Action<Guid, IDictionary<string, object>> Started { get; set; }
        public Action<Guid, IDictionary<string, object>> Completed { get; set; }
        public Action<Guid> Idle { get; set; }
        public Action<Guid, String, String> Incomplete { get; set; }

        public Guid Run<T>(IDictionary<String, Object> parameters) where T : Activity, new()
        {
            Guid id = Guid.Empty;
            T activity = new T();
            WorkflowApplication wfApp = null;
            if (parameters != null)
            {
                wfApp = new WorkflowApplication(activity,parameters);
            }
            else
            {
                wfApp = new WorkflowApplication(activity);
            }
            id = wfApp.Id;
            _wfApps.Add(id,wfApp);
            wfApp.Completed = AppCompleted;
            wfApp.Aborted = AppAborted;
            wfApp.Idle = AppIdle;
            wfApp.OnUnhandledException = AppException;
            if (Started != null)
            {
                Started(id, parameters);
            }
            wfApp.Run();
            return id;
        }

        public Guid Run(Activity workflow) 
        {
            Guid id = Guid.Empty;
            
            WorkflowApplication wfApp = null;
            wfApp = new WorkflowApplication(workflow);

            id = wfApp.Id;
            _wfApps.Add(id, wfApp);
            wfApp.Completed = AppCompleted;
            wfApp.Aborted = AppAborted;
            wfApp.Idle = AppIdle;
            wfApp.OnUnhandledException = AppException;
            if (Started != null)
            {
                Started(id, new Dictionary<string, object>());
            }
            wfApp.Run();
            return id;
        }
        private void AppIdle(WorkflowApplicationIdleEventArgs args)
        {
            if (Idle != null)
            {
                Idle(args.InstanceId);
            }
        }

        private UnhandledExceptionAction AppException(WorkflowApplicationUnhandledExceptionEventArgs args)
        {
            if (Incomplete != null)
            {
                Incomplete(args.InstanceId, "Exception", args.UnhandledException.Message);
            }
            RemoveInstance(args.InstanceId);
            return UnhandledExceptionAction.Cancel;
        }

        private void AppAborted(WorkflowApplicationAbortedEventArgs args)
        {
            if (Incomplete != null)
            {
                Incomplete(args.InstanceId, "Aborted", args.Reason.Message);
            }
            RemoveInstance(args.InstanceId);
        }

        private void AppCompleted(WorkflowApplicationCompletedEventArgs args)
        {
            switch (args.CompletionState)
            {
                case ActivityInstanceState.Closed:
                    if (Completed != null)
                    {
                        Completed(args.InstanceId, args.Outputs);
                    }
                    RemoveInstance(args.InstanceId);
                    break;
                case ActivityInstanceState.Canceled:
                    if (Incomplete != null)
                    {
                        Incomplete(args.InstanceId, "Canceled", String.Empty);
                    }
                    break;                
                case ActivityInstanceState.Faulted:
                    if (Incomplete != null)
                    {
                        Incomplete(args.InstanceId, "Faulted", args.TerminationException.Message);
                    }
                    break;                
            }
        }

        private void RemoveInstance(Guid instanceId)
        {
            lock (_wfApps)
            {
                if (_wfApps.ContainsKey(instanceId))
                {
                    _wfApps[instanceId].Completed = null;
                    _wfApps[instanceId].Idle = null;
                    _wfApps[instanceId].Aborted = null;
                    _wfApps[instanceId].OnUnhandledException = null;
                    _wfApps.Remove(instanceId);

                }
            }
        }
    }
}
