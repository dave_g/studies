﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using WorkflowLibrary.MAC;

namespace WorkflowLibrary.Tests
{

    public sealed class ChildExecutionProperty : NativeActivity
    {
        // Define an activity input argument of type string
        public InArgument<string> Text { get; set; }
        private static int cnt = 0;
        private Activity delay;
        private Variable<NoPersistHandle> NoPersistHandle { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 10);
            delay = new Delay()
            {
                DisplayName = "ChildExecutionDelay",
                Duration = timeSpan,
            };
            //delay = new WriteLine(){Text = "Delay"};
            metadata.AddImplementationChild(delay);
            
            NoPersistHandle = new Variable<NoPersistHandle>();
            metadata.AddImplementationVariable(NoPersistHandle);
            
            base.CacheMetadata(metadata);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var noPersistHandle = NoPersistHandle.Get(context); 
            noPersistHandle.Enter(context);
            // Obtain the runtime value of the Text input argument
            string text = context.GetValue(this.Text);            
            Console.WriteLine("Start");
            cnt++;
            bool complete = cnt > 10;

            var bookMarkProperty = context.Properties.Single(p => p.Key == PolarExecutionFlow.GotoPropertyName);
            Bookmark bookmark = bookMarkProperty.Value as Bookmark;
            if (complete)
            {                
                context.ResumeBookmark(bookmark, null);                
            }
            else
            {
                
                context.ScheduleActivity(delay, OnCompleted);
                
            }
            
        }

        protected override bool CanInduceIdle
        {
            get { return false; }
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedInstance)
        {
            var noPersistHandle = NoPersistHandle.Get(context); 
            noPersistHandle.Exit(context);
            var bookMarkProperty = context.Properties.Single(p => p.Key == PolarExecutionFlow.GotoPropertyName);
            Bookmark bookmark = bookMarkProperty.Value as Bookmark;
            context.ResumeBookmark(bookmark, this.DisplayName);
        }
    }
}
