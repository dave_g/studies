﻿using System;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Xaml;
using WorkflowLibrary.MAC;

namespace WorkflowLibrary.Utilities
{

    public sealed class LoadActivityFromXaml : NativeActivity
    {
       
        public InArgument<string> FileName { get; set; }
        public Activity LoadedActivity { get; set; }
        private ActivityAction _activityAction;

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            metadata.AddDelegate(_activityAction);
            metadata.AddArgument(new RuntimeArgument("FileName", typeof(String), ArgumentDirection.In));            
        }

        protected override void Execute(NativeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            string file = context.GetValue(FileName);
            var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(LoadActivityFromXaml).Assembly };
            var reader = new XamlXmlReader(file, settings);
            Activity workflowload = ActivityXamlServices.Load(reader);
            LoadedActivity =  workflowload;
            //_activityAction = new ActivityAction() { Handler = workflowload,DisplayName = "Activity Action"};
            //context.ScheduleAction(_activityAction);
            WorkflowInvoker.Invoke(workflowload);
        }
    }
}
