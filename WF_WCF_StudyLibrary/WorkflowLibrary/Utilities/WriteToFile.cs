﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.Utilities
{

    public sealed class WriteToFile : NativeActivity
    {
        public InArgument<string> Text { get; set; }
        
        protected override void Execute(NativeActivityContext context)
        {            
            string text = String.Format("{0} {1}\r\n" , DateTime.Now , context.GetValue(this.Text));
            File.AppendAllText(@"c:\ssg_logs\servicelog.txt", text);  
        }
    }
}
