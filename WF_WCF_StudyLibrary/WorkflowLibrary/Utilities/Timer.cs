﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.Utilities
{

    public sealed class Timer : CodeActivity
    {
        
        public InArgument<Double> Value { get; set; }
        System.Timers.Timer _timer = new System.Timers.Timer();
        private bool _loop = true;
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            double timerValue = context.GetValue(this.Value);
            _timer.Interval = timerValue;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            _timer.Start();
            while (_loop)
            {
                
            }
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Stop();
            _loop = false;
        }
    }
}
