﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.Utilities
{

    public sealed class GetParamaterFromExecutionContext : NativeActivity
    {
        public InArgument<string> ParamaterCollectionName { get; set; }
        public InArgument<string> ParamaterName { get; set; }
        public OutArgument<string> ParamaterValue { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            if (context.Properties.Any(p => p.Key == ParamaterCollectionName.Get(context)))
            {
                KeyValuePair<string, object> property = context.Properties.Single(p => p.Key == ParamaterCollectionName.Get(context));
                var wfPparams = (Dictionary<string, string>)property.Value;
                if (wfPparams.Any(wfp => wfp.Key == ParamaterName.Get(context)))
                {
                    ParamaterValue.Set(context, wfPparams[ParamaterName.Get(context)]); 
                }
            }
        }
    }
}
