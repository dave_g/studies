﻿using System;
using System.Activities;
using WorkflowLibrary.ProWF.Extensions;

namespace WorkflowLibrary.MAC
{

    public sealed class NotifyHost : NativeActivity<String>
    {
        // Define an activity input argument of type string
        public InArgument<string> Message { get; set; }
        public InArgument<string> BookmarkName { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(NativeActivityContext context)
        {
            IHostNotification extention = context.GetExtension<IHostNotification>();
            if (extention != null)
            {
                extention.Notify(Message.Get(context), BookmarkName.Get(context));
            }
            context.CreateBookmark("NotifyHost", Resumed);
        }

        private void Resumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (value != null && value is String)
            {
                Result.Set(context, value);
            }
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }
    }
}
