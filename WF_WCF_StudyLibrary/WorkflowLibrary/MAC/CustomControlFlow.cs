﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.MAC
{

    [Designer(typeof(ActivityDesignerLibrary.CustomControlFlow))]
    public class CustomControlFlow : NativeActivity
    {
        internal static readonly string GotoPropertyName = "StudyLibrary.WorkflowLibrary.MAC.CustomControlflow.Goto";
        Collection<Activity> _activities;
        Collection<Variable> _variables;

        //current activity being executed
        Variable<ActivityInstance> CurrentActivity = new Variable<ActivityInstance>();

        public InArgument<string> BookmarkName { get; set; }
        public InArgument<Dictionary<string, string>> Params { get; set; }

        public Collection<Activity> ControlFlowChildActivities
        {
            get
            {
                if (_activities == null) _activities = new Collection<Activity>();
                return _activities;
            }
        }

        public Collection<Variable> ControlFlowVariables
        {
            get
            {
                if (_variables == null) _variables = new Collection<Variable>();
                return _variables;
            }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            metadata.SetVariablesCollection(ControlFlowVariables);
            metadata.SetChildrenCollection(ControlFlowChildActivities);
            metadata.AddImplementationVariable(CurrentActivity);
            metadata.AddArgument(new RuntimeArgument("BookmarkName", typeof(string), ArgumentDirection.In));
            metadata.AddArgument(new RuntimeArgument("Params", typeof(Dictionary<string, string>), ArgumentDirection.In));
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {

            var wfparams = Params.Get(context);
            if (wfparams != null)
            {
                context.Properties.Add("params", wfparams);
            }            
            
            if (ControlFlowChildActivities.Count > 0)
            {
                // Create a bookmark for signaling the Goto
                Bookmark internalBookmark = context.CreateBookmark(Goto,
                                                                   BookmarkOptions.MultipleResume |
                                                                   BookmarkOptions.NonBlocking);

                //save the name of the bookmark as an execution property so that child activites can access it
                context.Properties.Add(GotoPropertyName, internalBookmark);

                CurrentActivity.Set(context, context.ScheduleActivity(ControlFlowChildActivities[0], OnChildCompleted, OnFaulted));

                //create a bookmark for external (host) resumtion
                if (BookmarkName.Get(context) != null)
                {
                    context.CreateBookmark(BookmarkName.Get(context), Goto, BookmarkOptions.MultipleResume |
                                                                            BookmarkOptions.NonBlocking);
                }
            }
        }

        private void OnFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception, ActivityInstance propagatedfrom)
        {
            throw new NotImplementedException();
        }

        private void Goto(NativeActivityContext context, Bookmark bookmark, object value)
        {
            var targetActivityName = value as string;
            var current = CurrentActivity.Get(context);
            var containsActivity = ControlFlowChildActivities.Any(a => a.DisplayName.Equals(targetActivityName));
            ActivityInstance instance = null;
            //if a valid activity has been passed in
            if (containsActivity)
            {
                var targetActivity = ControlFlowChildActivities.Single(a => a.DisplayName.Equals(targetActivityName));
                //var currentlyExecutingIndex = ControlFlowChildActivities.IndexOf(current.Activity);
                //var nextExecutingIndex = ControlFlowChildActivities.IndexOf(targetActivity);

                //do not allow jumping passed non-executed activities
                //if ((nextExecutingIndex < currentlyExecutingIndex))
                //{

                //}

                if (targetActivity != null)
                {
                    instance = context.ScheduleActivity(targetActivity, OnChildCompleted);
                    context.CancelChild(CurrentActivity.Get(context));
                    CurrentActivity.Set(context, instance);
                    return;
                }
            }
            //all else failed so just restart the current activity
            instance = context.ScheduleActivity(ControlFlowChildActivities.Single(a => a.DisplayName.Equals(current.Activity.DisplayName)), OnChildCompleted);
            context.CancelChild(current);
            CurrentActivity.Set(context, instance);

        }

        private void OnChildCompleted(NativeActivityContext context, ActivityInstance completedInstance)
        {
            if (completedInstance.State == ActivityInstanceState.Closed)
            {
                //Calculate the index of the next child to schedule
                var currentExecuting = ControlFlowChildActivities.IndexOf(completedInstance.Activity);
                var next = currentExecuting + 1;

                if (next < ControlFlowChildActivities.Count)
                {
                    CurrentActivity.Set(context, context.ScheduleActivity(ControlFlowChildActivities[next], OnChildCompleted));
                }
            }

        }

        protected override void Abort(NativeActivityAbortContext context)
        {
            base.Abort(context);
        }

        protected override void Cancel(NativeActivityContext context)
        {
            base.Cancel(context);
        }
    }
}
