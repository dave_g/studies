﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.MAC
{

    public sealed class SetBookmark : NativeActivity
    {
        [RequiredArgument]
        public InArgument<String> BookmarkName { get; set; }

        private Variable<NoPersistHandle> NoPersistHandle { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            NoPersistHandle = new Variable<NoPersistHandle>();
            metadata.AddImplementationVariable(NoPersistHandle);
            base.CacheMetadata(metadata);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var handle = NoPersistHandle.Get(context);
            handle.Enter(context);
            context.CreateBookmark(BookmarkName.Get(context), Resumed);
            handle.Exit(context);
        }

        private void Resumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (value != null && value is String)
            {
               
            }
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }
    }
}
