﻿using System;
using System.Activities;

namespace WorkflowLibrary.MAC
{

    public sealed class GetValue : NativeActivity<String>
    {        
        protected override void Execute(NativeActivityContext context)
        {
            context.CreateBookmark("GetValue", Resumed);
        }

        private void Resumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (value != null && value is String)
            {
                Result.Set(context, value);   
            }
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }
    }
}
