﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowLibrary.MAC
{
    public class GoTo : NativeActivity
    {
        [RequiredArgument]
        public InArgument<string> TargetActivityName { get; set; }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            //get the bookmark created by the parent control flow
            Bookmark bookmark = context.Properties.Find(CustomControlFlow.GotoPropertyName) as Bookmark;            
            //resume the bookmark passing the target activity name
            context.ResumeBookmark(bookmark, TargetActivityName.Get(context));
            //create a bookmark to leave this idle waiting when it does not have further work to do. ControlFlow will cancel this in its goto method.
            context.CreateBookmark("SynchBookmark");
        }
    }
}
