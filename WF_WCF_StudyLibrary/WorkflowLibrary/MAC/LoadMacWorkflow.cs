﻿using System;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Threading;
using System.Xaml;

namespace WorkflowLibrary.MAC
{

    public sealed class LoadMacWorkflow : NativeActivity<Activity>
    {
        
        public String XamlFile { get; set; }
        public Activity LoadedWorkflow { get; set; }               

        protected override void Execute(NativeActivityContext context)
        {
            string file = XamlFile;
            var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(LoadMacWorkflow).Assembly };
            var reader = new XamlXmlReader(file, settings);
            Activity workflowload = ActivityXamlServices.Load(reader);
            LoadedWorkflow = workflowload;            
        }
    }
}
