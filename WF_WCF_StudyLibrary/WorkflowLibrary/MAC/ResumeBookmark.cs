﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.MAC
{

    public sealed class ResumeBookmark : NativeActivity
    {
        public InArgument<string> BookmarkToResume { get; set; }
        public InArgument<object> BookmarkData { get; set; }

        protected override void Execute(NativeActivityContext context)
        {            
            context.ResumeBookmark(new Bookmark(BookmarkToResume.Get(context)), BookmarkData.Get(context));
        }
    }
}
