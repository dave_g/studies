﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowLibrary.ProWF
{
    public class Item
    {
        private int _itemid;

        public int Itemid
        {
            get { return _itemid; }
            set { _itemid = value; }
        }
    }
}
