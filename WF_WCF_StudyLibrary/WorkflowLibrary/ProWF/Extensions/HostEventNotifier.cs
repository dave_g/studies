﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WorkflowLibrary.ProWF.Extensions
{
    public class HostEventNotifier : IHostNotification
    {
        public event EventHandler<HostNotifyEventArgs> Notification;
 
        public void Notify(string message, string bookmarkName)
        {
            if (Notification != null)
            {
                ThreadPool.QueueUserWorkItem(delegate(Object state)
                                                 {
                    Notification(this,
                        new HostNotifyEventArgs(state as String, bookmarkName));
                }, message);
                //needs to raise event on a different thread.
                //Notification(this, new HostNotifyEventArgs(message,bookmarkName));
            }
        }
    }

    public class HostNotifyEventArgs : EventArgs
    {
        public String Message { get;  set; }
        public String BookmarkName { get; set; }

        public HostNotifyEventArgs(string message, string bookmarkName)
        {
            Message = message;
            BookmarkName = bookmarkName;
        }
    }
}
