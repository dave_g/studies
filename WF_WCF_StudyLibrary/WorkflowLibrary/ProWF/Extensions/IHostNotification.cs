﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkflowLibrary.ProWF.Extensions
{
    public interface IHostNotification
    {
        void Notify(String message, String bookmarkName);
    }
}
