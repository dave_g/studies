﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WorkflowLibrary.ProWF.Extensions
{
    public class HostEventNotifierSingleThread 
    {
        public event EventHandler<HostNotifyEventArgs> Notification;

        public void Notify(HostNotifyEventArgs message)
        {
            if (Notification != null)
            {                
                Notification(this,message);
            }
        }
    }

    
}
