﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary.ProWF
{

    public sealed class GetWorkflowId : CodeActivity
    {
        
        public OutArgument<String> WorkflowId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            Guid id = context.WorkflowInstanceId;
            WorkflowId.Set(context, id.ToString());

        }
        
    }
}
