﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using WorkflowLibrary.ProWF.Extensions;

namespace WorkflowLibrary.ProWF
{

    public sealed class NotifyHost : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<string> Message { get; set; }
        public InArgument<string> BookmarkName { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            IHostNotification extention = context.GetExtension<IHostNotification>();
            if (extention != null)
            {
                extention.Notify(Message.Get(context), BookmarkName.Get(context));
            }
        }
    }
}
