﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkflowLibrary
{

    public sealed class ParseCalculatorExpression : CodeActivity
    {
        // Define an activity input argument of type string
        [RequiredArgument]
        public InArgument<string> Expression { get; set; }

        public OutArgument<Double> FirstNumber { get; set; }
        public OutArgument<Double> SecondNumber { get; set; }
        public OutArgument<string> Operator { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var expression = Expression.Get(context);
            //set default arg values
            FirstNumber.Set(context,0);
            SecondNumber.Set(context,0);
            Operator.Set(context,"error");
            //parse the expression
            if (!String.IsNullOrEmpty(expression))
            {
                var values = expression.Split(' ');
                double fn = 0;
                Double.TryParse(values[0], out fn);

                string op = values[1];

                double sn = 0;
                Double.TryParse(values[2], out sn);

                FirstNumber.Set(context, fn);
                SecondNumber.Set(context, sn);
                Operator.Set(context, op);    
            }
            
        }
    }
}
