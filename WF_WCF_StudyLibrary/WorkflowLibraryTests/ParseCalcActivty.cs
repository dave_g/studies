﻿using System.Activities;
using System.Activities.XamlIntegration;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkflowLibrary.MAC;

using System.Xaml;

namespace WorkflowLibraryTests
{
    [TestClass]
    public class ParseCalcActivty
    {
        [TestMethod]
        public void TestMethod1()
        {
            var wf = new WorkflowLibrary.ParseCalculatorExpression();
            wf.Expression = "1 + 2";
            var result = System.Activities.WorkflowInvoker.Invoke(wf);
            Assert.IsNotNull(result);
            Assert.AreEqual(1d,result["FirstNumber"]);
            Assert.AreEqual(2d, result["SecondNumber"]);
            Assert.AreEqual("+", result["Operator"]);
        }

        [TestMethod]
        public void TestMethod()
        {
            var xamlName = "PickTest.xaml";
            string fullfilePath = Path.Combine(@"..\..\..\WorkflowLibrary\MAC", xamlName);            
            var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(LoadMacWorkflow).Assembly };
            var reader = new XamlXmlReader(fullfilePath, settings);
            var workflowload = ActivityXamlServices.Load(reader);
            var activity = workflowload as Activity;
            var result = System.Activities.WorkflowInvoker.Invoke(activity);
            Assert.IsNotNull(result);           
        }
    }
}
