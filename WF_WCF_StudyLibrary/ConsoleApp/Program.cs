﻿using System;
using System.Activities.DurableInstancing;
using System.Activities.Hosting;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Activities;
using System.Activities.Statements;
using System.Linq;
using System.ServiceModel.Activities;
using System.ServiceModel.Activities.Description;
using System.ServiceModel.Description;
using System.Threading;
using System.Xaml;
using WorkflowLibrary;

using WorkflowLibrary.MAC;
using WorkflowLibrary.ProWF;
using WorkflowLibrary.ProWF.Extensions;
using WorkflowLibrary.Tests;


// ReSharper disable UnusedMember.Local
// ReSharper disable UnusedParameter.Local
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable JoinDeclarationAndInitializer
namespace ConsoleApp
{
    

    class Program
    {
        public static List<WorkflowServiceHost> _hosts = new List<WorkflowServiceHost>();
        private static System.Threading.Timer _timer;
        //allows the host to wait until the wf is completed on a different thread.
        //private static AutoResetEvent waitEvent = new AutoResetEvent(false);


        static void Main(string[] args)
        {
            //WorkflowInvoker Methods
            //InvokeHostWorkflow();
            //InvokeAsyncHostingWorkflow();
            //BeginInvokeHostingWorkflow();
            //InvokeWorkflow1();
            //InvokeCalculatorWorkflow();
            //TestMultipleParameters();

            //WorkflowApplication Methods
            //InvokeHostingDemoWorkflow();
            
            //InvokeCalculatorFlow();
            //InvokeCalculatorFlowBookmarkExtension();

            //InvokeXaml();

            //InvokeWorkflowApplicationManager<HostingDemoWorkflow>(new Dictionary<string, object>
            //                   {
            //                       {"ArgNumberToEcho", 1005}
            //                   });
            //InvokeWorkflowApplicationManager<CustomControlFlow>(null);

            //InvokeCustomControlFlowWorkflow();         
            //StartWorkflowServiceHost();            
            _timer = new Timer(new TimerCallback(Target),null,10000,35000);            
            InvokeGenericHost();
        }

        private static void Target(object state)
        {
            Console.WriteLine("Tick");
            WorkflowLibrary.GenericHost.ReloadExpiredApplication();
        }

        #region GenericHost

        private static void InvokeGenericHost()
        {
            AutoResetEvent waitEvent = new AutoResetEvent(false);      

            //var xamlName = "CustomControlWorkflow.xaml";
            var xamlName = "PickTest.xaml";
            string fullfilePath = Path.Combine(@"..\..\..\WorkflowLibrary\MAC", xamlName);            

            var workflow = new PolarExecutionFlow()
            {
                BookmarkName = "HostBookmark",
                ControlFlowChildActivities =
                                       {
                                           new ChildExecutionProperty()
                                                {
                                                    DisplayName = "Start",
                                                    Text = "Step 1"
                                                },                                            
                                            new WriteLine
                                                {
                                                    DisplayName = "First Step",
                                                    Text = "Step 2"
                                                },                                            
                                       }
            };

            WorkflowLibrary.GenericHost.HostException = HostException;
            //Guid WorkflowId = WorkflowLibrary.GenericHost.StartPersistableInstance(new Dictionary<string, object>(), new PickTest());
            Guid WorkflowId = WorkflowLibrary.GenericHost.StartPersistableInstance(new Dictionary<string, object>(), workflow);


            //Guid WorkflowId = new Guid("799AB121-F607-4E4E-90E8-A066A5D07A5E");
            //WorkflowLibrary.GenericHost.LoadInstanceWithBookmark("Pause", WorkflowId, "End", new PickTest());
            //WorkflowLibrary.GenericHost.LoadInstanceWithBookmark("HostBookmark", WorkflowId, "First Step", workflow);
            
            
            //WorkflowLibrary.GenericHost.LoadInstance(WorkflowId, fullfilePath);
            //GenericHost.CancelInstance(WorkflowId);
            //GenericHost.LoadInstance(WorkflowId,fullfilePath);
            //GenericHost.LoadInstance(WorkflowId2, fullfilePath);
            waitEvent.WaitOne();
            //var wf = WorkflowInspectionServices.GetActivities(wfapp.WorkflowDefinition);
            //var bks = wfapp.GetBookmarks();
            //wfapp.ResumeBookmark("Pause", null);
        }

        
        private static void HostException()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region WorkflowServiceHost

        private static void StartWorkflowServiceHost()
        {
            try
            {
                CreateServiceHost("MacWorkflowInvoker.xamlx");

                foreach (WorkflowServiceHost host in _hosts)
                {                    
                    host.Open();
                    foreach (ServiceEndpoint serviceEndpoint in host.Description.Endpoints)
                    {
                        Console.WriteLine("Contract: {0}", serviceEndpoint.Contract.Name);
                        Console.WriteLine(" at {0}", serviceEndpoint.Address);
                    }
                }
                Console.WriteLine("Press any key to stop hosting and exit");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Service Exception: {0}" , ex);
            }finally
            {
                Console.WriteLine("Closing Services....");
                foreach (WorkflowServiceHost workflowServiceHost in _hosts)
                {
                    workflowServiceHost.Close();
                }
                Console.WriteLine("Services closed.");
                _hosts.Clear();
            }
        }
        
        private static WorkflowServiceHost CreateServiceHost(string xamlxName)
        {
            WorkflowService wfService = LoadService(xamlxName);
            WorkflowServiceHost host = new WorkflowServiceHost(wfService);
            
            //todo: load datasource from configuration
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder
                                                                     {
                                                                         DataSource = @".\SQLEXPRESS10_50",
                                                                         InitialCatalog = "WorkflowPersistence",
                                                                         IntegratedSecurity = true
                                                                     };


            SqlWorkflowInstanceStoreBehavior storeBehavior = new SqlWorkflowInstanceStoreBehavior(connectionStringBuilder.ConnectionString)
                                                                 {     
                                                                     RunnableInstancesDetectionPeriod = new TimeSpan(0,0,2),
                                                                     InstanceCompletionAction =
                                                                         InstanceCompletionAction.DeleteAll,
                                                                     InstanceLockedExceptionAction =
                                                                         InstanceLockedExceptionAction.BasicRetry,
                                                                     InstanceEncodingOption =
                                                                         InstanceEncodingOption.GZip,
                                                                     HostLockRenewalPeriod = TimeSpan.FromMinutes(1)
                                                                 };
            
            
            //aggressive setting for testing
            //todo: adjust for production
            WorkflowIdleBehavior idleBehavior = new WorkflowIdleBehavior(){TimeToUnload = TimeSpan.FromSeconds(0)};
            
            host.Description.Behaviors.Add(storeBehavior);
            host.Description.Behaviors.Add(idleBehavior);
            //todo: add extensions
            
            _hosts.Add(host);
            return host;
        }

        private static WorkflowService LoadService(string xamlxName)
        {
            string fullfilePath = Path.Combine(@"..\..\..\WorkflowLibrary\MAC\Services", xamlxName);            
            WorkflowService service = XamlServices.Load(fullfilePath) as WorkflowService;
            if (service != null)
            {
                return service;
            }
            
            throw new NullReferenceException(String.Format("Unable to load service definition from {0}", fullfilePath));
        }

        
        #endregion

        private static void InvokeCustomControlFlowWorkflow()
        {
            var counter = new Variable<int>();
            #region original control flow setup
            //var workflow = new CustomControlFlow
            //                   {
            //                       BookmarkName = "HostBookmark",
            //                       ControlFlowVariables = { counter },
            //                       ControlFlowChildActivities =
            //                           {
            //                               new WriteLine
            //                                    {
            //                                        DisplayName = "Start",
            //                                        Text = "Step 1"
            //                                    },                                            
            //                                new WriteLine
            //                                    {
            //                                        DisplayName = "First Step",
            //                                        Text = "Step 2"
            //                                    },                                                
            //                                new Assign<int>
            //                                    {
            //                                        To = counter,
            //                                        Value = new InArgument<int>(c => counter.Get(c) + 1)
            //                                    },                                                                                              
            //                                new If
            //                                    {
            //                                        Condition = new InArgument<bool>(c => counter.Get(c) == 3),
            //                                        Then = new WriteLine
            //                                                    {
            //                                                        Text = "Step 3"
            //                                                    },
            //                                        //Else = new GoTo {TargetActivityName = "First Step"}                                                    
            //                                    },
            //                                new WriteLine
            //                                    {
            //                                        DisplayName = "End",
            //                                        Text = "The end."
            //                                    },
            //                                new WriteLine
            //                                    {
            //                                      Text  = new InArgument<string>((c => counter.Get(c).ToString(CultureInfo.InvariantCulture)))
            //                                    },
            //                                    new WorkflowLibrary.MAC.NotifyHost
            //                                    {
            //                                        BookmarkName = new InArgument<string>("HostBookmark"),
            //                                        DisplayName = "Notify Host",
            //                                        Message = new InArgument<string>("Notify")
            //                                    },
            //                           }
            //                   };
            #endregion

            #region trying to work on load from file
            //var settings = new XamlXmlReaderSettings() { LocalAssembly = typeof(WorkflowTest).Assembly };
            //var reader = new XamlXmlReader("MacWorkflow.xaml", settings);
            //Activity workflowload = ActivityXamlServices.Load(reader);
            //var workflow = new RunWorkflow() { Body = workflowload };            

            //var loader = new LoadMacWorkflow() { XamlFile = "MacWorkflow.xaml" };

            //var output = WorkflowInvoker.Invoke(
            //        (Activity)loader
            //        );

            //var workflow = new RunWorkflow() { Body = loader.LoadedWorkflow};
            #endregion

            #region load from file wrapped in a class
            var workflow = new RunWorkflow();
            WorkflowInvoker.Invoke(workflow);
            #endregion
            
            
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            HostEventNotifier extension = new HostEventNotifier();
            WorkflowApplication wfApp = InvokeWorkflowApplication(workflow, null, waitEvent, new object[] { extension });

            extension.Notification += delegate(object sender, HostNotifyEventArgs args)
                                          {
                                              Console.WriteLine("Start, First Step, End");
                                              //Console.WriteLine(args.Message + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                                              //Console.WriteLine(args.BookmarkName + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                                              String expression = Console.ReadLine();

                                              if (expression == "quit")
                                              {
                                                  wfApp.Cancel();
                                                  Console.WriteLine("App Canceled" + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                                              }

                                              if (isValidBookMark(wfApp, args.BookmarkName))
                                              {
                                                  Console.WriteLine("bookmark valid");
                                                  BookmarkResumptionResult result = wfApp.ResumeBookmark(args.BookmarkName, expression);
                                                  Console.WriteLine(result);
                                              }
                                              else
                                              {
                                                  Console.WriteLine("bookmark NOT valid");
                                              }
                                          };

            waitEvent.WaitOne();
            

        }

        private static void InvokeCalculatorFlowBookmarkExtension()
        {

            Func<bool> toRun = () =>
            {
                bool returnVal = true;
                AutoResetEvent waitEvent = new AutoResetEvent(false);
                HostEventNotifier extension = new HostEventNotifier();
                WorkflowApplication wfApp = InvokeWorkflowApplication(new CalculatorFlowBookmarkExtension(), null, waitEvent, new object[] { extension });

                extension.Notification += delegate(object sender, HostNotifyEventArgs args)
                {
                    Console.WriteLine(args.Message + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                    String expression = Console.ReadLine();

                    if (expression == "quit")
                    {
                        wfApp.Cancel();
                        Console.WriteLine("App Canceled" + " thread {0}", Thread.CurrentThread.ManagedThreadId);
                        returnVal = false;
                    }

                    if (isValidBookMark(wfApp, args.BookmarkName))
                    {
                        wfApp.ResumeBookmark(args.BookmarkName, expression);
                    }
                };

                wfApp.Run();
                waitEvent.WaitOne();
                return returnVal;
            };

            ConsoleLoopRunner(toRun);
        }

        private static void ConsoleLoopRunner(Func<bool> method)
        {
            bool running = true;
            while (running)
            {
                running = method();
            }

        }

        #region ActivityXamlServices

        private static void InvokeXaml()
        {
            Activity activity = LoadActivityFromXamlFile(@"..\..\..\WorkflowLibrary\ProWf\HostingDemoWorkflow.xaml");

            AutoResetEvent waitEvent = new AutoResetEvent(false);

            if (activity != null)
            {
                InvokeWorkflowApplication(activity, null, waitEvent, null);
            }
            waitEvent.WaitOne();
        }

        private static Activity LoadActivityFromXamlFile(string path)
        {
            if (File.Exists(path))
            {
                return ActivityXamlServices.Load(path);
            }
            return null;
        }

        #endregion/


        #region workflowApplication

        

        private static void InvokeCalculatorFlow()
        {
            bool isRunning = true;

            while (isRunning)
            {
                AutoResetEvent waitEvent = new AutoResetEvent(false);
                Activity workflow = new CalculatorFlow();
                var wfApp = InvokeWorkflowApplication(workflow, null, waitEvent, null);
                Console.WriteLine("Enter an Expression or quit");
                var expression = Console.ReadLine();
                if (expression == null) continue;
                if (expression.ToLower() == "quit")
                {
                    wfApp.Cancel();
                    Console.WriteLine("Exiting Application");
                    isRunning = false;
                }
                if (isValidBookMark(wfApp, "GetString"))
                {
                    wfApp.ResumeBookmark("GetString", expression);
                }

                waitEvent.WaitOne();
            }

        }

        private static bool isValidBookMark(WorkflowApplication wfApp, string bookMarkName)
        {
            bool result = false;
            ReadOnlyCollection<BookmarkInfo> bookmarks;
            try
            {
                bookmarks = wfApp.GetBookmarks();
            }
            catch (Exception)
            {
                return result;
            }


            if (bookmarks == null) return result;
            result = bookmarks.Any(b => b.BookmarkName == bookMarkName);
            return result;
        }


        private static void InvokeHostingDemoWorkflow()
        {
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            Activity workflow = new HostingDemoWorkflow();
            var wfparams = new Dictionary<string, object>
                               {
                                   {"ArgNumberToEcho", 1005}
                               };
            InvokeWorkflowApplication(workflow, wfparams, waitEvent, null);
            waitEvent.WaitOne();
        }


        private static WorkflowApplication InvokeWorkflowApplication(Activity workflow, Dictionary<string, object> wfParams, AutoResetEvent waitEvent, IEnumerable<object> extensions)
        {

            WorkflowApplication wfapp;


            wfapp = wfParams != null ? new WorkflowApplication(workflow, wfParams) : new WorkflowApplication(workflow);


            wfapp.Completed = delegate(WorkflowApplicationCompletedEventArgs args)
                                  {
                                      switch (args.CompletionState)
                                      {
                                          case ActivityInstanceState.Closed:
                                              Console.WriteLine("Host: {0} Closed - Thread: {1} - {2}", wfapp.Id, Thread.CurrentThread.ManagedThreadId, args.Outputs.ContainsKey("Result") ? args.Outputs["Result"] : "No Result Supplied");
                                              break;
                                          case ActivityInstanceState.Canceled:
                                              Console.WriteLine("Host: {0} Canceled - Thread: {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                                              break;
                                          case ActivityInstanceState.Executing:
                                              Console.WriteLine("Host: {0} Executing - Thread: {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                                              break;
                                          case ActivityInstanceState.Faulted:
                                              Console.WriteLine("Host: {0} Faulted - Thread: {1} - Error {2} : {3}", wfapp.Id,
                                                  Thread.CurrentThread.ManagedThreadId, args.TerminationException.GetType(), args.TerminationException.Message);
                                              break;
                                      }
                                      waitEvent.Set();
                                  };

            wfapp.OnUnhandledException = delegate(WorkflowApplicationUnhandledExceptionEventArgs args)
                                             {
                                                 Console.WriteLine("Host: {0} UnHandledException - Thread: {1} - Error {2} ", wfapp.Id,
                                                    Thread.CurrentThread.ManagedThreadId, args.UnhandledException.Message);
                                                 waitEvent.Set();
                                                 return UnhandledExceptionAction.Cancel;
                                             };

            wfapp.Aborted = delegate(WorkflowApplicationAbortedEventArgs args)
                                {

                                    Console.WriteLine("Host: {0} Abortion - Thread: {1} - Reason {2} : {3} ", wfapp.Id,
                                                    Thread.CurrentThread.ManagedThreadId, args.Reason.GetType(), args.Reason);
                                    waitEvent.Set();
                                };

            wfapp.Idle =
                args =>
                    {
                        Console.WriteLine("Host: {0} Idle - Thread: {1}", wfapp.Id,
                                  Thread.CurrentThread.ManagedThreadId);                        
                        Console.WriteLine();
                    };
                

            wfapp.PersistableIdle = args =>
                                        {
                                            Console.WriteLine("Host: {0} PersisableIdle - Thread: {1}", wfapp.Id,
                                                              Thread.CurrentThread.ManagedThreadId);
                                            return PersistableIdleAction.Unload;
                                        };
            wfapp.Unloaded =
                args =>
                Console.WriteLine("Host: {0} Unloaded: {1}", wfapp.Id,
                                  Thread.CurrentThread.ManagedThreadId);


            try
            {
                Console.WriteLine("Host: about to run {0} - Thread {1}", wfapp.Id, Thread.CurrentThread.ManagedThreadId);
                if (extensions != null && extensions.Any())
                {
                    foreach (var extension in extensions)
                    {
                        wfapp.Extensions.Add(extension);
                    }
                }
                wfapp.Run();
                //if (cancel)
                //{
                //    Thread.Sleep(1000);
                //    //wfapp.Cancel();
                //    //wfapp.Abort("Reason : Too young!");
                //    wfapp.Terminate("I'll be back.");
                //}
                //waitEvent.WaitOne();
            }
            catch (Exception e)
            {

                Console.WriteLine("Host: {0} Exception {1} : {2}", wfapp.Id, e.GetType(), e.Message);
            }

            return wfapp;
        }

        private static void InvokeWorkflowApplicationManager<T>(Dictionary<string, object> wfParams) where T : Activity, new()
        {
            WorkflowManager workflowManager = new WorkflowManager();
            AutoResetEvent waitResetEvent = new AutoResetEvent(false);


            workflowManager.Completed = delegate(Guid id, IDictionary<string, object> args)
            {
                Console.WriteLine("Host: {0} Closed - Thread: {1} - {2}", id, Thread.CurrentThread.ManagedThreadId, args.ContainsKey("Result") ? args["Result"] : "No Result Supplied");
                waitResetEvent.Set();
            };

            workflowManager.Incomplete = delegate(Guid id, string arg2, string arg3)
            {
                switch (arg2)
                {
                    case "Canceled":
                        Console.WriteLine("Host: {0} Canceled - Thread: {1}", id, Thread.CurrentThread.ManagedThreadId);
                        break;
                    case "Faulted":
                        Console.WriteLine("Host: {0} Faulted - Thread: {1} - Error {2} ", id,
                            Thread.CurrentThread.ManagedThreadId, arg3);
                        break;
                    case "Aborted":
                        Console.WriteLine("Host: {0} Abortion - Thread: {1} - Reason {2} ", id,
                                Thread.CurrentThread.ManagedThreadId, arg3);
                        break;
                    case "Exception":
                        Console.WriteLine("Host: {0} UnHandledException - Thread: {1} - Error {2} ", id,
                   Thread.CurrentThread.ManagedThreadId, arg3);
                        break;

                }

                waitResetEvent.Set();
            };

            workflowManager.Idle =
                id =>
                Console.WriteLine("Host: {0} Idle - Thread: {1}", id,
                                  Thread.CurrentThread.ManagedThreadId);

            //workflowManager.PersistableIdle = delegate(WorkflowApplicationIdleEventArgs args)
            //{
            //    Console.WriteLine("Host: {0} PersisableIdle - Thread: {1}", workflowManager.Id, System.Threading.Thread.CurrentThread.ManagedThreadId);
            //    return PersistableIdleAction.Unload;
            //};
            //workflowManager.Unloaded = delegate(WorkflowApplicationEventArgs args)
            //{
            //    Console.WriteLine("Host: {0} Unloaded: {1}", workflowManager.Id, System.Threading.Thread.CurrentThread.ManagedThreadId);
            //};


            try
            {
                Console.WriteLine("Host: about to run - Thread {0}", Thread.CurrentThread.ManagedThreadId);
                workflowManager.Run<T>(wfParams);
                waitResetEvent.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine("Host: Run Exception {0} : {1}", e.GetType(), e.Message);
            }

        }

        private static void InvokeWorkflowApplicationManager(Activity workflow)
        {
            WorkflowManager workflowManager = new WorkflowManager();
            AutoResetEvent waitResetEvent = new AutoResetEvent(false);


            workflowManager.Completed = delegate(Guid id, IDictionary<string, object> args)
            {
                Console.WriteLine("Host: {0} Closed - Thread: {1} - {2}", id, Thread.CurrentThread.ManagedThreadId, args.ContainsKey("Result") ? args["Result"] : "No Result Supplied");
                waitResetEvent.Set();
            };

            workflowManager.Incomplete = delegate(Guid id, string arg2, string arg3)
            {
                switch (arg2)
                {
                    case "Canceled":
                        Console.WriteLine("Host: {0} Canceled - Thread: {1}", id, Thread.CurrentThread.ManagedThreadId);
                        break;
                    case "Faulted":
                        Console.WriteLine("Host: {0} Faulted - Thread: {1} - Error {2} ", id,
                            Thread.CurrentThread.ManagedThreadId, arg3);
                        break;
                    case "Aborted":
                        Console.WriteLine("Host: {0} Abortion - Thread: {1} - Reason {2} ", id,
                                Thread.CurrentThread.ManagedThreadId, arg3);
                        break;
                    case "Exception":
                        Console.WriteLine("Host: {0} UnHandledException - Thread: {1} - Error {2} ", id,
                   Thread.CurrentThread.ManagedThreadId, arg3);
                        break;

                }

                waitResetEvent.Set();
            };

            workflowManager.Idle =
                id =>
                Console.WriteLine("Host: {0} Idle - Thread: {1}", id,
                                  Thread.CurrentThread.ManagedThreadId);

            //workflowManager.PersistableIdle = delegate(WorkflowApplicationIdleEventArgs args)
            //{
            //    Console.WriteLine("Host: {0} PersisableIdle - Thread: {1}", workflowManager.Id, System.Threading.Thread.CurrentThread.ManagedThreadId);
            //    return PersistableIdleAction.Unload;
            //};
            //workflowManager.Unloaded = delegate(WorkflowApplicationEventArgs args)
            //{
            //    Console.WriteLine("Host: {0} Unloaded: {1}", workflowManager.Id, System.Threading.Thread.CurrentThread.ManagedThreadId);
            //};


            try
            {
                Console.WriteLine("Host: about to run - Thread {0}", Thread.CurrentThread.ManagedThreadId);
                Guid id = workflowManager.Run(workflow);
                waitResetEvent.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine("Host: Run Exception {0} : {1}", e.GetType(), e.Message);
            }

        }
        #endregion


        #region workflowInvoker
        private static void BeginInvokeHostingWorkflow()
        {
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            Console.WriteLine("Host: About to run workflow - Thread {0}", Thread.CurrentThread.ManagedThreadId);
            try
            {
                var wfinvoker = new WorkflowInvoker(new HostingDemoWorkflow { ArgNumberToEcho = 1001 });
                IAsyncResult result = wfinvoker.BeginInvoke(WfCompletedCallback, wfinvoker);

                Console.WriteLine("Host: workflow started with BeginInvoke - Thread {0}", Thread.CurrentThread.ManagedThreadId);
                waitEvent.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine("Host: Workflow exception:{0}:{1}", e.GetType(), e.Message);
            }
        }

        private static void WfCompletedCallback(IAsyncResult ar)
        {
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            var output = ((WorkflowInvoker)ar.AsyncState).EndInvoke(ar);
            Console.WriteLine("Host: Workflow Completed Callback - Thread {0} - {1}", Thread.CurrentThread.ManagedThreadId, output["Result"]);
            waitEvent.Set();
        }

        private static void InvokeAsyncHostingWorkflow()
        {
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            Console.WriteLine("Host: About to run workflow - Thread {0}", Thread.CurrentThread.ManagedThreadId);
            try
            {
                var wfinvoker = new WorkflowInvoker(new HostingDemoWorkflow());

                wfinvoker.InvokeCompleted += delegate(object sender, InvokeCompletedEventArgs e)
                                                 {
                                                     Console.WriteLine("Host: Workflow Completed - Thread {0} - {1}", Thread.CurrentThread.ManagedThreadId, e.Outputs["Result"]);
                                                     waitEvent.Set();
                                                 };

                wfinvoker.InvokeAsync(
                    new Dictionary<string, object>
                        {
                            {"ArgNumberToEcho", 1001}
                        }
                    );

                Console.WriteLine("Host: workflow started - Thread {0}", Thread.CurrentThread.ManagedThreadId);
                waitEvent.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine("Host: Workflow exception:{0}:{1}", e.GetType(), e.Message);
            }

        }



        private static void InvokeHostWorkflow()
        {
            Console.WriteLine("Host: About to run workflow - Thread {0}", Thread.CurrentThread.ManagedThreadId);
            try
            {
                var output = WorkflowInvoker.Invoke(
                    new HostingDemoWorkflow(),
                    new Dictionary<string, object>
                        {
                            {"ArgNumberToEcho", 1001},
                        }
                    );
                Console.WriteLine("Host: Workflow Completed - Thread {0} - {1}", Thread.CurrentThread.ManagedThreadId, output["Result"]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Host: Workflow exception:{0}:{1}", e.GetType(), e.Message);
            }
        }


        private static void TestMultipleParameters()
        {
            var wf = new CodeActivity1();
            var wfargs = new Dictionary<string, object> { { "Text", "Text 1" } };
            wf.Expression = new Variable<string>("one");
            WorkflowInvoker.Invoke(wf, wfargs);
            wfargs.Clear();
            wfargs.Add("Text", "Text 2");
            wf.Expression = new Variable<string>("two");
            WorkflowInvoker.Invoke(wf, wfargs);
        }

        private static void InvokeCalculatorWorkflow()
        {
            var workflow = new Calculator();
            while (true)
            {
                Console.WriteLine("Enter an expression like 1 * 5. Type exit to....exit.");
                string expression = Console.ReadLine();
                if (!String.IsNullOrEmpty(expression))
                {
                    if (expression.ToLower() == "exit")
                    {
                        return;
                    }
                    workflow.argExpression = expression;

                    try
                    {
                        //var result = WorkflowInvoker.Invoke(workflow);
                        WorkflowApplication wfapp = new WorkflowApplication(workflow);
                        wfapp.Run();
                        //Console.WriteLine(result["Result"]);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        private static void InvokeWorkflow1()
        {
            var workflow = new Workflow1();
            var workflowArgs = new Dictionary<string, object> { { "ArgTextIn", "Yo. Foo!" } };
            WorkflowInvoker.Invoke(workflow, workflowArgs);
        }
        #endregion
    }
}
// ReSharper restore UnusedMember.Local
// ReSharper restore UnusedParameter.Local
// ReSharper restore SuggestUseVarKeywordEvident
// ReSharper restore JoinDeclarationAndInitializer