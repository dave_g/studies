﻿using System.Collections.ObjectModel;
using System.Activities;

namespace MacWorkflowInvoker
{
    
    public class WorkFlowInvoke : NativeActivity
    {
        internal static readonly string GotoPropertyName = "StudyLibrary.WorkflowLibrary.MAC.CustomControlflow.Goto";
        Collection<Activity> _activities;
        Collection<Variable> _variables;
        
        //current activity being executed
        public Variable<ActivityInstance> CurrentActivity = new Variable<ActivityInstance>();               

        public Collection<Activity> ControlFlowChildActivities
        {            
            get
            {
                if (_activities == null) _activities = new Collection<Activity>();
                return _activities;
            }
        }


        public Collection<Variable> ControlFlowVariables
        {
            get
            {
                if (_variables == null) _variables = new Collection<Variable>();
                return _variables;
            }
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {            
            metadata.SetVariablesCollection(ControlFlowVariables);
            metadata.SetChildrenCollection(ControlFlowChildActivities);            
            metadata.AddImplementationVariable(CurrentActivity);                        
            metadata.AddArgument(new RuntimeArgument("BookmarkName", typeof(string), ArgumentDirection.In));            
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            
            context.ScheduleActivity(ControlFlowChildActivities[0]);
            
            //if (Workflow != null)
            //{
            //    //var activity = ActivityXamlServices.Load(@"WorkflowTest.xaml");
                

            ////    // Create a bookmark for signaling the Goto
            ////    Bookmark internalBookmark = context.CreateBookmark(Goto,
            ////                                                       BookmarkOptions.MultipleResume |
            ////                                                       BookmarkOptions.NonBlocking);

            ////    //save the name of the bookmark as an execution property so that child activites can access it
            ////    context.Properties.Add(GotoPropertyName, internalBookmark);

            ////    CurrentActivity.Set(context, context.ScheduleActivity(ControlFlowChildActivities[0], OnChildCompleted,OnFaulted));

            ////    //create a bookmark for external (host) resumtion
            ////    if (BookmarkName.Get(context) != null)
            ////    {
            ////        context.CreateBookmark(BookmarkName.Get(context), Goto, BookmarkOptions.MultipleResume |
            ////                                                                BookmarkOptions.NonBlocking);
            ////    }
            //}
        }
        

        private void OnChildCompleted(NativeActivityContext context, ActivityInstance completedInstance)
        {                        
            if (completedInstance.State == ActivityInstanceState.Closed)
            {
                ////Calculate the index of the next child to schedule
                //var currentExecuting = ControlFlowChildActivities.IndexOf(completedInstance.Activity);
                //var next = currentExecuting + 1;

                //if (next < ControlFlowChildActivities.Count)
                //{
                //    CurrentActivity.Set(context, context.ScheduleActivity(ControlFlowChildActivities[next], OnChildCompleted));
                //}
            }
            
        }
    }
}
