﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="WFTest.aspx.cs" Inherits="ServiceFaults.WCF.WfTest" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <p>
        This sample illustrates various options for handling faults with WF</p>
    <h2>
        Try It
    </h2>
    <p>
        Invoke the service with a integer &lt; 0 to cause an exception on the server
        <br />
        Integer Value:
        <asp:TextBox ID="TextBoxValue" runat="server" Text="-1" /><asp:CompareValidator ID="CompareValidatorValue"
            runat="server" ErrorMessage="Please enter a valid integer" Operator="DataTypeCheck"
            Type="Integer" ControlToValidate="TextBoxValue" ForeColor="Red" />
        <br />
        <asp:LinkButton ID="ButtonInvokeWF" OnClick="ButtonInvokeServicesClick" runat="server">Invoke Service</asp:LinkButton>
    </p>
    <br />
    <asp:Table ID="TableWfResults" runat="server" Visible="False" CellPadding="2" GridLines="Both"
        BorderStyle="Solid">
        <asp:TableHeaderRow BorderStyle="Solid">
            <asp:TableHeaderCell BorderStyle="Solid" HorizontalAlign="Left">Scenario</asp:TableHeaderCell>
            <asp:TableHeaderCell BorderStyle="Solid" HorizontalAlign="Left">WF Results</asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell BorderStyle="Solid">Exception</asp:TableCell>
            <asp:TableCell ID="WfUhandledExceptionResult" BorderStyle="Solid">(No Results)</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BorderStyle="Solid">FaultException</asp:TableCell>
            <asp:TableCell ID="WfFaultExceptionResult" BorderStyle="Solid">(No Results)</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell BorderStyle="Solid">FaultException&lt;TDetail&gt;</asp:TableCell>
            <asp:TableCell ID="WfFaultExceptionDetailResult" BorderStyle="Solid">(No Results)</asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
