﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WCFService.svc.cs" company="Microsoft">
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceFaults.WCF
{
    using System;
    using System.ServiceModel;

    using ServiceFaults.WCF.BusinessLogic;
    using ServiceFaults.WCF.Contracts;
    using ServiceFaults.WCF.Properties;

    /// <summary>
    /// The WCF service.
    /// </summary>
    public class WcfService : IWcfService
    {
        #region Implemented Interfaces

        #region IWcfService

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="data">
        /// The int data.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The data is less than zero
        /// </exception>
        public string GetDataArgException(int data)
        {
            // TODO (01): Notice a service throwing an internal exception type for validation purposes - very bad
            if (data < 0)
            {
                throw new ArgumentOutOfRangeException("data", data, string.Format(Resources.ValueOutOfRange, "data", Resources.ValueGreaterThanZero));
            }

            return "Data: " + data;
        }

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="data">
        /// The int data.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        /// <exception cref="FaultException">
        /// The data is less than zero
        /// </exception>
        /// <remarks>
        /// By using a FaultException the client application will receive
        ///   detailed information about the source of the problem
        ///   even without the serviceDebug behavior
        /// </remarks>
        public string GetDataFaultException(int data)
        {
            // TODO (02): This is better because it throws a FaultException but the validation rule is in the service method
            if (data < 0)
            {
                // Let the sender know it is their problem
                // Workaround for creating a version aware FaultCode
                var faultCode =
                    FaultCodeFactory.CreateVersionAwareSenderFaultCode(
                        ContractFaultCodes.InvalidArgument.ToString(), Service.Namespace);

                var faultReason = string.Format(Resources.ValueOutOfRange, "Data", Resources.ValueGreaterThanZero);

                throw new FaultException(faultReason, faultCode);
            }

            return "Data: " + data;
        }

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        /// <remarks>
        /// Notice that this method does not contain any validation code.
        ///   The validation is handled by the GetDataRequest class which
        ///   will do validation when the property setter is invoked
        /// </remarks>
        public string GetDataWithArgValidation(GetDataRequest request)
        {
            // TODO (03): This is way better - no validation logic here - check GetDataRequest - run now and see the difference in the web UI

            // GetDataRequest and its members have already been validated
            return "Data: " + FooProcessor.ShowFooData((InternalFoo)request.Foo);
        }

        #endregion

        #endregion
    }
}