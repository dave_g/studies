﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WcfTest.aspx.cs" company="Microsoft">
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceFaults.WCF
{
    using System;
    using System.ServiceModel;
    using System.Web.UI;

    using ServiceFaults.WCF.Properties;
    using ServiceFaults.WCF.WCFServiceReference;

    // TODO (10): If you share the contracts assembly with the caller, validation will occur before the service is even called

    /// <summary>
    /// The default page class.
    /// </summary>
    /// <remarks>
    /// If you want to share validation logic using the contract types
    ///   1. Configure the service reference to re-use types from referenced assemblies
    ///   2. add using ServiceFaults.WCF.Contracts;
    /// </remarks>
    public partial class WcfTest : Page
    {
        #region Methods

        /// <summary>
        /// Invokes the WCF services
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        protected void ButtonInvokeServicesClick(object sender, EventArgs e)
        {
            int value;
            if (!int.TryParse(this.TextBoxValue.Text, out value))
            {
                return;
            }

            this.WcfUhandledExceptionResult.Text = InvokeWcfAndCatchException(value);
            this.WcfFaultExceptionResult.Text = InvokeWcfAndCatchFaultException(value);
            this.WcfFaultExceptionDetailResult.Text = InvokeWcfAndCatchFaultExceptionWithDetail(value);
            this.TableWcfResults.Visible = true;
        }

        /// <summary>
        /// Gets a formatted error text from the fault exception
        /// </summary>
        /// <param name="faultException">
        /// The fault exception.
        /// </param>
        /// <returns>
        /// The fault exception error text.
        /// </returns>
        private static string GetFaultExceptionDetailErrText(FaultException<ArgumentValidationFault> faultException)
        {
            if (faultException.Detail != null)
            {
                // TODO (09): Because we used a FaultException<TDetail> we can get better information about the fault from the server
                // Note: You cannot use faultException.HelpLink here
                // It is never serialized (along with other parts of the base class Exception)
                // Instead you can pass a HelpLink in the Detail
                return string.Format(
                    Resources.ArgInvalidWebUIFormat, 
                    faultException.Code.IsSenderFault, 
                    faultException.Code.Name, 
                    faultException.Detail.ArgumentName, 
                    faultException.Detail.ActualValue, 
                    faultException.Detail.ValidationRule, 
                    faultException.Reason, 
                    faultException.Detail.HelpLink);
            }

            return GetFaultExceptionErrText(faultException);
        }

        /// <summary>
        /// Gets a formatted error text from the fault exception
        /// </summary>
        /// <param name="faultException">
        /// The fault exception.
        /// </param>
        /// <returns>
        /// The fault exception error text.
        /// </returns>
        private static string GetFaultExceptionErrText(FaultException faultException)
        {
            return string.Format(
                Resources.ExceptionFormat, 
                faultException.Code.IsSenderFault, 
                faultException.Code.Name, 
                faultException.Reason);
        }

        /// <summary>
        /// Invokes the WCF service and catches a communication exception
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The response from the service or the exception text
        /// </returns>
        private static string InvokeWcfAndCatchException(int value)
        {
            var proxy = new WcfServiceClient();

            try
            {
                return proxy.GetDataArgException(value);
            }
            catch (FaultException faultException)
            {
                proxy.Abort();
                return string.Format(
                    Resources.ExceptionFormat, 
                    faultException.Code.IsSenderFault, 
                    faultException.Code.Name, 
                    faultException.Reason);
            }
            finally
            {
                proxy.Close();
            }
        }

        /// <summary>
        /// Invokes the WCF service and catches a FaultException
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The response from the service or the exception text
        /// </returns>
        private static string InvokeWcfAndCatchFaultException(int value)
        {
            var proxy = new WcfServiceClient();

            try
            {
                return proxy.GetDataFaultException(value);
            }
            catch (FaultException faultException)
            {
                proxy.Abort();
                return GetFaultExceptionErrText(faultException);
            }
            finally
            {
                proxy.Close();
            }
        }

        /// <summary>
        /// Invokes the WCF service and catches a FaultException(of TDetail)
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The response from the service or the exception text
        /// </returns>
        private static string InvokeWcfAndCatchFaultExceptionWithDetail(int value)
        {
            var proxy = new WcfServiceClient();

            try
            {
                // The server object GetDataRequest does validation when you
                // set the value and will throw a FaultException when that happens
                // In some cases this is a great thing.
                // var request = new GetDataRequest { Data = value };

                // For this sample code we are using a class that will 
                // not validate on the client side so we can see
                // the server throw the fault
                return proxy.GetDataWithArgValidation(new GetDataRequest { Foo = new Foo { Data = value } });
            }
            catch (FaultException<ArgumentValidationFault> faultException)
            {
                proxy.Abort();
                return GetFaultExceptionDetailErrText(faultException);
            }
            finally
            {
                proxy.Close();
            }
        }

        #endregion
    }
}