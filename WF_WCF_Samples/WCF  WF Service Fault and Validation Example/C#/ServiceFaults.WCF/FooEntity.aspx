﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Foo Entity Help</h1>
    <p>
        The Foo Entity is an example of a DataContract type that has some simple validation rules</p>
    <h3>
        Validation Rules
    </h3>
    <ul>
    <li>Data must be greater than zero
    </li>
    <li>Text must be empty or at least 2 characters in length</li>
    <li>If the text is not empty, Data must be less than the length of Text</li>
    </ul>
</asp:Content>
