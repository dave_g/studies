﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.372
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceFaults.WCF.WFServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetDataRequest", Namespace="Microsoft.ServiceModel.Samples.ServiceFaults")]
    [System.SerializableAttribute()]
    public partial class GetDataRequest : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private ServiceFaults.WCF.WFServiceReference.Foo FooField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SomeOtherFieldField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> TheDateField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ServiceFaults.WCF.WFServiceReference.Foo Foo {
            get {
                return this.FooField;
            }
            set {
                if ((object.ReferenceEquals(this.FooField, value) != true)) {
                    this.FooField = value;
                    this.RaisePropertyChanged("Foo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SomeOtherField {
            get {
                return this.SomeOtherFieldField;
            }
            set {
                if ((object.ReferenceEquals(this.SomeOtherFieldField, value) != true)) {
                    this.SomeOtherFieldField = value;
                    this.RaisePropertyChanged("SomeOtherField");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> TheDate {
            get {
                return this.TheDateField;
            }
            set {
                if ((this.TheDateField.Equals(value) != true)) {
                    this.TheDateField = value;
                    this.RaisePropertyChanged("TheDate");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Foo", Namespace="Microsoft.ServiceModel.Samples.ServiceFaults")]
    [System.SerializableAttribute()]
    public partial class Foo : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int DataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TextField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Data {
            get {
                return this.DataField;
            }
            set {
                if ((this.DataField.Equals(value) != true)) {
                    this.DataField = value;
                    this.RaisePropertyChanged("Data");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Text {
            get {
                return this.TextField;
            }
            set {
                if ((object.ReferenceEquals(this.TextField, value) != true)) {
                    this.TextField = value;
                    this.RaisePropertyChanged("Text");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ArgumentValidationFault", Namespace="Microsoft.ServiceModel.Samples.ServiceFaults")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceFaults.WCF.WFServiceReference.GetDataRequest))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(ServiceFaults.WCF.WFServiceReference.Foo))]
    public partial class ArgumentValidationFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string ArgumentNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string HelpLinkField;
        
        private string ValidationRuleField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private object ActualValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string ArgumentName {
            get {
                return this.ArgumentNameField;
            }
            set {
                if ((object.ReferenceEquals(this.ArgumentNameField, value) != true)) {
                    this.ArgumentNameField = value;
                    this.RaisePropertyChanged("ArgumentName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string HelpLink {
            get {
                return this.HelpLinkField;
            }
            set {
                if ((object.ReferenceEquals(this.HelpLinkField, value) != true)) {
                    this.HelpLinkField = value;
                    this.RaisePropertyChanged("HelpLink");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string ValidationRule {
            get {
                return this.ValidationRuleField;
            }
            set {
                if ((object.ReferenceEquals(this.ValidationRuleField, value) != true)) {
                    this.ValidationRuleField = value;
                    this.RaisePropertyChanged("ValidationRule");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=3)]
        public object ActualValue {
            get {
                return this.ActualValueField;
            }
            set {
                if ((object.ReferenceEquals(this.ActualValueField, value) != true)) {
                    this.ActualValueField = value;
                    this.RaisePropertyChanged("ActualValue");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="\"Microsoft.ServiceModel.Samples.ServiceFaults\"", ConfigurationName="WFServiceReference.IWfService")]
    public interface IWfService {
        
        // CODEGEN: Generating message contract since the operation GetDataArgException is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataArgException", ReplyAction="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataArgExceptionResp" +
            "onse")]
        ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionResponse GetDataArgException(ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionRequest request);
        
        // CODEGEN: Generating message contract since the operation GetDataFaultException is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataFaultException", ReplyAction="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataFaultExceptionRe" +
            "sponse")]
        ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionResponse GetDataFaultException(ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionRequest request);
        
        // CODEGEN: Generating message contract since the operation GetDataWithArgValidation is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataWithArgValidatio" +
            "n", ReplyAction="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataWithArgValidatio" +
            "nResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(ServiceFaults.WCF.WFServiceReference.ArgumentValidationFault), Action="\"Microsoft.ServiceModel.Samples.ServiceFaults\"/IWfService/GetDataWithArgValidatio" +
            "nArgumentValidationFaultFault", Name="ArgumentValidationFault", Namespace="Microsoft.ServiceModel.Samples.ServiceFaults")]
        ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationResponse GetDataWithArgValidation(ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataArgExceptionRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public System.Nullable<int> @int;
        
        public GetDataArgExceptionRequest() {
        }
        
        public GetDataArgExceptionRequest(System.Nullable<int> @int) {
            this.@int = @int;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataArgExceptionResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public string @string;
        
        public GetDataArgExceptionResponse() {
        }
        
        public GetDataArgExceptionResponse(string @string) {
            this.@string = @string;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataFaultExceptionRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public System.Nullable<int> @int;
        
        public GetDataFaultExceptionRequest() {
        }
        
        public GetDataFaultExceptionRequest(System.Nullable<int> @int) {
            this.@int = @int;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataFaultExceptionResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public string @string;
        
        public GetDataFaultExceptionResponse() {
        }
        
        public GetDataFaultExceptionResponse(string @string) {
            this.@string = @string;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataWithArgValidationRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="Microsoft.ServiceModel.Samples.ServiceFaults", Order=0)]
        public ServiceFaults.WCF.WFServiceReference.GetDataRequest GetDataRequest;
        
        public GetDataWithArgValidationRequest() {
        }
        
        public GetDataWithArgValidationRequest(ServiceFaults.WCF.WFServiceReference.GetDataRequest GetDataRequest) {
            this.GetDataRequest = GetDataRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetDataWithArgValidationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/", Order=0)]
        public string @string;
        
        public GetDataWithArgValidationResponse() {
        }
        
        public GetDataWithArgValidationResponse(string @string) {
            this.@string = @string;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWfServiceChannel : ServiceFaults.WCF.WFServiceReference.IWfService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WfServiceClient : System.ServiceModel.ClientBase<ServiceFaults.WCF.WFServiceReference.IWfService>, ServiceFaults.WCF.WFServiceReference.IWfService {
        
        public WfServiceClient() {
        }
        
        public WfServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WfServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WfServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WfServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionResponse ServiceFaults.WCF.WFServiceReference.IWfService.GetDataArgException(ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionRequest request) {
            return base.Channel.GetDataArgException(request);
        }
        
        public string GetDataArgException(System.Nullable<int> @int) {
            ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionRequest inValue = new ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionRequest();
            inValue.@int = @int;
            ServiceFaults.WCF.WFServiceReference.GetDataArgExceptionResponse retVal = ((ServiceFaults.WCF.WFServiceReference.IWfService)(this)).GetDataArgException(inValue);
            return retVal.@string;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionResponse ServiceFaults.WCF.WFServiceReference.IWfService.GetDataFaultException(ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionRequest request) {
            return base.Channel.GetDataFaultException(request);
        }
        
        public string GetDataFaultException(System.Nullable<int> @int) {
            ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionRequest inValue = new ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionRequest();
            inValue.@int = @int;
            ServiceFaults.WCF.WFServiceReference.GetDataFaultExceptionResponse retVal = ((ServiceFaults.WCF.WFServiceReference.IWfService)(this)).GetDataFaultException(inValue);
            return retVal.@string;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationResponse ServiceFaults.WCF.WFServiceReference.IWfService.GetDataWithArgValidation(ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationRequest request) {
            return base.Channel.GetDataWithArgValidation(request);
        }
        
        public string GetDataWithArgValidation(ServiceFaults.WCF.WFServiceReference.GetDataRequest GetDataRequest) {
            ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationRequest inValue = new ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationRequest();
            inValue.GetDataRequest = GetDataRequest;
            ServiceFaults.WCF.WFServiceReference.GetDataWithArgValidationResponse retVal = ((ServiceFaults.WCF.WFServiceReference.IWfService)(this)).GetDataWithArgValidation(inValue);
            return retVal.@string;
        }
    }
}
