﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ThrowSenderFault.cs" company="Microsoft">
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceFaults.WCF.Activities
{
    using System.Activities;
    using System.ServiceModel;

    using ServiceFaults.WCF.Contracts;

    /// <summary>
    /// The throw sender fault exception.
    /// </summary>
    public sealed class ThrowSenderFault : CodeActivity
    {
        #region Properties

        /// <summary>
        ///   Gets or sets Reason.
        /// </summary>
        public InArgument<string> Reason { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="FaultException">
        /// </exception>
        protected override void Execute(CodeActivityContext context)
        {
            var faultReason = context.GetValue(this.Reason);

            // Let the sender know it is their problem
            // Workaround for creating a version aware FaultCode
            var faultCode =
                FaultCodeFactory.CreateVersionAwareSenderFaultCode(
                    ContractFaultCodes.InvalidArgument.ToString(), Service.Namespace);

            throw new FaultException(faultReason, faultCode);
        }

        #endregion
    }
}