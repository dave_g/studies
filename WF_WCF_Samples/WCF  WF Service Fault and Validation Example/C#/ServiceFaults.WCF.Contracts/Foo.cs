﻿namespace ServiceFaults.WCF.Contracts
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Runtime.Serialization;
    using System.ServiceModel;

    using ServiceFaults.WCF.Contracts.Properties;

    /// <summary>
    ///   The foo data contract
    /// </summary>
    /// <remarks>
    ///   This type is used at the service boundary and delegates the validation
    ///   to it's internal type.  It converts argument exceptions into fault exceptions
    ///   and throws them.
    ///   ----
    ///   Be sure to follow Best Practices: Data Contract Versioning
    ///   http://msdn.microsoft.com/en-us/library/ms733832.aspx
    /// </remarks>
    [DataContract(Namespace = Service.Namespace)]
    public class Foo : IFoo, IExtensibleDataObject
    {
        #region Constants and Fields

        /// <summary>
        ///   The version 1.
        /// </summary>
        /// <remarks>
        ///   Describes properties that were a part since version 1
        /// </remarks>
        private const int Version1 = 1;

        /// <summary>
        ///   The data value.
        /// </summary>
        private int data;

        /// <summary>
        ///   The text value.
        /// </summary>
        private string text;

        #endregion

        #region Properties

        #region IExtensibleDataObject Members

        /// <summary>
        ///   Gets or sets ExtensionData.
        /// </summary>
        public virtual ExtensionDataObject ExtensionData { get; set; }

        #endregion

        #region IFoo Members

        /// <summary>
        ///   Gets or sets Data.
        /// </summary>
        [DataMember(IsRequired = false, Order = Version1)]
        public int Data
        {
            get
            {
                return this.data;
            }

            set
            {
                try
                {
                    Contract.Requires(FooValidator.IsValidDataValue(value));

                    // TODO (06): Notice how the Foo type delegates the validation to FooValidator

                    // Validate the value to be set before setting it
                    FooValidator.ValidateDataValue(value);

                    this.data = value;

                    // After set, validate the overall state of the object
                    // Some validation rules depend on more than one member
                    FooValidator.ValidateState(this, "Data");
                }
                catch (ArgumentException exception)
                {
                    // TODO (08): Foo is a DataContract type so we must convert the exception into a fault
                    // The validator throws internal exception types
                    // At the boundary convert these into FaultExceptions
                    // That can be used with a FaultContract
                    throw CreateFaultFromException(exception);
                }
            }
        }

        /// <summary>
        ///   Gets or sets Text.
        /// </summary>
        [DataMember(IsRequired = false, Order = Version1)]
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                try
                {
                    // Validate the value to be set before setting it
                    FooValidator.ValidateTextValue(value);

                    this.text = value;

                    // After set, validate the overall state of the object
                    // Some validation rules depend on more than one member
                    FooValidator.ValidateState(this, "Text");
                }
                catch (ArgumentException exception)
                {
                    // The validator throws internal exception types
                    // At the boundary convert these into FaultExceptions
                    // That can be used with a FaultContract
                    throw CreateFaultFromException(exception);
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///   The create fault from exception.
        /// </summary>
        /// <param name = "exception">
        ///   The exception.
        /// </param>
        /// <returns>
        ///   An initialized Fault exception
        /// </returns>
        private static FaultException<ArgumentValidationFault> CreateFaultFromException(ArgumentException exception)
        {
            // This is a sender fault
            var faultCode =
                FaultCodeFactory.CreateVersionAwareSenderFaultCode(
                    ContractFaultCodes.InvalidArgument.ToString(), Service.Namespace);

            // Filter on specific exceptions thrown from InternalFoo
            if (exception is ArgumentOutOfRangeException)
            {
                return new FaultException<ArgumentValidationFault>(
                    GetValidationFaultForParam(exception),
                    string.Format(Resources.ValueOutOfRange, exception.ParamName),
                    faultCode);
            }

            // Text is null or empty
            if (exception is ArgumentNullException && exception.ParamName == "Text")
            {
                return new FaultException<ArgumentValidationFault>(
                    GetValidationFaultForParam(exception),
                    string.Format(Resources.ValueIsNullOrEmpty, exception.ParamName),
                    faultCode);
            }

            // Some other kind of failure
            return new FaultException<ArgumentValidationFault>(
                GetValidationFaultForParam(exception),
                string.Format(Resources.ArgumentInvalid, exception.ParamName),
                faultCode);
        }

        /// <summary>
        ///   The get validation fault for param.
        /// </summary>
        /// <param name = "exception">
        ///   The exception.
        /// </param>
        /// <returns>
        ///   An argument validation fault
        /// </returns>
        private static ArgumentValidationFault GetValidationFaultForParam(ArgumentException exception)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(exception.ParamName));

            switch (exception.ParamName)
            {
                case "Data":
                    var fault = new ArgumentValidationFault
                        {
                            ArgumentName = exception.ParamName,
                            ValidationRule = Resources.ValueGreaterThanZero,
                            HelpLink = LinkHelper.GetAbsoluteUriHelpLink("FooEntity.aspx"),
                        };
                    if (exception is ArgumentOutOfRangeException)
                    {
                        fault.ActualValue = ((ArgumentOutOfRangeException)exception).ActualValue;
                    }

                    return fault;
                case "Text":
                    return new ArgumentValidationFault
                        {
                            ArgumentName = exception.ParamName,
                            ValidationRule = Resources.FooText_must_be_null__empty_or_a_string_longer_than_1_character,
                            HelpLink = LinkHelper.GetAbsoluteUriHelpLink("FooEntity.aspx"),
                        };
                default:
                    return null;
            }
        }

        #endregion
    }
}