﻿namespace ServiceFaults.WCF.Contracts
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The request parameter for the GetData service method
    /// </summary>
    /// <remarks>
    /// Be sure to follow Best Practices: Data Contract Versioning
    ///   http://msdn.microsoft.com/en-us/library/ms733832.aspx
    /// </remarks>
    [DataContract(Namespace = Service.Namespace)]
    public class GetDataRequest : IExtensibleDataObject
    {
        #region Constants and Fields

        /// <summary>
        /// The version 1.
        /// </summary>
        /// <remarks>
        /// Describes properties that were a part since version 1
        /// </remarks>
        private const int Version1 = 1;

        /// <summary>
        /// The version 2.
        /// </summary>
        /// <remarks>
        /// Describes properties that were a part since version 2
        /// </remarks>
        private const int Version2 = 2;

        #endregion

        #region Properties

        /// <summary>
        ///   Gets or sets ExtensionData.
        /// </summary>
        public virtual ExtensionDataObject ExtensionData { get; set; }

        // TODO (04): Notice how the request message uses a Foo here

        /// <summary>
        ///   Gets or sets Data.
        /// </summary>
        [DataMember(Order = Version1)]
        public Foo Foo { get; set; }

        /// <summary>
        ///   Gets or sets SomeOtherField.
        /// </summary>
        [DataMember(Order = Version1)]
        public string SomeOtherField { get; set; }

        /// <summary>
        ///   Gets or sets TheDate
        /// </summary>
        /// <remarks>
        ///   This was added in V2.  It is not required
        ///   this allows V1 clients to ignore it
        /// </remarks>
        [DataMember(Order = Version2, IsRequired = false)]
        public DateTime? TheDate { get; set; }

        #endregion
    }
}