﻿namespace ServiceFaults.WCF.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Web;

    /// <summary>
    /// The link helper.
    /// </summary>
    internal static class LinkHelper
    {
        #region Methods

        /// <summary>
        /// Returns a help link assuming that the help page is at the same base URI as the service
        /// </summary>
        /// <param name="helpPage">
        /// The help page.
        /// </param>
        /// <returns>
        /// An absolute URI to the help page
        /// </returns>
        internal static string GetAbsoluteUriHelpLink(string helpPage)
        {
            Uri requestUri = null;

            // This may be called from the client (when constructing an argument)
            // or called from the server when processing a request
            // either way you need to obtain a URL of the current request
            if (OperationContext.Current != null)
            {
                requestUri = OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri;
            }
            else if (HttpContext.Current != null)
            {
                requestUri = HttpContext.Current.Request.Url;
            }

            var uriBuilder = requestUri == null ? new UriBuilder() : new UriBuilder(requestUri);

            uriBuilder.Path = helpPage;

            return uriBuilder.Uri.ToString();
        }

        #endregion
    }
}