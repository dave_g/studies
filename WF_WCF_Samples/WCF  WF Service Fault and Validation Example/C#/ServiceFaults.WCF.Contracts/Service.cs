﻿namespace ServiceFaults.WCF.Contracts
{
    /// <summary>
    ///   The contract constants.
    /// </summary>
    public static class Service
    {
        #region Constants and Fields

        /// <summary>
        ///   The namespace.
        /// </summary>
        public const string Namespace = "Microsoft.ServiceModel.Samples.ServiceFaults";

        #endregion
    }
}