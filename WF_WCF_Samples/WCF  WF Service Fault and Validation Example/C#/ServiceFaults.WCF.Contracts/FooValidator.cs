﻿namespace ServiceFaults.WCF.Contracts
{
    using System;

    using ServiceFaults.WCF.Contracts.Properties;

    /// <summary>
    ///   Provides validation for the IFoo interface
    /// </summary>
    /// <remarks>
    ///   Types that implement IFoo should delegate validation of properties to this class
    /// </remarks>
    public static class FooValidator
    {
        #region Public Methods

        /// <summary>
        ///   The is valid data value.
        /// </summary>
        /// <param name = "value">
        ///   The value.
        /// </param>
        /// <returns>
        ///   true if the data value is valid
        /// </returns>
        public static bool IsValidDataValue(int value)
        {
            return value >= 0;
        }

        /// <summary>
        ///   The validate data value.
        /// </summary>
        /// <param name = "data">
        ///   The data value.
        /// </param>
        /// <exception cref = "ArgumentOutOfRangeException">
        /// </exception>
        public static void ValidateDataValue(int data)
        {
            // TODO (07): FooValidator always throws internal exception types making it safe to use anywhere
            if (!IsValidDataValue(data))
            {
                throw new ArgumentOutOfRangeException("Data", data, Resources.FooData_must_not_be_less_than_zero);
            }
        }

        /// <summary>
        ///   The validate state.
        /// </summary>
        /// <param name = "foo">
        ///   The foo instance.
        /// </param>
        /// <param name = "propName">
        ///   The prop Name.
        /// </param>
        /// <exception cref = "InvalidOperationException">
        /// </exception>
        public static void ValidateState(IFoo foo, string propName)
        {
            // Cross member validation
            if (!string.IsNullOrWhiteSpace(foo.Text))
            {
                if (foo.Text.Length < foo.Data)
                {
                    throw new ArgumentException(
                        Resources.FooText_Length_must_be_greater_than_or_equal_to_the_value_of_Data, propName);
                }
            }
        }

        /// <summary>
        ///   The validate text value.
        /// </summary>
        /// <param name = "text">
        ///   The value.
        /// </param>
        /// <exception cref = "ArgumentNullException">
        ///   The text is null or empty
        /// </exception>
        /// <exception cref = "ArgumentException">
        ///   The text is length 1
        /// </exception>
        public static void ValidateTextValue(string text)
        {
            // Text can be null but cannot be length 1
            if (text != null && text.Length == 1)
            {
                throw new ArgumentException(
                    Resources.FooText_must_be_null__empty_or_a_string_longer_than_1_character, "Text");
            }
        }

        #endregion
    }
}