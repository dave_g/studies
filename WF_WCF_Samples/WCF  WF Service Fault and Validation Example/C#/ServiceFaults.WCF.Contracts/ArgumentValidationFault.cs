﻿namespace ServiceFaults.WCF.Contracts
{
    using System.Runtime.Serialization;

    /// <summary>
    ///   The argument validation fault.
    /// </summary>
    /// <remarks>
    ///   Be sure to follow Best Practices: Data Contract Versioning
    ///   http://msdn.microsoft.com/en-us/library/ms733832.aspx
    /// </remarks>
    [DataContract(Namespace = Service.Namespace)]
    public class ArgumentValidationFault : IExtensibleDataObject
    {
        #region Constants and Fields

        /// <summary>
        ///   The version 1.
        /// </summary>
        /// <remarks>
        ///   Describes properties that were a part since version 1
        /// </remarks>
        private const int Version1 = 1;

        /// <summary>
        ///   The version 2.
        /// </summary>
        private const int Version2 = 2;

        #endregion

        #region Properties

        /// <summary>
        ///   Gets or sets ActualValue.
        /// </summary>
        [DataMember(IsRequired = false, Order = Version2)]
        public object ActualValue { get; set; }

        /// <summary>
        ///   Gets or sets the name of the Argument that failed validation.
        /// </summary>
        [DataMember(IsRequired = true, Order = Version1)]
        public string ArgumentName { get; set; }

        /// <summary>
        ///   Gets or sets HelpLink.
        /// </summary>
        /// <remarks>
        ///   Exception.HelpLink cannot be used with FaultExceptions because it will not be serialized on the wire
        /// </remarks>
        [DataMember(IsRequired = false, Order = Version1)]
        public string HelpLink { get; set; }

        /// <summary>
        ///   Gets or sets text describing the validation rule.
        /// </summary>
        [DataMember(IsRequired = true, Order = Version1)]
        public string ValidationRule { get; set; }

        /// <summary>
        ///   Gets or sets ExtensionData.
        /// </summary>
        public virtual ExtensionDataObject ExtensionData { get; set; }

        #endregion
    }
}