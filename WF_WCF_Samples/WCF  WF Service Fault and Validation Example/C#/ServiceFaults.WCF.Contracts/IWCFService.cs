﻿namespace ServiceFaults.WCF.Contracts
{
    using System.ServiceModel;

    /// <summary>
    /// The WCF service interface.
    /// </summary>
    [ServiceContract(Namespace = Service.Namespace)]
    public interface IWcfService
    {
        #region Public Methods

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="data">
        /// The data value.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        [OperationContract]
        string GetDataArgException(int data);

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="data">
        /// The int data.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        /// <exception cref="FaultException">
        /// The data is less than zero
        /// </exception>
        /// <remarks>
        /// By using a FaultException the client application will receive
        ///   detailed information about the source of the problem
        ///   even without the serviceDebug behavior
        /// </remarks>
        [OperationContract]
        string GetDataFaultException(int data);

        /// <summary>
        /// Returns a string based on the data you send
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// A string that contains the data
        /// </returns>
        /// <remarks>
        /// Notice that this method does not contain any validation code.
        ///   The validation is handled by the GetDataRequest class which
        ///   will do validation when the property setter is invoked
        /// </remarks>
        [OperationContract]
        [FaultContract(typeof(ArgumentValidationFault))]
        string GetDataWithArgValidation(GetDataRequest request);

        #endregion
    }
}