﻿namespace ServiceFaults.WCF.Contracts
{
    /// <summary>
    ///   The i foo.
    /// </summary>
    public interface IFoo
    {
        #region Properties

        /// <summary>
        ///   Gets or sets Data.
        /// </summary>
        int Data { get; set; }

        /// <summary>
        ///   Gets or sets Text.
        /// </summary>
        string Text { get; set; }

        #endregion
    }
}