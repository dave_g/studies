﻿namespace ServiceFaults.WCF.Contracts
{
    /// <summary>
    ///   The contract fault codes.
    /// </summary>
    public enum ContractFaultCodes
    {
        /// <summary>
        ///   One or more arguments was invalid, see details for more
        /// </summary>
        InvalidArgument,
    }
}