﻿namespace ServiceFaults.WCF.Contracts
{
    using System;
    using System.ServiceModel;

    /// <summary>
    ///   Extension methods for FaultCode
    /// </summary>
    public static class FaultCodeFactory
    {
        #region Public Methods

        /// <summary>
        ///   Creates a SOAP version aware Fault Code
        /// </summary>
        /// <param name = "name">
        ///   The sub faultcode name.
        /// </param>
        /// <param name = "nameSpace">
        ///   The sub faultcode namespace.
        /// </param>
        /// <returns>
        ///   A Sender fault appropriate for SOAP 1.1 or SOAP 1.2
        /// </returns>
        /// <remarks>
        ///   From the SOAP 1.1 Spec http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383510
        ///   The FaultCode "Client"
        ///   The Client class of errors indicate that the message was incorrectly formed or did not contain the appropriate 
        ///   information in order to succeed. For example, the message could lack the proper authentication or payment information. 
        ///   It is generally an indication that the message should not be resent without change. 
        ///   ---
        ///   From the SOAP 1.2 Spec http://www.w3.org/TR/soap12-part1/#tabsoapfaultcodes
        ///   The FaultCode "Sender" 
        ///   The message was incorrectly formed or did not contain the appropriate information in order to succeed. 
        ///   For example, the message could lack the proper authentication or payment information. 
        ///   It is generally an indication that the message is not to be resent without change 
        ///   (see also 5.4 SOAP Fault for a description of the SOAP fault detail sub-element).
        /// </remarks>
        public static FaultCode CreateVersionAwareSenderFaultCode(string name, string nameSpace)
        {
            // Not invoked from WCF
            if (OperationContext.Current == null)
            {
                // Default to SOAP 1.1
                return new FaultCode("Client");
            }

            if (OperationContext.Current.IncomingMessageHeaders.MessageVersion.Envelope == EnvelopeVersion.Soap11)
            {
                // BasicHttpBinding uses SOAP 1.1
                return new FaultCode("Client");
            }

            if (OperationContext.Current.IncomingMessageHeaders.MessageVersion.Envelope == EnvelopeVersion.Soap12)
            {
                // Other bindings use SOAP 1.2
                return FaultCode.CreateSenderFaultCode(name, nameSpace);
            }
            throw new InvalidOperationException("Unknown SOAP version");
        }

        #endregion
    }
}