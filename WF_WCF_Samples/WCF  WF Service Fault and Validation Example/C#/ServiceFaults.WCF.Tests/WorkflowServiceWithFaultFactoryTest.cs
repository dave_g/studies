﻿namespace ServiceFaults.WCF.Tests
{
    using System;
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///   This is a test class for WorkflowServiceWithFaultFactoryTest and is intended
    ///   to contain all WorkflowServiceWithFaultFactoryTest Unit Tests
    /// </summary>
    [TestClass]
    public class WorkflowServiceWithFaultFactoryTest
    {
        #region Properties

        /// <summary>
        ///   Gets or sets the test context which provides
        ///   information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #endregion

        #region Public Methods

        [TestMethod]
        public void SplitTest()
        {
            var name = this.GetXmlQualifiedName("{Microsoft.ServiceModel.Samples.ServiceFaults}IWfService");
            Assert.AreEqual("Microsoft.ServiceModel.Samples.ServiceFaults", name.Namespace);
            Assert.AreEqual("IWfService", name.Name);
        }

        [TestMethod]
        public void SplitTestBadName()
        {
            var name = this.GetXmlQualifiedName("Microsoft.ServiceModel.Samples.ServiceFaults}IWfService");
            Assert.AreEqual("Microsoft.ServiceModel.Samples.ServiceFaults", name.Namespace);
            Assert.AreEqual("IWfService", name.Name);
        }

        [TestMethod]
        public void SplitTestOnlyName()
        {
            var name = this.GetXmlQualifiedName("IWfService");
            Assert.IsTrue(string.IsNullOrWhiteSpace(name.Namespace));
            Assert.AreEqual("IWfService", name.Name);
        }

        public XmlQualifiedName GetXmlQualifiedName(string contractName)
        {
            var args = contractName.Split(new[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries);
            switch (args.Length)
            {
                case 1:
                    return new XmlQualifiedName(args[0]);
                case 2:
                    return new XmlQualifiedName(args[1], args[0]);
                default:
                    throw new ArgumentException(string.Format("Cannot parse contract name from {0}", contractName));
            }
        }

        #endregion
    }
}