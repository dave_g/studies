﻿namespace ServiceFaults.WCF.Tests
{
    using System.ServiceModel;

    using Microsoft.Activities.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using ServiceFaults.WCF.Contracts;

    /// <summary>
    ///   This is a test class for FooTest and is intended
    ///   to contain all FooTest Unit Tests
    /// </summary>
    [TestClass]
    public class FooTest
    {
        #region Properties

        /// <summary>
        ///   Gets or sets the test context which provides
        ///   information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an Foo is constructed with no arguments
        ///   Then
        ///   * It should not throw an exception
        /// </summary>
        [TestMethod]
        public void WhenFooIsConstructedNoArgsItShouldNotThrow()
        {
            // Validates default constructor
            var target = new Foo();
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an Foo is constructed with valid arguments
        ///   Then
        ///   * It should not throw an exception
        /// </summary>
        [TestMethod]
        public void WhenFooIsConstructedValidArgsItShouldNotThrow()
        {
            var target = new Foo { Data = 3, Text = "1234" };
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an Foo is constructed with Text shorter than Data
        ///   Then
        ///   * It should throw an argument exception
        /// </summary>
        [TestMethod]
        public void WhenFooIsConstructedWithTextShorterThanDataItShouldThrow()
        {
            AssertHelper.Throws<FaultException<ArgumentValidationFault>>(() => new Foo { Data = 3, Text = "12" });
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an Foo is constructed with a Text of 1 character
        ///   Then
        ///   * It should throw an argument exception
        /// </summary>
        [TestMethod]
        public void WhenFooIsConstructedWithOneCharTextItShouldThrow()
        {
            AssertHelper.Throws<FaultException<ArgumentValidationFault>>(() => new Foo { Text = "1" });
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an Foo is constructed with invalid Data
        ///   Then
        ///   * It should throw an ArgumentOutOfRangeException
        /// </summary>
        [TestMethod]
        public void WhenFooIsConstructedWithInvalidDataItShouldThrow()
        {
            AssertHelper.Throws<FaultException<ArgumentValidationFault>>(() => new Foo { Data = -1, Text = "12" });
        }

        #endregion
    }
}