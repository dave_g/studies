﻿namespace ServiceFaults.WCF.Tests
{
    using System;

    using Microsoft.Activities.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using ServiceFaults.WCF.BusinessLogic;

    /// <summary>
    ///   This is a test class for InternalFooTest and is intended
    ///   to contain all InternalFooTest Unit Tests
    /// </summary>
    [TestClass]
    public class InternalFooTest
    {
        #region Properties

        /// <summary>
        ///   Gets or sets the test context which provides
        ///   information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an InternalFoo is constructed with no arguments
        ///   Then
        ///   * It should not throw an exception
        /// </summary>
        [TestMethod]
        public void WhenInternalFooIsConstructedNoArgsItShouldNotThrow()
        {
            // Validates default constructor
            var target = new InternalFoo();
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an InternalFoo is constructed with valid arguments
        ///   Then
        ///   * It should not throw an exception
        /// </summary>
        [TestMethod]
        public void WhenInternalFooIsConstructedValidArgsItShouldNotThrow()
        {
            var target = new InternalFoo { Data = 3, Text = "1234" };
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an InternalFoo is constructed with Text shorter than Data
        ///   Then
        ///   * It should throw an argument exception
        /// </summary>
        [TestMethod]
        public void WhenInternalFooIsConstructedWithTextShorterThanDataItShouldThrow()
        {
            AssertHelper.Throws<ArgumentException>(() => new InternalFoo { Data = 3, Text = "12" });
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an InternalFoo is constructed with one char Text 
        ///   Then
        ///   * It should throw an argument exception
        /// </summary>
        [TestMethod]
        public void WhenInternalFooIsConstructedWithOneCharTextItShouldThrow()
        {
            AssertHelper.Throws<ArgumentException>(() => new InternalFoo { Text = "1" });
        }

        /// <summary>
        ///   Given
        ///   * nothing
        ///   When
        ///   * an InternalFoo is constructed with invalid Data
        ///   Then
        ///   * It should throw an ArgumentOutOfRangeException
        /// </summary>
        [TestMethod]
        public void WhenInternalFooIsConstructedWithInvalidDataItShouldThrow()
        {
            AssertHelper.Throws<ArgumentOutOfRangeException>(() => new InternalFoo { Data = -1, Text = "12" });
        }

        #endregion
    }
}