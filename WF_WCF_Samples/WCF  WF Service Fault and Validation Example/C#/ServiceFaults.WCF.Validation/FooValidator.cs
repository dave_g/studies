﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FooValidator.cs" company="Microsoft">
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceFaults.WCF.Validation
{
    using System;

    using ServiceFaults.WCF.Contracts;

    /// <summary>
    /// Provides validation for the IFoo interface
    /// </summary>
    /// <remarks>
    /// Types that implement IFoo should delegate validation of properties to this class
    /// </remarks>
    public static class FooValidator
    {
        #region Public Methods

        /// <summary>
        /// The validate data value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        public static void ValidateDataValue(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException("value", "Foo.Data must not be less than zero");
            }
        }

        /// <summary>
        /// The validate text value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// The text is null or empty
        /// </exception>
        /// <exception cref="ArgumentException">
        /// The text is length 1
        /// </exception>
        public static void ValidateTextValue(string value)
        {
            // Text cannot be null or empty
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("Text");
            }

            // Text cannot be length 1
            if (value.Length == 1)
            {
                throw new ArgumentException("Text must be null, empty or a string longer than 1 character", "Text");
            }
        }

        /// <summary>
        /// The validate state.
        /// </summary>
        /// <param name="foo">
        /// The foo instance.
        /// </param>
        /// <param name="propName">
        /// The prop Name.
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// </exception>
        public static void ValidateState(IFoo foo, string propName)
        {
            // Cross member validation
            if (!string.IsNullOrWhiteSpace(foo.Text))
            {
                if (foo.Text.Length < foo.Data)
                {
                    throw new ArgumentException(
                        "Text.Length must be greater than or equal to the value of Data", propName);
                }
            }
        }

        #endregion
    }
}