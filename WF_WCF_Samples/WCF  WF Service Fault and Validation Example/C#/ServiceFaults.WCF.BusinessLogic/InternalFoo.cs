﻿namespace ServiceFaults.WCF.BusinessLogic
{
    using ServiceFaults.WCF.Contracts;

    /// <summary>
    ///   An internal class that provides semantics for the Foo entity.
    /// </summary>
    /// <remarks>
    ///   As an internal class, this type is never exposed via DataContract but it provides one place where
    ///   centralized validation can be done.
    /// </remarks>
    public class InternalFoo : IFoo
    {
        #region Constants and Fields

        /// <summary>
        ///   The foo data.
        /// </summary>
        private int data;

        /// <summary>
        ///   The text string.
        /// </summary>
        private string text;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "InternalFoo" /> class.
        /// </summary>
        public InternalFoo()
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "InternalFoo" /> class.
        /// </summary>
        /// <param name = "foo">
        ///   The foo reference.
        /// </param>
        public InternalFoo(Foo foo)
        {
            // Foo is data assumed to validated
            // do not repeat validation here
            this.data = foo.Data;
            this.text = foo.Text;
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Gets or sets Data.
        /// </summary>
        public int Data
        {
            get
            {
                return this.data;
            }

            set
            {
                FooValidator.ValidateDataValue(value);
                this.data = value;
                FooValidator.ValidateState(this, "Data");
            }
        }

        /// <summary>
        ///   Gets or sets Text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                FooValidator.ValidateTextValue(value);
                this.text = value;
                FooValidator.ValidateState(this, "Text");
            }
        }

        #endregion

        #region Operators

        /// <summary>
        ///   Explicit type conversion to support cast from Foo
        /// </summary>
        /// <param name = "foo">
        ///   The foo.
        /// </param>
        /// <returns>
        ///   An internalFoo from a Foo 
        /// </returns>
        public static explicit operator InternalFoo(Foo foo)
        {
            return new InternalFoo(foo);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   The to foo.
        /// </summary>
        /// <returns>
        /// </returns>
        public Foo ToFoo()
        {
            return new Foo { Data = this.Data, Text = this.Text };
        }

        #endregion

        // public InternalFoo
    }
}