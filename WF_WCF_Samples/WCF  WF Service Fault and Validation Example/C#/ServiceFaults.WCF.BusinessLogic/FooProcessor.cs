﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FooProcessor.cs" company="Microsoft">
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceFaults.WCF.BusinessLogic
{
    /// <summary>
    /// The foo processor.
    /// </summary>
    public static class FooProcessor
    {
        #region Public Methods

        /// <summary>
        /// The show foo data.
        /// </summary>
        /// <param name="foo">
        /// The foo reference.
        /// </param>
        /// <returns>
        /// Returns the foo data as a string
        /// </returns>
        public static string ShowFooData(InternalFoo foo)
        {
            return foo.Data.ToString();
        }

        #endregion
    }
}