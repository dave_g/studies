<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:my="urn:sample" extension-element-prefixes="msxsl">

  <msxsl:script language="JScript" implements-prefix="my">
    function today()
    {
    return new Date();
    }
  </msxsl:script>
  <xsl:template match="/">

    Today = <xsl:value-of select="my:today()"/>
    <xsl:element name="App_Data">
      <xsl:attribute name="App">MOD</xsl:attribute>
      <xsl:attribute name="Name">Foo</xsl:attribute>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>