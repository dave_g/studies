﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ThreadUtilities
{
    public class FileOperations 
    {
        public delegate void MessageDelegate(object sender, MessageEventArgs args);

        public event MessageDelegate Messages;
        public event MessageDelegate ErrorMessages;       
        public event MessageDelegate SuccessMessages;

        

        private List<Thread> _threads = new List<Thread>();
        private CancellationTokenSource _cancelSource = new CancellationTokenSource();
        private Queue<CopyFileProperties> _copyFiles = new Queue<CopyFileProperties>();
        private System.Threading.Timer _timer;
        private object _threadlock = new object();

        private void TimerEvent(object state)
        {
            //reenterant
            lock (_threadlock)
            {
                if (_copyFiles.Any())
                {                    
                    CopyFileProperties copyFileProperty = _copyFiles.Dequeue();
                    _threads.Add(StartCopyFile(copyFileProperty.From, copyFileProperty.To));
                }
                _threads.RemoveAll(t => t.ThreadState != ThreadState.Running);                
            }
        }

        public int FilesInQueue()
        {
            return _copyFiles.Count;
        }

        public int FilesInProcess()
        {
            return _threads.Count;
        }

        public FileOperations()
        {
            TimerCallback timerDelegate = TimerEvent;
            _timer = new Timer(timerDelegate,null,0,2000);
            
        }



        public void InvokeMessage(string message)
        {
            MessageDelegate handler = Messages;
            if (handler != null) handler(this, new MessageEventArgs(DateTime.Now + " : " + message));
        }

        public void InvokeErrorMessages(string message, CopyFileProperties copyFileProperties)
        {
            MessageDelegate handler = ErrorMessages;
            if (handler != null) handler(this, new MessageEventArgs(DateTime.Now + " : " + message, copyFileProperties));
        }

        public void InvokeSuccessMessages(string message, CopyFileProperties copyFileProperties)
        {
            MessageDelegate handler = SuccessMessages;
            if (handler != null) handler(this, new MessageEventArgs(DateTime.Now + " : " + message, copyFileProperties));
        }
        

        public void CopyFile(string fromFileName,string toFileName)
        {        
            _copyFiles.Enqueue(new CopyFileProperties(fromFileName,toFileName));            
        }
        
        public void CancelOperations()
        {
            _cancelSource.Cancel();
        }

        private Thread StartCopyFile(string fromFileName, string toFileName)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    _cancelSource.Token.ThrowIfCancellationRequested();
                    DateTime end;
                    DateTime start = DateTime.Now;                    
                    InvokeMessage("Starting copy of " + fromFileName );
                    
                    bool copyfileret = Utilites.FileUtilities.CopyFile(fromFileName, toFileName, true);

                    if (copyfileret)
                    {
                        File.GetAttributes(toFileName);
                        end = DateTime.Now;
                        TimeSpan dur = start - end;
                        InvokeSuccessMessages("Finished copy of " + fromFileName + ". Total duration = " + dur, new CopyFileProperties(fromFileName,toFileName));
                    }
                    else
                    {
                        InvokeErrorMessages("Error copying file :" + fromFileName, new CopyFileProperties(fromFileName,toFileName));
                    }
                    
                    
                }
                catch (ThreadAbortException)
                {
                    InvokeErrorMessages("Thread Aborted :" , new CopyFileProperties(fromFileName, toFileName));                    
                }
                catch (OperationCanceledException)
                {
                    InvokeMessage("Copy canceled.");
                }
                catch(IOException ex)
                {
                    InvokeErrorMessages("Copy error :" + ex.Message, new CopyFileProperties(fromFileName, toFileName));                    
                }           
            });
            thread.Start();
            return thread;
        }        
        
        public void KillEmAll()
        {
            _cancelSource.Cancel();
            foreach (Thread thread in _threads)
            {
                thread.Abort();
            }
        }

        public void ReSet()
        {
            _cancelSource.Dispose();
            _cancelSource = new CancellationTokenSource();
        }

        public class MessageEventArgs : EventArgs
        {
            private string _message;            
            private CopyFileProperties _fileProperties;

            public MessageEventArgs(string message, CopyFileProperties fileProperties)
            {
                _message = message;                
                _fileProperties = fileProperties;
            }

            public CopyFileProperties FileProperties
            {
                get { return _fileProperties; }
                set { _fileProperties = value; }
            }

            public string Message
            {
                get { return _message; }
                set { _message = value; }
            }            

            public MessageEventArgs(string message)
            {
                this._message = message;                
            }
        }

        public class CopyFileProperties
        {
            private string _from;
            private string _to;

            public CopyFileProperties(string @from, string to)
            {
                _from = from;
                _to = to;
            }

            public string From
            {
                get { return _from; }
                set { _from = value; }
            }

            public string To
            {
                get { return _to; }
                set { _to = value; }
            }
        }



       
    }
}
