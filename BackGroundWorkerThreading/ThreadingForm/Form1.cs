﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadingForm
{
    public partial class Form1 : Form
    {
        private bool _toggle1;
        System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
        private Thread _thread;
        private Task task;
        CancellationTokenSource _cancelSource;

        public Form1()
        {
            InitializeComponent();
            _timer.Interval = 1000;
            _timer.Tick += new EventHandler(_timer_Tick);
            
        }

        

        

        void _timer_Tick(object sender, EventArgs e)
        {
            textBox1.Text = String.Format(textBox1.Text + "\r\n{0}", "foo");            
        }

        private void buttonStartThread1_Click(object sender, EventArgs e)
        {
            if (!_toggle1)
            {
                _timer.Start();
            }
            else
            {
                _timer.Stop();
            }
            _toggle1 = !_toggle1;
        }

        private void buttonStartThread2_Click(object sender, EventArgs e)
        {
            //task = Task.Factory.StartNew(DoWork);
            _cancelSource = new CancellationTokenSource();
            _thread = new Thread(() =>
            {
                try
                {
                    DoWork(_cancelSource.Token);
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Canceled");
                }
            });
            _thread.Start();                                                           
        }

        void DoWork(CancellationToken c)
        {

            for (int i = 0; i < 5000000; i++)
            {                
                c.ThrowIfCancellationRequested();
                Console.WriteLine("here");
            }

        }

        private void buttonInterupt_Click(object sender, EventArgs e)
        {
           // _thread.Abort();
           // textBox2.Text = task.Status.ToString();
           _cancelSource.Cancel();
           //_cancelSource.Dispose();
        }
    }
}
