﻿namespace ThreadingFileCopy
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStartCopyThreads = new System.Windows.Forms.Button();
            this.buttonInterupt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonKill = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonStartCopyThreads
            // 
            this.buttonStartCopyThreads.Location = new System.Drawing.Point(13, 544);
            this.buttonStartCopyThreads.Name = "buttonStartCopyThreads";
            this.buttonStartCopyThreads.Size = new System.Drawing.Size(91, 23);
            this.buttonStartCopyThreads.TabIndex = 0;
            this.buttonStartCopyThreads.Text = "Start Copies";
            this.buttonStartCopyThreads.UseVisualStyleBackColor = true;
            this.buttonStartCopyThreads.Click += new System.EventHandler(this.buttonStartThread2_Click);
            // 
            // buttonInterupt
            // 
            this.buttonInterupt.Location = new System.Drawing.Point(123, 544);
            this.buttonInterupt.Name = "buttonInterupt";
            this.buttonInterupt.Size = new System.Drawing.Size(91, 23);
            this.buttonInterupt.TabIndex = 0;
            this.buttonInterupt.Text = "Interupt";
            this.buttonInterupt.UseVisualStyleBackColor = true;
            this.buttonInterupt.Click += new System.EventHandler(this.buttonInterupt_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 13);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(422, 518);
            this.textBox1.TabIndex = 1;
            // 
            // buttonKill
            // 
            this.buttonKill.Location = new System.Drawing.Point(229, 544);
            this.buttonKill.Name = "buttonKill";
            this.buttonKill.Size = new System.Drawing.Size(75, 23);
            this.buttonKill.TabIndex = 3;
            this.buttonKill.Text = "Kill";
            this.buttonKill.UseVisualStyleBackColor = true;
            this.buttonKill.Click += new System.EventHandler(this.buttonKill_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(441, 13);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(486, 518);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(319, 544);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 5;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 579);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.buttonKill);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonInterupt);
            this.Controls.Add(this.buttonStartCopyThreads);
            this.Name = "Form2";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStartCopyThreads;
        private System.Windows.Forms.Button buttonInterupt;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonKill;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button buttonReset;
    }
}

