﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using ThreadUtilities;
using Timer = System.Windows.Forms.Timer;

namespace ThreadingFileCopy
{
    public partial class Form2 : Form
    {
                     
        CancellationTokenSource _cancelSource;
        
        
        private delegate void SetTextDelegate(string text);
        private SetTextDelegate _setTextDelegate;

        private delegate void SetTextResultDelegate(string text, FileOperations.MessageEventArgs args);
        private SetTextResultDelegate _setTextResultDelegate;

        private ThreadUtilities.FileOperations _fileOperations = new FileOperations();
        private List<Thread> _threads = new List<Thread>();

        

        public Form2()
        {
            InitializeComponent();
            _setTextResultDelegate = new SetTextResultDelegate(SetErrorText);
            _setTextDelegate = new SetTextDelegate(SetText);

            _fileOperations.Messages += new FileOperations.MessageDelegate(_fileOperations_Messages);
            _fileOperations.ErrorMessages += new FileOperations.MessageDelegate(_fileOperations_ErrorMessages);
            _fileOperations.SuccessMessages += new FileOperations.MessageDelegate(_fileOperations_SuccessMessages);

        }




        private void buttonStartThread2_Click(object sender, EventArgs e)
        {
            
            //_cancelSource = _fileOperations.CopyFile(@"s:\temp\test.mp4", @"c:\temp\test.mp4");
            _fileOperations.CopyFile(@"s:\temp\test.mp4", @"c:\temp\test.mp4");
            _fileOperations.CopyFile(@"s:\temp\test2.mp4", @"c:\temp\test2.mp4");
            _fileOperations.CopyFile(@"s:\temp\test3.mp4", @"c:\temp\test3.mp4");
        }

        private void DoCopy(string  from, string to)
        {
            _fileOperations.CopyFile(from, to);
        }

        void _fileOperations_SuccessMessages(object sender, FileOperations.MessageEventArgs args)
        {
            SetText(args.Message);
            DoCopy(args.FileProperties.From, args.FileProperties.To);            
        }

        void _fileOperations_ErrorMessages(object sender, FileOperations.MessageEventArgs args)
        {
            SetErrorText(args.Message, args);
        }

        void _fileOperations_Messages(object sender, FileOperations.MessageEventArgs args)
        {
            SetText(args.Message);
        }




        private void SetErrorText(string text, FileOperations.MessageEventArgs args)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.richTextBox1.InvokeRequired)
            {
                this.Invoke(_setTextResultDelegate, new object[] { text, args });
            }
            else
            {
                richTextBox1.SelectionColor = Color.Red;
                richTextBox1.AppendText(String.Format("Copy Failed {0} \r\n", text));
            }
        }

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.richTextBox1.InvokeRequired)
            {
                this.Invoke(_setTextDelegate, new object[] { text });
            }
            else
            {
                richTextBox1.AppendText(String.Format("{0} \r\n", text));                
            }
        }



        private void buttonInterupt_Click(object sender, EventArgs e)
        {
            _fileOperations.CancelOperations();
            
        }

        private void buttonKill_Click(object sender, EventArgs e)
        {
            _fileOperations.KillEmAll();
        }        

        private void buttonReset_Click(object sender, EventArgs e)
        {
            _fileOperations.ReSet();
        }
    }
}
