﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilites;
namespace ThreadingFileCopy
{
    public partial class Form1 : Form
    {
        private bool _toggle1;
        System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
        private Thread _thread;
        private Task task;
        CancellationTokenSource _cancelSource;
        string[] files;

        private delegate void SetTextDelegate(string text);
        private SetTextDelegate _setTextDelegate;

        private delegate void SetTextResultDelegate(string text, bool ret);
        private SetTextResultDelegate _setTextResultDelegate;

        public Form1()
        {
            InitializeComponent();
            _setTextResultDelegate = new SetTextResultDelegate(SetText);
            _setTextDelegate = new SetTextDelegate(SetText);
        }



        private void buttonStartThread1_Click(object sender, EventArgs e)
        {

            string errs;
            Utilites.FileUtilities.GetFilesFromDirectory(@"C:\temp\macafur", "*", true, out files, out errs);
            foreach (string file in files)
            {
                textBox1.Text = String.Format(textBox1.Text + "{0} \r\n", file);
            }
        }

        private void buttonStartThread2_Click(object sender, EventArgs e)
        {
            //task = Task.Factory.StartNew(DoWork);
            _cancelSource = new CancellationTokenSource();
            //Queue<string> listFiles = new Queue<string>(files);

            //_thread = new Thread(() =>
            //{
            //    for(int i = 0; i < listFiles.Count; i++)
            //    {
            //        try
            //        {
            //            CopyFile(listFiles.Dequeue(), _cancelSource.Token);
            //        }
            //        catch (OperationCanceledException)
            //        {
            //            Console.WriteLine("Canceled");
            //        }    
            //    }

            //});
            //_thread.Start();  


            _thread = new Thread(() =>
            {
                try
                {
                    CopyFile(@"s:\temp\test.mp4", _cancelSource.Token);
                    //CopyFile(@"s:\temp\foo.mov");
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Canceled");
                }
            });
            _thread.Start();
        }

        void CopyFile(string fileName, CancellationToken c)
        {
            

            for (int i = 0; i < 10; i++)
            {
                c.ThrowIfCancellationRequested();
                DateTime end;
                DateTime start = DateTime.Now;
                SetText("Starting copy of " + fileName + " " + start);
                bool copyfileret = Utilites.FileUtilities.CopyFile(fileName, @"C:\temp\Copyfiles\" + Utilites.FileUtilities.StripFileNameFromPath(fileName), true);
                end = DateTime.Now;
                SetText("Finished copy of " + fileName + " " + end, copyfileret);
                TimeSpan dur = start - end;
                SetText("Duration =  " + dur);    
            }
                        
        }


        void CopyFile(string fileName)
        {
            DateTime end;
            DateTime start = DateTime.Now;
            SetText("Starting copy of " + fileName + " " + start);            

            FileStream fileStream = File.Create(@"c:\temp\copyfiles\foo.mov");

            FileStream fileStreamOpen = File.OpenRead(fileName);



            //FileStream fs = new FileStream(@"c:\temp\copyfiles\foo.mov", FileMode.CreateNew);
            
            // Create the writer for data.
            BinaryWriter w = new BinaryWriter(fileStream);
            //BinaryReader r = new BinaryReader(fileStreamOpen, Encoding.Unicode);

            //// Write data to Test.data.
            //int i = r.Read();
            //do
            //{
            //    w.Write((int)i);
            //    i = r.Read();
            //} while (i != -1);

            
            //w.Close();
            //r.Close();

            byte[] valueBytes;
            
            //List<byte> octets = new List<byte>();
            using (BinaryReader reader = new BinaryReader(fileStreamOpen))
            {
                // Read Everything
                bool isDone = false;
                while (!isDone)
                {
                    valueBytes = new byte[0];
                    valueBytes = reader.ReadBytes(1024);
                    w.Write(valueBytes);                    
                    //octets.AddRange(valueBytes);
                    isDone = (valueBytes.Length < 1024);
                    
                }
            }
            fileStreamOpen.Close();

            end = DateTime.Now;
            SetText("Finished copy of " + fileName + " " + end);
            TimeSpan dur = start - end;
            SetText("Duration =  " + dur);
        }

        private void SetText(string text, bool success)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBox2.InvokeRequired)
            {
                this.Invoke(_setTextResultDelegate, new object[] { text, success });
            }
            else
            {
                textBox2.Text = success ? String.Format(textBox2.Text + "Copied {0} \r\n", text) : String.Format(textBox2.Text + "Copy {0} Failed  \r\n", text);
            }
        }

        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBox2.InvokeRequired)
            {
                this.Invoke(_setTextDelegate, new object[] { text });
            }
            else
            {
                textBox2.Text = String.Format(textBox2.Text + "{0} \r\n", text);
            }
        }



        private void buttonInterupt_Click(object sender, EventArgs e)
        {
            _cancelSource.Cancel();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _thread.Abort();
        }
    }
}
