﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
//using System.Threading;

namespace BackGroundWorker
{
    public partial class Form1 : Form
    {
        private bool _toggle1;
        System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
        private  BackgroundWorker _bw;

        public Form1()
        {
            InitializeComponent();
            _timer.Interval = 1000;
            _timer.Tick += new EventHandler(_timer_Tick);
            _bw = new BackgroundWorker{WorkerSupportsCancellation = true,WorkerReportsProgress = true};
            
            _bw.DoWork += new DoWorkEventHandler(_bw_DoWork);
            _bw.ProgressChanged += new ProgressChangedEventHandler(_bw_ProgressChanged);
            _bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_bw_RunWorkerCompleted);
        }

        void _bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Interupt!");
            }else if (e.Error != null)
            {
                Console.WriteLine("Error!");
            }
            Console.WriteLine("Complete");
        }

        void _bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Console.WriteLine("Progress " + e.ProgressPercentage);
        }

        void _bw_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do not access the form's BackgroundWorker reference directly.
            // Instead, use the reference provided by the sender parameter.
            BackgroundWorker bw = sender as BackgroundWorker;

            // Extract the argument.
            //int arg = (int)e.Argument;
            
            for (int i = 0; i < 5000000; i++)
            {
                if(bw.CancellationPending)
                {
                    e.Cancel = true;
                    return;                    
                }
                //Console.WriteLine(String.Format("\r\n{0}{1}", i, "foo"));
                bw.ReportProgress(i);
                //if(i % 100 == 0){Thread.Sleep(1000);}
            }
            e.Result = 0;
            //textBox2.Text = String.Format(textBox2.Text + "\r\n{0}", "foo");                              
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            textBox1.Text = String.Format(textBox1.Text + "\r\n{0}", "foo");            
        }

        private void buttonStartThread1_Click(object sender, EventArgs e)
        {
            if (!_toggle1)
            {
                _timer.Start();
            }
            else
            {
                _timer.Stop();
            }
            _toggle1 = !_toggle1;
        }

        private void buttonStartThread2_Click(object sender, EventArgs e)
        {
            _bw.RunWorkerAsync();
            
        }

        private void buttonInterupt_Click(object sender, EventArgs e)
        {
            Console.WriteLine("khgkjgjhgkjgkjggkjghgjhgkjgjh");
            if (_bw.IsBusy)
            {
                _bw.CancelAsync();                
            }
        }
    }
}
