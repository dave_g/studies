﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.EntityClient;

namespace EFTest
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private int foo()
        {

            using (var context = new ATD())
            {
                var features = context.Features;
                foreach (var feature in features)
                {
                    Console.WriteLine(feature.featureid);
                }
            }
            return 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DoEntitySQLQuery();
            //DoMethodQuery();
            EntityClientQueryContacts();
            //foo();
        }

        private void DoMethodQuery()
        {
            using (var context = new ATD())
            {
               var features = context.Features.Where(f => f.featureid != "TV52931").OrderBy(o => o.featureid);
               Console.WriteLine(features.Count());
               Console.WriteLine(features.Count());

               foreach(var f in features){
                   Console.WriteLine(f.featureid);
               }

            }
        }

        private void DoEntitySQLQuery()
        {
            string qStr;

            using (var context = new ATD())
            {
                qStr = "select value f from AirTextDeuxEntities.Features as f where f.featureid = 'TV52931'";
                var features = context.CreateQuery<Feature>(qStr);
            }
        }

        static void EntityClientQueryContacts()
        {
            using (EntityConnection conn = new
             EntityConnection("name=ATD"))
            {
                conn.Open();

                var queryString = "select value f from ATD.Features as f where f.featureid = 'TV52931'";

                EntityCommand cmd = conn.CreateCommand();
                cmd.CommandText = queryString;
                using (EntityDataReader rdr =
                 cmd.ExecuteReader(System.Data.CommandBehavior.SequentialAccess))
                {
                    while (rdr.Read())
                    {
                        var firstname = rdr.GetString(1);
                        var lastname = rdr.GetString(2);
                        var title = rdr.GetString(3);
                        Console.WriteLine("{0} {1} {2}",
                              title.Trim(), firstname.Trim(), lastname);
                    }
                }
                conn.Close();
                
            }
        }



    }
}
