﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class EomCalculator
    {
        public static string CalculateEom(string som, long durationInframes)
        {
            if (durationInframes < 1) return "Invalid duration.";
            int[] timecodeFields = TimecodeUtility.SplitFields(som);
            while (durationInframes > 1)
            {
                timecodeFields = IncrementTimecode(timecodeFields);
                durationInframes--;
            }

            return formatTimecodeFields(timecodeFields);
        }

        //public static string CalculateEom(string som, long durationInSeconds)
        //{
        //    if (durationInSeconds < 1) return "Invalid duration.";
        //    int[] timecodeFields = TimecodeUtility.SplitFields(som);
        //    while (durationInSeconds > 0)
        //    {
        //        for (int i = 0; i < 30; ++i)
        //        {
        //            timecodeFields = IncrementTimecode(timecodeFields);
        //        }
        //        durationInSeconds--;
        //    }

        //    return formatTimecodeFields(timecodeFields);
        //}

        public static int[] IncrementTimecode(int[] timecodeFields)
        {
            timecodeFields = IncrementFrame(timecodeFields);
            return timecodeFields;
        }

        private static int[] IncrementFrame(int[] timecodeFields)
        {
            if (timecodeFields[3] == 29)
            {
                timecodeFields[3] = 0;
                return IncrementSeconds(timecodeFields);
            }
            timecodeFields[3]++;
            return timecodeFields;
        }

        private static int[] IncrementSeconds(int[] timecodeFields)
        {
            if (timecodeFields[2] == 59)
            {
                timecodeFields[2] = 0;
                return IncrementMinutes(timecodeFields);
            }
            timecodeFields[2]++;
            return timecodeFields;
        }

        private static int[] IncrementMinutes(int[] timecodeFields)
        {
            if (timecodeFields[1] == 59)
            {
                timecodeFields[1] = 0;
                return IncrementHours(timecodeFields);
            }
            timecodeFields[1]++;
            if ((timecodeFields[1]%10 != 0) && timecodeFields[2] == 0 && timecodeFields[3] == 0) timecodeFields[3] = 2;
            return timecodeFields;
        }

        private static int[] IncrementHours(int[] timecodeFields)
        {
            if (timecodeFields[0] == 23)
            {
                timecodeFields[0] = 0;
                return timecodeFields;
            }
            timecodeFields[0]++;
            return timecodeFields;
        }

        private static string formatTimecodeFields(int[] timecodeFields)
        {
            return String.Format("{0:00}:{1:00}:{2:00};{3:00}",
                                 timecodeFields[0], timecodeFields[1], timecodeFields[2], timecodeFields[3]);
        }
    }
}
