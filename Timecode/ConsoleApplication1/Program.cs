﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        private static void Main(string[] args)
        {
            //HeidlebergsTimecode.ConvertFrame(900);
            //var frame =TimecodeUtility.GetDfFrameNumber(0, 0, 30, 0);
            //TimecodeUtility.FormatDfTimecode(900);
            //var timeissec = 900/29.97;
            //var framenum = 30*29.97;
            var frames1Minute = TimecodeUtility.GetDfFrameNumber(0, 1, 0, 0);
            var frames1MinuteTwoFrames = TimecodeUtility.GetDfFrameNumber(0, 1, 0,2);
            var frames59 = TimecodeUtility.GetDfFrameNumber(0, 0, 59, 0);
            var frames30seconds = TimecodeUtility.GetDfFrameNumber(0, 0, 30, 0);
            var formatdf = TimecodeUtility.FormatDfTimecode(900);
            //Console.WriteLine(frames);
            //Console.WriteLine(TimecodeUtility.FormatDfTimecode(frames));
            //var frames = 1;
            while (Console.ReadLine() != "\n")
            {
                Console.WriteLine(EomCalculator.CalculateEom("00:00:00:00", frames1Minute));
                Console.WriteLine(EomCalculator.CalculateEom("00:00:00:00", frames1MinuteTwoFrames));
                Console.WriteLine(EomCalculator.CalculateEom("00:00:00:00", frames59));
                Console.WriteLine(EomCalculator.CalculateEom("00:00:07:00", frames30seconds));
                frames1Minute++;
                Thread.Sleep(new TimeSpan(0, 0, 0, 0, 500));
            }

            //Console.ReadKey();
        }
    }
}
