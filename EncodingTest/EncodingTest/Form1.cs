﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EncodingTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            string fullPathToFile = @"c:\Dec12SpanishChyron.txt";
            // Code page 1252 represents Latin characters.
            // PrintCPBytes("Hello, World!", 1252);

            byte[] bytes = File.ReadAllBytes(fullPathToFile);
            var encoding = Encoding.GetEncoding(1252);
            var encString = encoding.GetString(bytes);
            textBox1.Text = encString;
        }

        public static void PrintCPBytes(string str, int codePage)
        {
            Encoding targetEncoding;
            byte[] encodedChars;
            // Get the encoding for the specified code page.
            targetEncoding = Encoding.GetEncoding(codePage);

            // Get the byte representation of the specified string.
            encodedChars = targetEncoding.GetBytes(str);

            // Print the bytes.
            Console.WriteLine
            ("Byte representation of '{0}' in Code Page '{1}':", str, codePage);
            
            for (int i = 0; i < encodedChars.Length; i++)
                Console.WriteLine("Byte {0}: {1}", i, encodedChars[i]);
        }
    }
}
