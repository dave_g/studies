﻿using System.IO;
using System.Text;
using System.Xml.Linq;

namespace FileTester.utilities
{
    public static class FileUtilities
    {
        public static void WriteXMLFile(XDocument pxfField, string fileName)
        {
            using (var stringWriter = new StringWriterUtf8())
            {
                pxfField.Save(stringWriter);
                var ws = stringWriter.ToString();
                WriteToCache(ws, fileName);
            }
        }

        public static string GetCacheDir()
        {
            var syspath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            return Path.Combine(syspath, "cache");
        }

        public static void WriteToCache(string contents, string filename)
        {
            var fullfilename = Path.Combine(GetCacheDir(), filename);
            if (!Directory.Exists(Path.GetDirectoryName(fullfilename)))
                Directory.CreateDirectory(Path.GetDirectoryName(fullfilename));
            File.WriteAllText(fullfilename, contents, Encoding.UTF8);
        }

        public static void MoveFromCache(string fileName, string outputFolder)
        {
            var fullfilename = Path.Combine(GetCacheDir(), fileName);
            var outfilename = Path.Combine(outputFolder, fileName);
            MoveFile(fullfilename,outfilename);
        }

        public static void MoveFile(string fullFileName, string newFullFileName)
        {            
            if (!Directory.Exists(Path.GetDirectoryName(newFullFileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(newFullFileName));
            
            if (File.Exists(newFullFileName))
            {
                newFullFileName = IncrementFileName(newFullFileName);
            }
            File.Move(fullFileName, newFullFileName);
        }

        public static void MoveFile(string fullFileName, string newFullFileName, bool overwrite = false)
        {
            if (!Directory.Exists(Path.GetDirectoryName(newFullFileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(newFullFileName));

            if (File.Exists(newFullFileName))
            {
                if (!overwrite)
                {
                    newFullFileName = IncrementFileName(newFullFileName);
                }
                else
                {
                    File.Delete(newFullFileName);
                }
            }
            
            File.Move(fullFileName, newFullFileName);
        }

        public static void CopyFile(string fullFileName, string newFullFileName, bool overwrite = false)
        {
            if (!Directory.Exists(Path.GetDirectoryName(newFullFileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(newFullFileName));

            if (!overwrite && File.Exists(newFullFileName))
            {
                newFullFileName = IncrementFileName(newFullFileName);
            }
            File.Copy(fullFileName, newFullFileName, overwrite);
        }
        private static string IncrementFileName(string fullPath)
        {
            int count = 1;

            string fileNameOnly = Path.GetFileNameWithoutExtension(fullPath);
            string extension = Path.GetExtension(fullPath);
            string path = Path.GetDirectoryName(fullPath);
            string newFullPath = fullPath;

            while (File.Exists(newFullPath))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                newFullPath = Path.Combine(path, tempFileName + extension);
            }
            return newFullPath;
        }
    }
}
