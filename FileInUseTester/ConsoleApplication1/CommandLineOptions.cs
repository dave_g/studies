﻿using CommandLine;
using CommandLine.Text;

namespace FileTester
{
    class CommandLineOptions
    {
        [Option('c', "command", Required = true,
    HelpText = "command to run.")]
        public string Command { get; set; }

        [Option('i', "input", Required = false,
   HelpText = "input file")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = false,
  HelpText = "input file")]
        public string OutputFile { get; set; }

        [Option('v', "verbose", DefaultValue = true,
          HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
