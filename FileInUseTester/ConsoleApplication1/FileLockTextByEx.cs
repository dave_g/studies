﻿using System.IO;

namespace FileTester
{
    public class FileLockTextByEx
    {
        public bool FileIsLocked(string fileName)
        {
            try
            {
                using (File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    return false;
                }
            }
            catch
            {
                // ignore
            }

            return true;
        }
    }
}
