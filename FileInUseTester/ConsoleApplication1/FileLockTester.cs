﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace FileTester
{
    public static class FileLockTester
    {
        [SuppressUnmanagedCodeSecurity]
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern Microsoft.Win32.SafeHandles.SafeFileHandle CreateFile(string lpFileName,
                                                                                    UInt32 dwDesiredAccess,
                                                                                    UInt32 dwShareMode,
                                                                                    IntPtr pSecurityAttributes,
                                                                                   UInt32 dwCreationDisposition,
                                                                                    UInt32 dwFlagsAndAttributes,
                                                                                    IntPtr hTemplateFile);

        private const uint GenericWrite = 0x40000000;
        private const uint OpenExisting = 3;

        public static bool IsFileInUse(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return false;
            }

            SafeHandle handleValue = null;

            try
            {
                handleValue = CreateFile(filePath, GenericWrite, 0, IntPtr.Zero,
                                                         OpenExisting, 0, IntPtr.Zero);

                bool inUse = handleValue.IsInvalid;

                return inUse;
            }
            finally
            {
                if (handleValue != null)
                {
                    handleValue.Close();

                    handleValue.Dispose();

                    handleValue = null;
                }
            }
        }
    }
}
