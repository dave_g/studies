﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace FileTester
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //DoFileLockTest();
            var options = new CommandLineOptions();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                // Values are available here
                switch (options.Command)
                {
                    case "move":
                        if (string.IsNullOrEmpty(options.InputFile)) Console.WriteLine($"missing option inputFile");
                        if (string.IsNullOrEmpty(options.OutputFile)) Console.WriteLine($"missing option OutputFile");
                        domove(options.InputFile, options.OutputFile);
                        break;
                    case "forcemove":
                        if (string.IsNullOrEmpty(options.InputFile)) Console.WriteLine($"missing option inputFile");
                        if (string.IsNullOrEmpty(options.OutputFile)) Console.WriteLine($"missing option OutputFile");
                        forcemove(options.InputFile, options.OutputFile,true);
                        break;
                    case "incrementmove":
                        if (string.IsNullOrEmpty(options.InputFile)) Console.WriteLine($"missing option inputFile");
                        if (string.IsNullOrEmpty(options.OutputFile)) Console.WriteLine($"missing option OutputFile");
                        forcemove(options.InputFile, options.OutputFile, false);
                        break;
                    case "copy":
                        if (string.IsNullOrEmpty(options.InputFile)) Console.WriteLine($"missing option inputFile");
                        if (string.IsNullOrEmpty(options.OutputFile)) Console.WriteLine($"missing option OutputFile");
                        copy(options.InputFile, options.OutputFile, true);
                        break;
                    case "incrementcopy":
                        if (string.IsNullOrEmpty(options.InputFile)) Console.WriteLine($"missing option inputFile");
                        if (string.IsNullOrEmpty(options.OutputFile)) Console.WriteLine($"missing option OutputFile");
                        copy(options.InputFile, options.OutputFile, false);
                        break;
                }
                //if (options.Verbose) Console.WriteLine("Filename: {0}", options.InputFile);
            }

        }

        private static void copy(string inputFile, string outputFile, bool b)
        {
            utilities.FileUtilities.CopyFile(inputFile,outputFile,b);
        }

        private static void forcemove(string inputFile, string outputFile, bool overwrite)
        {
            utilities.FileUtilities.MoveFile(inputFile,outputFile,overwrite);
        }

        private static void domove(string inputFile, string outputFile)
        {
            utilities.FileUtilities.MoveFile(inputFile, outputFile);
        }

        //testing performance of 'Exception thowing tesing' versus Marshalling DllImport when testing if file is locked.
        private static void DoFileLockTest()
        {
            var testByex = new FileLockTextByEx();
            var file = @"c:\temp\foo.txt";
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            FileLockTester.IsFileInUse(file);
            stopwatch.Stop();
            //Console.WriteLine("Dllimport took {0}. elapsed.", stopwatch.Elapsed);
            Console.WriteLine("Dllimport took {0} ms.", stopwatch.ElapsedMilliseconds);
            //Console.WriteLine("Dllimport took {0} ticks.", stopwatch.ElapsedTicks);

            stopwatch.Start();
            testByex.FileIsLocked(file);
            stopwatch.Stop();
            //Console.WriteLine("EX took {0}.", stopwatch.Elapsed);
            Console.WriteLine("EX took {0} ms.", stopwatch.ElapsedMilliseconds);
            //Console.WriteLine("EX took {0}. ticks", stopwatch.ElapsedTicks);           

            Console.WriteLine("Trying loops");
            stopwatch.Start();
            for (int i = 0; i < 10000; i++)
            {
                testByex.FileIsLocked(file);
            }
            stopwatch.Stop();
            //Console.WriteLine("EX loop took {0} elapsed.", stopwatch.Elapsed);
            Console.WriteLine("EX loop took {0} ms.", stopwatch.ElapsedMilliseconds);
            //Console.WriteLine("EX loop took {0} ticks.", stopwatch.ElapsedTicks);

            stopwatch.Start();
            for (int i = 0; i < 10000; i++)
            {
                FileLockTester.IsFileInUse(file);
            }
            stopwatch.Stop();
            //Console.WriteLine("Dllimport loop took {0} elapsed.", stopwatch.Elapsed);
            Console.WriteLine("Dllimport loop took {0} ms.", stopwatch.ElapsedMilliseconds);
            //Console.WriteLine("Dllimport loop took {0} ticks.", stopwatch.ElapsedTicks);

            Console.ReadKey();
        }
    }
}
