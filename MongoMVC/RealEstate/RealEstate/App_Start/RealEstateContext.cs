﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace RealEstate.App_Start
{
    public class RealEstateContext
    {
        public MongoDatabase Database;
        public RealEstateContext()
        {
            var client = new MongoClient(@"mongodb://localhost");
            var server = client.GetServer();
            Database = server.GetDatabase("realestate");
        }
    }
}
