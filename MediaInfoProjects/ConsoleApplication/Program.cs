﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var mediaFile = args[0];
            var mediaFunctions = new MediaFunctions();
            mediaFunctions.GetMediaInfo(mediaFile);
        }
    }
}
