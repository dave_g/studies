using System;
using System.Text.RegularExpressions;

namespace MacafurCore.Utility
{
    public static class TimecodeUtility
    {
        public static int GetFrameNumber(int hrs, int mins, int seconds, int frames)
        {
            return (108000 * hrs) +
                   (1800 * mins) +
                   (30 * seconds) +
                   frames;
        }

        public static int GetDfFrameNumber(int hours, int minutes, int seconds, int frames)
        {
            int totalMinutes = 60 * hours + minutes;
            int frameNumber = (108000 * hours) +
                              (1800 * minutes) +
                              (30 * seconds) +
                              frames - 2 * (totalMinutes - totalMinutes / 10);
            return frameNumber;
        }

        public static int ParseHhMmSsFf(string timecode)
        {
            int[] fields = SplitFields(timecode);
            return GetFrameNumber(fields[0], fields[1], fields[2], fields[3]);
        }

        public static int ParseDfHhMmSsFf(string timecode)
        {
            int[] fields = SplitFields(timecode);
            return GetDfFrameNumber(fields[0], fields[1], fields[2], fields[3]);
        }

        public static int ParseHhMmSsMs(string timecode)
        {
            int[] fields = SplitFields(timecode);
            return GetFrameNumber(
                fields[0],
                fields[1],
                fields[2],
                (int) Math.Round(fields[3] * 30.0 / 1000.0));
        }

        public static int ParseDfHhMmSsMs(string timecode)
        {
            int[] fields = SplitFields(timecode);
            return GetDfFrameNumber(
                fields[0],
                fields[1],
                fields[2],
                (int) Math.Round(fields[3] * 30.0 / 1001.0));
        }

        public static string FormatTimecode(int frameNumber, char framesSeparator = ':')
        {
            int frames = frameNumber % 30;
            int seconds = (frameNumber / 30) % 60;
            int minutes = ((frameNumber / 30) / 60) % 60;
            int hours = (((frameNumber / 30) / 60) / 60) % 24;
            return String.Format("{0:00}:{1:00}:{2:00}{3}{4:00}",
                                 hours, minutes, seconds, framesSeparator, frames);
        }

        public static string FormatDfTimecode(int frameNumber, char framesSeparator = ';')
        {
            int d = frameNumber / 17982;
            int m = frameNumber % 17982;
            frameNumber += 18 * d + 2 * ((m - 2) / 1798);
            return FormatTimecode(frameNumber, framesSeparator);
        }

        public static string Format(int frameNumber, bool isDropFrame, char framesSeparator)
        {
            return isDropFrame
                       ? FormatDfTimecode(frameNumber, framesSeparator)
                       : FormatTimecode(frameNumber, framesSeparator);
        }

        private const string HhmmssffDfPattern = "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9];[0-9][0-9]";
        private const string HhmmssffNdfPattern = "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]:[0-9][0-9]";
        private const string HhmmssmssPattern = "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9][.][0-9][0-9][0-9]";

        /// <summary>
        /// Attempts to parse a timecode from one of the following formats:
        /// hh:mm:ss:ff, hh:mm:ss;ff or hh:mm:ss.nnn
        /// </summary>
        /// <param name="rawTimecode">The timecode in string form.</param>
        /// <param name="isDropFrame">Only valid when parsing the hh:mm:ss.nnn format.</param>
        /// <returns>The frame number.</returns>
        public static int Parse(string rawTimecode, bool isDropFrame = true)
        {
            string timecode = rawTimecode.Split('@')[0]; // remove the @29.97

            if (Regex.IsMatch(timecode, HhmmssffDfPattern))
            {
                return ParseDfHhMmSsFf(timecode);
            }

            if (Regex.IsMatch(timecode, HhmmssffNdfPattern))
            {
                return ParseHhMmSsFf(timecode);
            }

            if (Regex.IsMatch(timecode, HhmmssmssPattern))
            {
                return isDropFrame
                           ? ParseDfHhMmSsMs(timecode)
                           : ParseHhMmSsMs(timecode);
            }

            throw new ArgumentException("could not parse timecode: " + timecode);
        }

        public static int[] SplitFields(string timecode)
        {
            timecode = timecode.Split('@')[0];
            string[] fields = timecode.Split(new[] { ':', '.', ';' });
            return new[]
                       {
                           int.Parse(fields[0]),
                           int.Parse(fields[1]),
                           int.Parse(fields[2]),
                           int.Parse(fields[3])
                       };
        }

        public static string FixInvalidTimecode(string timecode)
        {
            var fields = SplitFields(timecode);
            if (fields[1] % 10 == 0) return timecode;
            if (fields[2] != 0) return timecode;
            if (fields[3] != 0 && fields[3] != 1) return timecode;
            fields[3] = 2;
            var newtimecode = String.Format("{0:00}:{1:00}:{2:00};{3:00}",
                                  fields[0], fields[1], fields[2], fields[3]);
            return newtimecode;
        }

        public static bool IsValidTimecode(string timecode)
        {
            var fields = SplitFields(timecode);
            if (fields[1] % 10 == 0) return true;
            if (fields[2] != 0) return true;
            if (fields[3] != 0 && fields[3] != 1) return true;
            return false;
        }
    }
}