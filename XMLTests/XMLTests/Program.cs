﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;

namespace XMLTests
{
    public class Program
    {
        static string xmlFile = @"..\..\XMLFile1.xml";

        public static void Main(string[] args)
        {
            
            try
            {
                //LoadByXmlDocumentDTD();
                //LoadByXmlDocSchema();
                //LoadByXLinq();
                LoadByXmlDocSchemaNS();
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("Error {0}", ex));                
            }
            
        }

        private static void LoadByXLinq()
        {
            var doc = XElement.Load(xmlFile);
            var query = from ele in doc.Elements("{ssg:items}Item") select ele;

            foreach (var element in query)
            {
                Console.WriteLine(element);
            }

            

            //with namespace
            query = from ele in doc.Elements("{vendor:items}SubItems") select ele;

            foreach (var element in query)
            {
                Console.WriteLine(element);
            }

            var sum = query.Sum(ele => int.Parse(ele.Value));
        }

        private static void LoadByXmlDocumentDTD()
        {
            var xml = new XmlDocument();
            

            #region dtd validation

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.DTD;
            settings.DtdProcessing = DtdProcessing.Parse;
            var xmlreader = XmlReader.Create(xmlFile, settings);

            #endregion

            try
            {
                /* Dirrect load no dtd 
                xml.Load(xmlFile);
                */
                /* dtd load */
                xml.Load(xmlreader);
            }
            catch (XmlSchemaValidationException e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            var body = xml.SelectSingleNode("/Items");
            Console.WriteLine(body.InnerText);
            Console.WriteLine(body.InnerXml);
            var subItems = xml.SelectSingleNode("/Items/Item/SubItems");
            foreach (XmlElement subItem in subItems)
            {
                Console.WriteLine(subItem.InnerText);
            }
        }

        private static void LoadByXmlDocSchema()
        {
            var xml = new XmlDocument();
            xmlFile = @"..\..\xmlWithSchema.xml";

            #region schema validation

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            var schemas = new XmlSchemaSet();            
            schemas.Add("", @"C:\Users\dgraha001\Documents\Code\VS2010\Tests\XMLTests\XMLTests\xmlWithSchema.xsd");
            settings.Schemas = schemas;            
            settings.DtdProcessing = DtdProcessing.Parse;
            var xmlreader = XmlReader.Create(xmlFile, settings);

            #endregion

            try
            {                
                xml.Load(xmlreader);
            }
            catch (XmlSchemaValidationException e)
            {
                Console.WriteLine(String.Format("Schema Validation Error : {0}", e.Message));
            }

            var body = xml.SelectSingleNode("/ItemCollection");
            Console.WriteLine(body.InnerText);
            Console.WriteLine(body.InnerXml);
            //var subItems = xml.SelectSingleNode("/Items/Item/SubItems");
            //foreach (XmlElement subItem in subItems)
            //{
            //    Console.WriteLine(subItem.InnerText);
            //}
        }

        private static void LoadByXmlDocSchemaNS()
        {
            
            xmlFile = @"..\..\xmlWithSchemaNS.xml";

            #region schema validation

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            var schemas = new XmlSchemaSet();
            schemas.Add("urn:www.ssg.com", @"C:\Users\dgraha001\Documents\Code\VS2010\Tests\XMLTests\XMLTests\xmlWithSchemaNS.xsd");            
            settings.Schemas = schemas;                        
            
            var xmlreader = XmlReader.Create(xmlFile, settings);
            
            var xml = new XmlDocument();
            
            var namespaceManager = new XmlNamespaceManager(xml.NameTable);
            namespaceManager.AddNamespace("xyz", "urn:www.ssg.com");            

            #endregion

            try
            {
                
                xml.Load(xmlreader);
            }
            catch (XmlSchemaValidationException e)
            {
                Console.WriteLine(String.Format("Schema Validation Error : {0}", e.Message));
            }

            
            var body = xml.SelectSingleNode("/xyz:ItemCollection",namespaceManager);            
            

            if (body != null)
            {
                Console.WriteLine(body.InnerText);
                Console.WriteLine(body.InnerXml);    
            }
            else
            {
                Console.WriteLine("Node not found!");
            }

            
            
        }
    }
}
