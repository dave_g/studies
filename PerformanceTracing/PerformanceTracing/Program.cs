﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using ThreadState = System.Diagnostics.ThreadState;

namespace PerformanceTracing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            String input;

            Console.Write(">type 'startloop' to loop monitor.\n");
            Console.Write(">type 'showprocesses' to view processes.\n");
            Console.Write(">type 'showprocessthreads[spt] {process id}' to view process threads.\n");
            Console.Write(">");
            GetStackTrace();

            while ((input = Console.In.ReadLine().ToLower()) != "exit")
            {

                if (input == "showprocesses" | input == "sp")
                {
                    foreach (Process process in Process.GetProcesses().OrderBy(p => p.ProcessName))
                    {
                        Console.WriteLine(String.Format("{0} {1}", process.Id, process.ProcessName));
                    }
                }

                if (input.StartsWith("showprocessthreads") | input.StartsWith("spt"))
                {
                    string[] iargs = input.Split(Char.Parse(" "));
                    var arg = input.Substring(input.IndexOf(" "), input.Length - input.IndexOf(" "));
                    int id;
                    Int32.TryParse(iargs[1], out id);
                    Process process = Process.GetProcessById(id);

                    if (iargs.Count() > 2 && iargs[2] == "/l")
                    {

                        EventWaitHandle stopper = new ManualResetEvent(false);

                        new Thread(() =>
                                       {
                                           while (!stopper.WaitOne(200, false))
                                           {
                                               Console.Clear();
                                               Console.WriteLine("Monitoring - press any key to quit");
                                               foreach (ProcessThread thread in process.Threads)
                                               {
                                                   Console.WriteLine(String.Format("{0} {1} {2} {3} {4}", thread.Id,
                                                                                   thread.StartTime,
                                                                                   thread.TotalProcessorTime,
                                                                                   thread.ThreadState,
                                                                                   thread.ThreadState ==
                                                                                   ThreadState.Wait
                                                                                       ? thread.WaitReason.ToString()
                                                                                       : ""));
                                               }
                                           }
                                       }

                            ).Start();


                        Console.ReadKey();
                        stopper.Set();
                        Console.Write(">");
                    }
                    else
                    {
                        foreach (ProcessThread thread in process.Threads)
                        {
                            Console.WriteLine(String.Format("{0} {1} {2} {3} {4}", thread.Id, thread.StartTime, thread.TotalProcessorTime, thread.ThreadState, thread.ThreadState == ThreadState.Wait ? thread.WaitReason.ToString() : ""));
                        }
                    }
                }
                if (input == "startloop")
                {
                    EventWaitHandle stopper = new ManualResetEvent(false);

                    new Thread(() =>
                       MonitorLoop(stopper)
                ).Start();

                    Console.WriteLine("Monitoring - press any key to quit");
                    Console.ReadKey();
                    stopper.Set();
                }

                Console.Write(">");
            }
        }

        private static void GetStackTrace()
        {                    
            StackTrace s = new StackTrace(true);

            Console.WriteLine("Total frames:   " + s.FrameCount);
            Console.WriteLine("Current method: " + s.GetFrame(0).GetMethod().Name);
            Console.WriteLine("Calling method: " + s.GetFrame(1).GetMethod().Name);
            Console.WriteLine("Entry method:   " + s.GetFrame
                                                 (s.FrameCount - 1).GetMethod().Name);
            Console.WriteLine("Call Stack:");
            foreach (StackFrame f in s.GetFrames())
                Console.WriteLine(
                  "  File: " + f.GetFileName() +
                  "  Line: " + f.GetFileLineNumber() +
                  "  Col: " + f.GetFileColumnNumber() +
                  "  Offset: " + f.GetILOffset() +
                  "  Method: " + f.GetMethod().Name);

        }

        private static void StartMonitorLoop()
        {
            EventWaitHandle stopper = new ManualResetEvent(false);

            new Thread(() =>
                       MonitorLoop("Processor", "% Processor Time", "_Total", stopper)
                ).Start();

            new Thread(() =>
                       MonitorLoop("LogicalDisk", "% Idle Time", "C:", stopper)
                ).Start();

            Console.WriteLine("Monitoring - press any key to quit");
            Console.ReadKey();
            stopper.Set();
        }


        static void MonitorLoop(string category, string counter, string instance,
                     EventWaitHandle stopper)
        {
            ValidatePerformanceCounter(category, counter, instance);

            float lastValue = 0f;
            using (PerformanceCounter pc = new PerformanceCounter(category,
                                                                counter, instance))
                while (!stopper.WaitOne(200, false))
                {
                    float value = pc.NextValue();
                    if (value != lastValue)         // Only write out the value
                    {                               // if it has changed.
                        Console.WriteLine(category + value);
                        lastValue = value;
                    }
                }
        }

        static void MonitorLoop(EventWaitHandle stopper)
        {

            ValidatePerformanceCounter("Processor", "% Processor Time", "_Total");
            ValidatePerformanceCounter("LogicalDisk", "% Idle Time", "C:");

            PerformanceCounter pc = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            PerformanceCounter pc2 = new PerformanceCounter("LogicalDisk", "% Idle Time", "C:");
            float lastValue1 = 0f;
            float lastValue2 = 0f;
            float value;
            float value2;

            while (!stopper.WaitOne(200, false))
            {


                value = pc.NextValue();
                if (value != lastValue1)         // Only write out the value
                {                               // if it has changed.                        
                    lastValue1 = value;
                }

                value2 = pc2.NextValue();
                if (value2 != lastValue2)         // Only write out the value
                {                               // if it has changed.                        
                    lastValue2 = value2;
                }

                Console.Clear();
                Console.WriteLine("Processor " + lastValue1);
                Console.WriteLine("LogicalDisk " + lastValue2);
            }

            pc.Dispose();
            pc2.Dispose();

        }

        private static void ValidatePerformanceCounter(string category, string counter, string instance)
        {
            if (!PerformanceCounterCategory.Exists(category))
                throw new InvalidOperationException("Category does not exist");

            if (!PerformanceCounterCategory.CounterExists(counter, category))
                throw new InvalidOperationException("Counter does not exist");

            if (instance == null) instance = "";   // "" == no instance (not null!)
            if (instance != "" &&
                !PerformanceCounterCategory.InstanceExists(instance, category))
                throw new InvalidOperationException("Instance does not exist");
        }
    }
}
