########################################################################################################
# PowerShell script for detecting location of effective child elements of service behavior configuration
# Application Server Extensions for .NET 4
# Copyright (c) Microsoft Corporation.  All rights reserved.
########################################################################################################

# Load Application Server AppFabric powershell module
if ((Get-Command -Module ApplicationServer) -eq $null)
{
    Import-Module ApplicationServer
}

# Load Microsoft.Web.Administration API for IIS config reading
[System.Reflection.Assembly]::LoadFrom( "C:\windows\system32\inetsrv\Microsoft.Web.Administration.dll" ) | Out-Null

# Cmdlet function (the entry point)
function Get-ServiceBehaviorConfigLocation
{  
   [CmdletBinding(DefaultParameterSetName="SiteNameAndVirtualPath")]

   Param ( [Parameter(ParameterSetName="ServiceObject",ValueFromPipeline=$true,Position=0,Mandatory=$true)] 
           [Microsoft.ApplicationServer.Management.Data.ServiceInfo]
           $ServiceInfo
           ,
           [Parameter(ParameterSetName="SiteNameAndVirtualPath",Position=0,Mandatory=$true)] 
           [string]
           $SiteName
           ,
           [Parameter(ParameterSetName="SiteNameAndVirtualPath",Position=1)] 
           [string]
           $VirtualPath
         )

   Process 
   {
        if ($ServiceInfo -ne $null)
        {
            GetBehaviorConfigLocationPerService $ServiceInfo
        }
        else
        {
            Get-ASAppService $SiteName $VirtualPath | foreach-object { GetBehaviorConfigLocationPerService $_ }
        }
   }
   
   End
   {
        if ($serverManager -ne $null)
        {
            $serverManager.Dispose()
        }
   }

}

# The main logic for determining the location of effective behavior configuration
function GetBehaviorConfigLocationPerService([Microsoft.ApplicationServer.Management.Data.ServiceInfo] $service)
{
    $serverManager = New-Object Microsoft.Web.Administration.ServerManager
    $config = $serverManager.GetWebConfiguration($service.SiteName, $service.VirtualPath)
    $behaviorElements = FindBehaviorElementsByName $service.BehaviorConfigurationName $config
        
    $effectiveChildElementMap = @{}  # a hashtable
       
    # Iterate through the matched <behavior> elements
    # The underlying API (MWA) sorts the element by the depth in the IIS heirachy
    foreach ($behavior in $behaviorElements)
    {
        if (IsClearTagPresent $behavior)
        {
            $effectiveChildElementMap.Clear() # all chid elements defined in parent <behavior> are removed
        }
            
        foreach ($child in $behavior.ChildElements)
        {            
            $childName = $child.ElementTagName
            if (IsRemoveTagPresent $childName $behavior)
            {
                $effectiveChildElementMap.Remove($childName)  
            }
                                           
            if ($child.GetMetaData("isPresent"))
            {                    
                $effectiveChildElementMap[$childName] = $behavior # overrides the config declared higher in the heirachy
            }
        }
    }
        
    $effectiveChildElementMap.GetEnumerator() | select-object @{Name="Name"; Expression={$_.Name}}, @{Name="Location"; Expression={$_.Value.GetMetadata("collectionItemFileConfigPath")}}   
}

# Helper functions

# Finds all <behavior> elements with "name" attribute matching the specified name.
function FindBehaviorElementsByName([string] $name, [Microsoft.Web.Administration.Configuration] $config)
{
    $behaviorSection = $config.GetSection("system.serviceModel/behaviors")
    $serviceBehaviorsElements = $behaviorSection.GetChildElement("serviceBehaviors").GetCollection()
    
    foreach ($element in $serviceBehaviorsElements)
    {
        $behaviorName = $element.GetAttribute("name").Value
        if ($behaviorName -eq $name)
        {
            $matchedElements += ,$element
        }
    }
    return $matchedElements
}

# Determines if <clear> element is present under the specified <behavior> element
function IsClearTagPresent([Microsoft.Web.Administration.ConfigurationElement] $behaviorElement)
{
    $removeClearCollection = $behaviorElement.GetCollection()
    
    foreach ($element in $removeClearCollection)
    {
        if ($element.ElementTagname -eq "clear")        
        {
            return $true
        }
    }
    return $false    
}

# Determines if <remove> element with matching "name" attribute is present under the specified <behavior> element
function IsRemoveTagPresent([string] $elementName, [Microsoft.Web.Administration.ConfigurationElement] $behaviorElement)
{
    $removeClearCollection = $behaviorElement.GetCollection()
    
    foreach ($element in $removeClearCollection)
    {
        if ($element.ElementTagname -eq "remove" -and $element.GetAttributeValue("name") -eq $elementName)        
        {
            return $true
            
        }
    }
    return $false
}

# Example1: Run Get-ServiceBehaviorConfigLocation on one service
# . .\detectServiceBehaviorConfigLocation.ps1
# Get-ServiceBehaviorConfigLocation -SiteName "Default Web Site" -VirtualPath /App/service.svc

# Example2: Run Get-ServiceBehaviorConfigLocation on all services under a specified scope (e.g. application). Also adding service identifier (SiteName/VirtualPath) to the output.
# . .\detectServiceBehaviorConfigLocation.ps1
#    Get-ASAppService -SiteName "Default Web Site" -VirtualPath /App | 
#      foreach-object {$service=$_; Get-ServiceBehaviorConfigLocation $service | 
#        select-object @{Name="SiteName"; Expression={$service.SiteName}}, @{Name="VirtualPath"; Expression={$service.VirtualPath}}, "Name", "Location"}