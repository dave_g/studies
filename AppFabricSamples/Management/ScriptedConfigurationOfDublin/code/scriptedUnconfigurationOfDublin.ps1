﻿############################################################
# PowerShell script to configure
# Application Server Extensions for .NET 4
# Copyright (c) Microsoft Corporation.  All rights reserved.
############################################################
if ((Get-Command -Module ApplicationServer) -eq $null)
{
    Import-Module ApplicationServer # Application Server Extensions for .NET 4
}

############################################################

function RevertServiceCredential([string]$serviceName)
{
    $service = Get-WmiObject Win32_Service -filter "name='$serviceName'" 
    $service.Change($null, $null, $null, $null, $null, $null,"NT AUTHORITY\LocalService", "") |out-null
    Restart-Service -name "$serviceName"
}
##############################
### User defined variables ###
##############################

$computer = "."
                
### Event Collector Service (ECS) and Workflow Management Service (WMS) service names.
$ECS_ServiceName = "AppFabricEventCollectionService"
$WMS_ServiceName = "AppFabricWorkflowManagementService" 
                
### Domain user, member of Application Server Administrators group.
$systemService_Domain ="corp"
$systemService_UserName = "dubAdmin"

### Domain user, member of Application Server User group.
$appPool_Domain = "corp"
$appPool_UserName = "dubUser"
                
### Monitoring database
$Monitoring_ConnectionStringName = "monitoringDB"
$Monitoring_ConnectionString = "Data Source=AD-SQL;Initial Catalog=monitoringDB;Integrated Security=true"
$Monitoring_MonitoringLevel = "HealthMonitoring" # Used by the monitoring behavior.
                
### Persistence database
$Persistence_ConnectionStringName = "persistenceDB"
$Persistence_InstanceStoreName = "sqlPersistence"
$Persistence_ConnectionString = "Data Source=AD-SQL;Initial Catalog=PersistenceDB;Integrated Security=true"

##########################
###Revert Configuration###
##########################
Write-Output "Removing the Administrator user from the local Administrators group..."
$oGroup = [ADSI]"WinNT://$computer/Administrators"
trap { continue } #'Administrator user is not a member of the local Administrators group...'; 
& { $oGroup.remove("WinNT://$systemService_Domain/$systemService_UserName") }


Write-Output "Reverting Event Collection service..."
RevertServiceCredential $ECS_ServiceName

Write-Output "Reverting Workflow Management service..."
RevertServiceCredential $WMS_ServiceName

Write-Output "Removing persistence behavior ..."
clear-ASAppSqlServicePersistence -Root -confirm:$false

Write-Output "Removing persistence instance store ..."
Remove-ASAppSqlInstanceStore -Name $Persistence_InstanceStoreName -Root -confirm:$false