﻿############################################################
# PowerShell script to configure
# Application Server Extensions for .NET 4
# Copyright (c) Microsoft Corporation.  All rights reserved.
############################################################

if ((Get-Command -Module ApplicationServer) -eq $null)
{
    Import-Module ApplicationServer # Application Server Extensions for .NET 4
}

############################################################

function GetUserCredential([string]$serviceName, [string]$serviceDescription, [string]$serviceUser)
{
    $credential = $host.ui.PromptForCredential($serviceName, $serviceDescription, $serviceUser, "")
    return $credential # PSCredential object
}

function SetServiceCredential([string]$serviceName, $credential)
{
    $domainAndUserName = $credential.GetNetworkCredential().Domain + "\" + $credential.GetNetworkCredential().UserName;
    $service = Get-WmiObject Win32_Service -filter "name='$serviceName'"
    $service.Change($null, $null, $null, $null, $null, $null, $domainAndUserName, $credential.GetNetworkCredential().Password) |out-null
    Restart-Service -name "$serviceName"
}

# Gets a SQL connection string to the specified server and database.
function GetSqlConnectionString([string]$server, [string]$database)
{
    $builder = New-Object System.Data.SqlClient.SqlConnectionStringBuilder
    $builder.PSBase.DataSource = $server
    $builder.PSBase.InitialCatalog = $database
    $builder.PSBase.IntegratedSecurity = $true
    return [string] $builder.ToString()
}

# Adds or updates the specified connection string setting in the specified .NET configuration file.
function UpdateConnectionString([string]$name, [string]$connectionString, [string]$providerName)
{  
    & $appcmd set config /clr:4 /commit:WEBROOT /section:connectionStrings /+"[connectionString='$connectionString',name='$name',providerName='$providerName']" |out-null
}

##############################
### User defined variables ###
##############################

$computer = "."
$appcmd = "$env:SystemRoot\system32\inetsrv\appcmd.exe"

### Event Collector Service (ECS) and Workflow Management Service (WMS) service names.
$ECS_ServiceName = "AppFabricEventCollectionService"
$WMS_ServiceName = "AppFabricWorkflowManagementService"

### Domain user, member of Application Server Administrators group.
### ECS and WMS will run under this identity.
$systemService_Domain ="corp"
$systemService_UserName = "dubAdmin"

### Monitoring database
$Monitoring_ConnectionStringName = "monitoringDB"
$Monitoring_ConnectionString = GetSqlConnectionString "AD-SQL" "monitoringDB"

$Monitoring_MonitoringLevel = "HealthMonitoring" # Used by the monitoring behavior.

### Persistence database
$Persistence_ConnectionStringName = "persistenceDB"
$Persistence_InstanceStoreName = "sqlPersistence"
$Persistence_ConnectionString = GetSqlConnectionString "AD-SQL" "persistenceDB"

###########################
### Collect credentials ###
###########################

$systemService_Credentials = GetUserCredential "System Services" "Provide the credentials for the System Services user:" "$systemService_Domain\$systemService_UserName"
$systemService_Domain = $systemService_Credentials.GetNetworkCredential().Domain
$systemService_UserName = $systemService_Credentials.GetNetworkCredential().UserName

############################
### Update configuration ###
############################

Write-Output "Adding the Administrator user to the local Administrators group..."
$oGroup = [ADSI]"WinNT://$computer/Administrators"
trap { continue } #'Administrator user already a member of the local Administrators group...'; 
& { $oGroup.Add("WinNT://$systemService_Domain/$systemService_UserName") }

Write-Output "Updating Event Collection service..."
SetServiceCredential $ECS_ServiceName $systemService_Credentials

Write-Output "Updating Workflow Management service..."
SetServiceCredential $WMS_ServiceName $systemService_Credentials

Write-Output "Creating connection strings..."
UpdateConnectionString $Monitoring_ConnectionStringName $Monitoring_ConnectionString "System.Data.SqlClient" |out-null
UpdateConnectionString $Persistence_ConnectionStringName $Persistence_ConnectionString "System.Data.SqlClient" |out-null

Write-Output "Creating Persistence Instance Store..."
Add-ASAppSqlInstanceStore -Name $Persistence_InstanceStoreName -ConnectionStringName $Persistence_ConnectionStringName -Root |out-null

Write-Output "Creating default behaviors..." 
Set-ASAppMonitoring -ConnectionStringName $Monitoring_ConnectionStringName -MonitoringLevel $Monitoring_MonitoringLevel -Root |out-null
Set-ASAppSqlServicePersistence -ConnectionStringName $Persistence_ConnectionStringName -Root -HostLockRenewalPeriod "00:00:20" -InstanceEncodingOption "GZip" -InstanceCompletionAction "DeleteNothing" -InstanceLockedExceptionAction "BasicRetry" |out-null