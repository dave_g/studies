# Copyright (c) Microsoft Corporation.  All rights reserved

function RegisterComponents()
{
    $snapin = "SqlServerCmdletSnapin100"
    $reg = Get-PSSnapin -Registered -Name $snapin
    $added = Get-PSSnapin -Name $snapin -ErrorAction SilentlyContinue
    if ($reg.Name -eq $snapin)
    {
        if ($added.Name -ne $snapin)
        {
            Add-PSSnapin $snapin
        } 
    } else {
        write-error -Message "Cannot register SQL server snap in. Please make sure SQL Server 2008 client components are installed on the machine and try again."
    }
    Import-Module ApplicationServer
}


function PrepareWhereClause()
{
	if ($where.Equals(""))
	{
		$where = "WHERE ";
	}
	else
	{
		$where += " AND ";
	}
	return $where
}

function GetCommonWherePredicates()
{
	$where = "";
	if (-not [String]::IsNullOrEmpty($SiteName))
	{
		$where = PrepareWhereClause;
		$where += "Site='$SiteName'"
	}
	if (-not [String]::IsNullOrEmpty($VirtualPath))
	{
		$where = PrepareWhereClause;
		$where += "[ASEventSources].[VirtualPath] LIKE '$VirtualPath%'"
	}
	if (-not [String]::IsNullOrEmpty($MachineName))
	{
		$where = PrepareWhereClause;
		$where += "Computer='$MachineName'"
	}
	if (-not [String]::IsNullOrEmpty($ActiveSince))
	{
		$where = PrepareWhereClause;
		$where += "LastModifiedTime>='$ActiveSince'"
	}
	return $where
}

function NormalizeParams()
{
        if ([String]::IsNullOrEmpty($Server))
        {
            $Server = [System.Environment]::MachineName + "\SQLExpress"
        }
    
        if ([String]::IsNullOrEmpty($Database))
        {
            $Database = "ApplicationServerMonitoring"
        }
    
        switch ($PsCmdlet.ParameterSetName) 
        { 
            "SiteNameAndVirtualPath"
            {
                if (-not [String]::IsNullOrEmpty($VirtualPath) -and -not $VirtualPath.StartsWith("/"))
                {
                    $VirtualPath = "/$VirtualPath"
                }
                break
            } 
            
            "ApplicationObject"
            {
                $SiteName = $ApplicationObject.SiteName
                $VirtualPath = $ApplicationObject.VirtualPath
                break
            }
        }
        
        $Server
        $Database
        $SiteName
        $VirtualPath
}

function GetActiveMachinesFromTracking 
{
        $where = GetCommonWherePredicates

        $query = "SELECT DISTINCT [Computer] AS [ComputerName] FROM (
                    SELECT [Computer],[LastModifiedTime] FROM [ASEventSources]
                    INNER JOIN [ASWfInstances] ON [ASEventSources].[Id] = [ASWfInstances].[LastEventSourceId]
                    $where
                    ) a";
                    
        Write-Verbose "Tracked instances query: $query"                    

        $tracked = Invoke-Sqlcmd -Query "$query" -Server "$Server" -Database "$Database" | %{ $_["ComputerName"].ToUpper() }
                
        $tracked
}

function GetActiveMachinesFromPersistence 
{
        $command = "Get-ASAppServiceInstance -MachineName $Server"
        
        if($ActiveSince -ne $null)
        { 
           $command += " -ModifiedTimeFrom '$ActiveSince'"
        }
        if(-not [String]::IsNullOrEmpty($SiteName))
        {
            $command += " -SiteName '$SiteName'"
        }
        if(-not [String]::IsNullOrEmpty($VirtualPath))
        {
            $command += " -VirtualPath $VirtualPath"
        }
        
        write-verbose "Executing '$command'"
        
        $persisted = Invoke-Expression $command | %{ $_.LastUpdatedBy.ToUpper() } | Select -Unique
                
        $persisted
}




function Get-ASActiveMachine
{

   [CmdletBinding(DefaultParameterSetName="Default")]   
   param(

       [Parameter(
       Position=1,
       ParameterSetName=("SiteNameAndVirtualPath"),
       ValueFromPipelineByPropertyName=$true)]
       [ValidateNotNull()]
       [String]$SiteName,

       [Parameter(
       Position=2,
       ParameterSetName=("SiteNameAndVirtualPath"),
       ValueFromPipelineByPropertyName=$true)]
       [ValidateNotNull()]
       [String]$VirtualPath,
       
       [Parameter(
       Mandatory=$true,
       ValueFromPipelineByPropertyName=$true)]
       [ValidateNotNullOrEmpty()]
       [String]$Database,

       [Parameter(
       Mandatory=$true,
       ValueFromPipelineByPropertyName=$true)]
       [ValidateNotNullOrEmpty()]
       [String]$Server,

       [Parameter(
       ParameterSetName=("ApplicationObject"),
       ValueFromPipeline=$true,
       Mandatory=$true)]
       [ValidateNotNull()]
       [Microsoft.ApplicationServer.Management.Data.ApplicationInfo]$ApplicationObject,
       
       [Parameter(
       ValueFromPipelineByPropertyName=$true)]
       [ValidateNotNull()]
       [DateTime]$ActiveSince
   )

    Process 
    {
        $params = NormalizeParams
        $Server = $params[0]
        $Database = $params[1]
        $SiteName = $params[2]
        $VirtualPath = $params[3]
    
        $tracked = @(GetActiveMachinesFromTracking)
        $persisted = @(GetActiveMachinesFromPersistence)
        @($tracked + $persisted | select -Unique)
    }
}

RegisterComponents
