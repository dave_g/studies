@echo off

if "%1"=="" goto print_syntax
if "%1"=="-fromLocalServer" goto sync_server
if "%1"=="-fromPackage" goto sync_package
goto print_syntax

:sync_server
choice /c yn /M "All of the existing applications on the remote machines will be removed. Do you want to continue?"
if errorlevel 2 goto done
for /f %%i in ('type machines.txt') do (
start syncOne.cmd %%i %2
)
goto done

:sync_package
if "%2"=="" goto print_syntax 
for /f %%i in ('type machines.txt') do (
start deployOne.cmd %%i %2 %3
)
goto done

:print_syntax
echo Invalid command syntax.
echo Usage1: sync.cmd -fromLocalServer
echo Usage2: sync.cmd -fromPackage ^<package file path^>

:done