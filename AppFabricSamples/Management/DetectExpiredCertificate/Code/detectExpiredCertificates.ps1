#############################################################################
# PowerShell script for detecting expired certificate used by WCF/WF services
# Windows Server AppFabric
# Copyright (c) Microsoft Corporation.  All rights reserved.
#############################################################################

# Helper function - Resolve runtime default values for the config properties returned by the Get-ASAppServiceCertificate
function ResolveDefaultValue($original, $defaultValue)
{
    if ($original -eq $null)
    {
        return $defaultValue
    }
    return $original
}

# Helper function - Query the certificate stores using System.Security.Cryptography.X509Certificates API
function FindCertificates($query)
{
    $store = New-Object System.Security.Cryptography.X509Certificates.X509Store($query.StoreName, $query.StoreLocation)
    $store.Open("OpenExistingOnly")
    return $store.Certificates.Find($query.FindType, $query.FindValue, $false)
}

# The main script

# Load Application Server AppFabric powershell module
if ((Get-Command -Module ApplicationServer) -eq $null)
{
    Import-Module ApplicationServer 
}

# Get the list of all services under the specified scope via Get-ASAppService cmdlet
if ($args[0] -eq $null)
{
    $services = Get-ASAppService -Root
}
elseif ($args[1] -eq $null)
{
    $services = Get-ASAppService -SiteName $args[0]
}
else
{
    $services = Get-ASAppService -SiteName $args[0] -VirtualPath $args[1]
}

# Read service certificate configuration and resolve the runtime default values
$certQueriesUsedByServices = $services | Get-ASAppServiceCertificate | Select-Object FindValue,
                                                                         @{Name="StoreLocation";Expression={ ResolveDefaultValue $_.StoreLocation "LocalMachine"}},
                                                                         @{Name="StoreName";Expression={ ResolveDefaultValue $_.StoreName "My"}},
                                                                         @{Name="FindType";Expression={ ResolveDefaultValue $_.FindType "FindBySubjectDistinguishedName"}}

# Read certificate store and search for ones that have "NotAfter" property less than Now
if ($certQueriesUsedByServices -ne $null)
{
    $certQueriesUsedByServices | foreach-object {FindCertificates $_} | select-object -unique | where-object {($_.NotBefore -lt [DateTime]::Now) -or ($_.NotAfter -lt [DateTime]::Now)} | select-object Thumbprint,Subject,NotBefore,NotAfter
}

# Example1: Detects expired certificates being used by any services on deployed on the machine
# .\detectExpiredCertificates.ps1

# Example2: Detects expired certificates being used by services under a specified application
# .\detectExpiredCertificates.ps1 "Default Web Site" /MyApp
