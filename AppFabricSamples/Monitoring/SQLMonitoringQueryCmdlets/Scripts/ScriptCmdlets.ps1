$global:DefaultMaxResult = "50"

function RegisterSnapIn($snapin)
{
	$reg = Get-PSSnapin -Registered -Name $snapin
	$added = Get-PSSnapin -Name $snapin -ErrorAction SilentlyContinue
	if ($reg.Name -eq $snapin)
	{
		if ($added.Name -ne $snapin)
		{
			Add-PSSnapin $snapin
		}
	}
}

RegisterSnapIn("SqlServerProviderSnapin100")
RegisterSnapIn("SqlServerCmdletSnapin100")
Import-Module ApplicationServer

function PrepareWhereClause()
{
	if ($where.Equals(""))
	{
		$where = "WHERE ";
	}
	else
	{
		$where += " AND ";
	}
	return $where
}

function GetCommonWherePredicates()
{
	$where = "";
	if (-not [String]::IsNullOrEmpty($SiteName))
	{
		$where = PrepareWhereClause;
		$where += "Site='$SiteName'"
	}
	if (-not [String]::IsNullOrEmpty($VirtualPath))
	{
		$where = PrepareWhereClause;
		$where += "[ASEventSources].[VirtualPath] LIKE '$VirtualPath%'"
	}
	if (-not [String]::IsNullOrEmpty($MachineName))
	{
		$where = PrepareWhereClause;
		$where += "Computer='$MachineName'"
	}
	if (-not [String]::IsNullOrEmpty($ActiveSince))
	{
		$where = PrepareWhereClause;
		$where += "LastModifiedTime>='$ActiveSince'"
	}
	return $where
}

function GetInstanceWhereClause()
{
	$where = GetCommonWherePredicates;

	if (-not [String]::IsNullOrEmpty($ModifiedSince))
	{
		$ModifiedSince = $ModifiedSince.ToUniversalTime();
		$where = PrepareWhereClause;
		$where += "LastModifiedTime >= '$ModifiedSince'"
	}

	return $where
}

function GetEventWhereClause()
{
	$where = GetCommonWherePredicates;

	if (-not [String]::IsNullOrEmpty($EmitTimeFrom))
	{
		$EmitTimeFrom = $EmitTimeFrom.ToUniversalTime();
		$where = PrepareWhereClause;
		$where += "TimeCreated >= '$EmitTimeFrom'"
	}

	if (-not [String]::IsNullOrEmpty($EmitTimeTo))
	{
		$EmitTimeTo = $EmitTimeTo.ToUniversalTime();
		$where = PrepareWhereClause;
		$where += "TimeCreated <= '$EmitTimeTo'"
	}
	
		if (-not [String]::IsNullOrEmpty($Status))
	{
		$where = PrepareWhereClause;
		$where += " State = '$Status'";
	}

	return $where
}

function NormalizeParams()
{
	switch ($PsCmdlet.ParameterSetName) 
	{
		"SiteNameAndVirtualPath"
		{
			if (-not [String]::IsNullOrEmpty($VirtualPath) -and -not $VirtualPath.StartsWith("/"))
			{
				$VirtualPath = "/$VirtualPath"
			}
			break
		} 
		"ApplicationObject"
		{
			$SiteName = $ApplicationObject.SiteName
			$VirtualPath = $ApplicationObject.VirtualPath
			break
		}
	}

	$Server
	$Database
	$SiteName
	$VirtualPath
}

function Get-ASAppTrackedPropertyName
{
	[CmdletBinding(DefaultParameterSetName="Default")]
	param(
		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Database,

		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Server,

		[Parameter(
		Position=1,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$SiteName,

		[Parameter(
		Position=2,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$VirtualPath,

		[Parameter(
		ParameterSetName=("ApplicationObject"),
		ValueFromPipeline=$true,
		Mandatory=$true)]
		[ValidateNotNull()]
		[Microsoft.ApplicationServer.Management.Data.ApplicationInfo]$ApplicationObject,
 
		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[String]$MachineName
	)

	Process 
	{
		$params = NormalizeParams
		$Server = $params[0]
		$Database = $params[1]
		$SiteName = $params[2]
		$VirtualPath = $params[3]

		$where = GetCommonWherePredicates

		$query = "SELECT [dbo].[ASWfPropertyNames].[Name]
				FROM [dbo].[ASWfPropertyNames]
				INNER JOIN [dbo].[ASEventSources]
				ON [dbo].[ASWfPropertyNames].[EventSourceId] = [dbo].[ASEventSources].[Id] 
				$where";

		Write-Verbose $query
		Invoke-Sqlcmd -Query "$query" -Server "$Server" -Database "$Database"
	}
}

function Get-ASAppTrackedInstance
{
	[CmdletBinding(DefaultParameterSetName="Default")]
	param(
		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Database,

		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Server,

		[Parameter(
		Position=1,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$SiteName,

		[Parameter(
		Position=2,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$VirtualPath,

		[Parameter(
		ParameterSetName=("ApplicationObject"),
		ValueFromPipeline=$true,
		Mandatory=$true)]
		[ValidateNotNull()]
		[Microsoft.ApplicationServer.Management.Data.ApplicationInfo]$ApplicationObject,

		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[String]$MachineName,

		[Parameter(
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[DateTime]$ModifiedSince,

		[Parameter()]
		[ValidateNotNull()]
		[Int]$MaxResults,

		[Parameter()]
		[System.Management.Automation.SwitchParameter]$Count
	)

	Process
	{
		$params = NormalizeParams
		$Server = $params[0]
		$Database = $params[1]
		$SiteName = $params[2]
		$VirtualPath = $params[3]

		$where = GetInstanceWhereClause 
        
		if ($MaxResults)
		{
			$topCount = $MaxResults
		}
		else
		{
			$topCount = $DefaultMaxResult
		}

		if($Count)
		{
			$query = "SELECT COUNT(*) AS [Total] 
				FROM [dbo].[ASEventSources]
				INNER JOIN [dbo].[ASWfInstances] ON [ASEventSources].[Id] = [ASWfInstances].[LastEventSourceId]
				$where";
		}
		else
		{
			$query = "SELECT TOP $topCount [ASEventSources].[Name] AS [ServiceName]
					,[ASEventSources].[Computer]
					,[ASEventSources].[Site]
					,[ASEventSources].[VirtualPath]
					,[ASWfInstances].[WorkflowInstanceId]
					,[ASWfInstances].[LastEventSourceId]
					,[ASWfInstances].[LastEventStatus]
					,[ASWfInstances].[StartTime]
					,[ASWfInstances].[LastModifiedTime]
					,[ASWfInstances].[CurrentDuration]
					,[ASWfInstances].[ExceptionCount]
					,[ASWfInstances].[LastAbortedTime]
					FROM [dbo].[ASEventSources]
					INNER JOIN [dbo].[ASWfInstances] ON [ASEventSources].[Id] = [ASWfInstances].[LastEventSourceId]
					$where";
		}
		Write-Verbose $query;
		Invoke-Sqlcmd -Query "$query" -Server "$Server" -Database "$Database"
	}
}

function Get-ASAppTrackedWcfEvent
{
	[CmdletBinding(DefaultParameterSetName="Default")]
	param(
		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Database,

		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Server,

		[Parameter(
		Position=1,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$SiteName,

		[Parameter(
		Position=2,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$VirtualPath,

		[Parameter(
		ParameterSetName=("ApplicationObject"),
		ValueFromPipeline=$true,
		Mandatory=$true)]
		[ValidateNotNull()]
		[Microsoft.ApplicationServer.Management.Data.ApplicationInfo]$ApplicationObject,

		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[String]$MachineName,

		[Parameter(
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[DateTime]$EmitTimeFrom,

		[Parameter(
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[DateTime]$EmitTimeTo,

		[Parameter()]
		[ValidateNotNull()]
		[Int]$MaxResults,

		[Parameter()]
		[System.Management.Automation.SwitchParameter]$Count
	)

	Process
	{
		$params = NormalizeParams
		$Server = $params[0]
		$Database = $params[1]
		$SiteName = $params[2]
		$VirtualPath = $params[3]

		$where = GetEventWhereClause

		if ($MaxResults)
		{
			$topCount = $MaxResults
		}
		else
		{
			$topCount = $DefaultMaxResult
		}

		if($Count)
		{
			$query = "SELECT COUNT(*) AS [WCF Events] 
				FROM [dbo].[ASEventSources]
				INNER JOIN [dbo].[ASWcfEvents] ON [ASEventSources].[Id] = [ASWcfEvents].[EventSourceId]
				$where";
		}
		else
		{
			$query = "SELECT TOP $topCount [ASEventSources].[Name] AS [ServiceName]
				,[ASEventSources].[Computer]
				,[ASEventSources].[Site]
				,[ASEventSources].[VirtualPath]
				,[ASWcfEvents].[EventTypeId]
				,[ASWcfEvents].[EventType]
				,[ASWcfEvents].[EventVersion]
				,[ASWcfEvents].[EventSourceId]
				,[ASWcfEvents].[ProcessId]
				,[ASWcfEvents].[TraceLevelId]
				,[ASWcfEvents].[TraceLevel]
				,[ASWcfEvents].[E2EActivityId]
				,[ASWcfEvents].[TimeCreated]
				,[ASWcfEvents].[CorrelationId]
				,[ASWcfEvents].[ServiceTypeName]
				,[ASWcfEvents].[InspectorTypeName]
				,[ASWcfEvents].[ErrorHandlerType]
				,[ASWcfEvents].[Handled]
				,[ASWcfEvents].[ExceptionMessage]
				,[ASWcfEvents].[ExceptionTypeName]
				,[ASWcfEvents].[ThrottleProperty]
				,[ASWcfEvents].[ThrottleCapacity]
				,[ASWcfEvents].[Uri]
				,[ASWcfEvents].[OperationName]
				,[ASWcfEvents].[ContractName]
				,[ASWcfEvents].[Destination]
				,[ASWcfEvents].[Duration]
				FROM [dbo].[ASEventSources]
				INNER JOIN [dbo].[ASWcfEvents] ON [ASEventSources].[Id] = [ASWcfEvents] .[EventSourceId]
				$where";
		}
		Write-Verbose $query;
		Invoke-Sqlcmd -Query "$query" -Server "$Server" -Database "$Database"
	}
}

function Get-ASAppTrackedWfEvent
{
	[CmdletBinding(DefaultParameterSetName="Default")]
	param(
		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Database,

		[Parameter(
		ValueFromPipelineByPropertyName=$true,
		Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[String]$Server,

		[Parameter(
		Position=1,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$SiteName,

		[Parameter(
		Position=2,
		ParameterSetName=("SiteNameAndVirtualPath"),
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[String]$VirtualPath,

		[Parameter(
		ParameterSetName=("ApplicationObject"),
		ValueFromPipeline=$true,
		Mandatory=$true)]
		[ValidateNotNull()]
		[Microsoft.ApplicationServer.Management.Data.ApplicationInfo]$ApplicationObject,

		[Parameter()]
		[ValidateNotNullOrEmpty()]
		[String]$MachineName,

		[Parameter(
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[DateTime]$EmitTimeFrom,

		[Parameter(
		ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNull()]
		[DateTime]$EmitTimeTo,

		[Parameter()]
		[ValidateNotNull()]
		[Int]$MaxResults,

		[Parameter()]
		[System.Management.Automation.SwitchParameter]$Count
	)

	Process 
	{
		$params = NormalizeParams
		$Server = $params[0]
		$Database = $params[1]
		$SiteName = $params[2]
		$VirtualPath = $params[3]

		$where = GetEventWhereClause

		if ($MaxResults)
		{
			$topCount = $MaxResults
		}
		else
		{
			$topCount = $DefaultMaxResult
		}

		if($Count)
		{
			$query = "SELECT COUNT(*) AS [WF Events] 
				FROM [dbo].[ASEventSources]
				INNER JOIN [dbo].[ASWfEvents] ON [ASEventSources].[Id] = [ASWfEvents].[EventSourceId]
				$where";
		}
		else
		{
			$query = "SELECT TOP $topCount [ASEventSources].[Name] AS [ServiceName]
				,[ASEventSources].[Computer]
				,[ASEventSources].[Site]
				,[ASEventSources].[VirtualPath]
				,[ASWfTrackingProfiles].[Name] AS [TrackingProfileName]
				,[ASWfEvents].[Id] AS [EventId]
				,[ASWfEvents].[EventTypeId]
				,[ASWfEvents].[EventType]
				,[ASWfEvents].[EventVersion]
				,[ASWfEvents].[EventSourceId]
				,[ASWfEvents].[ProcessId]
				,[ASWfEvents].[WorkflowInstanceId]
				,[ASWfEvents].[TrackingProfileId]
				,[ASWfEvents].[E2EActivityId]
				,[ASWfEvents].[TraceLevelId]
				,[ASWfEvents].[TraceLevel]
				,[ASWfEvents].[RecordNumber]
				,[ASWfEvents].[AnnotationSetId]
				,[ASWfEvents].[TimeCreated]
				,[ASWfEvents].[ActivityName]
				,[ASWfEvents].[ActivityId]
				,[ASWfEvents].[ActivityInstanceId]
				,[ASWfEvents].[ActivityRootId]
				,[ASWfEvents].[ActivityTypeName]
				,[ASWfEvents].[OwnerType]
				,[ASWfEvents].[State]
				,[ASWfEvents].[ChildActivityId]
				,[ASWfEvents].[ChildInstanceId]
				,[ASWfEvents].[ChildTypeName]
				,[ASWfEvents].[ChildActivityName]
				,[ASWfEvents].[FaultSrcName]
				,[ASWfEvents].[FaultSrcId]
				,[ASWfEvents].[FaultSrcInstanceId]
				,[ASWfEvents].[HandlerId]
				,[ASWfEvents].[HandlerInstanceId]
				,[ASWfEvents].[FaultHandler]
				,[ASWfEvents].[FaultSourceType]
				,[ASWfEvents].[FaultHandlerType]
				,[ASWfEvents].[Fault]
				,[ASWfEvents].[IsFaultSource]
				,[ASWfEvents].[SubInstanceID]
				,[ASWfEvents].[OwnerActivityId]
				,[ASWfEvents].[OwnerInstanceId]
				,[ASWfEvents].[OwnerActivityName]
				,[ASWfEvents].[Exception]
				,[ASWfEvents].[Reason]
				FROM [dbo].[ASEventSources]
				INNER JOIN [dbo].[ASWfEvents] ON [ASEventSources].[Id] = [ASWfEvents].[EventSourceId]
				INNER JOIN [dbo].[ASWfTrackingProfiles] ON [ASWfEvents].[TrackingProfileId] = [ASWfTrackingProfiles].[Id]
				$where";
		}
		Write-Verbose $query;
		Invoke-Sqlcmd -Query "$query" -Server "$Server" -Database "$Database"
	}
}