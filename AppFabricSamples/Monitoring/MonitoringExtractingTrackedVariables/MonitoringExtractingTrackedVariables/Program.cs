﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Samples.MonitoringExtractingTrackedVariables
{
    class Program
    {
        public const string DatabaseName = "ApplicationServerMonitoring";
        public const string SqlConnectionString        = "metadata=res://*/AppFabricMonitoringDataModel.csdl|res://*/AppFabricMonitoringDataModel.ssdl|res://*/AppFabricMonitoringDataModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source=localhost;Initial Catalog=" + DatabaseName + ";Integrated Security=True;MultipleActiveResultSets=True\"";
        public const string SqlExpressConnectionString = "metadata=res://*/AppFabricMonitoringDataModel.csdl|res://*/AppFabricMonitoringDataModel.ssdl|res://*/AppFabricMonitoringDataModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source=localhost\\SqlExpress;Initial Catalog=" + DatabaseName + ";Integrated Security=True;MultipleActiveResultSets=True\"";

        static void Main(string[] args)
        {
            AppFabricMonitoringEntities context = new AppFabricMonitoringEntities(SqlExpressConnectionString);

            // Make sure there is at least one tracked variable in the database
            var lastVarQuery = (from e in context.ASWfEventProperties select e).OrderByDescending(e => e.EventId).OrderByDescending(e=> e.TimeCreated).Take(1);
            if (lastVarQuery.Count() == 0)
            {
                Console.WriteLine("There are no tracked variables in the database");
                return;
            }
            
            // Get the Event that owns the most recent tracked variable
            ASWfEventProperty lastProperty = lastVarQuery.First();
            var lastEventQuery = from e in context.ASWfEvents where e.Id == lastProperty.EventId select e;
            if (lastEventQuery.Count() == 0)
            {
                Console.WriteLine("Could not find the event with Id {0} that corresponds with the variable", lastProperty.EventId);
            }
            ASWfEvent eventWithVariables = lastEventQuery.First();

            // Retrieve and deserialize all variables that were tracked with the event 
            IEnumerable<TrackedWFVariable> deserialized = TrackedWFVariable.GetVariables(context, eventWithVariables);
            foreach (TrackedWFVariable var in deserialized)
            {
                if (var.Exception == null)
                {
                    Console.WriteLine("Retrieved variable '{0}' of type '{1}'.  Value = '{2}'\n", var.Name, var.TypeFullName, var.ResolvedValue != null ? var.ResolvedValue.ToString() : "null");
                }
                else
                {
                    Console.WriteLine("Error Retrieving variable '{0}' of type '{1}'. {2}\n", var.Name, var.TypeFullName, var.Exception.Message);
                }
            }
        }
    }
}
