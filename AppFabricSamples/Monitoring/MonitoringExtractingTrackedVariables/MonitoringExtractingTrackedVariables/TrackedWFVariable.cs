﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Microsoft.Samples.MonitoringExtractingTrackedVariables
{
    public class TrackedWFVariable
    {
        /// <summary>
        /// Value retrieved from the Type column in the WFPropertiesTable
        /// </summary>
        public string TypeFullName { get; set; }
        /// <summary>
        /// CLR object that was retrieved from either the Value or ValueBlob column in the WFPropertiesTable.  If the object
        /// could not be resolved then this will be set to null and the Exception property will be populated.
        /// </summary>
        public object ResolvedValue { get; set; }
        /// <summary>
        /// The raw string value of the object retrieved from either the Value or ValueBlob column in the WFPropertiesTable.
        /// </summary>
        public string RawValue { get; set; }
        /// <summary>
        /// Value retrieved from the Name column in the WFPropertiesTable
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// If their was an exception deserializing a complex object then this field will be populated.
        /// </summary>
        public Exception Exception { get; set; }

        public static Dictionary<ASWfEvent, IEnumerable<TrackedWFVariable>> GetVariables(AppFabricMonitoringEntities context, IEnumerable<ASWfEvent> events)
        {
            Dictionary<ASWfEvent, IEnumerable<TrackedWFVariable>> eventsWithVariables = new Dictionary<ASWfEvent, IEnumerable<TrackedWFVariable>>();
            foreach (ASWfEvent e in events) eventsWithVariables.Add(e, GetVariables(context, e));
            return eventsWithVariables;
        }

        public static IEnumerable<TrackedWFVariable> GetVariables(AppFabricMonitoringEntities context, ASWfEvent wfEvent)
        {
            var trackedVars = from row in context.ASWfEventProperties where row.EventId == wfEvent.Id select row;

            List<TrackedWFVariable> ret = new List<TrackedWFVariable>();

            foreach (ASWfEventProperty prop in trackedVars)
            {
                TrackedWFVariable next = new TrackedWFVariable()
                {
                    Name = prop.Name,
                    TypeFullName = prop.Type,
                    RawValue = prop.ValueBlob == null ? prop.Value : prop.ValueBlob
                };

                try
                {
                    if (string.IsNullOrEmpty(prop.Type))
                    {
                        next.ResolvedValue = null;
                        next.RawValue = null;
                        next.TypeFullName = "UnknownBecauseValueWasNull";
                    }
                    else
                    {
                        if (prop.ValueBlob == null) next.ResolvedValue = ParseSimpleType(prop);
                        else next.ResolvedValue = DeserializeComplexType(prop.ValueBlob);
                    }
                }
                catch (Exception ex)
                {
                    next.Exception = ex;
                }
                ret.Add(next);
            }

            return ret;
        }

        private static object ParseSimpleType(ASWfEventProperty prop)
        {
            switch (prop.Type)
            {
                case "System.String":           return prop.Value;
                case "System.Char":             return Char.Parse(prop.Value.ToString());
                case "System.Boolean":          return Boolean.Parse(prop.Value.ToString());
                case "System.Int32":            return Int32.Parse(prop.Value.ToString());
                case "System.Int16":            return Int16.Parse(prop.Value.ToString());
                case "System.Int64":            return Int64.Parse(prop.Value.ToString());
                case "System.UInt32":           return UInt32.Parse(prop.Value.ToString());
                case "System.UInt16":           return UInt16.Parse(prop.Value.ToString());
                case "System.UInt64":           return UInt64.Parse(prop.Value.ToString());
                case "System.Single":           return Single.Parse(prop.Value.ToString());
                case "System.Double":           return Double.Parse(prop.Value.ToString());
                case "System.Guid":             return Guid.Parse(prop.Value.ToString());
                case "System.DateTimeOffset":   return DateTimeOffset.Parse(prop.Value.ToString());
                case "System.DateTime":         return DateTime.Parse(prop.Value.ToString());
                default:                        throw new ArgumentException("Unexpected Simple Type: " + prop.Type);
            }
        }

        private static object DeserializeComplexType(string xmlBlob)
        {
            using (Stream stream = new MemoryStream())
            {
                byte[] bytes = ASCIIEncoding.ASCII.GetBytes(xmlBlob);

                stream.Write(bytes, 0, bytes.Length);
                stream.Position = 0;

                NetDataContractSerializer dcs = new NetDataContractSerializer();
                return dcs.ReadObject(stream);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}<{1}> - {2}", Name, TypeFullName, Exception == null ? "Resolved" : "Error");
        }
    }
}
