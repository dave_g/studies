﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Microsoft.Samples.CallWorkflowActivities
{
    /// <summary>
    /// Interaction logic for CallWorkflowConfiguration.xaml
    /// </summary>
    public partial class WebConfigContent : Window
    {
        private string data = string.Empty;
        private string updatedData = string.Empty;
        private string serviceAddress = string.Empty;
        private string baseCallbackAddress = string.Empty;
        private string relativeCallbackAddress = string.Empty;

        public WebConfigContent()
        {
            InitializeComponent();
        }

        public void SetWebConfigContent(string configName, string requestContractName, string requestEndpointName, string responseContractName, string responseEndpointName, string requestCustomBindingConfigName, string responseCustomBindingConfigName, BindingItem bindingItem, bool isTransactional, bool isCaller)
        {
            int heighOffset = 55;

            this.Title = configName + ": " + this.Title;
            if (!isCaller)
            {
                this.warningImage.Visibility = System.Windows.Visibility.Hidden;
                this.warningLabel.Visibility = System.Windows.Visibility.Hidden;

                this.Height = this.Height - heighOffset;
                serviceUrlText.Visibility = System.Windows.Visibility.Hidden;
                serviceUrlLabel.Visibility = System.Windows.Visibility.Hidden;

                callbackLabel.Visibility = System.Windows.Visibility.Hidden;
                baseCallbackText.Visibility = System.Windows.Visibility.Hidden;
                relativeCallbackText.Visibility = System.Windows.Visibility.Hidden;

                bottomGrid.Margin = new Thickness(bottomGrid.Margin.Left, bottomGrid.Margin.Top - heighOffset, bottomGrid.Margin.Right, bottomGrid.Margin.Bottom);
            }

            BindingItem bi = bindingItem;

            if (isCaller) requestCustomBindingConfigName = requestCustomBindingConfigName + "_InitCallback";

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Xml.XmlTextWriter tw = new System.Xml.XmlTextWriter(sw);
            tw.Formatting = System.Xml.Formatting.Indented;

            tw.WriteStartDocument();
            tw.WriteStartElement("configuration");
            tw.WriteStartElement("system.serviceModel");

            tw.WriteStartElement("services");
            tw.WriteStartElement("service");
            tw.WriteStartAttribute("name"); tw.WriteValue(configName); tw.WriteEndAttribute();

            if (isCaller)
            {
                tw.WriteComment(" IMPORTANT: An endpoint is required for calling this service from a client application. Define it here - for example, <endpoint address=\"\" binding=\"basicHttpBinding\" contract=\"IService\" /> ");
            }

            tw.WriteStartElement("endpoint");
            if (isCaller)
            {
                tw.WriteStartAttribute("address"); tw.WriteValue("[RELATIVE_CALLBACK_ENDPOINT_ADDRESS]"); tw.WriteEndAttribute();
            }
            else
            {
                tw.WriteStartAttribute("address"); tw.WriteEndAttribute();
            }

            tw.WriteStartAttribute("binding"); tw.WriteValue(bi.BindingString); tw.WriteEndAttribute();
            if (bi.BindingString == "customBinding" || isTransactional)
            {
                if (isCaller)
                {
                    tw.WriteStartAttribute("bindingConfiguration"); tw.WriteValue(responseCustomBindingConfigName); tw.WriteEndAttribute();
                }
                else
                {
                    tw.WriteStartAttribute("bindingConfiguration"); tw.WriteValue(requestCustomBindingConfigName); tw.WriteEndAttribute();
                }
                
            }
            if (isCaller)
            {
                tw.WriteStartAttribute("contract"); tw.WriteValue(responseContractName); tw.WriteEndAttribute();
                tw.WriteStartAttribute("name"); tw.WriteValue(responseEndpointName); tw.WriteEndAttribute();
            }
            else
            {
                tw.WriteStartAttribute("contract"); tw.WriteValue(requestContractName); tw.WriteEndAttribute();
                tw.WriteStartAttribute("name"); tw.WriteValue(requestEndpointName); tw.WriteEndAttribute();
            }
            tw.WriteEndElement(); // endpoint

            tw.WriteEndElement(); // service
            tw.WriteEndElement(); // services
            

            tw.WriteStartElement("client");

            tw.WriteStartElement("endpoint");
            if (isCaller)
            {
                tw.WriteStartAttribute("address"); tw.WriteValue("[CALLED_SERVICE_ENDPOINT]"); tw.WriteEndAttribute();
            }
            tw.WriteStartAttribute("binding"); tw.WriteValue(bi.BindingString); tw.WriteEndAttribute();
            if ((bi.BindingString == "customBinding") || isCaller || isTransactional )
            {
                if (isCaller)
                {
                    tw.WriteStartAttribute("bindingConfiguration"); tw.WriteValue(requestCustomBindingConfigName); tw.WriteEndAttribute();
                }
                else
                {
                    tw.WriteStartAttribute("bindingConfiguration"); tw.WriteValue(responseCustomBindingConfigName); tw.WriteEndAttribute();
                }
            }
            if (isCaller)
            {
                tw.WriteStartAttribute("contract"); tw.WriteValue(requestContractName); tw.WriteEndAttribute();
                tw.WriteStartAttribute("name"); tw.WriteValue(requestEndpointName); tw.WriteEndAttribute();
            }
            else
            {
                tw.WriteStartAttribute("contract"); tw.WriteValue(responseContractName); tw.WriteEndAttribute();
                tw.WriteStartAttribute("name"); tw.WriteValue(responseEndpointName); tw.WriteEndAttribute();
            }
            tw.WriteEndElement(); // endpoint

            tw.WriteEndElement(); // client

            tw.WriteStartElement("bindings");
            if (bi.BindingString == "customBinding")
            {
                tw.WriteStartElement(bi.BindingString);

                WriteCustomBinding(tw, bi, requestCustomBindingConfigName, responseContractName, isTransactional, isCaller);
                if (requestCustomBindingConfigName != responseCustomBindingConfigName)
                    WriteCustomBinding(tw, bi, responseCustomBindingConfigName, responseContractName, isTransactional, false);

                tw.WriteEndElement(); // customBinding
            }
            else if (isCaller || isTransactional)
            {
                tw.WriteStartElement(bi.BindingString);

                tw.WriteStartElement("binding");
                tw.WriteStartAttribute("name"); tw.WriteValue(requestCustomBindingConfigName); tw.WriteEndAttribute();
                if (isTransactional)
                {
                    tw.WriteStartAttribute("transactionFlow"); tw.WriteValue("true"); tw.WriteEndAttribute();
                }
                if (isCaller)
                {
                    tw.WriteStartAttribute("clientCallbackAddress"); tw.WriteValue("[BASE_CALLBACK_ENDPOINT_ADDRESS]/[RELATIVE_CALLBACK_ENDPOINT_ADDRESS]"); tw.WriteEndAttribute();
                }
                tw.WriteEndElement(); //binding

                if (isTransactional && (requestCustomBindingConfigName != responseCustomBindingConfigName))
                {
                    tw.WriteStartElement("binding");
                    tw.WriteStartAttribute("name"); tw.WriteValue(responseCustomBindingConfigName); tw.WriteEndAttribute();
                    tw.WriteStartAttribute("transactionFlow"); tw.WriteValue("true"); tw.WriteEndAttribute();
                    tw.WriteEndElement(); //binding
                }

                tw.WriteEndElement(); // protocol specific binding

            }

            tw.WriteEndElement(); // bindings

            tw.WriteEndElement(); // system.servceModel
            tw.WriteEndElement(); // configuration
            tw.WriteEndDocument();
            tw.Flush();

            data = sw.ToString().Replace("utf-16", "utf-8");
            updatedData = data;

            DisplayWebConfigContent(data);

            tw.Close();
            sw.Close();
        }

        private void WriteCustomBinding(System.Xml.XmlTextWriter tw, BindingItem bi, string customBindingConfigName, string responseContractName, bool isTransactional, bool isCaller)
        {
            
            tw.WriteStartElement("binding");
            tw.WriteStartAttribute("name"); tw.WriteValue(customBindingConfigName); tw.WriteEndAttribute();
            
            tw.WriteStartElement("context");
            if (isCaller)
            {
                tw.WriteStartAttribute("clientCallbackAddress"); tw.WriteValue("[BASE_CALLBACK_ENDPOINT_ADDRESS]/[RELATIVE_CALLBACK_ENDPOINT_ADDRESS]"); tw.WriteEndAttribute();
            }
            tw.WriteEndElement(); // context

            if (bi.Transport == "Msmq")
            {
                tw.WriteStartElement("msmqTransport");

                if (isTransactional)
                {
                    tw.WriteStartAttribute("exactlyOnce"); tw.WriteValue("true"); tw.WriteEndAttribute();
                }

                tw.WriteStartElement("msmqTransportSecurity");
                tw.WriteStartAttribute("msmqAuthenticationMode"); tw.WriteValue("None"); tw.WriteEndAttribute();
                tw.WriteStartAttribute("msmqProtectionLevel"); tw.WriteValue("None"); tw.WriteEndAttribute();
                tw.WriteEndElement(); // msmqTransportSecurity
                tw.WriteEndElement(); // msmqTransport
            }

            tw.WriteEndElement(); // binding
        }


        private void DisplayWebConfigContent(string content)
        {
            configContentText.Document.Blocks.Clear();
            configContentText.Document.LineHeight = 1;
            configContentText.Document.PageWidth = 2000;
            configContentText.IsReadOnly = true;

            
            Paragraph p = new Paragraph();
            configContentText.Document.Blocks.Add(p);
            
            string[] lines = content.Split(new char[] { '\n' });
            foreach (string line in lines)
            {
                bool needsHighlighting = NeedsHighlighting(line);

                Run run = new Run(line);
                p.Inlines.Add(run);
                TextRange range = new TextRange(run.ContentStart, run.ContentEnd);

                if (needsHighlighting)
                {
                    range.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.LightYellow));
                    HighlightWord(range, "[BASE_CALLBACK_ENDPOINT_ADDRESS]");
                    HighlightWord(range, "[RELATIVE_CALLBACK_ENDPOINT_ADDRESS]");
                    HighlightWord(range, "[CALLED_SERVICE_ENDPOINT]");                
                }

            }
        }

        private void CopyToClipboard(bool copyAll)
        {
            string dataToCopy = string.Empty;

            if (copyAll)
            {
                dataToCopy = updatedData;
            }
            else
            {
                string[] lines = updatedData.Split(new char[] { '\r' });
                foreach (string line in lines)
                {
                    if (NeedsHighlighting(line))
                        dataToCopy += line + "\r";
                }
            }
            
            Clipboard.Clear();
            Clipboard.SetText(dataToCopy);
        }

        private bool NeedsHighlighting(string line)
        {
            string trimmedLine = line.Trim();

            return (trimmedLine.StartsWith("<endpoint") ||
                    trimmedLine.StartsWith("<customBinding") ||
                    trimmedLine.StartsWith("</customBinding") ||
                    trimmedLine.StartsWith("<basicHttpContextBinding") ||
                    trimmedLine.StartsWith("</basicHttpContextBinding") ||
                    trimmedLine.StartsWith("<wsHttpContextBinding") ||
                    trimmedLine.StartsWith("</wsHttpContextBinding") ||
                    trimmedLine.StartsWith("<netTcpContextBinding") ||
                    trimmedLine.StartsWith("</netTcpContextBinding") ||
                    trimmedLine.StartsWith("<bindings>") ||
                    trimmedLine.StartsWith("<binding ") ||
                    trimmedLine.StartsWith("<context") ||
                    trimmedLine.StartsWith("<msmq") ||
                    trimmedLine.StartsWith("</msmq") ||
                    trimmedLine.StartsWith("</binding>") ||
                    trimmedLine.StartsWith("</bindings>") ||
                    trimmedLine.StartsWith("<services>") ||
                    trimmedLine.StartsWith("</services>") ||
                    trimmedLine.StartsWith("<service ") ||
                    trimmedLine.StartsWith("</service>") ||
                    trimmedLine.StartsWith("<client") ||
                    trimmedLine.StartsWith("</client") ||
                    trimmedLine.StartsWith("<transactionFlow") ||
                    trimmedLine.StartsWith("<!--"));
        }

        TextRange FindWordFromPosition(TextPointer position, string word)
        {
            TextRange returnRange = null;

            while (position != null)
            {
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    string textRun = position.GetTextInRun(LogicalDirection.Forward);
                    // Find the starting index of any substring that matches "word".
                    int indexInRun = textRun.IndexOf(word);
                    if (indexInRun >= 0)
                    {
                        TextPointer start = position.GetPositionAtOffset(indexInRun);
                        TextPointer end = start.GetPositionAtOffset(word.Length);
                        returnRange = new TextRange(start, end);
                        break;
                    }
                }
                position = position.GetNextContextPosition(LogicalDirection.Forward);
            }
            return returnRange;
        }

        private void HighlightWord(TextRange run, string word)
        {
            TextRange range = FindWordFromPosition(run.Start, word);
            if (range != null)
            {
                //range.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.Yellow));
                range.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
            }
                
        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void copyToClipboardButton_Click(object sender, RoutedEventArgs e)
        {
            CopyToClipboard(!copySelectionCheckBox.IsChecked.Value);
            MessageBox.Show("The content has been copied to your clipboard", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }


        private void UpdateWebConfigContent()
        {
            updatedData = data; //resets to original content
            if (serviceAddress != string.Empty)
                updatedData = data.Replace("[CALLED_SERVICE_ENDPOINT]", serviceAddress);
            
            if (baseCallbackAddress != string.Empty)
                updatedData = updatedData.Replace("[BASE_CALLBACK_ENDPOINT_ADDRESS]", baseCallbackAddress);

            if (relativeCallbackAddress != string.Empty)
                updatedData = updatedData.Replace("[RELATIVE_CALLBACK_ENDPOINT_ADDRESS]", relativeCallbackAddress);

            if (serviceAddress != string.Empty && baseCallbackAddress != string.Empty && relativeCallbackAddress != string.Empty)
            {
                warningImage.Visibility = System.Windows.Visibility.Hidden;
                warningLabel.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                warningImage.Visibility = System.Windows.Visibility.Visible;
                warningLabel.Visibility = System.Windows.Visibility.Visible;
            }

            DisplayWebConfigContent(updatedData);
        }

        private void endpoints_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            serviceAddress = serviceUrlText.Text.Trim();
            baseCallbackAddress = baseCallbackText.Text.Trim();
            relativeCallbackAddress = relativeCallbackText.Text.Trim();
            UpdateWebConfigContent();
        }

    }
}
