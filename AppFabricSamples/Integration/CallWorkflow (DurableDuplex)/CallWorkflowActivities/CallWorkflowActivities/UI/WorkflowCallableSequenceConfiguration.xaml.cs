﻿using System.Windows;

namespace Microsoft.Samples.CallWorkflowActivities
{
    /// <summary>
    /// Interaction logic for CallWorkflowConfiguration.xaml
    /// </summary>
    public partial class WorkflowCallableSequenceConfiguration : Window
    {
        public WorkflowCallableSequenceConfiguration()
        {
            InitializeComponent();

            this.requestOperationText.Text = "StartWorkflow";
            this.responseOperationText.Text = "WorkflowCompleted";

            Helper.PopulateBindingItemsCollection(protocolDropDown.Items);
            protocolDropDown.SelectedIndex = 0;
        }

        public void InitializeWithValues(string serviceName, string configName)
        {
            this.serviceText.Text = serviceName;
            this.configText.Text = configName;
            this.contractText.Text = "ICall" + serviceName;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (serviceText.Text.Trim().Length == 0 ||
                configText.Text.Trim().Length == 0 ||
                contractText.Text.Trim().Length == 0 ||
                requestOperationText.Text.Trim().Length == 0 ||
                responseOperationText.Text.Trim().Length == 0)
            {
                System.Windows.MessageBox.Show("Service, Configuration, Contract and Request/Response Operation names are required.\r\nConfigure the values and try again.", "Missing required values", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
            else
            {
                this.DialogResult = true;
                this.Close();
            }
        }
    }
}
