﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ServiceModel.Activities;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Xaml;
using Microsoft.VisualBasic.Activities;

namespace Microsoft.Samples.CallWorkflowActivities
{

    /// <summary>
    /// Interaction logic for CallWorkflowConfiguration.xaml
    /// </summary>
    public partial class CallWorkflowConfiguration : Window
    {
        public CallWorkflowConfiguration()
        {
            InitializeComponent();

            Helper.PopulateBindingItemsCollection(protocolDropDown.Items);
            protocolDropDown.SelectedIndex = 0;

            this.configNowRadio.Checked += configRadio_Checked;
            this.configManuallyRadio.Checked += configRadio_Checked;
        }

        static Assembly XamlReferenceAssemblyResolver(object sender, ResolveEventArgs args)
        {
            Assembly assembly = null;

            string binFolder = @"\bin\";
            
            string assemblyName = args.Name.Split(new char[] {','})[0] + ".dll";
            string xamlDirName = Path.GetDirectoryName(AssemblyResoloverStatus.SearchPath);
            string[] filenames = Directory.GetFiles(xamlDirName, assemblyName, SearchOption.AllDirectories);
            if (filenames.Length == 0)
            {
                string dir = xamlDirName;
                DirectoryInfo dirInfo = Directory.GetParent(dir);
                while (dirInfo != null)
                {
                    string fileToLoad = dirInfo.FullName + binFolder + assemblyName;
                    if (File.Exists(fileToLoad))
                    {
                        assembly = Assembly.LoadFrom(fileToLoad);
                        break;
                    }
                    dirInfo = Directory.GetParent(dirInfo.FullName);
                }
            }
            else
            {
                assembly = Assembly.LoadFrom(filenames[0]);
            }

            return assembly;
            
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private Activity LoadServiceFromXaml(string filename)
        {
            Activity returnActivity = null;
            try
            {
                Thread.GetDomain().AssemblyResolve += new ResolveEventHandler(XamlReferenceAssemblyResolver);

                object service = null;
                service = XamlServices.Load(filename);
                if (service != null)
                    if (service.GetType() == typeof(WorkflowService))
                        returnActivity = ((WorkflowService)service).Body;
                    else
                        returnActivity = (Activity)service;
            }
            catch{}
            finally
            {
                Thread.GetDomain().AssemblyResolve -= new ResolveEventHandler(XamlReferenceAssemblyResolver);
            };

            return returnActivity;
        }

        private void fileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileOpen = new OpenFileDialog();
            fileOpen.Filter = "WCF Workflow Service|*.xamlx";
            fileOpen.FilterIndex = 2;
            fileOpen.RestoreDirectory = true;

            if (fileOpen.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    string filename = fileOpen.FileName;
                    AssemblyResoloverStatus.SearchPath = filename;

                    Activity root = LoadServiceFromXaml(filename);
                    if (root == null)
                    {
                        System.Windows.MessageBox.Show("The selected workflow definition or some of the assemblies it depends on could not be loaded automatically, or the workflow does not contain any activities.", "Assembly Resolution Failure", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    Activity receiveActivity = FindCallbackCorrelatedActivity(root, typeof(Receive), string.Empty);

                    Receive r = null;
                    Send s = null;
                    if (receiveActivity != null)
                    {
                        r = (Receive)receiveActivity;
                        foreach (CorrelationInitializer corrInit in r.CorrelationInitializers)
                        {
                            if (corrInit.GetType() == typeof(CallbackCorrelationInitializer))
                            {
                                string callbackHandleName = ((VisualBasicValue<CorrelationHandle>)corrInit.CorrelationHandle.Expression).ExpressionText;

                                Activity sendActivity = FindCallbackCorrelatedActivity(root, typeof(Send), callbackHandleName);
                                if (sendActivity != null)
                                {
                                    s = (Send)sendActivity;
                                    if (((VisualBasicValue<CorrelationHandle>)s.CorrelatesWith.Expression).ExpressionText == callbackHandleName)
                                    {
                                        this.filenameText.Text = filename;

                                        this.requestContractText.Text = r.ServiceContractName.LocalName;
                                        this.requestOperationText.Text = r.OperationName;

                                        this.responseContractText.Text = s.ServiceContractName.LocalName;
                                        this.responseOperationText.Text = s.OperationName;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if ((r == null) || (s == null))
                    {
                        System.Windows.MessageBox.Show("The workflow must implement a Workflow-Callable Sequence activity.\r\nSelect a different workflow definition.", "Incompatible workflow", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.ToString());
                }
            }

        }

        private Activity FindCallbackCorrelatedActivity(Activity root, Type type, string callbackHandleName)
        {
            IEnumerable<Activity> activities;
            activities = System.Activities.WorkflowInspectionServices.GetActivities(root);

            if (root.GetType() == type)
            {
                if (root.GetType() == typeof(Receive))
                {
                    Receive r = (Receive) root;
                    foreach (CorrelationInitializer corrInit in r.CorrelationInitializers)
                    {
                        if (corrInit.GetType() == typeof(CallbackCorrelationInitializer))
                        {
                            return root;
                        }
                    }
                };

                if (root.GetType() == typeof(Send))
                {
                    Send r = (Send)root;
                    if (((VisualBasicValue<CorrelationHandle>)r.CorrelatesWith.Expression).ExpressionText == callbackHandleName)
                    {
                        return root;
                    }
                }
            };

            activities = System.Activities.WorkflowInspectionServices.GetActivities(root);

            foreach (Activity child in activities)
            {
                Activity returnActivity = FindCallbackCorrelatedActivity(child, type, callbackHandleName);
                if (returnActivity != null)
                    return returnActivity;
            }

            return null;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (requestContractText.Text.Trim().Length == 0 ||
                requestOperationText.Text.Trim().Length == 0 ||
                responseContractText.Text.Trim().Length == 0 ||
                responseOperationText.Text.Trim().Length == 0)
            {
                System.Windows.MessageBox.Show("Request/Response contract and operation are required fields.\r\nSelect a workflow definition to configure the values automatically, or configure manually", "Missing required values", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
            else
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        private void configRadio_Checked(object sender, RoutedEventArgs e)
        {
            if (this.configNowRadio.IsChecked.HasValue)
            {
                bool configNowChecked = this.configNowRadio.IsChecked.Value;

                this.filenameText.IsEnabled = configNowChecked;
                this.fileButton.IsEnabled = configNowChecked;

                requestContractText.IsEnabled = !configNowChecked;
                requestOperationText.IsEnabled = !configNowChecked;
                responseContractText.IsEnabled = !configNowChecked;
                responseOperationText.IsEnabled = !configNowChecked;

                if (configNowChecked)
                {
                    requestContractText.Text = string.Empty;
                    requestOperationText.Text = string.Empty;
                    responseContractText.Text = string.Empty;
                    responseOperationText.Text = string.Empty;
                }
                else
                {
                    filenameText.Text = string.Empty;
                }
            }
        }

    }

    static class AssemblyResoloverStatus
    {
        public static string SearchPath = string.Empty;
    }
}
