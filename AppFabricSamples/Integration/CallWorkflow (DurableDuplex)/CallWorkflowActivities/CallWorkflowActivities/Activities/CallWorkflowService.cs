﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ServiceModel.Activities;
using Microsoft.VisualBasic.Activities;

namespace Microsoft.Samples.CallWorkflowActivities
{

    public sealed class CallWorkflowService : IActivityTemplateFactory
    {
        
        public System.Activities.Activity Create(System.Windows.DependencyObject target)
        {
            EnvDTE.DTE dte = (EnvDTE.DTE)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE");
            System.Windows.Interop.WindowInteropHelper interopWindow;

            object configNameProperty = Helper.GetWorkflowPropertyComputedValue(target, "ConfigurationName");
            if (configNameProperty == null)
            {
                System.Windows.MessageBox.Show("CallWorkflowService is designed for use only in a WCF Workflow Service (XAMLX).", "Incompatible workflow definition", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return null;
            }
            string configName = (string)configNameProperty;
            
            CallWorkflowConfiguration configWindow = new CallWorkflowConfiguration();
            interopWindow = new System.Windows.Interop.WindowInteropHelper(configWindow);
            interopWindow.Owner = new IntPtr(dte.MainWindow.HWnd);

            if (configWindow.ShowDialog().Value)
            {
                string requestContractName = configWindow.requestContractText.Text;
                string requestOperation = configWindow.requestOperationText.Text;
                string responseContractName = configWindow.responseContractText.Text;
                string responseOperation = configWindow.responseOperationText.Text;
                bool isTransactional = configWindow.transactionalCheckbox.IsChecked.Value;

                BindingItem bindingItem = (BindingItem)configWindow.protocolDropDown.SelectedItem;

                string requestEndpointName = configName + requestContractName + requestOperation; //request endpoint config name
                string responseEndpointName = configName + responseContractName + responseOperation; //response endpoint config name
                string requestCustomBindingConfigName = bindingItem.Transport + configName + requestContractName;
                string responseCustomBindingConfigName = bindingItem.Transport + configName + responseContractName;

                string callbackHandleName = "callbackHandle";
                string callbackRequestName = "request";
                string callbackResponseName = "response";
                Variable<CorrelationHandle> callbackHandle = new Variable<CorrelationHandle>(callbackHandleName);
                Variable<string> callbackRequest = new Variable<string> { Name = callbackRequestName };
                Variable<string> callbackResponse = new Variable<string> { Name = callbackResponseName };
                
                CorrelationInitializer callbackCorrelationInitializer = new CallbackCorrelationInitializer();
                callbackCorrelationInitializer.CorrelationHandle = new VisualBasicValue<CorrelationHandle> { ExpressionText = callbackHandleName };
                
                Send sendActivity = new Send
                {
                    DisplayName = "Send Request",
                    CorrelationInitializers = { callbackCorrelationInitializer },
                    ServiceContractName = requestContractName,
                    EndpointConfigurationName = requestEndpointName,
                    OperationName = requestOperation,
                    Content = SendContent.Create(new InArgument<string>(new VisualBasicValue<string> { ExpressionText = callbackRequestName }))
                };
                
                Receive receiveActivity = new Receive
                {
                    DisplayName = "Receive Completion Notification",
                    CorrelatesWith = new VisualBasicValue<CorrelationHandle>{ ExpressionText = callbackHandleName },
                    ServiceContractName = responseContractName,
                    OperationName = responseOperation,
                    Content = ReceiveContent.Create(new OutArgument<string>(new VisualBasicReference<string> { ExpressionText = callbackResponseName }))
                };


                WebConfigContent webConfigWindow = new WebConfigContent();
                interopWindow = new System.Windows.Interop.WindowInteropHelper(webConfigWindow);
                interopWindow.Owner = new IntPtr(dte.MainWindow.HWnd);
                webConfigWindow.SetWebConfigContent(configName, requestContractName, requestEndpointName, responseContractName, responseEndpointName, requestCustomBindingConfigName, responseCustomBindingConfigName, bindingItem, isTransactional, true);
                webConfigWindow.ShowDialog();

                
                
                if (isTransactional)
                {
                    TransactionScope requestTransaction = new TransactionScope
                    {
                        DisplayName = "Request Transaction",
                        Body = sendActivity
                    };

                    TransactedReceiveScope responseTransaction = new TransactedReceiveScope
                    {
                        DisplayName = "Response Transaction",
                        Request = receiveActivity
                    };

                    return new Sequence
                    {
                        DisplayName = "Call Workflow",
                        Variables = { callbackHandle, callbackRequest, callbackResponse },
                        Activities = { requestTransaction, responseTransaction }
                    };
                }
                else
                {
                    return new Sequence
                    {
                        DisplayName = "Call Workflow",
                        Variables = { callbackHandle, callbackRequest, callbackResponse },
                        Activities = { sendActivity, receiveActivity }
                    };
                }
            }
            else
            {
                return null;
            }
        }

    }
}
