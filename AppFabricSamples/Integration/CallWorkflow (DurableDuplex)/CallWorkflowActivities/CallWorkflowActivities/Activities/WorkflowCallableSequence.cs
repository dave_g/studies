﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ServiceModel.Activities;
using Microsoft.VisualBasic.Activities;

namespace Microsoft.Samples.CallWorkflowActivities
{

    public sealed class WorkflowCallableSequence: IActivityTemplateFactory
    {

        public System.Activities.Activity Create(System.Windows.DependencyObject target)
        {
            EnvDTE.DTE dte = (EnvDTE.DTE)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE");
            System.Windows.Interop.WindowInteropHelper interopWindow;

            object serviceNameProperty = Helper.GetWorkflowPropertyComputedValue(target, "Name");
            object configNameProperty = Helper.GetWorkflowPropertyComputedValue(target, "ConfigurationName");
            if (serviceNameProperty == null || configNameProperty == null)
            {
                System.Windows.MessageBox.Show("Workflow-callable sequence is designed for use only in a WCF Workflow Service (XAMLX).", "Incompatible workflow definition", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return null;
            }

            string serviceName = ((System.Xml.Linq.XName)serviceNameProperty).LocalName;
            string configName = (string)configNameProperty;

            WorkflowCallableSequenceConfiguration configWindow = new WorkflowCallableSequenceConfiguration();
            interopWindow = new System.Windows.Interop.WindowInteropHelper(configWindow);
            interopWindow.Owner = new IntPtr(dte.MainWindow.HWnd);
            configWindow.InitializeWithValues(serviceName, configName);


            if (configWindow.ShowDialog().Value)
            {
                Helper.GetWorkflowProperty(target, "Name").SetValue((System.Xml.Linq.XName)(configWindow.serviceText.Text));
                Helper.GetWorkflowProperty(target, "ConfigurationName").SetValue(configWindow.configText.Text);

                configName = configWindow.configText.Text;
                string contractName = configWindow.contractText.Text;
                string requestOperation = configWindow.requestOperationText.Text;
                string responseOperation = configWindow.responseOperationText.Text;
                bool isTransactional = configWindow.transactionalCheckbox.IsChecked.Value;

                BindingItem bindingItem = (BindingItem)configWindow.protocolDropDown.SelectedItem;

                string requestEndpointName = configName + contractName + requestOperation; //request endpoint config name
                string responseEndpointName = configName + contractName + responseOperation; //response endpoint config name
                string customBindingConfigName = bindingItem.Transport + configName + contractName;


                string callbackHandleName = "callbackHandle";
                string callbackRequestName = "request";
                string callbackResponseName = "response";
                Variable<CorrelationHandle> callbackHandle = new Variable<CorrelationHandle> { Name = callbackHandleName };
                Variable<string> callbackRequest = new Variable<string> { Name = callbackRequestName };
                Variable<string> callbackResponse = new Variable<string> { Name = callbackResponseName };

                CorrelationInitializer callbackCorrelationInitializer = new CallbackCorrelationInitializer
                {
                    CorrelationHandle = new VisualBasicValue<CorrelationHandle> { ExpressionText = callbackHandleName }
                };

                Receive receiveActivity = new Receive
                {
                    DisplayName = "Receive Request",
                    CorrelationInitializers = { callbackCorrelationInitializer },
                    ServiceContractName = contractName,
                    OperationName = requestOperation,
                    CanCreateInstance = true,
                    Content = ReceiveContent.Create(new OutArgument<string>(new VisualBasicReference<string> { ExpressionText = callbackRequestName }))
                };

                Sequence bodyActivity = new Sequence
                {
                    DisplayName = "Body"
                };

                Send sendActivity = new Send
                {
                    DisplayName = "Send Completion Notification",
                    CorrelatesWith = new VisualBasicValue<CorrelationHandle> { ExpressionText = callbackHandleName },
                    ServiceContractName = contractName,
                    EndpointConfigurationName = responseEndpointName,
                    OperationName = responseOperation,
                    Content = SendContent.Create(new InArgument<string>(new VisualBasicValue<string> { ExpressionText = callbackResponseName }))
                };

                WebConfigContent webConfigWindow = new WebConfigContent();
                interopWindow = new System.Windows.Interop.WindowInteropHelper(webConfigWindow);
                interopWindow.Owner = new IntPtr(dte.MainWindow.HWnd);
                webConfigWindow.SetWebConfigContent(configName, contractName, requestEndpointName, contractName, responseEndpointName, customBindingConfigName, customBindingConfigName, bindingItem, isTransactional, false);
                webConfigWindow.ShowDialog();

                if (isTransactional)
                {
                    TransactedReceiveScope requestTransaction = new TransactedReceiveScope
                    {
                        DisplayName = "Request Transaction",
                        Request = receiveActivity,
                    };

                    TransactionScope responseTransaction = new TransactionScope
                    {
                        DisplayName = "Response Transaction",
                        Body = sendActivity
                    };

                    return new Sequence
                    {
                        DisplayName = "Workflow-callable Sequence",
                        Variables = { callbackHandle, callbackRequest, callbackResponse },
                        Activities = { requestTransaction, responseTransaction }
                    };
                }
                else
                {
                    return new Sequence
                    {
                        DisplayName = "Workflow-callable Sequence",
                        Variables = { callbackHandle, callbackRequest, callbackResponse },
                        Activities = { receiveActivity, bodyActivity, sendActivity }
                    };
                }

            }
            else
            {
                return null;
            }
        }

    }
}
