﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System.Activities.Presentation;
using System.Activities.Presentation.Model;
using System.Windows;
using System.Windows.Controls;

namespace Microsoft.Samples.CallWorkflowActivities
{
    public class BindingItem {

      public string Name {get;set;}
      public string BindingString { get; set; }
      public string Transport { get; set; }

      public BindingItem(string name, string bindingString)
      {
          Name = name;
          BindingString = bindingString;
      }
      public BindingItem(string name, string bindingString, string transport)
      {
          Name = name;
          BindingString = bindingString;
          Transport = transport;
      }

      public override string ToString()
      {
          return Name;
      }
    }

    internal static class Helper
    {
        public static void PopulateBindingItemsCollection(ItemCollection items)
        {
            items.Add(new BindingItem("basicHttpContextBinding", "basicHttpContextBinding"));
            items.Add(new BindingItem("wsHttpContextBinding", "wsHttpContextBinding"));
            items.Add(new BindingItem("netTcpContextBinding", "netTcpContextBinding"));
            items.Add(new BindingItem("customBinding w/ MSMQ", "customBinding", "Msmq"));
        }

        public static ModelProperty GetWorkflowProperty(DependencyObject target, string propertyName)
        {
            ModelProperty returnProperty = null;

            try
            {
                ModelItem modelItem = ((WorkflowViewElement)target).ModelItem;
                modelItem = modelItem.Root;
                returnProperty = modelItem.Properties.Find(propertyName);
            }
            catch { }

            return returnProperty;
        }

        public static object GetWorkflowPropertyComputedValue(DependencyObject target, string propertyName)
        {
            object returnValue = null;

            ModelProperty modelProperty = GetWorkflowProperty(target, propertyName);
            if (modelProperty != null)
                returnValue = modelProperty.ComputedValue;

            return returnValue;
        }

    }

}



//public static System.Activities.Presentation.Model.ModelProperty GetWorkflowPropertyValue(System.Windows.DependencyObject target, string propertyName)
//{
//    System.Activities.Presentation.Model.ModelProperty returnProperty = null;

//    System.Activities.Presentation.Model.ModelItem modelItem = ((System.Activities.Presentation.WorkflowViewElement)target).ModelItem;
//    while (modelItem.Parent != null) modelItem = modelItem.Parent;

//    foreach (System.Activities.Presentation.Model.ModelProperty modelProperty in modelItem.Properties)
//        if (modelProperty.Name == propertyName)
//        {
//            returnProperty = modelProperty;
//            break;
//        };

//    return returnProperty;
//}