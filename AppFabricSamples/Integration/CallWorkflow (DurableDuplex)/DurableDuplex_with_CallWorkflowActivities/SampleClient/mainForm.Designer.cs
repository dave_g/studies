﻿namespace Microsoft.Samples.Client
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.callParentButton = new System.Windows.Forms.Button();
            this.eventLog = new System.Diagnostics.EventLog();
            this.eventsText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // callParentButton
            // 
            this.callParentButton.Location = new System.Drawing.Point(12, 12);
            this.callParentButton.Name = "callParentButton";
            this.callParentButton.Size = new System.Drawing.Size(129, 23);
            this.callParentButton.TabIndex = 0;
            this.callParentButton.Text = "Call Parent Service";
            this.callParentButton.UseVisualStyleBackColor = true;
            this.callParentButton.Click += new System.EventHandler(this.callParentButton_Click);
            // 
            // eventLog
            // 
            this.eventLog.Log = "Application";
            this.eventLog.Source = "Samples";
            this.eventLog.SynchronizingObject = this;
            this.eventLog.EntryWritten += new System.Diagnostics.EntryWrittenEventHandler(this.eventLog_EntryWritten);
            // 
            // eventsText
            // 
            this.eventsText.Location = new System.Drawing.Point(12, 60);
            this.eventsText.Multiline = true;
            this.eventsText.Name = "eventsText";
            this.eventsText.ReadOnly = true;
            this.eventsText.Size = new System.Drawing.Size(417, 183);
            this.eventsText.TabIndex = 1;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 255);
            this.Controls.Add(this.eventsText);
            this.Controls.Add(this.callParentButton);
            this.Name = "mainForm";
            this.Text = "Parent-Child Test Client";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button callParentButton;
        private System.Diagnostics.EventLog eventLog;
        private System.Windows.Forms.TextBox eventsText;
    }
}

