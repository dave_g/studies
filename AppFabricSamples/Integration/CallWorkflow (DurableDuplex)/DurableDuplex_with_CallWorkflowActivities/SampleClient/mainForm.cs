﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Microsoft.Samples.Client
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void callParentButton_Click(object sender, EventArgs e)
        {
            ParentServiceReference.ServiceClient service = new ParentServiceReference.ServiceClient();
            service.GetData("Hello");

            service = null;
        }



        private void eventLog_EntryWritten(object sender, System.Diagnostics.EntryWrittenEventArgs e)
        {
            if (e.Entry.Source == "Samples")
            {
                string msg = e.Entry.Message;
                if (msg.IndexOf(":") > 0)
                    msg = msg.Split(new char[] {':'})[1];
                eventsText.Text += e.Entry.TimeGenerated.ToString("G") +" " + msg + "\r\n";

            }
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.EventLog.CreateEventSource("Samples", "Application");
            }
            catch { }

            eventLog.EnableRaisingEvents = true;
        }
    }
}
