﻿namespace Microsoft.Samples.SaleClient
{
    partial class SaleClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonPlaceOrder = new System.Windows.Forms.Button();
            this.buttonCancelOrder = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.buttonGetNewCatalog = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonRecall = new System.Windows.Forms.Button();
            this.OrderGridView = new System.Windows.Forms.DataGridView();
            this.ItemOrderQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemStockQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxOrderID = new System.Windows.Forms.TextBox();
            this.labelOrderStatus = new System.Windows.Forms.Label();
            this.labelBillingTotal = new System.Windows.Forms.Label();
            this.labelExpiration = new System.Windows.Forms.Label();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelTelephone = new System.Windows.Forms.Label();
            this.labelAddress1 = new System.Windows.Forms.Label();
            this.labelAddress2 = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.labelZipCode = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxTelephone = new System.Windows.Forms.TextBox();
            this.textBoxAddress1 = new System.Windows.Forms.TextBox();
            this.textBoxAddress2 = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.textBoxState = new System.Windows.Forms.TextBox();
            this.textBoxZipCode = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPlaceOrder
            // 
            this.buttonPlaceOrder.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonPlaceOrder.Enabled = false;
            this.buttonPlaceOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPlaceOrder.Location = new System.Drawing.Point(603, 73);
            this.buttonPlaceOrder.Name = "buttonPlaceOrder";
            this.buttonPlaceOrder.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonPlaceOrder.Size = new System.Drawing.Size(111, 23);
            this.buttonPlaceOrder.TabIndex = 5;
            this.buttonPlaceOrder.Text = "Place Order";
            this.buttonPlaceOrder.UseVisualStyleBackColor = true;
            this.buttonPlaceOrder.Click += new System.EventHandler(this.buttonPlaceOrder_Click);
            // 
            // buttonCancelOrder
            // 
            this.buttonCancelOrder.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonCancelOrder.Enabled = false;
            this.buttonCancelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancelOrder.Location = new System.Drawing.Point(603, 113);
            this.buttonCancelOrder.Name = "buttonCancelOrder";
            this.buttonCancelOrder.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonCancelOrder.Size = new System.Drawing.Size(111, 23);
            this.buttonCancelOrder.TabIndex = 8;
            this.buttonCancelOrder.Text = "Cancel Order";
            this.buttonCancelOrder.UseVisualStyleBackColor = true;
            this.buttonCancelOrder.Click += new System.EventHandler(this.buttonCancelOrder_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.StatusLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(8, 310);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusLabel.Size = new System.Drawing.Size(723, 21);
            this.StatusLabel.TabIndex = 10;
            this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonGetNewCatalog
            // 
            this.buttonGetNewCatalog.Location = new System.Drawing.Point(603, 34);
            this.buttonGetNewCatalog.Name = "buttonGetNewCatalog";
            this.buttonGetNewCatalog.Size = new System.Drawing.Size(111, 23);
            this.buttonGetNewCatalog.TabIndex = 11;
            this.buttonGetNewCatalog.Text = "Get New Catalog";
            this.buttonGetNewCatalog.UseVisualStyleBackColor = true;
            this.buttonGetNewCatalog.Click += new System.EventHandler(this.buttonGetNewCatalog_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(571, 283);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.labelExpiration);
            this.tabPage1.Controls.Add(this.labelBillingTotal);
            this.tabPage1.Controls.Add(this.buttonRecall);
            this.tabPage1.Controls.Add(this.labelOrderStatus);
            this.tabPage1.Controls.Add(this.OrderGridView);
            this.tabPage1.Controls.Add(this.textBoxOrderID);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(563, 257);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Purchase Order";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxZipCode);
            this.tabPage2.Controls.Add(this.textBoxState);
            this.tabPage2.Controls.Add(this.textBoxCity);
            this.tabPage2.Controls.Add(this.textBoxAddress2);
            this.tabPage2.Controls.Add(this.textBoxAddress1);
            this.tabPage2.Controls.Add(this.textBoxTelephone);
            this.tabPage2.Controls.Add(this.textBoxEmail);
            this.tabPage2.Controls.Add(this.textBoxLastName);
            this.tabPage2.Controls.Add(this.textBoxFirstName);
            this.tabPage2.Controls.Add(this.labelZipCode);
            this.tabPage2.Controls.Add(this.labelState);
            this.tabPage2.Controls.Add(this.labelCity);
            this.tabPage2.Controls.Add(this.labelAddress2);
            this.tabPage2.Controls.Add(this.labelAddress1);
            this.tabPage2.Controls.Add(this.labelTelephone);
            this.tabPage2.Controls.Add(this.labelEmail);
            this.tabPage2.Controls.Add(this.labelLastName);
            this.tabPage2.Controls.Add(this.labelFirstName);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(563, 257);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Customer Info";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonRecall
            // 
            this.buttonRecall.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonRecall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRecall.Location = new System.Drawing.Point(293, 219);
            this.buttonRecall.Name = "buttonRecall";
            this.buttonRecall.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonRecall.Size = new System.Drawing.Size(79, 23);
            this.buttonRecall.TabIndex = 3;
            this.buttonRecall.Text = "Recall PO";
            this.buttonRecall.UseVisualStyleBackColor = true;
            this.buttonRecall.Click += new System.EventHandler(this.buttonRecall_Click);
            // 
            // OrderGridView
            // 
            this.OrderGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.OrderGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemDescription,
            this.ItemId,
            this.ItemPrice,
            this.ItemStockQuantity,
            this.ItemOrderQuantity});
            this.OrderGridView.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrderGridView.DefaultCellStyle = dataGridViewCellStyle27;
            this.OrderGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderGridView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.OrderGridView.Location = new System.Drawing.Point(6, 6);
            this.OrderGridView.Name = "OrderGridView";
            this.OrderGridView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.OrderGridView.Size = new System.Drawing.Size(547, 121);
            this.OrderGridView.TabIndex = 0;
            this.OrderGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrderGridView_CellValidated);
            // 
            // ItemOrderQuantity
            // 
            this.ItemOrderQuantity.DataPropertyName = "ItemOrderQuantity";
            this.ItemOrderQuantity.HeaderText = "Order Quantity";
            this.ItemOrderQuantity.Name = "ItemOrderQuantity";
            this.ItemOrderQuantity.Width = 75;
            // 
            // ItemStockQuantity
            // 
            this.ItemStockQuantity.DataPropertyName = "ItemStockQuantity";
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.Silver;
            this.ItemStockQuantity.DefaultCellStyle = dataGridViewCellStyle26;
            this.ItemStockQuantity.HeaderText = "Stock Quantity";
            this.ItemStockQuantity.Name = "ItemStockQuantity";
            this.ItemStockQuantity.ReadOnly = true;
            this.ItemStockQuantity.Width = 75;
            // 
            // ItemPrice
            // 
            this.ItemPrice.DataPropertyName = "ItemPrice";
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.Silver;
            this.ItemPrice.DefaultCellStyle = dataGridViewCellStyle25;
            this.ItemPrice.HeaderText = "Price";
            this.ItemPrice.Name = "ItemPrice";
            this.ItemPrice.ReadOnly = true;
            this.ItemPrice.Width = 50;
            // 
            // ItemId
            // 
            this.ItemId.DataPropertyName = "ItemId";
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.Silver;
            this.ItemId.DefaultCellStyle = dataGridViewCellStyle24;
            this.ItemId.HeaderText = "Product Id";
            this.ItemId.Name = "ItemId";
            this.ItemId.ReadOnly = true;
            // 
            // ItemDescription
            // 
            this.ItemDescription.DataPropertyName = "ItemDescription";
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.Silver;
            this.ItemDescription.DefaultCellStyle = dataGridViewCellStyle23;
            this.ItemDescription.HeaderText = "Description";
            this.ItemDescription.Name = "ItemDescription";
            this.ItemDescription.ReadOnly = true;
            this.ItemDescription.Width = 204;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 224);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order Id:";
            // 
            // textBoxOrderID
            // 
            this.textBoxOrderID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBoxOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOrderID.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxOrderID.Location = new System.Drawing.Point(71, 220);
            this.textBoxOrderID.Name = "textBoxOrderID";
            this.textBoxOrderID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxOrderID.Size = new System.Drawing.Size(216, 20);
            this.textBoxOrderID.TabIndex = 2;
            // 
            // labelOrderStatus
            // 
            this.labelOrderStatus.AutoSize = true;
            this.labelOrderStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelOrderStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrderStatus.Location = new System.Drawing.Point(17, 198);
            this.labelOrderStatus.Name = "labelOrderStatus";
            this.labelOrderStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelOrderStatus.Size = new System.Drawing.Size(72, 13);
            this.labelOrderStatus.TabIndex = 7;
            this.labelOrderStatus.Text = "Order Status :";
            // 
            // labelBillingTotal
            // 
            this.labelBillingTotal.AutoSize = true;
            this.labelBillingTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelBillingTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBillingTotal.Location = new System.Drawing.Point(17, 174);
            this.labelBillingTotal.Name = "labelBillingTotal";
            this.labelBillingTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelBillingTotal.Size = new System.Drawing.Size(67, 13);
            this.labelBillingTotal.TabIndex = 6;
            this.labelBillingTotal.Text = "Billing Total :";
            // 
            // labelExpiration
            // 
            this.labelExpiration.AutoSize = true;
            this.labelExpiration.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelExpiration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExpiration.Location = new System.Drawing.Point(17, 149);
            this.labelExpiration.Name = "labelExpiration";
            this.labelExpiration.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelExpiration.Size = new System.Drawing.Size(59, 13);
            this.labelExpiration.TabIndex = 8;
            this.labelExpiration.Text = "Expiration :";
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(17, 42);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(54, 13);
            this.labelFirstName.TabIndex = 0;
            this.labelFirstName.Text = "FirstName";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(17, 68);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(55, 13);
            this.labelLastName.TabIndex = 1;
            this.labelLastName.Text = "LastName";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(265, 42);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(73, 13);
            this.labelEmail.TabIndex = 2;
            this.labelEmail.Text = "Email Address";
            // 
            // labelTelephone
            // 
            this.labelTelephone.AutoSize = true;
            this.labelTelephone.Location = new System.Drawing.Point(265, 68);
            this.labelTelephone.Name = "labelTelephone";
            this.labelTelephone.Size = new System.Drawing.Size(58, 13);
            this.labelTelephone.TabIndex = 3;
            this.labelTelephone.Text = "Telephone";
            // 
            // labelAddress1
            // 
            this.labelAddress1.AutoSize = true;
            this.labelAddress1.Location = new System.Drawing.Point(17, 113);
            this.labelAddress1.Name = "labelAddress1";
            this.labelAddress1.Size = new System.Drawing.Size(77, 13);
            this.labelAddress1.TabIndex = 4;
            this.labelAddress1.Text = "Address Line 1";
            // 
            // labelAddress2
            // 
            this.labelAddress2.AutoSize = true;
            this.labelAddress2.Location = new System.Drawing.Point(17, 144);
            this.labelAddress2.Name = "labelAddress2";
            this.labelAddress2.Size = new System.Drawing.Size(77, 13);
            this.labelAddress2.TabIndex = 5;
            this.labelAddress2.Text = "Address Line 2";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(17, 177);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(24, 13);
            this.labelCity.TabIndex = 6;
            this.labelCity.Text = "City";
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(227, 177);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(32, 13);
            this.labelState.TabIndex = 7;
            this.labelState.Text = "State";
            // 
            // labelZipCode
            // 
            this.labelZipCode.AutoSize = true;
            this.labelZipCode.Location = new System.Drawing.Point(381, 177);
            this.labelZipCode.Name = "labelZipCode";
            this.labelZipCode.Size = new System.Drawing.Size(47, 13);
            this.labelZipCode.TabIndex = 8;
            this.labelZipCode.Text = "ZipCode";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(77, 38);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(182, 20);
            this.textBoxFirstName.TabIndex = 9;
            this.textBoxFirstName.Text = "John";
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(77, 64);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(182, 20);
            this.textBoxLastName.TabIndex = 10;
            this.textBoxLastName.Text = "Doe";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(356, 38);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(196, 20);
            this.textBoxEmail.TabIndex = 11;
            this.textBoxEmail.Text = "JohnDoe@ABC123.com";
            // 
            // textBoxTelephone
            // 
            this.textBoxTelephone.Location = new System.Drawing.Point(356, 64);
            this.textBoxTelephone.Name = "textBoxTelephone";
            this.textBoxTelephone.Size = new System.Drawing.Size(196, 20);
            this.textBoxTelephone.TabIndex = 12;
            // 
            // textBoxAddress1
            // 
            this.textBoxAddress1.Location = new System.Drawing.Point(100, 110);
            this.textBoxAddress1.Name = "textBoxAddress1";
            this.textBoxAddress1.Size = new System.Drawing.Size(452, 20);
            this.textBoxAddress1.TabIndex = 13;
            // 
            // textBoxAddress2
            // 
            this.textBoxAddress2.Location = new System.Drawing.Point(100, 141);
            this.textBoxAddress2.Name = "textBoxAddress2";
            this.textBoxAddress2.Size = new System.Drawing.Size(452, 20);
            this.textBoxAddress2.TabIndex = 14;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(47, 173);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(174, 20);
            this.textBoxCity.TabIndex = 15;
            // 
            // textBoxState
            // 
            this.textBoxState.Location = new System.Drawing.Point(265, 173);
            this.textBoxState.Name = "textBoxState";
            this.textBoxState.Size = new System.Drawing.Size(100, 20);
            this.textBoxState.TabIndex = 16;
            // 
            // textBoxZipCode
            // 
            this.textBoxZipCode.Location = new System.Drawing.Point(434, 173);
            this.textBoxZipCode.Name = "textBoxZipCode";
            this.textBoxZipCode.Size = new System.Drawing.Size(118, 20);
            this.textBoxZipCode.TabIndex = 17;
            // 
            // SaleClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 335);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.buttonGetNewCatalog);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.buttonCancelOrder);
            this.Controls.Add(this.buttonPlaceOrder);
            this.Name = "SaleClientForm";
            this.Text = "Sale Client Form";
            this.Load += new System.EventHandler(this.SaleClientForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPlaceOrder;
        private System.Windows.Forms.Button buttonCancelOrder;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button buttonGetNewCatalog;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label labelExpiration;
        private System.Windows.Forms.Label labelBillingTotal;
        private System.Windows.Forms.Button buttonRecall;
        private System.Windows.Forms.Label labelOrderStatus;
        private System.Windows.Forms.DataGridView OrderGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemStockQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemOrderQuantity;
        private System.Windows.Forms.TextBox textBoxOrderID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxZipCode;
        private System.Windows.Forms.TextBox textBoxState;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.TextBox textBoxAddress2;
        private System.Windows.Forms.TextBox textBoxAddress1;
        private System.Windows.Forms.TextBox textBoxTelephone;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label labelZipCode;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.Label labelAddress2;
        private System.Windows.Forms.Label labelAddress1;
        private System.Windows.Forms.Label labelTelephone;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.Label labelFirstName;
    }
}

