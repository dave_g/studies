﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace Microsoft.Samples.SaleClient
{
    class OrderManager
    {
        //=== Client connection to the Sale Service Web Service ===//
        private RetailSaleService.SaleServiceClient Client;

        //=== The ItemCatalog acts as a sale catalog or orderform ===/
        private RetailSaleService.ItemCatalog saleCatalog;

        //=== Current order status information based on last call to the web service ===//
        private RetailSaleService.OrderStatus orderStatus;


        public OrderManager()
        {
            //======================================================================================//
            //=== Bind to the Sale catalog service.  We use HttpContext binding for correaltion. ===//
            //=== Correlation allows the client to place the order to the same WF instance that  ===//
            //=== provided the catalog or get status of an order correlated by Order Id.         ===//
            //======================================================================================//

            orderStatus = new RetailSaleService.OrderStatus();

            try
            {
                Client = new RetailSaleService.SaleServiceClient("WSHttpContextBinding_ISaleService");
                saleCatalog = Client.GetCatalog();
                orderStatus.StatusText = "Catalog Provided";
            }
            catch (Exception ex)
            {
                orderStatus = new Microsoft.Samples.SaleClient.RetailSaleService.OrderStatus();
                orderStatus.StatusText = "Exception: " + ex.Message;    
            }
        }

        public OrderManager(System.Guid OrderId)
        {
            Client = new RetailSaleService.SaleServiceClient("WSHttpContextBinding_ISaleService");

            try
            {
                orderStatus = Client.GetStatus(OrderId);
                saleCatalog = new RetailSaleService.ItemCatalog();
                saleCatalog.CatalogId = orderStatus.PO.CatalogID;
                saleCatalog.Items = orderStatus.PO.Items;
            }
            catch (Exception ex)
            {
                orderStatus = new Microsoft.Samples.SaleClient.RetailSaleService.OrderStatus();
                orderStatus.StatusText = "Exception: " + ex.Message;    
            }
        }

        public RetailSaleService.ItemCatalog SaleCatalog
        {
            get { return saleCatalog; }
        }

        public RetailSaleService.OrderStatus OrderStatus
        {
            get { return orderStatus; }
        }

        public RetailSaleService.PurchaseOrder PurchaseOrder
        {
            get { return orderStatus.PO; }
            set { orderStatus.PO = value; }
        }

        public void PlaceOrder()
        {
            foreach (RetailSaleService.ItemInfo item in orderStatus.PO.Items)
            {
                if (item.ItemOrderQuantity > 0)
                {
                    try
                    {
                        orderStatus = Client.SubmitOrder(orderStatus.PO);
                    }
                    catch (Exception ex)
                    {
                        orderStatus.StatusText = "Exception: " + ex.Message;    
                    }
                    break;
                }
            }
        }

        public void CancelOrder()
        {
            if ((orderStatus.StatusText.ToUpper() == "ORDER RECEIVED") || (orderStatus.StatusText.ToUpper() == "ORDER FILLED"))
            {
                try
                {
                    orderStatus = Client.CancelOrder(orderStatus.PO.OrderID);
                }
                catch (Exception ex)
                {
                    orderStatus.StatusText = "Exception: " + ex.Message;    
                }
            }
        }

        public string GetStatus()
        {
            try
            {
                orderStatus = Client.GetStatus(orderStatus.PO.OrderID);
            }
            catch (Exception ex)
            {
                orderStatus.StatusText = "Exception: " + ex.Message;
            }

            return orderStatus.StatusText;
        }
    }
}
