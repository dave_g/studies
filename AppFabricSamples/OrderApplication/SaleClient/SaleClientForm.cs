﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Microsoft.Samples.SaleClient
{
    public partial class SaleClientForm : Form
    {
        //===============================================================//
        //=== Provides a wrapper around the service communications    ===//
        //=== with minimal exception handling and data encapsulation. ===//
        //===============================================================//
        private OrderManager orderManager = null;

        //=== Delegates to make all service calls on a secondary thread. ===//
        private delegate void GetNewCatalogDelegate();
        private delegate void PlaceOrderDelegate();
        private delegate void GetStatusDelegate();
        private delegate void CancelOrderDelegate();
        private delegate void RecallDelegate(System.Guid OrderId);
        private delegate void CallbackDelegate();

        private GetNewCatalogDelegate NewOrderManagerHandler;
        private PlaceOrderDelegate PlaceNewOrderHandler;
        private GetStatusDelegate GetStatusHandler;
        private CancelOrderDelegate CancelOrderHandler;
        private RecallDelegate RecallOrderHandler;
        private CallbackDelegate CallbackHandler;


        private bool OrderPlaced;

        public SaleClientForm()
        {
            InitializeComponent();
        }

        private void SaleClientForm_Load(object sender, EventArgs e)
        {
            NewOrderManagerRoutine();
        }

        private void buttonGetNewCatalog_Click(object sender, EventArgs e)
        {
            NewOrderManagerRoutine();
        }


        private void NewOrderManagerRoutine()
        {
            labelExpiration.BackColor = SystemColors.Control;
            labelExpiration.ForeColor = SystemColors.ControlText;
            textBoxOrderID.Clear();
            OrderPlaced = false;
            StatusLabel.Text = "Status: Getting New Catalog from Sale Service.  Please Wait...";

            //=== Make the Get Catalog request on a secondary thread so as not to block the UI thread ===//
            NewOrderManagerHandler = new GetNewCatalogDelegate(this.GetNewOrderManager);
            NewOrderManagerHandler.BeginInvoke(null, null);
        }

        private void GetNewOrderManager()
        {
            //=== A Blocking service call executed on secondary thread===//
            orderManager = new OrderManager();

            //=== Make UI updates back on the primary thread ===//
            CallbackHandler = new CallbackDelegate(this.NewOrderManagerCallback);
            this.BeginInvoke(CallbackHandler);
        }

        private void NewOrderManagerCallback()
        {
            try
            {
                BindGrid();
            }
            catch { }

            timer1.Enabled = true;
            StatusLabel.Text = "Status: New Catalog Received and Ready for Orders";
            UpdateOrderStatus();
            UpdateState();
        }

        public void BindGrid()
        {
            if (orderManager.OrderStatus.PO != null)
            {
                OrderGridView.DataSource = orderManager.OrderStatus.PO.Items;
                
            }
            else
            {
                if (orderManager.SaleCatalog != null)
                    OrderGridView.DataSource = orderManager.SaleCatalog.Items;
            }

            foreach (DataGridViewColumn col in OrderGridView.Columns)
            {
                if (col.Name != "ItemOrderQuantity")
                    col.ReadOnly = true;
            }
        }

        private void CalcBillingTotal()
        {
            float BillingTotal = 0.0f;

            if ((orderManager != null) && (orderManager.SaleCatalog != null))
            {
                foreach (RetailSaleService.ItemInfo item in orderManager.SaleCatalog.Items)
                {
                    if (item.ItemOrderQuantity > 0)
                        BillingTotal += item.ItemPrice * item.ItemOrderQuantity;
                }
            }

            this.labelBillingTotal.Text = "Billing Total : " + BillingTotal.ToString("c");
        }

        private void UpdateOrderStatus()
        {
            if (orderManager.OrderStatus.StatusText.Substring(0,9).ToUpper() == "EXCEPTION") 
                ReportException(this.StatusLabel.Text = orderManager.OrderStatus.StatusText);
            else
                this.labelOrderStatus.Text = "Order Status : " + orderManager.OrderStatus.StatusText;
        }

        private void buttonPlaceOrder_Click(object sender, EventArgs e)
        {
            if (this.labelBillingTotal.Text != "Billing Total : $0.00")
            {
                buttonPlaceOrder.Enabled = false;

                RetailSaleService.PurchaseOrder PO = new RetailSaleService.PurchaseOrder();
                PO.Items = orderManager.SaleCatalog.Items;
                PO.CatalogID = orderManager.SaleCatalog.CatalogId;
                PO.FirstName = textBoxFirstName.Text;
                PO.LastName = textBoxLastName.Text;
                PO.EmailAddress = textBoxEmail.Text;
                PO.TelephoneNumber = textBoxTelephone.Text;
                PO.AddressLine1 = textBoxAddress1.Text;
                PO.AddressLine2 = textBoxAddress2.Text;
                PO.City = textBoxCity.Text;
                PO.State = textBoxState.Text;
                PO.ZipCode = textBoxZipCode.Text;

                orderManager.PurchaseOrder = PO;

                this.StatusLabel.Text = "Placing Order with Sale Service.  Please Wait...";

                //=== Passing this off to a secondary thread so as not to block primary UI thread ===//
                PlaceNewOrderHandler = new PlaceOrderDelegate(this.PlaceNewOrder);
                PlaceNewOrderHandler.BeginInvoke(null, null);
            }
        }

        private void PlaceNewOrder()
        {
            //===============================================================================//
            //=== Executed on secondary thread placing the order based on item quantities ===//
            //=== set in OrderManager::saleCatalog.                                       ===//
            //===============================================================================//
            orderManager.PlaceOrder();

            //=== Make UI updates back on the primary thread ===//
            CallbackHandler = new CallbackDelegate(this.PlaceNewOrderCallback);
            this.BeginInvoke(CallbackHandler);

        }

        private void PlaceNewOrderCallback()
        {
            //===============================================================//
            //=== If the order was received by the service supply the     ===//
            //=== order id to the user for possible correlation later     ===//
            //=== with the workflow service.                              ===//
            //===============================================================//
            if (orderManager.OrderStatus.StatusText.ToUpper() == "ORDER RECEIVED")
            {
                this.textBoxOrderID.Text = orderManager.OrderStatus.PO.OrderID.ToString();
                OrderPlaced = true;
            }

            this.StatusLabel.Text = "Order placed with Sale Service.";
            UpdateOrderStatus();
            UpdateState();
        }
    

        private void buttonRecall_Click(object sender, EventArgs e)
        {
            //===============================================================//
            //=== This method allows the user to correlate an existing    ===//
            //=== workflow instance with a previously supplied order id   ===//
            //=== which was recorded and entered by the user.             ===//
            //===============================================================//

            if (textBoxOrderID.Text != "")
            {
                try
                {
                    System.Guid OrderId = new System.Guid(textBoxOrderID.Text);
                    buttonRecall.Enabled = false;
                    this.StatusLabel.Text = "Recalling the Order from the Sale Service. Please Wait...";

                    //===============================================================================//
                    //=== Executed on secondary thread so the UI thread is not blocked.           ===//
                    //===============================================================================//
                    RecallOrderHandler = new RecallDelegate(this.RecallOrder);
                    RecallOrderHandler.BeginInvoke(OrderId, null, null);
                }
                catch (Exception ex)
                {
                    ReportException(this.StatusLabel.Text = "Exception: " + ex.Message);
                }
            }
        }

        private void RecallOrder(System.Guid OrderId)
        {
            //========================================================================//
            //=== Recalling the order is done by correlating the order Id with the ===//
            //=== workflow. This is accomplished in the OrderManager wrapper by    ===//
            //=== making a GetStatus call with the correct order ID.               ===//
            //========================================================================//
            orderManager = new OrderManager(OrderId);

            //=== Make UI updates back on the primary thread ===//
            CallbackHandler = new CallbackDelegate(this.RecallOrderCallback);
            this.BeginInvoke(CallbackHandler);
        }

        private void RecallOrderCallback()
        {
            OrderPlaced = true;
            timer1.Enabled = true;

            try
            {
                BindGrid();
                CalcBillingTotal();
            }
            catch { }

            UpdateOrderStatus();
            UpdateState();
            buttonRecall.Enabled = true;
            this.StatusLabel.Text = "Order Recalled";
        }


        private void buttonCancelOrder_Click(object sender, EventArgs e)
        {
            this.StatusLabel.Text = "Requesting the cancellation of the Order. Please Wait...";

            //===============================================================================//
            //=== Executed on secondary thread so the UI thread is not blocked.           ===//
            //===============================================================================//
            CancelOrderHandler = new CancelOrderDelegate(this.CancelOrder);
            CancelOrderHandler.BeginInvoke(null, null);                  
        }

        private void CancelOrder()
        {
            //=== A Blocking service call executed on secondary thread ===//
            orderManager.CancelOrder();

            //=== Make UI updates back on the primary thread ===//
            CallbackHandler = new CallbackDelegate(this.CancelOrderCallback);
            this.BeginInvoke(CallbackHandler);
        }

        private void CancelOrderCallback()
        {
            //=== UI updates back on the primary thread ===//
            timer1.Enabled = false;
            UpdateOrderStatus();
            UpdateState();
            this.StatusLabel.Text = "Cancellation Request Complete.";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //===============================================================//
            //=== Show catalog expiration visibly                         ===//
            //===============================================================//
                try
                {
                    DateTime dtExpiration = orderManager.SaleCatalog.Expiration;
                    TimeSpan tilExpiration = (dtExpiration - DateTime.Now);
                    this.labelExpiration.Text = String.Format("Expiration : {0}", dtExpiration.ToString());

                    if (OrderPlaced == false)
                    {
                        if (tilExpiration > TimeSpan.Zero)
                        {
                            this.labelExpiration.Text += String.Format(" ({0:D2}:{1:D2})", tilExpiration.Minutes, tilExpiration.Seconds);
                        }
                        else
                        {
                            labelExpiration.BackColor = Color.Red;
                            labelExpiration.ForeColor = Color.White;
                            orderManager.OrderStatus.StatusText = "Catalog Expired";
                            UpdateState();
                        }
                    }
                    else
                    {
                        this.labelExpiration.Text = String.Format("Expiration : N/A");
                    }
                }
                catch { }


            //===============================================================//
            //=== Update order status information at each timer interval. ===//
            //=== The purpose for this is to show the user when the order ===//
            //=== is filled and shipped. Catalog expiration is also       ===//
            //=== reported in the status.                                 ===//
            //===============================================================//
            try
            {
                if ((orderManager.OrderStatus.StatusText.ToUpper() != "CATALOG PROVIDED") &&
                    (orderManager.OrderStatus.StatusText.ToUpper() != "CATALOG EXPIRED"))
                {
                    //===============================================================================//
                    //=== Executed on secondary thread so the UI thread is not blocked.           ===//
                    //===============================================================================//
                    GetStatusHandler = new GetStatusDelegate(this.GetStatus);
                    GetStatusHandler.BeginInvoke(null, null);
                }
            }
            catch(Exception Ex)
            {
                ReportException(Ex.Message);
            }


        }

        private void GetStatus()
        {
            //=== A Blocking service call executed on secondary thread ===//
            orderManager.GetStatus();

            //=== Make UI updates back on the primary thread ===//
            CallbackHandler = new CallbackDelegate(this.GetStatusCallback);
            this.BeginInvoke(CallbackHandler);
        }

        private void GetStatusCallback()
        {
            //===============================================================//
            //=== Any exceptions encountered will be reported here in the ===//
            //=== order status text                                       ===//
            //===============================================================//

            //=== UI updates back on the primary thread ===//
            UpdateOrderStatus();
            UpdateState();
        }


        private void UpdateState()
        {
            //===============================================================//
            //=== Enabling valid operations against the workflow based    ===//
            //=== off the order state text.                               ===//
            //===============================================================//

            switch (orderManager.OrderStatus.StatusText.ToUpper())
            {
                case "CATALOG PROVIDED":
                    buttonPlaceOrder.Enabled = true;
                    buttonCancelOrder.Enabled = false;
                    break;

                case "CATALOG EXPIRED":
                    buttonPlaceOrder.Enabled = false;
                    buttonCancelOrder.Enabled = false;
                    timer1.Enabled = false;
                    OrderPlaced = false;
                    this.StatusLabel.Text = "Catalog Has Expired";
                    break;

                case "ORDER RECEIVED":
                    buttonPlaceOrder.Enabled = false;
                    buttonCancelOrder.Enabled = true;
                    OrderPlaced = true;
                    break;

                case "ORDER CANCELED":
                    buttonPlaceOrder.Enabled = false;
                    buttonCancelOrder.Enabled = false;
                    timer1.Enabled = false;
                    break;

                case "ORDER FILLED":
                    buttonPlaceOrder.Enabled = false;
                    buttonCancelOrder.Enabled = true;
                    break;

                case "ORDER SHIPPED":
                    buttonPlaceOrder.Enabled = false;
                    buttonCancelOrder.Enabled = false;
                    timer1.Enabled = false;
                    break;
            }
        }

        private void OrderGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            CalcBillingTotal();
        }

        private void ReportException(String strMessage)
        {
            timer1.Enabled = false;
            MessageBox.Show(strMessage, "Exception Information");
        }
    }
}
