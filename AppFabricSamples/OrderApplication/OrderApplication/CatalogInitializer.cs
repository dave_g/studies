﻿namespace Microsoft.Samples.SaleService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Activities;

    // If your activity returns a value, derive from CodeActivity<TResult>
    // and return the value from the Execute method.



    public sealed class CatalogInitializer : CodeActivity
    {
        //=== Define an activity output argument of type ItemCatalog to return the initialized  ===//
        //=== daily sale catalog.                                                               ===//
        public OutArgument<ItemCatalog> Catalog { get; set; }

        //=== Define an activity output argument of type OrderStatus which will hold all catalog ===//
        //=== and order information made available to a client.                                  ===//
        public OutArgument<OrderStatus> StatusObject { get; set; }

        //=== Define an activity output argument of type string which will provide direct access ===//
        //=== to a status string.                                                                ===//
        public OutArgument<string> StatusText { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            ItemCatalog catalog = new ItemCatalog();

            List<ItemInfo> Items = new List<ItemInfo>();
            Items.Add(new ItemInfo() { ItemId = "Mon24Wid", ItemDescription = "24 Inch Monitor", ItemStockQuantity = 2434, ItemPrice=265.99f, ItemOrderQuantity=0 });
            Items.Add(new ItemInfo() { ItemId = "Q28Proc", ItemDescription = "Quad Core 2.8 GHz Processor", ItemStockQuantity = 3105, ItemPrice = 305.99f, ItemOrderQuantity = 0 });
            Items.Add(new ItemInfo() { ItemId = "XABC-123MB", ItemDescription = "XABC-123 Motherboard", ItemStockQuantity = 2153, ItemPrice = 185.99f, ItemOrderQuantity = 0 });
            Items.Add(new ItemInfo() { ItemId = "2GB-DDR2800", ItemDescription = "2GB DDR2800 RAM", ItemStockQuantity = 3253, ItemPrice = 39.99f, ItemOrderQuantity = 0 });

            catalog.Items = Items;
            catalog.Expiration = DateTime.Now.AddMinutes(3.0);
            catalog.CatalogId = System.Guid.NewGuid();
            
            context.SetValue(Catalog, catalog);

            OrderStatus status = new OrderStatus();

            status.StatusText = "Catalog Provided";

            context.SetValue(StatusObject, status);
            context.SetValue(StatusText, status.StatusText);
        }
    }
}
