﻿namespace Microsoft.Samples.SaleService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Activities;

    // If your activity returns a value, derive from CodeActivity<TResult>
    // and return the value from the Execute method.

    public sealed class CancelOrder : CodeActivity
    {
        //=== Define an activity output argument of type OrderStatus which will hold all catalog ===//
        //=== and order information made available to a client.                                  ===//
        public InOutArgument<OrderStatus> StatusObject { get; set; }

        //=== Define an activity output argument of type string which will provide direct access ===//
        //=== to a status string.                                                                ===//
        public OutArgument<string> StatusText { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            //=== Obtain the runtime value of the Order to be canceled ===//
            OrderStatus status = context.GetValue(this.StatusObject);
            PurchaseOrder CanceledOrder = status.PO; 

            //=== Perform necessary steps to properly cancel the order if still possible. ===//
            if ((status.StatusText.ToUpper() == "ORDER RECEIVED") || (status.StatusText.ToUpper() == "ORDER FILLED"))
            {
                status.StatusText = "Order Canceled";
            }

            context.SetValue(StatusObject, status);
            context.SetValue(StatusText, status.StatusText);      
        }
    }
}
