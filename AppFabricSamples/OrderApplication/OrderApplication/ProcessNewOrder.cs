﻿namespace Microsoft.Samples.SaleService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Activities;

    // If your activity returns a value, derive from CodeActivity<TResult>
    // and return the value from the Execute method.

    public sealed class OrderProcessor : CodeActivity
    {
        //=== Define an activity output argument of type OrderStatus which will hold all catalog ===//
        //=== and order information made available to a client.                                  ===//
        public OutArgument<OrderStatus> StatusObject { get; set; }

        //=== Define an activity input argument of type PurchaseOrder which will receive        ===//
        //=== the new purchase order.                                                           ===//
        public InArgument<PurchaseOrder> NewPurchaseOrder { get; set; }

        //=== Define an activity output argument of type string which will provide direct access ===//
        //=== to a status string.                                                                ===//
        public OutArgument<string> StatusText { get; set; }

        //=== Define an activity input argument of type guid to hold Order Id.                   ===//
        public OutArgument<Guid> OrderId { get; set; }

        //=== Define an activity input argument of type float to hold purchase total.            ===//
        public OutArgument<Double> Total { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the status object
            PurchaseOrder NewPO = context.GetValue(this.NewPurchaseOrder);

            // Add up the purchase total
            float total = 0.0f;
            foreach (ItemInfo item in NewPO.Items)
            {
                if (item.ItemOrderQuantity > 0)
                    total += item.ItemPrice * item.ItemOrderQuantity;
            }

            // Set the runtime value of the Total output argument
            context.SetValue(this.Total, total);

            NewPO.OrderID = System.Guid.NewGuid();

            OrderStatus status = new OrderStatus();
            status.PO = NewPO;
            status.StatusText = "Order Received";

            context.SetValue(StatusObject, status);
            context.SetValue(StatusText, status.StatusText);
            context.SetValue(OrderId, NewPO.OrderID);
        }
    }
}
