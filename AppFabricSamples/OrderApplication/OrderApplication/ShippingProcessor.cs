﻿namespace Microsoft.Samples.SaleService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Activities;

    // If your activity returns a value, derive from CodeActivity<TResult>
    // and return the value from the Execute method.

    public sealed class ShippingProcessor : CodeActivity
    {
        //=== Define an activity output argument of type OrderStatus which will hold all catalog ===//
        //=== and order information made available to a client.                                  ===//
        public InOutArgument<OrderStatus> StatusObject { get; set; }

        
        //=== Define an activity output argument of type string which will provide direct access ===//
        //=== to a status string.                                                                ===//
        public OutArgument<string> StatusText { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //=== Obtain the runtime value of the Order status ===//
            OrderStatus CurrentStatus = context.GetValue(this.StatusObject);

            //=== Perform necessary steps to properly process the order ===//
            switch (CurrentStatus.StatusText)
            {
                case "Order Received":
                    CurrentStatus.StatusText = "Order Filled";
                    break;

                case "Order Filled":
                    CurrentStatus.StatusText = "Order Shipped";
                    break;
            }


            //=== Provide workflow access to status information on the order ===//
            context.SetValue(StatusObject, CurrentStatus);
            context.SetValue(StatusText, CurrentStatus.StatusText);
        }
    }
}
