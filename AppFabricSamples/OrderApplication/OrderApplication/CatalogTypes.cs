﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Microsoft.Samples.SaleService
{

    [DataContract]
    public class ItemInfo
    {
        [DataMember]
        public string ItemId;
        [DataMember]
        public string ItemDescription;
        [DataMember]
        public float ItemPrice;
        [DataMember]
        public int ItemStockQuantity;
        [DataMember]
        public int ItemOrderQuantity;
    }

    
    [DataContract]
    public class ItemCatalog
    {
        [DataMember]
        public Guid CatalogId;

        [DataMember]
        public DateTime Expiration;

        [DataMember]
        public List<ItemInfo> Items;
    }

    [DataContract]
    public class PurchaseOrder
    {
        [DataMember]
        public Guid CatalogID;
        [DataMember]
        public Guid OrderID;
        [DataMember]
        public string FirstName;
        [DataMember]
        public string LastName;
        [DataMember]
        public string EmailAddress;
        [DataMember]
        public string TelephoneNumber;
        [DataMember]
        public string AddressLine1;
        [DataMember]
        public string AddressLine2;
        [DataMember]
        public string City;
        [DataMember]
        public string State;
        [DataMember]
        public string ZipCode;
        [DataMember]
        public List<ItemInfo> Items;
    }

    [DataContract]
    public class OrderStatus
    {
        [DataMember]
        public PurchaseOrder PO;

        [DataMember]
        public string StatusText;
    }

}


