﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.ApplicationServer.Caching;

public partial class CreateOrder : System.Web.UI.Page
{
    private DataCache m_cache = null;

    /// <summary>
    /// Get access to the cache on the Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        m_cache = CacheUtil.GetCache();
    }

    /// <summary>
    /// Store the order created into the cache
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CreateOrder_Click(object sender, EventArgs e)
    {
        string result = null;

        try
        {
            Order order = new Order();
            order.Id = Convert.ToInt32(OrderId.Text);
            order.Description = OrderDesc.Text;
            order.Amount = OrderAmt.Text;
            order.Quantity = Convert.ToInt32(OrderQuant.Text);

            //Add data to the cache
            m_cache.Add(OrderId.Text, order);
            result = "Order " + OrderId.Text + " successfully created";
        }
        catch (Exception e1)
        {
            result = "Exception: " + e1.Message;
        }

        CreateResult.Text = result ?? String.Empty;
    }
}
