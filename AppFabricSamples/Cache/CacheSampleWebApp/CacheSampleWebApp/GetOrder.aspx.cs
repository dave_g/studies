﻿using System;
using Microsoft.ApplicationServer.Caching;


public partial class Default2 : System.Web.UI.Page
{
    private DataCache m_cache = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        m_cache = CacheUtil.GetCache();
        OrderDesc.Enabled = false;
        OrderAmt.Enabled = false;
        OrderQuant.Enabled = false;
    }

    protected void GetOrder_Click(object sender, EventArgs e)
    {
        try
        {
            //Get data from the cache
            Order order = (Order)m_cache.Get(OrderId.Text);
            if (order == null)
            {
                GetResult.Text = "Order " + OrderId.Text + " not found";
            }
            else
            {
                OrderDesc.Text = order.Description;
                OrderAmt.Text = order.Amount;
                OrderQuant.Text = order.Quantity.ToString();
                GetResult.Text = String.Empty;
            }
        }
        catch (Exception e1)
        {
            GetResult.Text = "Exception " + e1.Message;
        }
    }
}
