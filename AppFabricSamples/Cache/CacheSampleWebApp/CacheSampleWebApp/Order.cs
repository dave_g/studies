﻿using System;

/// <summary>
/// Summary description for Order
/// </summary>
[Serializable]
public class Order
{
    public int Id 
    {
      get { return m_id; }
      set { m_id = value; }
    }

    public string Description 
    {
      get { return m_description; }
      set { m_description = value; }
    }
     
    public int Quantity
    {
      get { return m_quantity; }
      set { m_quantity = value; }
    }

    public string Amount 
    {
      get { return m_amount; }
      set { m_amount = value; }
    }

    private int m_id = 0;
    private string m_description = null;
    private int m_quantity = 0;
    private string m_amount;
}
