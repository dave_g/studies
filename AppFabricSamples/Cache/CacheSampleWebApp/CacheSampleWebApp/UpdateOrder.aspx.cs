﻿using System;
using Microsoft.ApplicationServer.Caching;


public partial class UpdateOrder : System.Web.UI.Page
{
    private DataCache m_cache = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        m_cache = CacheUtil.GetCache();
        OrderId.Enabled = true;
        OrderDesc.Enabled = false;
        OrderAmt.Enabled = false;
        OrderQuant.Enabled = false;
        UpdateButton.Enabled = false;
    }

    protected void GetOrder_Click(object sender, EventArgs e)
    {
        Order order = null;
        DataCacheItemVersion version = null;

        try
        {
            //Get data from cache
            order = (Order)m_cache.Get(OrderId.Text, out version);
            if (order == null)
            {
                UpdateResult.Text = "Order " + OrderId.Text + " not found";
            }
            else
            {
                OrderDesc.Text = order.Description;
                OrderDesc.Enabled = true;
                OrderAmt.Text = order.Amount;
                OrderAmt.Enabled = true;
                OrderQuant.Text = order.Quantity.ToString();
                OrderQuant.Enabled = true;
                UpdateButton.Enabled = true;
                OrderId.Enabled = false;
                UpdateResult.Text = String.Empty;
            }
        }
        catch (Exception e1)
        {
            UpdateResult.Text = "Exception " + e1.Message;
        }
    }

    protected void UpdateOrder_Click(object sender, EventArgs e)
    {
        Order order = null;

        try
        {
            //Check if Cache still has the item
            order = (Order)m_cache.Get(OrderId.Text);
            if (order == null)
            {
                UpdateResult.Text = "Order " + OrderId.Text + " not found";
            }
            else
            {
                order.Description = OrderDesc.Text;
                order.Amount = OrderAmt.Text;
                order.Quantity = Convert.ToInt32(OrderQuant.Text);
                UpdateResult.Text = String.Empty;
            }
        }
        catch (Exception e1)
        {
            UpdateResult.Text = "Invalid Order: " + e1.Message;
        }

        try
        {
            //Put the modified order in the Cache
            m_cache.Put(OrderId.Text, order);
            UpdateResult.Text = "Order " + OrderId.Text + " successfully updated";
        }
        catch (Exception e2)
        {
            UpdateResult.Text = "Update Failed: " + e2.Message;
        }
    }
}
