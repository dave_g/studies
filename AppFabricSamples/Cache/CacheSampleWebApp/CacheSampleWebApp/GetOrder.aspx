﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Default2" Codebehind="GetOrder.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Distributed Cache: Get Order</title>
</head>
<body>
 <h2>Distributed Cache Sample</h2>
 <br />
 <form id="form1" runat="server">
  <table border="0" >
   <tr>
    <td><a href="CreateOrder.aspx">Create Order</a></td>
    <td>&nbsp</td>
    <td align="right">Order Id</td>
    <td><asp:TextBox ID="OrderId" runat="server" Width="85px"></asp:TextBox></td>
   </tr>
   <tr>
    <td><a href="GetOrder.aspx">Get Order</a></td>
    <td></td>
    <td align="right">Order Description</td>
    <td><asp:TextBox ID="OrderDesc" runat="server" Width="200px"></asp:TextBox></td>    
   </tr>
   <tr>
    <td><a href="UpdateOrder.aspx">Update Order</a></td>
    <td></td>
    <td align="right">Order Quantity</td>
    <td><asp:TextBox ID="OrderQuant" runat="server" Width="85"></asp:TextBox></td>    
   </tr>
   <tr>
    <td></td>
    <td></td>
    <td align="right">Order Amount</td>
    <td><asp:TextBox ID="OrderAmt" runat="server" Width="85"></asp:TextBox></td>    
   </tr>
   <tr>
    <td></td>
    <td></td>
    <td></td>
    <td><asp:Button ID="GetOrderButton" runat="server" Text="Get Order" onclick="GetOrder_Click" /></td>
   </tr>
   <tr>
    <td></td>
    <td></td>
    <td></td>
    <td><asp:Label ID="GetResult" runat="server" Text="" Width="150"></asp:Label></td>
   </tr>
  </table>
 </form>
</body>

</html>
