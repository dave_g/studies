﻿To setup the developer environment on Visual Studio do the following
1) Install Windows Server AppFabric Cache Service and Admin. This will also GAC the required DLLs.
2) Ensure that the user Id which you are using to run this sample is allowed in the cluster
   as a client account. [Use the 'Grant-CacheAllowedClientAccount' command].
3) Start the cluster using 'Start-CacheCluster' command.
4) Build and Run this sample. The sample will automatically link to the 
    Distributed Cache DLLs (Microsoft.ApplicationServer.Caching.Client.dll and
    Microsoft.ApplicationServer.Caching.Core.dll) from GAC.
5) This sample needs to be passed 1 command line parameter: 
   Path to the 'fileshare' directory (This directory is part of the 'DiskVersusCachePerformance' folder).
   You can also add these to the project properties, in the section 'command line arguments'.
   
Note: For each subsequent run of this sample, please do 'Restart-CacheCluster' and wait for 10-15 seconds.