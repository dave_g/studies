﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security.AccessControl;
using Microsoft.ApplicationServer.Caching;
using System.Security.Principal;

namespace DiskVersusCachePerformance
{
    class UsersLoggedIn
    {
        public string loggedInUser = "";
        List<string> existingUsers;

        public UsersLoggedIn()
        {
            loggedInUser = "";
            existingUsers = new List<string>();
        }

        public bool LogOutUser(string username)
        {
            if (existingUsers.Contains(username) && !string.IsNullOrEmpty(loggedInUser))
            {
                loggedInUser = "";
                return true;
            }
            else
            {
                Console.WriteLine("Error : User cannot be loggedout " + username);
                return false;
            }
        }

        public bool LoginUser(string username)
        {
            if (existingUsers.Contains(username))
            {
                loggedInUser = username;
                return true;
            }
            else
            {
                return false;
            }
        }


        public int LoadConfig(string root, string configfile)
        {
            XmlDocument configfilexml = new XmlDocument();
            Console.WriteLine();
            Console.WriteLine("Loading config.xml");
            try
            {
                configfilexml.Load(root + configfile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }

            XmlNodeList list = configfilexml.SelectNodes("/userdatabase/user/userid");
            Console.WriteLine(" Total # of users " + list.Count);
            foreach (XmlNode user in list)
            {
                existingUsers.Add(user.InnerText);
                Console.WriteLine(" <user = " + user.InnerText + ">");
            }

            Console.WriteLine("Completed loading config.xml");
            Console.WriteLine();
            return 0;
        }
    }

    class UserAction
    {
        UsersLoggedIn userDatabase;
        List<XmlNode> userAction;
        string rootPath;
        string myCacheRegion;
        DataCache myCache;

        public UserAction(UsersLoggedIn tempLoggedInUsers)
        {
            userDatabase = tempLoggedInUsers;
        }

        public int LoadConfig(string root, string useractionfile, DataCache mycache, string myregion)
        {
            myCacheRegion = myregion;
            myCache = mycache;
            rootPath = new string(root.ToCharArray());
            XmlDocument useractionxml = new XmlDocument();
            userAction = new List<XmlNode>();
            Console.WriteLine("Loading useraction.xml");
            try
            {
                useractionxml.Load(rootPath + useractionfile);
            }
            catch (Exception ex)
            {
                Console.WriteLine("FAIL - Error loading useraction file");
                Console.WriteLine(ex.Message);
                return -1;
            }

            Console.Write("Header ");
            XmlNode node = useractionxml.DocumentElement;
            Console.WriteLine(node.Name);

            Console.Write("Total no. of useractions ");
            XmlNodeList list = useractionxml.SelectNodes("/useractiondb/useraction");
            Console.WriteLine(list.Count);

            foreach (XmlNode user in list)
            {
                userAction.Add(user);
                Console.WriteLine("- " + user.InnerText);
            }
            Console.WriteLine("Completed loading useraction.xml");
            Console.WriteLine();
            return 0;
        }

        public double Run(string whoami)
        {
            Album loadalbum;
            int currentpage = 1, currentalbum = 1;
            XmlNode currentUserAction;
            XmlNode currentUserData;
            double delay = 0, mydelay = 0;
            int pageSize = 20; //default value if none specified in useraction.xml

            Console.WriteLine("Running User Actions Start");
            loadalbum = new Album(whoami, rootPath, "albums.xml", myCache, myCacheRegion);
            foreach (XmlNode action in userAction)
            {
                /* Options
                * LogIn, ViewPage, ViewNextPage, ViewPrevPage, LogOut
                * AddPhoto, CopyPage, DeletePage
                */
                try
                {
                    IEnumerator en = action.ChildNodes.GetEnumerator();
                    en.MoveNext();
                    currentUserAction = (XmlNode)en.Current;
                    en.MoveNext();
                    currentUserData = (XmlNode)en.Current;
                }
                catch (InvalidOperationException ex)
                {
                    System.Console.WriteLine(ex.Message);
                    return -1;
                }
                if (currentUserAction.InnerText.Equals("PageSize"))
                    pageSize = System.Convert.ToInt32(currentUserData.InnerText, 10);
                else if (currentUserAction.InnerText.Equals("LogIn"))
                {
                    userDatabase.LoginUser(currentUserData.InnerText);
                    Console.WriteLine("User " + currentUserData.InnerText + " is logged in");
                    delay = 0;
                }
                else if (currentUserAction.InnerText.Equals("ViewAlbum"))
                {
                    currentalbum = System.Convert.ToInt32(currentUserData.InnerText, 10);
                    Console.WriteLine("-->SetCurrentAlbum " + currentalbum);
                }
                else if (currentUserAction.InnerText.Equals("ViewPage"))
                {

                    currentpage = System.Convert.ToInt32(currentUserData.InnerText, 10);
                    Console.WriteLine("-->View Album/Page " + currentalbum + "/" + currentpage);
                    mydelay = loadalbum.ReadPage(userDatabase.loggedInUser, currentalbum, currentpage, pageSize);
                    if (mydelay == -1)
                        Console.WriteLine("Error : ReadPage Fails");
                    else
                        delay += mydelay;
                }
                else if (currentUserAction.InnerText.Equals("ViewNextPage"))
                {
                    currentpage++;
                    Console.WriteLine("-->View Next Page Album/Page " + currentalbum + "/" + currentpage);
                    mydelay = loadalbum.ReadPage(userDatabase.loggedInUser, currentalbum, currentpage, pageSize);
                    if (mydelay == -1)
                        Console.WriteLine("Error : ReadPage Fails");
                    else
                        delay += mydelay;
                }
                else if (currentUserAction.InnerText.Equals("ViewPrevPage"))
                {
                    currentpage--;
                    Console.WriteLine("-->View Prev Page Album/Page " + currentalbum + "/" + currentpage);
                    mydelay = loadalbum.ReadPage(userDatabase.loggedInUser, currentalbum, currentpage, pageSize);
                    if (mydelay == -1)
                        Console.WriteLine("Error : ReadPage Fails");
                    else
                        delay += mydelay;
                }
                else if (currentUserAction.InnerText.Equals("LogOut"))
                {
                    userDatabase.LogOutUser(currentUserData.InnerText);
                    Console.WriteLine("Logging out " + currentUserData.InnerText + " Total Delay is " + delay);
                    loadalbum.PrintSummary();
                }
            }
            Console.WriteLine("Running User Actions Complete");
            return delay;
        }
    }

    class Album
    {
        struct Summary
        {
            public string header;
            public int totalItems;
            public long avgFileSize;
            public double avgFromDisk;
            public double avgFromCache;
        } ;
        Summary largestFile, smallestFile, avgLessThan50kb, avgLessThan500kb, avgLessThan2mb, avgMoreThan2mb;

        int albumSize;
        string userIdentity;
        string myRegion;
        DataCache myCache;
        XmlNodeList albumConfig;
        string rootPath;

        public void PrintSummary()
        {
            ConsoleColor existingColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            string formatedString = "{0,-16}{1,6}{2,8}{3,13}{4,12}{5,15}{6,8}";
            Console.WriteLine("Summary of Disk vs Cache");
            Console.WriteLine(formatedString, "                ", "Total", "Average", "Average", "", "Average", "");
            Console.WriteLine(formatedString, "                ", "Files", "Size", "Disk Delay", "", "Cache Delay", "");
            Console.WriteLine(formatedString, "                ", "#", "(bytes)", "ticks", "ms", "ticks", "ms");
            Console.WriteLine(formatedString,
                                largestFile.header,
                                largestFile.totalItems,
                                largestFile.avgFileSize,
                                System.Math.Round(largestFile.avgFromDisk, 2),
                                System.Math.Round((largestFile.avgFromDisk / System.TimeSpan.TicksPerMillisecond), 2),
                                System.Math.Round(largestFile.avgFromCache, 2),
                                System.Math.Round((largestFile.avgFromCache / System.TimeSpan.TicksPerMillisecond), 2));
            Console.WriteLine(formatedString,
                                smallestFile.header,
                                smallestFile.totalItems,
                                smallestFile.avgFileSize,
                                System.Math.Round(smallestFile.avgFromDisk, 2),
                                System.Math.Round((smallestFile.avgFromDisk / System.TimeSpan.TicksPerMillisecond), 2),
                                System.Math.Round(smallestFile.avgFromCache, 2),
                                System.Math.Round((smallestFile.avgFromCache / System.TimeSpan.TicksPerMillisecond), 2));
            Console.WriteLine(formatedString,
                                avgLessThan50kb.header,
                                avgLessThan50kb.totalItems,
                                avgLessThan50kb.avgFileSize,
                                System.Math.Round(avgLessThan50kb.avgFromDisk, 2),
                                System.Math.Round((avgLessThan50kb.avgFromDisk / System.TimeSpan.TicksPerMillisecond), 2),
                                System.Math.Round(avgLessThan50kb.avgFromCache, 2),
                                System.Math.Round((avgLessThan50kb.avgFromCache / System.TimeSpan.TicksPerMillisecond), 2));
            Console.WriteLine(formatedString,
                                avgLessThan500kb.header,
                                avgLessThan500kb.totalItems,
                                avgLessThan500kb.avgFileSize,
                                System.Math.Round(avgLessThan500kb.avgFromDisk, 2),
                                System.Math.Round(avgLessThan500kb.avgFromDisk / System.TimeSpan.TicksPerMillisecond, 2),
                                System.Math.Round(avgLessThan500kb.avgFromCache, 2),
                                System.Math.Round(avgLessThan500kb.avgFromCache / System.TimeSpan.TicksPerMillisecond, 2));
            Console.WriteLine(formatedString,
                                avgLessThan2mb.header,
                                avgLessThan2mb.totalItems,
                                avgLessThan2mb.avgFileSize,
                                System.Math.Round(avgLessThan2mb.avgFromDisk, 2),
                                System.Math.Round(avgLessThan2mb.avgFromDisk / System.TimeSpan.TicksPerMillisecond, 2),
                                System.Math.Round(avgLessThan2mb.avgFromCache, 2),
                                System.Math.Round(avgLessThan2mb.avgFromCache / System.TimeSpan.TicksPerMillisecond, 2));
            Console.WriteLine(formatedString,
                               avgMoreThan2mb.header,
                               avgMoreThan2mb.totalItems,
                               avgMoreThan2mb.avgFileSize,
                               System.Math.Round(avgMoreThan2mb.avgFromDisk, 2),
                               System.Math.Round(avgMoreThan2mb.avgFromDisk / System.TimeSpan.TicksPerMillisecond, 2),
                               System.Math.Round(avgMoreThan2mb.avgFromCache, 2),
                               System.Math.Round(avgMoreThan2mb.avgFromCache / System.TimeSpan.TicksPerMillisecond, 2));

            Console.WriteLine("================================");
            Console.WriteLine("% Performance using the Cache");
            Console.WriteLine(largestFile.header + "\t Access time Cache/Disk %\t: " +
                            System.Math.Round((double)(largestFile.avgFromCache / largestFile.avgFromDisk) * 100), 2);
            if (smallestFile.avgFromDisk > 0)
                Console.WriteLine(smallestFile.header + "\t Access time Cache/Disk %\t: " +
                                System.Math.Round((double)(smallestFile.avgFromCache / smallestFile.avgFromDisk) * 100), 2);
            if (avgLessThan50kb.avgFromDisk > 0)
                Console.WriteLine(avgLessThan50kb.header + "\t Access time Cache/Disk %\t: " +
                                System.Math.Round((double)(avgLessThan50kb.avgFromCache / (avgLessThan50kb.avgFromDisk)) * 100), 2);
            if (avgLessThan500kb.avgFromDisk > 0)
                Console.WriteLine(avgLessThan500kb.header + "\t Access time Cache/Disk %\t: " +
                                System.Math.Round((double)(avgLessThan500kb.avgFromCache / (avgLessThan500kb.avgFromDisk)) * 100), 2);

            if (avgLessThan2mb.avgFromDisk > 0)
                Console.WriteLine(avgLessThan2mb.header + "\t Access time Cache/Disk %\t: " +
                             System.Math.Round((double)(avgLessThan2mb.avgFromCache / (avgLessThan2mb.avgFromDisk)) * 100), 2);

            if (avgMoreThan2mb.avgFromDisk > 0)
                Console.WriteLine(avgMoreThan2mb.header + "\t Access time Cache/Disk %\t: " +
                                System.Math.Round((double)(avgMoreThan2mb.avgFromCache / (avgMoreThan2mb.avgFromDisk)) * 100), 2);

            Console.ForegroundColor = existingColor;
        }

        private int UpdateSummary(FileInfo albumitem, double delay, int source)
        {
            // Update LargestFile struct
            if (albumitem.Length >= largestFile.avgFileSize)
            {
                largestFile.avgFileSize = albumitem.Length;
                if (source == 0)//from disk
                    largestFile.avgFromDisk = delay;
                else if (source == 1) //from cache
                    largestFile.avgFromCache = delay;
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
            }
            // Update SmallestFile struct
            if (albumitem.Length <= smallestFile.avgFileSize)
            {
                smallestFile.avgFileSize = albumitem.Length;
                if (source == 0)//from disk
                    smallestFile.avgFromDisk = delay;
                else if (source == 1) //from cache
                    smallestFile.avgFromCache = delay;
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
            }
            //Update avgLessThan50kb
            if (albumitem.Length <= 50 * 1024)
            {
                avgLessThan50kb.avgFileSize = ((avgLessThan50kb.avgFileSize * avgLessThan50kb.totalItems) + albumitem.Length) /
                                                        (avgLessThan50kb.totalItems + 1);
                if (source == 1)
                    avgLessThan50kb.avgFromCache = ((avgLessThan50kb.avgFromCache * avgLessThan50kb.totalItems) + delay) /
                                                        (avgLessThan50kb.totalItems + 1);
                else if (source == 0)
                    avgLessThan50kb.avgFromDisk = ((avgLessThan50kb.avgFromDisk * avgLessThan50kb.totalItems) + delay) /
                                                        (avgLessThan50kb.totalItems + 1);
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
                avgLessThan50kb.totalItems++;
            }
            //Update avgLessThan500kb
            else if (albumitem.Length <= 500 * 1024)
            {
                avgLessThan500kb.avgFileSize = ((avgLessThan500kb.avgFileSize * avgLessThan500kb.totalItems) + albumitem.Length)
                                                        / (avgLessThan500kb.totalItems + 1);
                if (source == 1)
                    avgLessThan500kb.avgFromCache = ((avgLessThan500kb.avgFromCache * avgLessThan500kb.totalItems) + delay)
                                                        / (avgLessThan500kb.totalItems + 1);
                else if (source == 0)
                    avgLessThan500kb.avgFromDisk = ((avgLessThan500kb.avgFromDisk * avgLessThan500kb.totalItems) + delay)
                                                        / (avgLessThan500kb.totalItems + 1);
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
                avgLessThan500kb.totalItems++;
            }
            //Update avgLessThan2mb
            else if (albumitem.Length <= 2 * 1024 * 1024)
            {
                avgLessThan2mb.avgFileSize = ((avgLessThan2mb.avgFileSize * avgLessThan2mb.totalItems) + albumitem.Length)
                                                        / (avgLessThan2mb.totalItems + 1);
                if (source == 1)
                    avgLessThan2mb.avgFromCache = ((avgLessThan2mb.avgFromCache * avgLessThan2mb.totalItems) + delay)
                                                        / (avgLessThan2mb.totalItems + 1);
                else if (source == 0)
                    avgLessThan2mb.avgFromDisk = ((avgLessThan2mb.avgFromDisk * avgLessThan2mb.totalItems) + delay)
                                                        / (avgLessThan2mb.totalItems + 1);
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
                avgLessThan2mb.totalItems++;
            }
            //Update avgMoreThan2mb
            else if (albumitem.Length > 2 * 1024 * 1024)
            {
                avgMoreThan2mb.avgFileSize = ((avgMoreThan2mb.avgFileSize * avgMoreThan2mb.totalItems) + albumitem.Length)
                                                        / (avgMoreThan2mb.totalItems + 1);
                if (source == 1)
                    avgMoreThan2mb.avgFromCache = ((avgMoreThan2mb.avgFromCache * avgMoreThan2mb.totalItems) + delay)
                                                        / (avgMoreThan2mb.totalItems + 1);
                else if (source == 0)
                    avgMoreThan2mb.avgFromDisk = ((avgMoreThan2mb.avgFromDisk * avgMoreThan2mb.totalItems) + delay)
                                                        / (avgMoreThan2mb.totalItems + 1);
                else
                {
                    Console.WriteLine("Error : useractions.xml should be configured in a way to read cache after disk read. Blog your xml files.");
                    return -1;
                }
                avgMoreThan2mb.totalItems++;
            }
            return 0;
        }

        public Album(string whoami, string root, string albumfile, DataCache mycache, string mycacheregion)
        {
            largestFile.header = "Largest File"; largestFile.totalItems = 1; largestFile.avgFileSize = 0;
            largestFile.avgFromCache = largestFile.avgFromDisk = 0;
            smallestFile.header = "Smallest File"; smallestFile.totalItems = 1; smallestFile.avgFileSize = long.MaxValue;
            smallestFile.avgFromCache = smallestFile.avgFromDisk = 0;
            avgLessThan50kb.header = "Size<50kb"; avgLessThan50kb.totalItems = 0; avgLessThan50kb.avgFileSize = 0;
            avgLessThan50kb.avgFromCache = avgLessThan50kb.avgFromDisk = 0;
            avgLessThan500kb.header = "50kb>Size<500kb"; avgLessThan500kb.totalItems = 0; avgLessThan500kb.avgFileSize = 0;
            avgLessThan500kb.avgFromCache = avgLessThan500kb.avgFromDisk = 0;
            avgLessThan2mb.header = "500kb>Size<2MB"; avgLessThan2mb.totalItems = 0; avgLessThan2mb.avgFileSize = 0;
            avgLessThan2mb.avgFromCache = avgLessThan2mb.avgFromDisk = 0;
            avgMoreThan2mb.header = "Size>2MB"; avgMoreThan2mb.totalItems = 0; avgMoreThan2mb.avgFileSize = 0;
            avgMoreThan2mb.avgFromCache = avgMoreThan2mb.avgFromDisk = 0;

            rootPath = new string(root.ToCharArray());
            userIdentity = whoami;
            myRegion = mycacheregion;
            myCache = mycache;
            XmlDocument albumfilexml = new XmlDocument();
            Console.WriteLine();
            Console.WriteLine("Loading albums.xml");
            try
            {
                albumfilexml.Load(root + albumfile);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : loading album file");
                Console.WriteLine(ex.Message);
                return;
            }
            Console.Write("Header ");
            Console.WriteLine(albumfilexml.DocumentElement.Name);

            Console.Write("Total no. of album ");
            albumConfig = albumfilexml.SelectNodes("/albumdatabase/user");
            Console.WriteLine(albumConfig.Count);

            foreach (XmlNode item in albumConfig)
            {
                Console.WriteLine("- item = " + item.InnerText);
            }
            Console.WriteLine("Completed loading album.xml");
            Console.WriteLine();
        }

        public double ReadPage(string currentuser, int currentalbum, int currentpage, int pageSize)
        {
            /*
             * Unit Tests
             * Album1 with 0, 1, PageSize - 1, PageSize, PageSize + 1. No of files
             */

            DirectoryInfo albumdir;
            FileInfo[] albumcontents;
            FileInfo albumitem;
            int firstitem, lastitem;
            bool found = false;
            string currentpath = new string(rootPath.ToCharArray());
            double delay = 0;

            // for use in summmary calculation and display
            int source = -1;

            // Search the album for the user
            foreach (XmlNode album in albumConfig)
            {
                if (album.FirstChild.InnerText.Equals(currentuser))
                {
                    found = true;
                    IEnumerator en = album.ChildNodes.GetEnumerator();
                    en.MoveNext(); en.MoveNext();
                    XmlNode pathnode = (XmlNode)en.Current;
                    currentpath += pathnode.InnerText;
                    break;
                }
            }
            if (!found)
                System.Console.WriteLine("Error : Path " + currentpath + "for " + currentuser + "not found");

            currentpath += ("\\album" + currentalbum.ToString());

            if (!System.IO.Directory.Exists(currentpath))
            {
                Console.WriteLine("Error : Directory path does not exist" + currentpath);
                return -1;
            }

            // Create a reference to the album directory.
            albumdir = new DirectoryInfo(currentpath);
            DirectoryInfo dInfo = new DirectoryInfo(currentpath);
            // Get a DirectorySecurity object that represents the 
            // current security settings.
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            // Add the FileSystemAccessRule to the security settings. 
            dSecurity.AddAccessRule(new FileSystemAccessRule(@userIdentity,
                                                            FileSystemRights.ReadData,
                                                            AccessControlType.Allow));
            // Set the new access settings.
            dInfo.SetAccessControl(dSecurity);

            // Create an array representing the files in the current directory.
            albumcontents = albumdir.GetFiles();
            albumSize = albumcontents.GetLength(0);

            if (albumcontents.GetLength(0) < (currentpage * pageSize))
            {
                Console.WriteLine("Error : Page limit exceeded Page No." + currentpage + " PageSize" + pageSize);
                return -1;
            }

            firstitem = (currentpage * pageSize > albumSize) ? albumSize : (currentpage * pageSize);
            lastitem = (firstitem + pageSize >= albumSize) ? albumSize - 1 : (firstitem + pageSize) - 1;

            Console.WriteLine("----->Reading Current Page " + currentpage + ".");
            Console.WriteLine("----->Albumsize " + albumcontents.GetLength(0) + " Firstitem " + firstitem + " Lastitem " + lastitem);

            // Read the stream of each file
            for (int itemtemp = firstitem; itemtemp <= lastitem; itemtemp++)
            {
                try
                {
                    StreamReader fileStream;
                    albumitem = albumcontents[itemtemp];
                    using (fileStream = new StreamReader(albumitem.Directory + "\\" + albumitem.Name))
                    {
                        string line = "";
                        int sizeread = 0;
                        string key = albumitem.Directory + "\\" + albumitem.Name;
                        // Read and display lines from the file until the end of 
                        // the file is reached.
                        Console.Write("-------->" + albumitem.Name);
                        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                        stopwatch.Reset();
                        stopwatch.Start();
                        DataCacheItemVersion dumref = null;
                        try
                        {
                            if ((line = (string)myCache.Get(key, out dumref, myRegion)) == null)
                            {
                                string currentline = "";
                                Console.Write("-DiskRead-"); source = 0;
                                do
                                {
                                    // Read the entire contents. 
                                    // Just a load test anyway, need 
                                    // not do anything with the stream
                                    currentline = fileStream.ReadToEnd();
                                    line += currentline;
                                    sizeread += currentline.Length;
                                } while (!string.IsNullOrEmpty(currentline));
                                myCache.Add(key, line, myRegion);
                            }
                            else //Get Passed
                            {
                                sizeread += line.Length;
                                Console.Write("-CacheRead-"); source = 1;
                            }
                        }
                        catch (DataCacheException ex)
                        {
                            Console.Write(ex.Message);
                            Console.Write("^^^^");
                        }

                        stopwatch.Stop();
                        string result = (sizeread == albumitem.Length) ? " done" : (" error " + (albumitem.Length - sizeread));
                        Console.WriteLine(result + " Delay (ticks) " + stopwatch.ElapsedTicks);
                        delay += stopwatch.ElapsedTicks;

                        UpdateSummary(albumitem, delay, source);
                    }
                }
                catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("Error : The file could not be read:");
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
            return delay;
        }
    }

    class FileServer
    {
        public static FileServer fileServer;
        public DataCacheFactory myCacheFactory;
        public DataCache cache;

        private void PrepareClient()
        {
            //-------------------------
            // Configure Cache Client 
            //-------------------------

            //Define Array for 1 Cache Host
            List<DataCacheServerEndpoint> servers = new List<DataCacheServerEndpoint>(1);

            //Specify Cache Host Details 
            //  Parameter 1 = host name
            //  Parameter 2 = cache port number
            servers.Add(new DataCacheServerEndpoint("localhost", 22233));

            //Create cache configuration
            DataCacheFactoryConfiguration configuration = new DataCacheFactoryConfiguration();

            //Set the cache host(s)
            configuration.Servers = servers;

            //Set default properties for local cache (local cache disabled)
            configuration.LocalCacheProperties = new DataCacheLocalCacheProperties();

            //Disable exception messages since this sample works on a cache aside
            DataCacheClientLogManager.ChangeLogLevel(System.Diagnostics.TraceLevel.Off);

            //Pass configuration settings to cacheFactory constructor
            myCacheFactory = new DataCacheFactory(configuration);

            //Get reference to default Cache
            cache = myCacheFactory.GetDefaultCache();
        }

        static void Main(string[] args)
        {
            string rootpath;
            if (args.Count() == 1)
            {
                rootpath = args[0];

            }
            else
            {
                Console.WriteLine("Syntax : DiskVersusCachePerformance <rootpath> <machinename>");
                Console.WriteLine("For eg : DiskVersusCachePerformance C:\\FileShare");
                Console.ReadKey();
                return;
            }
            fileServer = new FileServer();
            fileServer.PrepareClient();
            fileServer.Start(rootpath + "\\", WindowsIdentity.GetCurrent().Name);
            Console.ReadKey();

        }

        public void Start(string rootpath, string whoami)
        {
            string myregion = "MyRegion";

            bool createRegionStatus = cache.CreateRegion(myregion);
            if (createRegionStatus == true)
            {
                Console.WriteLine("CreateRegion {0} -PASS", myregion);
            }
            else
            {
                Console.WriteLine("CreateRegion {0} -FAIL, retrying after removing it.", myregion);
                cache.RemoveRegion(myregion); //Ignoring return value
                Console.WriteLine("CreateRegion {0} -PASS", myregion);

                cache.CreateRegion(myregion); //Ignoring return value

                Console.WriteLine("CreateRegion " + myregion + " -PASS");
            }

            UsersLoggedIn usersloggedin = new UsersLoggedIn();
            usersloggedin.LoadConfig(rootpath, "config.xml");

            UserAction useractions = new UserAction(usersloggedin);
            useractions.LoadConfig(rootpath, "useractions.xml", cache, myregion);
            useractions.Run(whoami);
        }
    }
}