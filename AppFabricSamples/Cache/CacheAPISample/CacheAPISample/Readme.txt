﻿To setup the developer environment on Visual Studio do the following
1) Install Distributed Cache Service and Admin. This will also GAC the required DLLs.
2) Ensure that the user Id which you are using to run this sample is allowed in the cluster
   as a client account. [Use the 'Grant-CacheAllowedClientAccount' command].
3) Start the cluster using 'Start-CacheCluster' command.
4) Build and Run this sample. The sample will automatically link to the 
    Distributed Cache DLLs (Microsoft.ApplicationServer.Caching.Client.dll and
    Microsoft.ApplicationServer.Caching.Core.dll) from GAC.

Note: For each subsequent run of this sample, please do 'Restart-CacheCluster' and wait for 10-15 seconds.