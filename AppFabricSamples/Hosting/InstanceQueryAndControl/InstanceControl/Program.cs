﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.ApplicationServer.StoreManagement.Control;

namespace InstanceQueryAndControl
{
    class Program
    {
        // THIS PATH NEEDS TO CHANGE FOR 64-BIT SYSTEMS.
        static string storeManagementDll = @"C:/Windows/System32/AppFabric/Microsoft.ApplicationServer.StoreManagement.dll";

        static InstanceControl instanceControl;

        static void Main(string[] args)
        {
            // Parse Guid. Default is "00000000-0000-0000-0000-000000000000".
            Guid instanceId = new Guid(0,0,0,0,0,0,0,0,0,0,0);
            if (args.Length > 0)
            {
                instanceId = new Guid(args[0]);
            }

            // Parse command. Default is "Suspend".
            CommandType cmd = CommandType.Suspend;
            if (args.Length > 1)
            {
                if (String.Compare(args[1], "suspend", true) == 0) { cmd = CommandType.Suspend; }
                else if (String.Compare(args[1], "resume", true) == 0) { cmd = CommandType.Resume; }
                else if (String.Compare(args[1], "cancel", true) == 0) { cmd = CommandType.Cancel; }
                else if (String.Compare(args[1], "terminate", true) == 0) { cmd = CommandType.Terminate; }
                else if (String.Compare(args[1], "delete", true) == 0) { cmd = CommandType.Delete; }
                else { Console.WriteLine("Command \"" + args[1] + "\" is not recognized"); return; }
            }

            // Build connection string for instance store.
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            //builder.DataSource = "localhost\\sqlexpress";
            builder.DataSource = "localhost";
            //builder.InitialCatalog = "ApplicationServerWorkflowInstanceStore";
            builder.InitialCatalog = "AppFabricPersistence";
            builder.IntegratedSecurity = true;
            //builder.UserID = "NAME OF USER WHO IS A MEMBER OF THE GROUP AS_Administrators";
            //builder.Password = "PASSWORD OF USER WHO IS A MEMBER OF THE GROUP AS_Administrators";
            
            Console.WriteLine("Enqueue " + cmd.ToString() + " command against instance " + instanceId.ToString() + " in database with connection string " + builder.ConnectionString);
        
            // Create a control provider for the instance store with the specified connection string.
            Assembly storeManagementAssembly = Assembly.LoadFrom(storeManagementDll);
            Type type = storeManagementAssembly.GetType("Microsoft.ApplicationServer.StoreManagement.Sql.Control.SqlInstanceControlProvider");
            InstanceControlProvider factory = Activator.CreateInstance(type) as InstanceControlProvider;
            NameValueCollection collection = new NameValueCollection();
            collection.Add("ConnectionString", builder.ConnectionString);
            factory.Initialize("storeA", collection);
            instanceControl = factory.CreateInstanceControl();

            // Define instance control command. SiteName, RelativeApplicationPath and VirtualPath
            // are required to determine the address of the instance control endpoint.
            InstanceCommand command = new InstanceCommand();
            command.CommandType = cmd;
            command.InstanceId = instanceId;
            command.ServiceIdentifier = new Dictionary<string, string>();
            command.ServiceIdentifier["SiteName"] = "Default Web Site";
            command.ServiceIdentifier["RelativeApplicationPath"] = "/TestWorkflow";
            command.ServiceIdentifier["VirtualPath"] = "/TestWorkflow/TestService.xamlx";

            // Issue command.
            try
            {
                instanceControl.CommandSend.BeginSend(command, TimeSpan.FromSeconds(60), new AsyncCallback(CommandSendCallback), instanceControl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }

            Console.WriteLine("\nPress Enter to exit ...\n");
            Console.ReadLine();
        }

        public static void CommandSendCallback(IAsyncResult result)
        {
            InstanceControl instanceControl = (InstanceControl)result.AsyncState;
            try
            {
                instanceControl.CommandSend.EndSend(result);
                if (result.IsCompleted == true)
                {
                    Console.WriteLine("Command successfully enqueued");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }
        }

    }
}
