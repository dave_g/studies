﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.ApplicationServer.StoreManagement.Query;

namespace InstanceQueryAndControl
{
    class Program
    {
        // THIS PATH NEEDS TO CHANGE FOR 64-BIT SYSTEMS.
        static string storeManagementDll = "C:/Windows/System32/AppFabric/Microsoft.ApplicationServer.StoreManagement.dll";

        static InstanceQuery instanceQuery;

        enum QueryAction { List, Count, Group};

        static void Main(string[] args)
        {
            // Parse parameter. Default is "List".
            QueryAction queryAction = QueryAction.List;
            if (args.Length > 1)
            {
                if (String.Compare(args[1], "list", true) == 0) { queryAction = QueryAction.List; }
                else if (String.Compare(args[1], "count", true) == 0) { queryAction = QueryAction.Count; }
                else if (String.Compare(args[1], "group", true) == 0) { queryAction = QueryAction.Group; }
                else { Console.WriteLine("Query action \"" + args[1] + "\" is not recognized"); return; }
            }

            // Build connection string for instance store.
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost\\sqlexpress";
            builder.InitialCatalog = "ApplicationServerWorkflowInstanceStore";
            builder.IntegratedSecurity = true;
            //builder.UserID = "NAME OF USER WHO IS A MEMBER OF THE GROUP AS_Administrators";
            //builder.Password = "PASSWORD OF USER WHO IS A MEMBER OF THE GROUP AS_Administrators";
            //Console.WriteLine(builder.ConnectionString);

            // Create a query provider for the instance store with the specified connection string.
            Assembly storeManagementAssembly = Assembly.LoadFrom(storeManagementDll);
            Type type = storeManagementAssembly.GetType("Microsoft.ApplicationServer.StoreManagement.Sql.Query.SqlInstanceQueryProvider");
            InstanceQueryProvider factory = Activator.CreateInstance(type) as InstanceQueryProvider;
            NameValueCollection collection = new NameValueCollection();
            collection.Add("ConnectionString", builder.ConnectionString);
            factory.Initialize("storeA", collection);
            instanceQuery = factory.CreateInstanceQuery();

            // Define query filter parameters.
            InstanceQueryArgs instanceQueryArgs = new InstanceQueryArgs();
            instanceQueryArgs.CreatedTimeFrom = null;
            instanceQueryArgs.CreatedTimeTo = null;
            instanceQueryArgs.ExceptionName = null;
            instanceQueryArgs.InstanceCondition = null;
            instanceQueryArgs.InstanceId = null;
            instanceQueryArgs.InstanceStatus = null;
            instanceQueryArgs.MachineName = null;
            instanceQueryArgs.ModifiedTimeFrom = null;
            instanceQueryArgs.ModifiedTimeTo = null;
            instanceQueryArgs.ServiceType = null;
            instanceQueryArgs.HostMetadata = new Dictionary<string, string>();
            instanceQueryArgs.HostMetadata.Add("SiteName", null);  // E.g., "Default Web Site".
            instanceQueryArgs.HostMetadata.Add("RelativeApplicationPath", null);  // E.g., /TestWorkflow".
            instanceQueryArgs.HostMetadata.Add("RelativeServicePath", null);  // E.g., /TestWorkflow/TestService.xamlx".
            instanceQueryArgs.HostMetadata.Add("VirtualPath", null);  // E.g., /TestWorkflow/TestService.xamlx".

            if (queryAction == QueryAction.List)
            {
                // Specify number and order of instanceInfo objects to be returned. 
                InstanceQueryExecuteArgs instanceQueryExecuteArgs = new InstanceQueryExecuteArgs();
                CopyQueryArgs(instanceQueryExecuteArgs, instanceQueryArgs);
                instanceQueryExecuteArgs.Count = 20; // Specify the maximum number of instances to be returned.
                instanceQueryExecuteArgs.OrderBy = Order.LastUpdatedTimeDescending; // Specify how results shall be sorted.
                ExecuteQuery(instanceQueryExecuteArgs);
            }

            if (queryAction == QueryAction.Count)
            {
                ExecuteCount(instanceQueryArgs);
            }

            if (queryAction == QueryAction.Group)
            {
                // Specify how instanceInfo objects should be grouped.
                InstanceQueryGroupArgs instanceQueryGroupArgs = new InstanceQueryGroupArgs();
                CopyQueryArgs(instanceQueryGroupArgs, instanceQueryArgs);
                List<GroupingMode> myGroupingMode = new List<GroupingMode>();
                myGroupingMode.Add(GroupingMode.Status);          // Group by Status,
                myGroupingMode.Add(GroupingMode.Condition);       //   then by Condition.
                instanceQueryGroupArgs.GroupBy = myGroupingMode;
                ExecuteGroupCount(instanceQueryGroupArgs);
            }
                
            Console.WriteLine("\nPress Enter to exit ...\n");
            Console.ReadLine();
        }


        public static void ExecuteQuery(InstanceQueryExecuteArgs instanceQueryExecuteArgs)
        {
            try
            {
                IAsyncResult asyncResult = instanceQuery.BeginExecuteQuery(instanceQueryExecuteArgs, TimeSpan.FromSeconds(60), new AsyncCallback(ExecuteQueryCallback), null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public static void ExecuteCount(InstanceQueryArgs instanceQueryArgs)
        {
            try
            {
                IAsyncResult asyncResult = instanceQuery.BeginExecuteCount(instanceQueryArgs, TimeSpan.FromSeconds(60), new AsyncCallback(ExecuteCountCallback), null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public static void ExecuteGroupCount(InstanceQueryGroupArgs instanceQueryGroupArgs)
        {
            try
            {
                IAsyncResult asyncResult = instanceQuery.BeginExecuteGroupCount(instanceQueryGroupArgs, TimeSpan.FromSeconds(60), new AsyncCallback(ExecuteGroupCountCallback), null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public static void ExecuteQueryCallback(IAsyncResult result)
        {
            // Print returned instance to console.
            foreach (InstanceInfo info in instanceQuery.EndExecuteQuery(result))
            {
                Console.WriteLine("Instance ID\t\t: " + info.InstanceId.ToString());
                Console.WriteLine("Status\t\t\t: " + info.InstanceStatus.ToString());
                Console.WriteLine("Condition\t\t: " + info.InstanceCondition.ToString());
                Console.WriteLine("ActiveBookmarks\t\t: " + info.ActiveBookmarks.ToString());
                Console.WriteLine("CreationTime\t\t: " + info.CreationTime.ToString());
                Console.WriteLine("LastUpdateTime\t\t: " + info.LastUpdateTime.ToString());
                Console.WriteLine("LastUpdatedBy\t\t: " + info.LastUpdatedBy.ToString());
                Console.WriteLine("StoreName\t\t: " + info.StoreName.ToString());
                if (info.ExceptionName != null)
                {
                    Console.WriteLine("ExceptionName\t\t: " + info.ExceptionName.ToString());
                    Console.WriteLine("ExceptionMessage\t: " + info.ExceptionMessage.ToString());
                }
                if (info.CommandInfo != null)
                {
                    Console.WriteLine("Command: Type\t\t: " + info.CommandInfo.CommandType.ToString());
                    Console.WriteLine("Command: Status\t\t: " + info.CommandInfo.CommandStatus.ToString());
                    Console.WriteLine("Command: Execution time\t: " + info.CommandInfo.ExecutionTime.ToString());
                    Console.WriteLine("Command: Num of attempts: " + info.CommandInfo.NumberOfExecutionsAttempted.ToString());
                    if (info.CommandInfo.ExecutionOnMachine != null)
                    {
                        Console.WriteLine("Command: Executed on\t: " + info.CommandInfo.ExecutionOnMachine.ToString());
                    }
                    if (info.CommandInfo.ErrorMessage != null)
                    {
                        Console.WriteLine("Command: Error message\t: " + info.CommandInfo.ErrorMessage.ToString());
                    }
                }
                Console.WriteLine("Host Type\t\t: " + info.HostInfo.HostType.ToString());
                foreach (KeyValuePair<string,string> metadata in info.HostInfo.HostMetadata)
                {
                    Console.WriteLine("Hostinfo\t\t: " + metadata.Key.ToString() + " = " + metadata.Value.ToString());
                }
                foreach (Microsoft.ApplicationServer.StoreManagement.Query.PropertyInfo propertyInfo in info.PrimitiveProperties)
                {
                    Console.WriteLine("PrimitiveProperty\t: " + propertyInfo.PropertyName.ToString() + " = " + propertyInfo.PropertyValue.ToString());
                }
                if (info.UserData != null)
                {
                    Console.WriteLine("UserData\t\t: " + info.UserData.ToString());
                }
                Console.WriteLine();
            }

            Console.WriteLine("Press Enter to exit ...\n");
            Console.ReadLine();
        }


        public static void ExecuteCountCallback(IAsyncResult result)
        {
            // Print returned number of instance to console.
            int count = instanceQuery.EndExecuteCount(result);
            Console.WriteLine("Number of instances: " + count);
        }


        public static void ExecuteGroupCountCallback(IAsyncResult result)
        {
            PrintGroupingResult(instanceQuery.EndExecuteGroupCount(result),0);
        }


        public static void PrintGroupingResult(IEnumerable<GroupingResult> groupingResult, int indent)
        {
            // Print grouped instance to console.
            string indentString = "";
            for( int i=0; i<indent; i++ ) { indentString += "   "; }
            foreach (GroupingResult g in groupingResult)
            {
                Console.WriteLine(indentString + g.Key + " : " + g.Count + " instance(s)");
                PrintGroupingResult(g.Groups,indent+1);
            }
        }


        static void CopyQueryArgs(InstanceQueryArgs to, InstanceQueryArgs from)
        {
            to.CreatedTimeFrom = from.CreatedTimeFrom;
            to.CreatedTimeTo = from.CreatedTimeTo;
            to.ExceptionName = from.ExceptionName;
            to.InstanceCondition = from.InstanceCondition;
            to.InstanceId = from.InstanceId;
            to.InstanceStatus = from.InstanceStatus;
            to.MachineName = from.MachineName;
            to.ModifiedTimeFrom = from.ModifiedTimeFrom;
            to.ModifiedTimeTo = from.ModifiedTimeTo;
            to.ServiceType = from.ServiceType;

            if (from.HostMetadata == null)
            {
                to.HostMetadata = null;
            }
            else
            {
                to.HostMetadata = new Dictionary<string, string>();
                foreach (KeyValuePair<string, string> metadata in from.HostMetadata)
                {
                    to.HostMetadata.Add(metadata.Key, metadata.Value);
                }
            }
        }
    }
}
