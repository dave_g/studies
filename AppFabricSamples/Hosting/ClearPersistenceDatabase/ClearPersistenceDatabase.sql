------------------------------------------------------------------
-- Copyright (c) Microsoft Corporation.  All rights reserved.
------------------------------------------------------------------

-- Warning: Running this T-SQL File will permanently erase all Workflow Data 
--          stored in the persistence database. This file is intended to be
--			used only in a private development environment.

truncate table [Microsoft.ApplicationServer.DurableInstancing].[InstanceControlCommandsTable]
go

truncate table [Microsoft.ApplicationServer.DurableInstancing].[AbandonedInstanceControlCommandsTable]
go

truncate table [Microsoft.ApplicationServer.DurableInstancing].[AbandonedInstanceControlCommandsCleanupTable]
go

truncate table [System.Activities.DurableInstancing].[InstancesTable]
go

truncate table [System.Activities.DurableInstancing].[RunnableInstancesTable]
go

truncate table [System.Activities.DurableInstancing].[KeysTable]
go

truncate table [System.Activities.DurableInstancing].[LockOwnersTable]
go

truncate table [System.Activities.DurableInstancing].[InstanceMetadataChangesTable]
go

truncate table [System.Activities.DurableInstancing].[ServiceDeploymentsTable]
go

truncate table [System.Activities.DurableInstancing].[InstancePromotedPropertiesTable]
go
