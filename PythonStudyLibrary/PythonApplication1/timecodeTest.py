import sys
from timecode import Timecode

def main():
  print ('arg -> ' , sys.argv, sys.argv[1])
  print(sys.version)
  print(sys.winver)

def test_instance_creation():
  tc_60_start = Timecode('59.94', '01:00:00:00')
  tc_60_start.frame_number == 0
  tc_60_end = Timecode('59.94', '01:01:00:00')
  tc_60_end.frame_number == 0
  
  tc_30_start = Timecode('29.97', '01:00:00:00')
  tc_30_start.frame_number == 0
  tc_30_end = Timecode('29.97', '01:01:00:00')
  tc_30_end.frame_number == 0
  
  tc_60_duration = tc_60_start + tc_60_end
  tc_30_duration = tc_30_start + tc_30_end
  #print( + tc_60_start.__repr__())
  #print(tc_60_start.frames)
  print("tc 60 duration : " + tc_60_duration.__repr__())
  print("tc 30 duration : " + tc_30_duration.__repr__())

if __name__ == '__main__':
  test_instance_creation()

def get_duration(start, end):
  tc_60_start = Timecode('59.94', start)
  tc_60_start.frame_number == 0
  tc_60_end = Timecode('59.94', end)
  tc_60_end.frame_number == 0
  return tc_60_start + tc_60_end