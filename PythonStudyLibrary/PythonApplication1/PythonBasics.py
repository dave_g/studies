
import sys
from timecode import Timecode

def main():
  print ('arg -> ' , sys.argv, sys.argv[1])
  print(sys.version)
  print(sys.winver)

def test_instance_creation(self):
  timeobj = Timecode('24', '01:00:00:00')
  print(timeobj.frames)

def printFunc():
  print("##### BEGIN PRINT FUNCTION")
  if 1==2 or 2 == 3:
    print('one is two')
  else:
    print('one is not two'.upper())    
    print('\'is\' is at %d' %('one is not two'.find('is')))
    print('e is at %d' %('one is not two'.find('e')))   
    str = 'hello'
    print(str[2:4])
    print(str[-4:-2])
    print(str[:-3])
    print(str[-3:])
    print("##### END PRINT FUNCTION")

def loopFunc():
  print("##### BEGIN LOOP FUNCTION")
  list = ['fee','fi',2,'foo']
  x = 3  
  if x in list:  
    for l in list: print(l)
  
  list.append(x)  

  if x in list:  
    for l in list: print(l)

  list.remove('foo')
  print("##### END LOOP FUNCTION")

def LastCharInString(s):
  return s[-1]

def sortFunc():
  print("##### BEGIN SORT FUNCTION")
  list = [4,2,8,9,5,6,87]
  sortedList = sorted(list)
  for item in sortedList: print(item)

  sortedList = sorted(list,reverse = True)
  for item in sortedList: print(item)

  list = ['aaaa','bbb','cc','d']
  sortedList = sorted(list,key = len)
  for item in sortedList: print(item)

  sortedList = sorted(list,key = len,reverse = True)
  for item in sortedList: print(item)

#custom sorting
  sortedList = sorted(list,key = LastCharInString)
  for item in sortedList: print(item)
  
#sorted tuples - sorts on first item then second
  tupleList = [(1,'a'),(2,'a'),(3,'z'),(3,'a')]
  sortedList = sorted(tupleList)
  for item in sortedList: print(item)
  
print("##### END LOOP FUNCTION")

def listFunc():
  list = ['aaaa','bbb','cc','d']
  joined =  ":".join(list)
  print(joined)
  newlist = joined.split(":")
  for item in newlist:print(item)

def dictionaryFunc():
  dict = {}
  dict['a'] = 124
  dict['b'] = 'foo'
  if 'a' in dict:  
    print(dict['a'])
    print(dict.keys())
    print(dict.values())
    #for k in dict.keys():
    print(dict.items())

def fileFunc():
  filename = 'c:\kix32.log'
  f = open(filename, 'rU')
  #more memory efficent  
  #for line in f: print(line)
  #memory same as file size
  #lines = f.readlines()
  #for l in lines: print(l)
  #read as one big string
  str = f.read()
  print(str)
  f.close()

def wordCount():
  filename = 'c:\kix32.log'
  file = open(filename, 'rU')
  words = {}
  ctr = 0
  for line in file:
    linewords = line.split()
    for word in linewords:
      if word in words: 
        words[word] = words.get(word) + 1 
      else:
        words[word] = 1   
  for word in sorted(words):
    print(word, words[word])  
#print(words.items())
  #print(words.keys())
  #print(words.values())
#name will not be main if loaded by import
if __name__ == '__main__':
  main()
  print(__name__)
  printFunc()
  loopFunc()
  sortFunc()
  listFunc()
  dictionaryFunc()
  fileFunc()
  wordCount()
 