﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {


            try
            {
                //var input = Console.ReadLine();
                //RunInlinePython("print('From Python: " + input + "')");
                //RunPythonFile("timecodeTest.py");
                //RunPythonScope();
                //RunPythonModuleScope("timecodeTest.py");
                ProductionTests("timecodeTest.py");
                //Test720PConversion("timecodeTest.py");
                var scope = GetPythonScope();
                var clipSom = convertFrom29_97To59_94(scope, "00:00:00:00");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void RunPythonScope()
        {
            // '01:00:00:00'
            //'01:01:00:00'

            var pySrc =
                @"def CalcAdd(Numb1, Numb2):
    return Numb1 + Numb2";

            // host python and execute script
            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();
            engine.Execute(pySrc, scope);

            // get function and dynamically invoke
            var calcAdd = scope.GetVariable("CalcAdd");
            var result = calcAdd(34, 8); // returns 42 (Int32)

            // get function with a strongly typed signature
            var calcAddTyped = scope.GetVariable<Func<decimal, decimal, decimal>>("CalcAdd");
            var resultTyped = calcAddTyped(5, 7); // returns 12m
        }

        private static void RunPythonModuleScope(string module)
        {
            //// host python and execute script
            //var engine = IronPython.Hosting.Python.CreateEngine();
            //var scope = engine.CreateScope();
            //var source = engine.CreateScriptSourceFromFile(module);

            //// get function and dynamically invoke
            //var getDuration = scope.GetVariable("get_duration");
            //var result = getDuration("01:00:00:00", "01:01:00:00");

            /*The ScriptSource represents source code and the ScriptScope is a namespace.We use them both to execute Python code - executing the code(the ScriptSource) in a namespace (a ScriptScope):*/

            //SourceCodeKind st = SourceCodeKind.Statements;
            //string source = "print 'Hello World'";
            var eng = IronPython.Hosting.Python.CreateEngine();
            //var script = eng.CreateScriptSourceFromString(source, st);
            //var scope = eng.CreateScope();
            //script.Execute(scope);

            ////To IronPython the ScriptScope is a module - a namespace that maps names to objects.
            //int value = 3;
            //scope.SetVariable("name", value);

            //script.Execute(scope);

            //int result = scope.GetVariable<int>("name");

            ScriptSource script;
            script = eng.CreateScriptSourceFromFile(module);
            CompiledCode code = script.Compile();
            ScriptScope scope = eng.CreateScope();
            code.Execute(scope);
            var add_timecode = scope.GetVariable("add_timecode");
            var getFrames = scope.GetVariable("get_frames");
            var getTimecode = scope.GetVariable("get_timecode_from_frames");

            var result = add_timecode("00:00:00:00", "01:32:28:00");
            Console.WriteLine(result.ToString());
            result = add_timecode("00:00:00:00", "01:32:27:23");
            Console.WriteLine(result.ToString());

            var frames1 = getFrames("00:00:00:00", "01:32:28:00", "59.94");
            Console.WriteLine(frames1);
            var frames2 = getFrames("00:00:00:00", "01:32:28:00", "29.97");
            Console.WriteLine(frames2);

            var f1 = int.Parse(frames1);
            var f2 = int.Parse(frames2);
            var timeCode = getTimecode(f1, "59.94");
            Console.WriteLine(timeCode);
            timeCode = getTimecode(f2, "29.97");
            Console.WriteLine(timeCode);

           

        }

        private static string convertFrom29_97To59_94(ScriptScope scope, string hhmmssff)
        {
            //double the frame
            var durations = hhmmssff.Replace(';', ':').Split(':');
            if (durations.Length < 4) throw new Exception($"Timecode mal formed. {hhmmssff}");
            //double the frames for 59.94
            var frames = 0;
            int.TryParse(durations[3], out frames);
            frames = frames * 2;
            var formatedFrames = frames.ToString("00");
            hhmmssff = $"{durations[0]}:{durations[1]}:{durations[2]};{formatedFrames}";

            var getTimecode = scope.GetVariable("get_timecode_from_hhmmssff");
            var timeCode = getTimecode(hhmmssff, "59.94", 0);

            durations = timeCode.Split(':');
            hhmmssff = $"{durations[0]}:{durations[1]}:{durations[2]};{durations[3]}";
            return hhmmssff;
        }

        private static ScriptScope GetPythonScope()
        {
            var eng = IronPython.Hosting.Python.CreateEngine();
            ScriptSource script;
            script = eng.CreateScriptSourceFromFile("timecodeTest.py");
            CompiledCode code = script.Compile();
            ScriptScope scope = eng.CreateScope();
            code.Execute(scope);
            return scope;
        }

        private static void Test720PConversion(string module)
        {
            var eng = IronPython.Hosting.Python.CreateEngine();
            ScriptSource script;
            script = eng.CreateScriptSourceFromFile(module);
            CompiledCode code = script.Compile();
            ScriptScope scope = eng.CreateScope();
            code.Execute(scope);

            var tc = "00:01:00;29";
            var hhmmssff = tc.Replace(';',':').Split(':');
            if (hhmmssff.Length < 4) return; //throw error
            //double the frames for 59.94
            var frames = 0;
            int.TryParse(hhmmssff[3],out frames);
            frames = frames*2;
            var formatedFrames = frames.ToString("00");
            tc = $"{hhmmssff[0]}:{hhmmssff[1]}:{hhmmssff[2]};{formatedFrames}";

            var getTimecodeFromHHMMSSFF = scope.GetVariable("get_timecode_from_hhmmssff_zero_start_frame");
            var timeCode = getTimecodeFromHHMMSSFF("00:00:00:00", "59.94",0);
            Console.WriteLine(timeCode);
            timeCode = getTimecodeFromHHMMSSFF(tc, "59.94",0);
            Console.WriteLine(timeCode);
        }
        private static void ProductionTests(string module)
        {
            var eng = IronPython.Hosting.Python.CreateEngine();
            ScriptSource script;
            script = eng.CreateScriptSourceFromFile(module);
            CompiledCode code = script.Compile();
            ScriptScope scope = eng.CreateScope();
            code.Execute(scope);

            var add_timecode = scope.GetVariable("add_timecode");
            var addFramesToTimecode = scope.GetVariable("add_frames_to_timecode");
            var addTimecode29 = scope.GetVariable("add_timecode_29");
            var getFrames = scope.GetVariable("get_frames");
            var getTimecodeFromFrames = scope.GetVariable("get_timecode_from_frames");
            
            //This is from a 720P file
            //3h 33mn 23s 90ms - reported by mediainfo gui
            //383718 frames - s
            //These numbers match the 29.97 frame rate parse below, do media info is reporting the number of frames as 29.97
            var timeCode = getTimecodeFromFrames(383718, "59.94"); // outputs "01:46:41:41"
            Console.WriteLine(timeCode);
            timeCode = getTimecodeFromFrames(383718, "29.97"); //outputs "03:33:23:11"
            Console.WriteLine(timeCode);

            //03:33:10:18 duration reported by mediainfo api
            //clip eom - calculate by macafur
            //03:33:23;11

            //segment 1 start 00:22:34:01 end 00:33:10:01 duration 00:10:36:00
            //PrintTimeCodeComparisons("00:22:34:01");
            //PrintTimeCodeComparisons("00:33:10:01");

            PrintTimeCodeComparisons("00:04:40:00");
            PrintTimeCodeComparisons("00:04:39.700");

            //test invalid
            //Console.WriteLine(addFramesToTimecode(1801,"00:00:00:00", "29.97"));
            //Console.WriteLine(addFramesToTimecode(3601, "00:00:00:00", "59.94"));

            //PrintTimeCodeComparisons("00:47:10:00");

            //Console.WriteLine(addFramesToTimecode(60, "00:00:00:00", "59.94"));
        }


        private static void PrintTimeCodeComparisons(string tc)
        {
            var eng = IronPython.Hosting.Python.CreateEngine();
            ScriptSource script;
            script = eng.CreateScriptSourceFromFile("timecodeTest.py");
            CompiledCode code = script.Compile();
            ScriptScope scope = eng.CreateScope();
            code.Execute(scope);

            var add_timecode = scope.GetVariable("add_timecode");
            var getFrames = scope.GetVariable("get_frames");
            var getTimecodeFromFrames = scope.GetVariable("get_timecode_from_frames");
            var getTimecodeFromHHMMSSFF = scope.GetVariable("get_timecode_from_hhmmssff");

            var timeCode = getTimecodeFromHHMMSSFF(tc, "59.94",0);
            Console.WriteLine(timeCode);
            timeCode = getTimecodeFromHHMMSSFF(tc, "29.97",0);
            Console.WriteLine(timeCode);
            timeCode = getTimecodeFromHHMMSSFF(tc, "ms", 0);
            Console.WriteLine(timeCode);
        }
        private static void RunPythonFile(string fileToRun)
        {
            var py = Python.CreateEngine();
            py.ExecuteFile(fileToRun);
        }

        public static void RunInlinePython(string pythonCommand)
        {
            var py = Python.CreateEngine();
            py.Execute(pythonCommand);
        }
    }
}
