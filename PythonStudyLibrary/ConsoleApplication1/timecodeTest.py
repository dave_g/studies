import sys
#sys.path.append(r"C:\Program Files (x86)\IronPython 2.7\Lib")
from timecode import Timecode

#print('Look at this python code go!')

def main():
  #print ('arg -> ' , sys.argv, sys.argv[1])
  print(sys.version)
  print(sys.winver)

def test_instance_creation():
  tc_60_start = Timecode('59.94', '01:00:00:00')
  tc_60_start.frame_number == 0
  tc_60_end = Timecode('59.94', '01:01:00:00')
  tc_60_end.frame_number == 0
  
  tc_30_start = Timecode('29.97', '01:00:00:00')
  tc_30_start.frame_number == 0
  tc_30_end = Timecode('29.97', '01:01:00:00')
  tc_30_end.frame_number == 0
  
  tc_60_duration = tc_60_start + tc_60_end
  tc_30_duration = tc_30_start + tc_30_end
  #print( + tc_60_start.__repr__())
  #print(tc_60_start.frames)
  print("tc 60 duration : " + tc_60_duration.__repr__())
  print("tc 30 duration : " + tc_30_duration.__repr__())

#main()
#test_instance_creation()

#if __name__ == '__main__':
#  main()
#  #test_instance_creation()

def add_timecode(start, end):
  tc_60_start = Timecode('59.94', start)
  tc_60_start.frame_number == 1
  tc_60_end = Timecode('59.94', end)
  tc_60_end.frame_number == 1
  return_var = tc_60_start + tc_60_end
  return return_var.__repr__()

def add_timecode_29(start, end):
  tc_60_start = Timecode('29.97', start)
  tc_60_start.frame_number == 1
  tc_60_end = Timecode('29.97', end)
  tc_60_end.frame_number == 1
  return_var = tc_60_start + tc_60_end
  return return_var.__repr__()

def get_frames(start, end, rate):
  tc_60_start = Timecode(rate, start)
  tc_60_start.frame_number == 0
  tc_60_end = Timecode(rate, end)
  tc_60_end.frame_number == 0
  return_var = tc_60_start + tc_60_end
  foo = return_var.frames
  return foo.__repr__()

def get_timecode_from_frames(frames, rate):
  tc = Timecode(rate, None, None, frames)  
  return tc.__repr__()

def get_timecode_from_hhmmssff_zero_start_frame(timecode, rate, start_frame):
  tc = Timecode(rate, timecode, None, None)
  tc.frame_number == start_frame
  return tc.__repr__()

def get_timecode_from_hhmmssff(timecode, rate, start_frame):
  tc = Timecode(rate, timecode, None, None)
  tc.frame_number == start_frame  
  return tc.__repr__()

def add_frames_to_timecode(frames, tc, rate):
  tc = Timecode(rate, tc, None, None)
  tc.add_frames(frames)
  return tc.__repr__()

def parse_timecode(tc):
  ptc = Timecode.parse_timecode(tc);
  return ptc