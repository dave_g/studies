﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EngineParameters;

namespace EngineTester
{
    class Program
    {
        static void Main(string[] args)
        {
            PowerPlant _powerPlant = new PowerPlant();
            //example engine
            _powerPlant.Pistons = 6;
            _powerPlant.Rod = 16.6;
            _powerPlant.DiplacemnetTotal = 3;


            //calculate bore/stroke from total displacement volume assuming a square engine.
            double x = EngineFormulas.CalculateSquareBoreAndStrokeFromTotalDisplacement(3,6);
            Console.WriteLine("CalculateSquareBoreAndStrokeFromTotalDisplacement");
            Console.WriteLine(x);
            
            Console.WriteLine("CalculateDisplacement");
            Console.WriteLine(EngineFormulas.CalculateDisplacement(8.6,8.6));
            Console.WriteLine(EngineFormulas.CalculateDisplacement(6,8.6, 8.6));

            Console.WriteLine(EngineFormulas.CalculateAveragePistonSpeedPerSec(.086,3600));

            while (Console.In.ReadLine() != " ")
            {
                
            }

        }
    }
}
