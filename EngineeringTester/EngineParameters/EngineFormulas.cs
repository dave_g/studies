﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EngineParameters
{
    public static class EngineFormulas
    {
        /// <summary>
        /// S=2a
        /// </summary>
        /// <param name="crankRadius"></param>
        /// <returns></returns>
        public static float CalculateStroke(float crankRadius)
        {
            return crankRadius * 2;
        }

        public static float CalculateCrankOffset(float stroke)
        {
            return stroke / 2;
        }

        public static float CalculateAveragePistonSpeedPerSec(float stroke, int engineSpeedRPM)
        {
            return 2 * stroke * (engineSpeedRPM / 60);
        }

        public static double CalculateDisplacement(double bore, double stroke)
        {
            double quarterPie = Math.PI/4;
            double boreSquared = Math.Pow(bore, 2);
            double returnValue = quarterPie*(boreSquared*stroke);
            return returnValue;
        }

        public static double CalculateDisplacement(int pistons, double bore, double stroke)
        {
            double quarterPie = Math.PI / 4;
            double boreSquared = Math.Pow(bore, 2);
            double returnValue = pistons * quarterPie * boreSquared * stroke;
            return returnValue;
        }

        /// <summary>
        /// re = (Vc + Vd)/Vc
        /// </summary>
        /// <param name="volumeDisplacment"></param>
        /// <param name="volumeClearance"></param>
        /// <returns></returns>
        public static double CalculateCompressionRatio(double volumeDisplacment, double volumeClearance)
        {
            double totalVolume = volumeClearance + volumeDisplacment;
            return totalVolume/volumeClearance;
        }
        
        /// <summary>
        /// s = (a)(cos(Angle)) + Sqr(r2 + (a2)(sin(angle)2))
        /// </summary>
        /// <param name="crankOffset"></param>
        /// <param name="rodLength"></param>
        /// <param name="crankAngleDegrees"></param>
        /// <returns></returns>
        public static double CalculateCrankAxisToWristPinDistance(float crankOffset, float rodLength, double crankAngleDegrees)
        {
            double crankAngleRadians = MathFunctions.ConvertDegreesToRadians(crankAngleDegrees);
            float a = crankOffset;
            float r = rodLength;

            double acosAngle = a*Math.Cos(crankAngleRadians);
            double a2 = Math.Pow(a, 2);
            double sin2Angle = Math.Pow(Math.Sin(crankAngleRadians), 2);
            double a2sin2Angle = a2*sin2Angle;
            double r2 = Math.Pow(r, 2);
            double squareRootOfR2Minusa2sin2Angle =  Math.Sqrt(r2) - Math.Sqrt(a2sin2Angle);





            return acosAngle + squareRootOfR2Minusa2sin2Angle;
        }

        public static double CalculateCylinderVolumeAtCrankAngle(double volumneClearance, double bore, double rod, double crankOffset, double pistonPosition)
        {

            double B2 = Math.Pow(bore, 2);
            double pB2_4 = (Math.PI*B2)/4;
            double RPlusAMinusS = rod + crankOffset - pistonPosition;

            return 0;
        }

    }
}
