﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EngineParameters
{
    public partial class EngineParameters : UserControl
    {
        public delegate void EventHandler(Object sender, EventArgs e);
        public event EventHandler CrankOffsetChanged;
        public event EventHandler BoreChanged;
        public event EventHandler StrokeChanged;

        public void InvokeBoreChanged(EventArgs e)
        {
            EventHandler handler = BoreChanged;
            if (handler != null) handler(this, e);
        }

        public void InvokeStrokeChanged(EventArgs e)
        {
            EventHandler handler = StrokeChanged;
            if (handler != null) handler(this, e);
        }

        public void InvokeCrankOffsetChanged(EventArgs e)
        {
            EventHandler handler = CrankOffsetChanged;
            if (handler != null) handler(this, e);
        }

        public EngineParameters()
        {
            InitializeComponent();
        }

        public string Bore
        {
            get { return textBoxBore.Text; }
            set
            {          
                textBoxBore.Text = value;
            }
        }

        public string Stroke
        {
            get { return textBoxStroke.Text; }
            set { textBoxStroke.Text = value; }
        }

        public string CrankOffset
        {
            get { return textBoxCrankOffset.Text; }
            set { textBoxCrankOffset.Text = value; }
        }

        private void textBoxCrankOffset_TextChanged(object sender, EventArgs e)
        {
            //InvokeCrankOffsetChanged(e);
        }

        private void textBoxStroke_TextChanged(object sender, EventArgs e)
        {
            //InvokeStrokeChanged(e);
        }

        private void textBoxBore_TextChanged(object sender, EventArgs e)
        {
            //InvokeBoreChanged(e);
        }
    }
}
