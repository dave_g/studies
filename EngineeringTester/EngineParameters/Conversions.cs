﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EngineParameters
{
    public static class Conversions
    {
        public static double InchesToCentimeters(double inches)
        {
            return inches * 2.54;
        }
        
        /// <summary>
        /// 1 cm = 0.39370078740157 inches
        ///     10 millimeters
        /// 0.01 metre, which can be represented by 1.00 E-2 m (1 metre is equal to 100 centimetres)
        /// about 0.393700787401575 inch (1 inch is equal to 2.54 centimetres) [2]

        /// </summary>
        /// <param name="centimeters"></param>
        /// <returns></returns>
        public static double CentimetersToInches(double centimeters)
        {
            return centimeters / 2.54;
        }

        public static double CentimetersToMillimeters(double centimeters)
        {
            return centimeters*10;
        }

        public static double MillimetersToCentimeters(double millimeters)
        {
            return millimeters / 10;
        }

        ///1 meter = 3.28083989501312 feet
        /// 
        public static double MetersToFeet(double meters)
        {
            return meters*3.28083989501312;
        }

        public static double FeetToMeters(double feet)
        {
            return feet / 3.28083989501312;
        }
    }
}
