﻿using System;

namespace EngineParameters
{
    public static class MathFunctions
    {
        public static string Demo()
        {
            String output = "";
            output +=
                "This example of trigonometric " +
                "Math.Sin( double ) and Math.Cos( double )\n" +
                "generates the following output.\n" + "\n";
            output +=
                "Convert selected values for X to radians \n" +
                "and evaluate these trigonometric identities:" + "\n";
            output += "   sin^2(X) + cos^2(X) == 1\n" +
                               "   sin(2 * X) == 2 * sin(X) * cos(X)" + "\n";
            output += "   cos(2 * X) == cos^2(X) - sin^2(X)" + "\n";

            output += UseSineCosine( 15.0);
            output += UseSineCosine( 30.0);
            output += UseSineCosine( 45.0);

            output +=
                "\nConvert selected values for X and Y to radians \n" +
                "and evaluate these trigonometric identities:" + "\n";
            output += "   sin(X + Y) == sin(X) * cos(Y) + cos(X) * sin(Y)" + "\n";
            output += "   cos(X + Y) == cos(X) * cos(Y) - sin(X) * sin(Y)" + "\n";

            output += UseTwoAngles( 15.0, 30.0);
            output += UseTwoAngles( 30.0, 45.0);

            return output;

        }

        // Evaluate trigonometric identities with a given angle.
        private static string UseSineCosine(double degrees)
        {
            string outputBlock = "";
            double angle = Math.PI * degrees / 180.0;
            double sinAngle = Math.Sin(angle);
            double cosAngle = Math.Cos(angle);

            // Evaluate sin^2(X) + cos^2(X) == 1.
            outputBlock += String.Format(
                "\n                           Math.Sin({0} deg) == {1:E16}\n" +
                "                           Math.Cos({0} deg) == {2:E16}",
                degrees, Math.Sin(angle), Math.Cos(angle)) + "\n";
            outputBlock += String.Format(
                "(Math.Sin({0} deg))^2 + (Math.Cos({0} deg))^2 == {1:E16}",
                degrees, sinAngle * sinAngle + cosAngle * cosAngle) + "\n";

            // Evaluate sin(2 * X) == 2 * sin(X) * cos(X).
            outputBlock += String.Format(
                "                           Math.Sin({0} deg) == {1:E16}",
                2.0 * degrees, Math.Sin(2.0 * angle)) + "\n";
            outputBlock += String.Format(
                "    2 * Math.Sin({0} deg) * Math.Cos({0} deg) == {1:E16}",
                degrees, 2.0 * sinAngle * cosAngle) + "\n";

            // Evaluate cos(2 * X) == cos^2(X) - sin^2(X).
            outputBlock += String.Format(
                "                           Math.Cos({0} deg) == {1:E16}",
                2.0 * degrees, Math.Cos(2.0 * angle)) + "\n";
            outputBlock += String.Format(
                "(Math.Cos({0} deg))^2 - (Math.Sin({0} deg))^2 == {1:E16}",
                degrees, cosAngle * cosAngle - sinAngle * sinAngle) + "\n";

            return outputBlock;
        }

        // Evaluate trigonometric identities that are functions of two angles.
        private static string UseTwoAngles(double degreesX, double degreesY)
        {
            string outputBlock = "";
            double angleX = Math.PI * degreesX / 180.0;
            double angleY = Math.PI * degreesY / 180.0;

            // Evaluate sin(X + Y) == sin(X) * cos(Y) + cos(X) * sin(Y).
            outputBlock += String.Format(
                "\n        Math.Sin({0} deg) * Math.Cos({1} deg) +\n" +
                "        Math.Cos({0} deg) * Math.Sin({1} deg) == {2:E16}",
                degreesX, degreesY, Math.Sin(angleX) * Math.Cos(angleY) +
                Math.Cos(angleX) * Math.Sin(angleY)) + "\n";
            outputBlock += String.Format(
                "                           Math.Sin({0} deg) == {1:E16}",
                degreesX + degreesY, Math.Sin(angleX + angleY)) + "\n";

            // Evaluate cos(X + Y) == cos(X) * cos(Y) - sin(X) * sin(Y).
            outputBlock += String.Format(
                "        Math.Cos({0} deg) * Math.Cos({1} deg) -\n" +
                "        Math.Sin({0} deg) * Math.Sin({1} deg) == {2:E16}",
                degreesX, degreesY, Math.Cos(angleX) * Math.Cos(angleY) -
                Math.Sin(angleX) * Math.Sin(angleY)) + "\n";
            outputBlock += String.Format(
                "                           Math.Cos({0} deg) == {1:E16}",
                degreesX + degreesY, Math.Cos(angleX + angleY)) + "\n";
            
            return outputBlock;
        }

        public static double ConvertDegreesToRadians(double degrees)
        {
            return Math.PI * degrees / 180.0;
        }

        public static double Power(double value,double power)
        {
            return Math.Pow(value, power);
        }
    }
}
