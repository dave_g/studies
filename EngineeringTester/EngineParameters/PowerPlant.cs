﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EngineParameters
{
    public class PowerPlant
    {
        public enum Square
        {
            UnderSquare = -1,
            Square = 0,            
            OverSquare = 1
        }

        private double _diplacemnetTotal; //cubic centimeters - liters
        private double _bore; //mm - cm 
        private double _stroke; //mm - cm
        private double _rod; //mm - cm
        private double _crankOffset; //mm - cm        
        private int _pistons;
        private double _volumeDisplacement;
        private double _volumeClearance;

        public Square IsSquare()
        {
            Square square;

            if (_bore == _stroke)
            {
                return Square.Square;
            }

            square = _stroke > _bore ? Square.UnderSquare : Square.OverSquare;

            return square;
        }


        public double DiplacemnetTotal
        {
            get { return _diplacemnetTotal; }
            set { _diplacemnetTotal = value; }
        }

        public double Bore
        {
            get { return _bore; }
            set { _bore = value; }
        }

        public double Stroke
        {
            get { return _stroke; }
            set { _stroke = value; }
        }

        public double Rod
        {
            get { return _rod; }
            set { _rod = value; }
        }

        public double CrankOffset
        {
            get { return _crankOffset; }
            set { _crankOffset = value; }
        }

        public int Pistons
        {
            get { return _pistons; }
            set { _pistons = value; }
        }

        public double VolumeDisplacement
        {
            get { return _volumeDisplacement; }
            set { _volumeDisplacement = value; }
        }

        public double VolumeClearance
        {
            get { return _volumeClearance; }
            set { _volumeClearance = value; }
        }
    }
}
