﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EngineParameters
{
    public partial class Variables : UserControl
    {
        public Variables()
        {
            InitializeComponent();
        }

        public TextBox TextBoxEngineSpeed
        {
            get { return textBoxEngineSpeed; }
            set { textBoxEngineSpeed = value; }
        }

        public TextBox TextBoxAveragePistonSpeed
        {
            get { return textBoxAveragePistonSpeed; }
            set { textBoxAveragePistonSpeed = value; }
        }
    }
}
